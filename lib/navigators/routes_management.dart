import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

abstract class RouteManagement {
  static void goToLoginOrRegister() {
    Get.offAllNamed<void>(
      Routes.loginOrRegister,
    );
  }

  static void goToSignUp() {
    Get.toNamed<void>(
      Routes.signUp,
    );
  }

  static void goToSignUpWithEmail() {
    Get.toNamed<void>(
      Routes.signUpWithEmail,
    );
  }

  static void goToTrackExternalFunds() {
    Get.toNamed<void>(
      Routes.trackExternalFunds,
    );
  }

  static void goToSignUpWithEmailVerifyOTP() {
    Get.toNamed<void>(
      Routes.signUpWithEmailVerifyOTP,
    );
  }

  static void goToSignUpWithEmailUserName() {
    Get.toNamed<void>(
      Routes.signUpWithEmailUserName,
    );
  }

  static void goToSignUpWithEmailCreatePassword() {
    Get.toNamed<void>(
      Routes.signUpWithEmailCreatePassword,
    );
  }

  static void goToSignUpWithEmailPhoneNumber() {
    Get.toNamed<void>(
      Routes.signUpWithEmailPhoneNumber,
    );
  }

  static void goToSignUpWithEmailPhoneNumberVerifyOTP() {
    Get.toNamed<void>(
      Routes.signUpWithEmailPhoneNumberVerifyOTP,
    );
  }

  static void goToSignUpWithEmailPanCardVerification() {
    Get.toNamed<void>(
      Routes.signUpWithEmailPanCardVerification,
    );
  }

  static void goToSignUpWithEmailDateOfBirth() {
    Get.toNamed<void>(
      Routes.signUpWithEmailDateOfBirth,
    );
  }

  static void goToSignUpWithEmailSetAppPin() {
    Get.toNamed<void>(
      Routes.signUpWithEmailSetAppPin,
    );
  }

  static void goToSignUpWithEmailConfirmAppPin() {
    Get.toNamed<void>(
      Routes.signUpWithEmailConfirmAppPin,
    );
  }

  static void goToSignUpWithEmailSuccess() {
    Get.offAllNamed<void>(
      Routes.signUpWithEmailSuccess,
    );
  }

  static void goToKYCSuccess() {
    Get.offAllNamed<void>(
      Routes.kycSuccess,
    );
  }

  static void goToSignIn() {
    Get.toNamed<void>(
      Routes.signIn,
    );
  }

  static void goToSignInWithPhone() {
    Get.toNamed<void>(
      Routes.signInWithPhone,
    );
  }

  static void goToSignInWithPhoneVerifyOTP() {
    Get.toNamed<void>(
      Routes.signInWithPhoneVerifyOTP,
    );
  }

  static void goToSignInEnterAppPin() {
    Get.toNamed<void>(
      Routes.signInEnterAppPin,
    );
  }

  static void goToHome() {
    Get.offAllNamed<void>(
      Routes.home,
    );
  }

  static void goToSignInWithEmail() {
    Get.toNamed<void>(
      Routes.signInWithEmail,
    );
  }

  static void goToForgotPinOrPasswordPAN() {
    Get.toNamed<void>(
      Routes.forgotPinOrPasswordPAN,
    );
  }

  static void goToForgotPinOrPasswordVerifyOTP() {
    Get.toNamed<void>(
      Routes.forgotPinOrPasswordVerifyOTP,
    );
  }

  static void goToForgotPinOrPasswordPassword() {
    Get.toNamed<void>(
      Routes.forgotPinOrPasswordPassword,
    );
  }

  static void goToForgotPinOrPasswordResetPassword() {
    Get.toNamed<void>(
      Routes.forgotPinOrPasswordResetPassword,
    );
  }

  static void goToForgotPinOrPasswordResetAppPin() {
    Get.toNamed<void>(
      Routes.forgotPinOrPasswordResetAppPin,
    );
  }

  static void goToForgotPinOrPasswordConfirmAppPin() {
    Get.toNamed<void>(
      Routes.forgotPinOrPasswordConfirmAppPin,
    );
  }

  static void goToForgotPinOrPasswordDateOfBirth() {
    Get.toNamed<void>(
      Routes.forgotPinOrPasswordDateOfBirth,
    );
  }

  static void goToOffSignIn() {
    Get.offNamed<void>(
      Routes.signIn,
    );
  }

  static void goToOffSignUp() {
    Get.offNamed<void>(
      Routes.signUp,
    );
  }

  static void goToOffAllKyc() {
    Get.offAllNamed<void>(
      Routes.startKyc,
    );
  }

  static void goToKycVerified() {
    Get.toNamed<void>(
      Routes.kycVerified,
    );
  }

  static void goToKycIncome() {
    Get.toNamed<void>(
      Routes.kycIncome,
    );
  }

  static void goToKycNomineeDetails() {
    Get.toNamed<void>(
      Routes.kycNomineeDetails,
    );
  }

  static void goToKycConfirmBankDetails() {
    Get.toNamed<void>(
      Routes.kycConfirmBankDetails,
    );
  }

  static void goToKycChooseBankAccount() {
    Get.toNamed<void>(
      Routes.kycChooseBankAccount,
    );
  }

  static void goToFundOrderingInvestmentCart() {
    Get.toNamed<void>(
      Routes.fundOrderingInvestmentCart,
    );
  }

  static void goToFundOrderingPaymentOptions() {
    Get.toNamed<void>(
      Routes.fundOrderingPaymentOptions,
    );
  }

  static void goToFundOrderingInvestmentAmount() {
    Get.toNamed<void>(
      Routes.fundOrderingInvestmentAmount,
    );
  }

  static void goToFundOrderingSuccess() {
    Get.toNamed<void>(
      Routes.fundOrderingSuccess,
    );
  }

  static void goToInvestYourself() {
    Get.toNamed<void>(
      Routes.investYourself,
    );
  }

  static void goToAllMutualFunds() {
    Get.toNamed<void>(
      Routes.allMutualFunds,
    );
  }

  static void goToPersonalInformation() {
    Get.toNamed<void>(
      Routes.personalInformation,
    );
  }

  static void goToBankAndAutoPay() {
    Get.toNamed<void>(
      Routes.bankAndAutoPay,
    );
  }

  static void goToBankDetails() {
    Get.toNamed<void>(
      Routes.bankDetails,
    );
  }

  static void goToForgotPinOrPasswordEmailSent() {
    Get.toNamed<void>(
      Routes.forgotPinOrPasswordEmailSent,
    );
  }

  static void goToKycFatcaDeclaration() {
    Get.toNamed<void>(
      Routes.kycFatcaDeclaration,
    );
  }

  static void goToKycAddBankAccount() {
    Get.toNamed<void>(
      Routes.kycAddBankAccount,
    );
  }

  static void goToKycScanCheque() {
    Get.toNamed<void>(
      Routes.kycScanCheque,
    );
  }

  static void goToKycTakeNewPicture() {
    Get.toNamed<void>(
      Routes.kycTakeNewPicture,
    );
  }

  static void goToSectorAndThemes() {
    Get.toNamed<void>(
      Routes.sectorAndThemes,
    );
  }
}
