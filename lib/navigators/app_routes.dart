// coverage:ignore-file
part of 'app_pages.dart';

/// A chunks of routes and the path names which will be used to create
/// routes in [AppPages].
abstract class Routes {
  static const splash = _Paths.splash;
  static const loginOrRegister = _Paths.loginOrRegister;
  static const signUp = _Paths.signUp;
  static const signUpWithEmail = _Paths.signUpWithEmail;
  static const signUpWithEmailVerifyOTP = _Paths.signUpWithEmailVerifyOTP;
  static const forgotPinOrPasswordPAN = _Paths.forgotPinOrPasswordPAN;
  static const signUpWithEmailUserName = _Paths.signUpWithEmailUserName;
  static const signUpWithEmailPanCardVerification =
      _Paths.signUpWithEmailPanCardVerification;
  static const signUpWithEmailCreatePassword =
      _Paths.signUpWithEmailCreatePassword;
  static const signUpWithEmailPhoneNumber = _Paths.signUpWithEmailPhoneNumber;
  static const signUpWithEmailPhoneNumberVerifyOTP =
      _Paths.signUpWithEmailPhoneNumberVerifyOTP;
  static const signUpWithEmailDateOfBirth = _Paths.signUpWithEmailDateOfBirth;
  static const signUpWithEmailSetAppPin = _Paths.signUpWithEmailSetAppPin;
  static const kycIncome = _Paths.kycIncome;
  static const kycNomineeDetails = _Paths.kycNomineeDetails;
  static const signUpWithEmailSuccess = _Paths.signUpWithEmailSuccess;
  static const bankAndAutoPay = _Paths.bankAndAutoPay;
  static const forgotPinOrPasswordResetAppPin =
      _Paths.forgotPinOrPasswordResetAppPin;
  static const signIn = _Paths.signIn;
  static const forgotPinOrPasswordConfirmAppPin =
      _Paths.forgotPinOrPasswordConfirmAppPin;
  static const signInWithPhone = _Paths.signInWithPhone;
  static const signInWithPhoneVerifyOTP = _Paths.signInWithPhoneVerifyOTP;
  static const signInWithEmail = _Paths.signInWithEmail;
  static const forgotPinOrPasswordDateOfBirth =
      _Paths.forgotPinOrPasswordDateOfBirth;
  static const signInEnterAppPin = _Paths.signInEnterAppPin;
  static const kycSuccess = _Paths.kycSuccess;
  static const forgotPinOrPasswordResetPassword =
      _Paths.forgotPinOrPasswordResetPassword;
  static const forgotPinOrPasswordPassword = _Paths.forgotPinOrPasswordPassword;
  static const startKyc = _Paths.startKyc;
  static const fundOrderingInvestmentAmount =
      _Paths.fundOrderingInvestmentAmount;
  static const fundOrderingInvestmentCart = _Paths.fundOrderingInvestmentCart;
  static const kycConfirmBankDetails = _Paths.kycConfirmBankDetails;
  static const kycChooseBankAccount = _Paths.kycChooseBankAccount;
  static const kycVerified = _Paths.kycVerified;
  static const fundOrderingPaymentOptions = _Paths.fundOrderingPaymentOptions;
  static const fundOrderingSuccess = _Paths.fundOrderingSuccess;
  static const home = _Paths.home;
  static const allMutualFunds = _Paths.allMutualFunds;
  static const investYourself = _Paths.investYourself;
  static const forgotPinOrPasswordVerifyOTP =
      _Paths.forgotPinOrPasswordVerifyOTP;
  static const bankDetails = _Paths.bankDetails;
  static const personalInformation = _Paths.personalInformation;
  static const trackExternalFunds = _Paths.trackExternalFunds;
  static const signUpWithEmailConfirmAppPin =
      _Paths.signUpWithEmailConfirmAppPin;
  static const forgotPinOrPasswordEmailSent =
      _Paths.forgotPinOrPasswordEmailSent;
  static const kycFatcaDeclaration = _Paths.kycFatcaDeclaration;
  static const kycAddBankAccount = _Paths.kycAddBankAccount;
  static const kycScanCheque = _Paths.kycScanCheque;
  static const kycTakeNewPicture = _Paths.kycTakeNewPicture;
  static const sectorAndThemes = _Paths.sectorAndThemes;
}

abstract class _Paths {
  static const trackExternalFunds = '/trackExternalFunds';
  static const splash = '/splash';
  static const loginOrRegister = '/loginOrRegister';
  static const signUp = '/signUp';
  static const signUpWithEmail = '/signUpWithEmail';
  static const fundOrderingPaymentOptions = '/fundOrderingPaymentOptions';
  static const fundOrderingInvestmentAmount = '/fundOrderingInvestmentAmount';
  static const fundOrderingSuccess = '/fundOrderingSuccess';
  static const home = '/home';
  static const investYourself = '/investYourself';
  static const allMutualFunds = '/allMutualFunds';
  static const signUpWithEmailVerifyOTP = '/signUpWithEmailVerifyOTP';
  static const signUpWithEmailUserName = '/signUpWithEmailUserName';
  static const signUpWithEmailCreatePassword = '/signUpWithEmailCreatePassword';
  static const signUpWithEmailPhoneNumber = '/signUpWithEmailPhoneNumber';
  static const signUpWithEmailDateOfBirth = '/signUpWithEmailDateOfBirth';
  static const signUpWithEmailSetAppPin = '/signUpWithEmailSetAppPin';
  static const signUpWithEmailConfirmAppPin = '/signUpWithEmailConfirmAppPin';
  static const kycChooseBankAccount = '/kycChooseBankAccount';
  static const kycSuccess = '/kycSuccess';
  static const fundOrderingInvestmentCart = '/fundOrderingInvestmentCart';
  static const signUpWithEmailSuccess = '/signUpWithEmailSuccess';
  static const forgotPinOrPasswordConfirmAppPin =
      '/forgotPinOrPasswordConfirmAppPin';
  static const signIn = '/signIn';
  static const forgotPinOrPasswordVerifyOTP = '/forgotPinOrPasswordVerifyOTP';
  static const personalInformation = '/personalInformation';
  static const bankAndAutoPay = '/bankAndAutoPay';
  static const bankDetails = '/bankDetails';
  static const forgotPinOrPasswordDateOfBirth =
      '/forgotPinOrPasswordDateOfBirth';
  static const signInWithPhone = '/signInWithPhone';
  static const forgotPinOrPasswordPassword = '/forgotPinOrPasswordPassword';
  static const forgotPinOrPasswordResetPassword =
      '/forgotPinOrPasswordResetPassword';
  static const signInEnterAppPin = '/signInEnterAppPin';
  static const startKyc = '/startKyc';
  static const kycVerified = '/kycVerified';
  static const kycIncome = '/kycIncome';
  static const kycNomineeDetails = '/kycNomineeDetails';
  static const kycConfirmBankDetails = '/kycConfirmBankDetails';
  static const signInWithEmail = '/signInWithEmail';
  static const forgotPinOrPasswordPAN = '/forgotPinOrPasswordPAN';
  static const forgotPinOrPasswordResetAppPin =
      '/forgotPinOrPasswordResetAppPin';
  static const signInWithPhoneVerifyOTP = '/signInWithPhoneVerifyOTP';
  static const signUpWithEmailPanCardVerification =
      '/signUpWithEmailPanCardVerification';
  static const signUpWithEmailPhoneNumberVerifyOTP =
      '/signUpWithEmailPhoneNumberVerifyOTP';
  static const forgotPinOrPasswordEmailSent = '/forgotPinOrPasswordEmailSent';
  static const kycFatcaDeclaration = '/kycFatcaDeclaration';
  static const kycAddBankAccount = '/kycAddBankAccount';
  static const kycScanCheque = '/kycScanCheque';
  static const kycTakeNewPicture = '/kycTakeNewPicture';
  static const sectorAndThemes = '/sectorAndThemes';
}
