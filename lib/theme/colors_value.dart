// coverage:ignore-file
import 'package:flutter/material.dart';
import 'package:get/get.dart';

/// A list of custom color used in the application.
///
/// Will be ignored for test since all are static values and would not change.
abstract class ColorsValue {
  static const Map<int, Color> primaryColorSwatch = {
    50: Color.fromRGBO(67, 19, 161, 0.1),
    100: Color.fromRGBO(67, 19, 161, 0.2),
    200: Color.fromRGBO(67, 19, 161, 0.3),
    300: Color.fromRGBO(67, 19, 161, 0.4),
    400: Color.fromRGBO(67, 19, 161, 0.5),
    500: Color.fromRGBO(67, 19, 161, 0.6),
    600: Color.fromRGBO(67, 19, 161, 0.7),
    700: Color.fromRGBO(67, 19, 161, 0.8),
    800: Color.fromRGBO(67, 19, 161, 0.9),
    900: Color.fromRGBO(67, 19, 161, 1.0),
  };

  static const Color primaryColorRgb = Color.fromRGBO(67, 19, 161, 1);

  static const Color primaryColorLight1Rgbo = Color.fromRGBO(1, 75, 147, .1);

  static const Color primaryColor = Color(
    primaryColorHex,
  );

  static const Color secondaryColor = Color(
    secondaryColorHex,
  );

  static const Color blueColor = Color(
    blueColorHex,
  );

  static const Color darkBlueColor = Color(
    darkBlueColorHex,
  );

  static const Color formFieldBorderColor = Color(0xff5F527A);

  static const Color greyColor = Color(
    greyColorHex,
  );

  static const Color formFieldColor = Color(
    formFieldHex,
  );

  static const Color lightGreyColor = Color(
    lightGreyColorHex,
  );

  static const Color regalBlueColor = Color(
    regalBlueColorHex,
  );

  static const Color blackColor = Color(
    blackColorHex,
  );

  static const Color lightPink = Color(
    lightPinkHex,
  );

  static const Color lightYellow = Color(
    lightYellowHex,
  );

  static const Color pinkColor = Color(
    pinkColorHex,
  );

  static const Color cardHeadingColor = Color(
    cardHeadingColorHex,
  );

  static const Color circleArrowColor = Color(
    circleArrowColorHex,
  );

  static const Color inactiveTrackColor = Color(
    inactiveTrackColorHex,
  );

  static const Color activeTrackColor = Color(
    activeTrackColorHex,
  );

  static const Color sliderThumbStrokeColor = Color(
    sliderThumbStrokeColorHex,
  );

  static const Color lightBgContainerColor = Color(
    lightBgContainerColorHex,
  );

  static const Color iconBlueColor = Color(
    iconBlueColorHex,
  );

  static const Color shineGreenColor = Color(
    shineGreenColorHex,
  );

  static const Color profileAvatarColor = Color(
    profileAvatarColorHex,
  );

  static const Color redColor = Color(redColorHex);

  static const Color textColor = Color(textColorHex);

  static const Color borderColor = Colors.transparent;

  static Color backgroundColor = const Color(0xffFCFAFE);

  static Color darkGreyText = const Color(0xff565656);

  static const int primaryColorHex = 0xff4313A1;

  static const int formFieldHex = 0xffF9FAFC;

  static const int secondaryColorHex = 0xffDAC5FF;

  static const int blueColorHex = 0xff0154A4;

  static const int darkBlueColorHex = 0xff012A52;

  static const int greyColorHex = 0xff707070;

  static const int lightGreyColorHex = 0xff00000029;

  static const int regalBlueColorHex = 0xff023A6F;

  static const int redColorHex = 0xffFF0000;

  static const int textColorHex = 0xff5F527A;

  static const int blackColorHex = 0xff18141F;

  static const int lightPinkHex = 0xffF7F2FC;

  static const int lightYellowHex = 0xffFFF5F5;

  static const int pinkColorHex = 0xffEFE8FC;

  static const int cardHeadingColorHex = 0xff483D5C;

  static const int circleArrowColorHex = 0xffF9F6FE;

  static const int inactiveTrackColorHex = 0xffDFD1FA;

  static const int activeTrackColorHex = 0xff8F5FEC;

  static const int sliderThumbStrokeColorHex = 0xff5518CD;

  static const int lightBgContainerColorHex = 0xffF6F2FD;

  static const int iconBlueColorHex = 0xff4313A1;

  static const int shineGreenColorHex = 0xff1ECD93;

  static const int profileAvatarColorHex = 0xffEBE3FD;

  // static const int backgroundColor = 0xffFF0000;

  static const Color transparent = Color.fromARGB(0, 255, 255, 255);

  static Color themeOppositeColor() =>
      Get.isDarkMode ? Colors.white : backgroundColor;
}
