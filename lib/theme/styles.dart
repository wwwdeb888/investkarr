import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:investkarr/lib.dart';

/// A chunk of styles used in the application.
/// Will be ignored for test since all are static values and would not change.
// coverage:ignore-file
abstract class Styles {
  static RoundedRectangleBorder buttonShapeBorder = RoundedRectangleBorder(
    side: BorderSide(
      color: Colors.white,
      width: Dimens.one,
      style: BorderStyle.solid,
    ),
    borderRadius: BorderRadius.circular(
      Dimens.fifty,
    ),
  );
  static var elevatedButtonTheme = ElevatedButtonThemeData(
    style: ButtonStyle(
      textStyle: MaterialStateProperty.all(
        boldWhite16,
      ),
      padding: MaterialStateProperty.all(
        Dimens.edgeInsets15,
      ),
      shape: MaterialStateProperty.all(
        RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(
            Dimens.fifty,
          ),
        ),
      ),
      backgroundColor: MaterialStateProperty.resolveWith<Color>(
        (Set<MaterialState> states) => states.contains(MaterialState.disabled)
            ? ColorsValue.lightGreyColor
            : ColorsValue.primaryColor,
      ),
    ),
  );
  static RoundedRectangleBorder border15 = RoundedRectangleBorder(
    borderRadius: BorderRadius.circular(
      Dimens.fifteen,
    ),
  );

  static const LinearGradient linearGradient = LinearGradient(
    colors: <Color>[ColorsValue.primaryColor, ColorsValue.secondaryColor],
  );

  static ButtonThemeData buttonThemeData = ButtonThemeData(
      buttonColor: ColorsValue.primaryColor,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(
          Dimens.fifty,
        ),
      ));

  static BoxDecoration pinPutDecoration = BoxDecoration(
    border: Border(
      bottom: BorderSide(color: Colors.grey, width: 2),
    ),
    // borderRadius: BorderRadius.circular(Dimens.three),
  );

  static BoxDecoration enabledPinPutDecoration = BoxDecoration(
    // border: Border.all(color: ColorsValue.primaryColor.withOpacity(.5)),
    border: Border(
      bottom: BorderSide(color: ColorsValue.primaryColor, width: 2),
    ),
    // borderRadius: BorderRadius.circular(Dimens.three),
  );

  static List<BoxShadow>? cardShadow = [
    BoxShadow(
        blurRadius: Dimens.two,
        offset: const Offset(0, 2),
        color: Colors.grey.withOpacity(0.1),
        spreadRadius: Dimens.one),
    BoxShadow(
        blurRadius: Dimens.two,
        offset: const Offset(-2, 0),
        color: Colors.white.withOpacity(0.5),
        spreadRadius: Dimens.one),
    BoxShadow(
        blurRadius: Dimens.two,
        offset: const Offset(2, 0),
        color: Colors.white.withOpacity(0.5),
        spreadRadius: Dimens.one)
  ];

  // Different style used in the application

  static TextStyle boldWhite16 = TextStyle(
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
    color: Colors.white,
    fontSize: Dimens.sixTeen,
  );

  static TextStyle subtitleLight7 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.thirteen,
  );

  static TextStyle bookGrey16 = TextStyle(
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
    color: ColorsValue.greyColor,
    fontSize: Dimens.sixTeen,
  );
  static TextStyle textFieldHintColor16 = TextStyle(
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
    color: ColorsValue.formFieldBorderColor,
    fontSize: Dimens.sixTeen,
  );
  static TextStyle textFieldHintColor14 = TextStyle(
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
    color: ColorsValue.formFieldBorderColor,
    fontSize: Dimens.fourteen,
  );

  static TextStyle white14 = TextStyle(
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
    color: Colors.white,
    fontSize: Dimens.fourteen,
  );

  static TextStyle bold8White = TextStyle(
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
    color: Colors.white,
    fontSize: Dimens.eight,
  );

  static TextStyle red13 = TextStyle(
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
    color: Colors.red,
    fontSize: Dimens.thirteen,
  );

  static TextStyle errorTextStyle = TextStyle(
      fontFamily: GoogleFonts.dmSans().fontFamily,
      fontWeight: FontWeight.normal,
      color: Colors.red,
      fontSize: Dimens.sixTeen);

  static TextStyle bookWhite12 = TextStyle(
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
    color: Colors.white,
    fontSize: Dimens.twelve,
  );
  static TextStyle bookBlack12 = TextStyle(
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
    color: Colors.black,
    fontSize: Dimens.twelve,
  );

  static TextStyle bookWhite20 = TextStyle(
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
    color: Colors.white,
    fontSize: Dimens.twenty,
  );
  static TextStyle bookBlack20 = TextStyle(
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
    color: Colors.black,
    fontSize: Dimens.twenty,
  );

  static TextStyle mediumBlue13 = TextStyle(
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
    color: ColorsValue.blueColor,
    fontSize: Dimens.thirteen,
  );

  static TextStyle mediumWhite13 = TextStyle(
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
    color: Colors.white,
    fontSize: Dimens.thirteen,
  );

  static TextStyle black18 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.eighteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );
  static TextStyle black20 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.twenty,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );
  static TextStyle boldBlack22 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.twenty + Dimens.two,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );
  static TextStyle bold24 = TextStyle(
    color: const Color(0xff1F1926),
    fontSize: Dimens.twenty + Dimens.four,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );
  static TextStyle light14 = TextStyle(
    color: const Color(0xff5F527A),
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
  );
  static TextStyle bold16Primary = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.sixTeen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );
  static TextStyle bold14Primary = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );

  static TextStyle bold14Black = TextStyle(
    color: ColorsValue.blackColor,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );

  static TextStyle bold18Black = TextStyle(
    color: ColorsValue.blackColor,
    fontSize: Dimens.eighteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );

  static TextStyle medium14Black = TextStyle(
    color: ColorsValue.blackColor,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );

  static TextStyle medium14Primary = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );

  static TextStyle medium14Grey = TextStyle(
    color: ColorsValue.textColor,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );

  static TextStyle medium18Black = TextStyle(
    color: ColorsValue.blackColor,
    fontSize: Dimens.eighteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );

  static TextStyle medium18CardHeading = TextStyle(
    color: ColorsValue.cardHeadingColor,
    fontSize: Dimens.eighteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );

  static TextStyle bold16Black = TextStyle(
    color: Colors.black,
    fontSize: Dimens.sixTeen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );
  static TextStyle bold24Black = TextStyle(
    color: Colors.black,
    fontSize: Dimens.twentyFour,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );
  static TextStyle medium24Black = TextStyle(
    color: Colors.black,
    fontSize: Dimens.twentyFour,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );
  static TextStyle bold26Black = TextStyle(
    color: Colors.black,
    fontSize: Dimens.twentySix,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );
  static TextStyle medium26Black = TextStyle(
    color: Colors.black,
    fontSize: Dimens.twentySix,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );
  static TextStyle bold16White = TextStyle(
    color: Colors.white,
    fontSize: Dimens.sixTeen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );
  static TextStyle bold14White = TextStyle(
    color: Colors.white,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );
  static TextStyle bold12Black = TextStyle(
    color: Colors.black,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );
    static TextStyle bold10Black = TextStyle(
    color: Colors.black,
    fontSize: Dimens.ten,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );
  static TextStyle bold20Black = TextStyle(
    color: Colors.black,
    fontSize: Dimens.twenty,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );
  static TextStyle bold12Primary = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );
  static TextStyle medium12Primary = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );
  static TextStyle medium10Primary = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.ten,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );
  static TextStyle medium10Grey = TextStyle(
    color: ColorsValue.textColor,
    fontSize: Dimens.ten,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );
  static TextStyle medium11White = TextStyle(
    color: Colors.white,
    fontSize: Dimens.ten+Dimens.one,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );
  static TextStyle medium12Grey = TextStyle(
    color: ColorsValue.textColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );
   static TextStyle medium12Black = TextStyle(
    color: ColorsValue.blackColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );
  static TextStyle bold12PrimaryUnderline = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
    decoration: TextDecoration.underline,
  );
  static TextStyle medium12PrimaryUnderline = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
    decoration: TextDecoration.underline,
  );
  static TextStyle bold13Primary = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.thirteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );
  static TextStyle boldWhite22 = TextStyle(
    color: Colors.white,
    fontSize: Dimens.twenty + Dimens.two,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );

  static TextStyle mediumBoldWhite25 = TextStyle(
    color: Colors.white,
    fontSize: Dimens.twentyFive,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );

  static TextStyle bookGrey20 = TextStyle(
    color: ColorsValue.greyColor,
    fontSize: Dimens.twenty,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );

  static TextStyle mediumBoldWhite20 = TextStyle(
    color: Colors.white,
    fontSize: Dimens.twenty,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );

  static TextStyle bodyTextDark3 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.twenty,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );

  static TextStyle boldBlack20 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.twenty,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );
  static TextStyle boldBlack24 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.twentyFour,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );
  static TextStyle boldBlack18 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.eighteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );
  static TextStyle primaryBold18 = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.eighteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );
  static TextStyle primaryBold22 = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.twentyTwo,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );
  static TextStyle primaryBold24 = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.twentyFour,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );
  static TextStyle primaryBold13 = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.thirteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );
  static TextStyle secondary14 = TextStyle(
    color: ColorsValue.secondaryColor,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );

  static TextStyle boldBlack14 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );

  static TextStyle boldBlack15 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.fifteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );

  static TextStyle boldBlack16 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.sixTeen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );

  static TextStyle boldGreen16 = TextStyle(
    color: Colors.green,
    fontSize: Dimens.fifteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );
  static TextStyle boldGreen18 = TextStyle(
    color: Colors.green,
    fontSize: Dimens.eighteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );

  static TextStyle forgotPasswordTextStyle = TextStyle(
    color: ColorsValue.formFieldBorderColor,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );

  static TextStyle bookBoldBlack18 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.eighteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );
  static TextStyle bookBoldBlack20 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.twenty,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );

  static TextStyle bookGrey18 = TextStyle(
    color: ColorsValue.greyColor,
    fontSize: Dimens.eighteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );

  static TextStyle boldBlues15 = TextStyle(
    color: ColorsValue.blueColor,
    fontSize: Dimens.fifteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );
  static TextStyle boldBlue20 = TextStyle(
    color: ColorsValue.blueColor,
    fontSize: Dimens.twenty,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );

  static TextStyle black15 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.fifteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );
  static TextStyle regular14Grey = TextStyle(
    color: ColorsValue.textColor,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
  );
  static TextStyle regular14Primary = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
  );
    static TextStyle regular14Black = TextStyle(
    color: ColorsValue.blackColor,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
  );
  static TextStyle regular16Grey = TextStyle(
    color: ColorsValue.textColor,
    fontSize: Dimens.sixTeen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
  );
  static TextStyle regular16Black = TextStyle(
    color: ColorsValue.blackColor,
    fontSize: Dimens.sixTeen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
  );
  static TextStyle medium14DarkGrey = TextStyle(
    color: ColorsValue.darkGreyText,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );

  static TextStyle black14Underline = TextStyle(
      color: Colors.black,
      fontSize: Dimens.fourteen,
      fontFamily: GoogleFonts.dmSans().fontFamily,
      fontWeight: FontWeight.normal,
      decoration: TextDecoration.underline);

  static TextStyle black16 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.sixTeen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );

  static TextStyle medium16Black = TextStyle(
    color: Colors.black,
    fontSize: Dimens.sixTeen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );

  static TextStyle medium16Primary = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.sixTeen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w500,
  );

  static TextStyle secondary15 = TextStyle(
    color: ColorsValue.textColor,
    fontSize: Dimens.fifteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );

  static TextStyle secondary12 = TextStyle(
    color: ColorsValue.textColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );

  static TextStyle regular12Grey = TextStyle(
    color: ColorsValue.textColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
  );

  static TextStyle regular12Black = TextStyle(
    color: ColorsValue.blackColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
  );
  static TextStyle regular13Black = TextStyle(
    color: ColorsValue.blackColor,
    fontSize: Dimens.thirteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
  );

  static TextStyle regular12Primary = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
  );

  static TextStyle regular12PrimaryUnderline = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
    decoration: TextDecoration.underline,
  );

  static TextStyle regular12GreyUnderline = TextStyle(
    color: ColorsValue.textColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
    decoration: TextDecoration.underline,
  );

  static TextStyle grey12Subs = TextStyle(
    color: ColorsValue.formFieldBorderColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );

  static TextStyle grey15 = TextStyle(
    color: Colors.grey,
    fontSize: Dimens.fifteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );

  static TextStyle infoTextPI14 = TextStyle(
    color: ColorsValue.textColor,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );

  static TextStyle darkGrey14 = TextStyle(
    color: ColorsValue.darkGreyText,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );

  static TextStyle darkGrey12Bold = TextStyle(
    color: ColorsValue.darkGreyText,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );

  static TextStyle darkGrey12Light = TextStyle(
    color: ColorsValue.darkGreyText,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );
  static TextStyle grey12 = TextStyle(
    color: Colors.grey,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );
  static TextStyle grey13 = TextStyle(
    color: Colors.grey,
    fontSize: Dimens.thirteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );
  static TextStyle green12 = TextStyle(
    color: Colors.green,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );
  static TextStyle black12 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );

  static TextStyle grey14 = TextStyle(
    color: ColorsValue.textColor,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );

  static TextStyle green16 = TextStyle(
    color: Colors.green,
    fontSize: Dimens.sixTeen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );
  static TextStyle green16Bold = TextStyle(
    color: Colors.green,
    fontSize: Dimens.sixTeen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );
  static TextStyle black13 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.thirteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );
  static TextStyle bottomNav13 = TextStyle(
    color: const Color(0xffA99DBE),
    fontSize: Dimens.thirteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );
  static TextStyle primary15Underline = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.fifteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    decoration: TextDecoration.underline,
    fontWeight: FontWeight.bold,
  );
  static TextStyle primary16 = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.sixTeen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
  );
  static TextStyle bold12GreyUnderline = TextStyle(
    color: ColorsValue.textColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    decoration: TextDecoration.underline,
    fontWeight: FontWeight.w700,
  );
  static TextStyle bold12Grey = TextStyle(
    color: ColorsValue.textColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    
    fontWeight: FontWeight.w700,
  );
  static TextStyle primary16Bold = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.sixTeen,
    fontWeight: FontWeight.bold,
    fontFamily: GoogleFonts.dmSans().fontFamily,
  );
  static TextStyle primary15 = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.fifteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
  );
  static TextStyle primary12Underline = TextStyle(
    color: ColorsValue.primaryColor,
    fontSize: Dimens.twelve,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    decoration: TextDecoration.underline,
    fontWeight: FontWeight.bold,
  );
  static TextStyle bookBlack15 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.fifteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );
  static TextStyle bold14Grey = TextStyle(
    color: ColorsValue.textColor,
    fontSize: Dimens.fourteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w700,
  );
  static TextStyle mediumBlack15 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.fifteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
  );
  static TextStyle bookWhite15 = TextStyle(
    color: Colors.white,
    fontSize: Dimens.fifteen,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
  );

  static TextStyle boldBlack35 = TextStyle(
    color: Colors.black,
    fontSize: Dimens.thirtyFive,
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.bold,
  );

  static TextStyle white15 = TextStyle(
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.w400,
    fontSize: Dimens.fifteen,
    color: Colors.white,
  );

  static TextStyle white17 = TextStyle(
    fontFamily: GoogleFonts.dmSans().fontFamily,
    fontWeight: FontWeight.normal,
    fontSize: Dimens.seventeen,
    color: Colors.white,
  );

  static var outlineBorderRadius50 = OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(
        Dimens.fifty,
      ),
    ),
    borderSide: BorderSide.none,
  );

  static var outlineBorderRadius5 = OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(
        Dimens.five,
      ),
    ),
    borderSide: BorderSide.none,
  );

  static var outlineBorderRadius15 = OutlineInputBorder(
    borderRadius: BorderRadius.all(
      Radius.circular(
        Dimens.fifteen,
      ),
    ),
    borderSide: BorderSide.none,
  );

  static BoxDecoration whiteThirtyDecoration = BoxDecoration(
    color: Colors.white,
    borderRadius: BorderRadius.circular(Dimens.thirty),
  );

// static var elevatedButtonTheme = ElevatedButtonThemeData(
//   style: ButtonStyle(
//     textStyle: MaterialStateProperty.all(
//       boldWhite16,
//     ),
//     padding: MaterialStateProperty.all(
//       Dimens.edgeInsets15,
//     ),
//     shape: MaterialStateProperty.all(
//       RoundedRectangleBorder(
//         borderRadius: BorderRadius.circular(
//           Dimens.five,
//         ),
//       ),
//     ),
//     backgroundColor: MaterialStateProperty.resolveWith<Color>(
//       (Set<MaterialState> states) => states.contains(MaterialState.disabled)
//           ? ColorsValue.lightGreyColor
//           : ColorsValue.primaryColor,
//     ),
//   ),
// );
}
