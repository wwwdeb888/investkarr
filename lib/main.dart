import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:investkarr/lib.dart';

import 'DevHttpOverrides.dart';

void main() async {
  try {
    WidgetsFlutterBinding.ensureInitialized();
    await GetStorage.init('appData');
    HttpOverrides.global = DevHttpOverrides();

    runApp(MyApp());
  } catch (error) {
    Utility.printELog(error.toString());
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return ScreenUtilInit(
      designSize: const Size(375, 812),
      builder: () => GetMaterialApp(
        locale: const Locale('en'),
        title: StringConstants.appName,
        debugShowCheckedModeBanner: false,
        themeMode: ThemeMode.system,
        translations: TranslationsFile(),
        supportedLocales: TranslationsFile.listOfLocales,
        getPages: AppPages.pages,
        initialRoute: AppPages.initial,
        enableLog: true,
      ),
    );
  }
}
