import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:investkarr/lib.dart';

class ForgotPinOrPasswordController extends GetxController {
  String? emailErrorText;

  var apiClient = ApiClient();

  final pinPutController3 = TextEditingController();
  final pinPutController4 = TextEditingController();

  var showResetPassword = false;

  bool isPasswordValid = false;

  var oldPassword = '';

  var dob = TextEditingController();

  bool isOTPValid = false;

  bool isPanValid = false;

  bool isPasswordVisible = false;
  bool isOldPasswordVisible = false;

  bool isAppPinValid = false;

  bool isEmailValid = false;

  bool isConfirmAppPinValid = false;

  bool onPasswordListening = false;

  bool isDOBValid = false;

  final pinPutController2 = TextEditingController();

  String panNumber = '';

  List<bool> passwordErrors = Utility.passwordValidator('');

  String emailId = '';

  String password = '';

  bool isPanShowing = false;

  @override
  void onInit() async {
    showResetPassword =
        await GetStorage('appData').read<dynamic>('showResetPassword') as bool;
    print(showResetPassword);
    super.onInit();
  }

  void showPanAndDob() {
    isPanShowing = !isPanShowing;
    update();
  }

  void onOTPChanged2(String value) {
    if (pinPutController2.text.length == 6) {
      isOTPValid = true;
    } else {
      isOTPValid = false;
    }
    update();
  }

  void checkIfEmailIsValid(String email) {
    isEmailValid = Utility.emailValidator(email);
    emailId = email;
    emailErrorText =
        isEmailValid ? null : StringConstants.pleaseEnterValidEmail;
    update();
  }

  void onOTPChanged3(String value) {
    if (pinPutController3.text.length == 4) {
      isAppPinValid = true;
    } else {
      isAppPinValid = false;
    }
    update();
  }

  void onOTPChanged4(String value) {
    if (pinPutController4.text.length == 4) {
      isConfirmAppPinValid = true;
    } else {
      isConfirmAppPinValid = false;
    }
    update();
  }

  void checkIfDateIsValid(String name) {
    if (dob.text.isNotEmpty) {
      isDOBValid = true;
    } else {
      isDOBValid = false;
    }
    update();
  }

  /// Check if the password is valid or not.
  void checkIfPasswordIsValid(String password) {
    this.password = password;
    passwordErrors = Utility.passwordValidator(password);
    isPasswordValid = !passwordErrors.contains(false);
    update();
  }

  void updatePasswordVisibility() {
    isPasswordVisible = !isPasswordVisible;
    update();
  }

  void updateOldPasswordVisibility() {
    isOldPasswordVisible = !isOldPasswordVisible;
    update();
  }

  Future<void> forgotPasswordOfMail() async {
    var res = await apiClient.forgotPassword(
        email: emailId,
        type: 'email',
        dob: dob.text,
        isLoading: true,
        pan: panNumber);
    if (!res.hasError) {
      print(res.data);
      var response = sendOtpResponseFromJson(res.data);
      if (response.statusCode == 200 && response.success == true) {
        Get.snackbar('Success', response.result!.otp.toString(),
            duration: const Duration(seconds: 5));
        //TODO
        // RouteManagement.goToForgotPinOrPasswordEmailSent();
        // RouteManagement.goToForgotPinOrPasswordVerifyOTP();

      }
    } else {
      print(res.data);
    }
  }

  void checkIfPanIsValid(String name) {
    panNumber = name;
    if (panNumber.isNotEmpty && panNumber.length == 10) {
      isPanValid = true;
    } else {
      isPanValid = false;
    }
    update();
  }

  Future<void> verifyEmailOTP() async {
    var res = await apiClient.verifyOTP(
        keyType1: 'email',
        value1: emailId,
        typeValue: 'email',
        isLogin: true,
        otp: pinPutController2.text,
        isLoading: true);
    if (!res.hasError) {
      print(res.data);
      var response = signUpResponseFromJson(res.data);
      if (response.statusCode == 200 && response.success == true) {
        await GetStorage('appData')
            .write('token', response.result!.accessToken);
        RouteManagement.goToForgotPinOrPasswordResetPassword();
      }
    } else {
      print(res.data);
    }
  }

  Future<void> updatePassword() async {
    var token = await GetStorage('appData').read('token');
    var res = await apiClient.updatePassword(
        newPassword: password, token: token.toString(), isLoading: true);
    if (!res.hasError) {
      print(res.data);
      var response = signUpResponseFromJson(res.data);
      if (response.statusCode == 200 && response.success == true) {
        await GetStorage('appData')
            .write('token', response.result!.accessToken);
        RouteManagement.goToLoginOrRegister();
      }
    } else {
      Get.snackbar('Error', 'Old password is wrong');
      print(res.data);
    }
  }

  Future<void> verifyAppPin() async {
    var res = await apiClient.verifyOTP(
        keyType1: 'email',
        value1: emailId,
        typeValue: 'email',
        isLogin: true,
        otp: pinPutController2.text,
        isLoading: true);
    if (!res.hasError) {
      print(res.data);
      var response = signUpResponseFromJson(res.data);
      if (response.statusCode == 200 && response.success == true) {
        await GetStorage('appData')
            .write('token', response.result!.accessToken);
        RouteManagement.goToSignInEnterAppPin();
      }
    } else {
      print(res.data);
    }
  }

  Future<void> verifyPin() async {
    var token = await GetStorage('appData').read('token');
    var res = await apiClient.updatePin(
        pin: pinPutController3.text, isLoading: true, token: token.toString());
    if (!res.hasError) {
      print(res.data);
      var response = signUpResponseFromJson(res.data);
      if (response.statusCode == 200 && response.success == true) {
        await GetStorage('appData')
            .write('token', response.result!.accessToken);
        await GetStorage('appData')
            .write('email', response.result!.user!.email);
        await GetStorage('appData').write('name',
            '${response.result!.user!.firstName} ${response.result!.user!.lastName}');
        RouteManagement.goToLoginOrRegister();
      }
    } else {
      Get.snackbar('Error', 'Invalid Pin');
    }
  }
}
