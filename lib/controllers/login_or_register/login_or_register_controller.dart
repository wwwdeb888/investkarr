import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class LoginOrRegisterController extends GetxController {
  var controller = PageController();

  static const kDuration = Duration(milliseconds: 300);

  static const kCurve = Curves.ease;

  final List<Widget> pages = <Widget>[
    ConstrainedBox(
      constraints: const BoxConstraints.expand(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            AssetConstants.loginOrRegister1,
            width: Dimens.percentWidth(.7),
          ),
          Dimens.boxHeight70,
          Text(
            StringConstants.goalBasedInvesting,
            style: Styles.bold24,
            textAlign: TextAlign.center,
          ),
          Dimens.boxHeight10,
          Text(
            StringConstants.investIntoTimeTestedPortfolios,
            style: Styles.light14,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    ),
    ConstrainedBox(
      constraints: const BoxConstraints.expand(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            AssetConstants.loginOrRegister2,
            width: Dimens.percentWidth(.7),
          ),
          Dimens.boxHeight70,
          Text(
            StringConstants.zeroCommission,
            style: Styles.bold24,
            textAlign: TextAlign.center,
          ),
          Dimens.boxHeight10,
          Text(
            StringConstants.investIntoDirectPlans,
            style: Styles.light14,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    ),
    ConstrainedBox(
      constraints: const BoxConstraints.expand(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            AssetConstants.loginOrRegister3,
            width: Dimens.percentWidth(.7),
          ),
          Dimens.boxHeight70,
          Text(
            StringConstants.unbiasedAdvice,
            style: Styles.bold24,
            textAlign: TextAlign.center,
          ),
          Dimens.boxHeight10,
          Text(
            StringConstants.independentCompany,
            style: Styles.light14,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    ),
    ConstrainedBox(
      constraints: const BoxConstraints.expand(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.asset(
            AssetConstants.loginOrRegister4,
            width: Dimens.percentWidth(.7),
          ),
          Dimens.boxHeight70,
          Text(
            StringConstants.dataSecurity,
            style: Styles.bold24,
            textAlign: TextAlign.center,
          ),
          Dimens.boxHeight10,
          Text(
            StringConstants.weDoNotSell,
            style: Styles.light14,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    ),
  ];
}
