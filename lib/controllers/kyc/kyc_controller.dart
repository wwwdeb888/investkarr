import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class KycController extends GetxController {
  List<String> listOfIncome = [
    'Upto 1 Lakh',
    '1 Lakh-5 Lakh',
    '5 Lakh-10 Lakh',
    '10 Lakh-25 Lakh',
    '10 Lakh-25 Lakh',
    '50 Lakh-1 Crore',
    '1 crore-5 crore',
    'More than 5 crore',
  ];

  List<String> listOfOccupation = [
    'Private sector',
    'Public sector',
    'Government',
    'Housewife',
    'Student',
    'Self Employed',
    'Business',
    'Professional',
    'Retired',
    'Farmar',
    'Service',
    'Agriculturist',
  ];

  int selectedOccupationIndex = -1;

  String? emailErrorText;

  String firstName = '';

  String bankNumber = '';

  bool isEmailValid = false;

  bool checkBoxValue = false;

  bool showAccountNumber = false;

  String emailId = '';

  int selectedIndex = -1;

  int selectedPopularBank = -1;

  int pageNumber = 1;

  bool isAccountNumberShown = false;

  void makeAccountNumberShow() {
    isAccountNumberShown = !isAccountNumberShown;
    update();
  }

  var searchFormField = TextEditingController();

  var accountNumber = TextEditingController();

  bool isAccountNumberValid = false;

  void enterAccountNumber() {
    if (accountNumber.text.isNotEmpty && accountNumber.text.length >= 12) {
      isAccountNumberValid = true;
    } else {
      isAccountNumberValid = false;
    }
    update();
  }

  List<DropdownMenuItem<RelationshipList>>? relationshipList = [];

  var selectedRelationship = Rx<RelationshipList?>(null);

  List<RelationshipList> relationshipItems = [
    RelationshipList(1, 'Father'),
    RelationshipList(2, 'Mother'),
    RelationshipList(3, 'Spouse'),
    RelationshipList(4, 'Son/Daughter'),
  ];

  List<DropdownMenuItem<PlaceOfBirthList>>? placeOfBirthList = [];

  var selectedPlaceOfBirth = Rx<PlaceOfBirthList?>(null);

  List<PlaceOfBirthList> placeOfBirthItems = [
    PlaceOfBirthList(1, 'Karnal'),
    PlaceOfBirthList(2, 'Kurukshetra'),
    PlaceOfBirthList(3, 'Panipat'),
    PlaceOfBirthList(4, 'Rohtak'),
  ];

  List<DropdownMenuItem<CountryOfNationalityList>>? countryOfNationalityList =
      [];

  var selectedCountryOfNationality = Rx<CountryOfNationalityList?>(null);

  List<CountryOfNationalityList> countryOfNationalityItems = [
    CountryOfNationalityList(1, 'India'),
    CountryOfNationalityList(2, 'Iceland'),
    CountryOfNationalityList(3, 'Japan'),
    CountryOfNationalityList(4, 'China'),
  ];

  String? politicallyExposedPerson = 'yes';

  void isPoliticallyExposedPerson(String? value) {
    politicallyExposedPerson = value;
    update();
  }

  String? relatedToPoliticallyExposedPerson = 'yes';

  void isRelatedToPoliticallyExposedPerson(String? value) {
    relatedToPoliticallyExposedPerson = value;
    update();
  }

  String? taxResidentOfAnyCountry = 'yes';

  void istaxResidentOfAnyCountry(String? value) {
    taxResidentOfAnyCountry = value;
    update();
  }

  String? citizenOfIndia = 'yes';

  void isCitizenOfIndia(String? value) {
    citizenOfIndia = value;
    update();
  }

  @override
  void onInit() async {
    relationshipList = buildRelationshipMenuItems(relationshipItems);
    placeOfBirthList = buildPlaceOfBirthItems(placeOfBirthItems);
    countryOfNationalityList =
        buildCountryOfNationalityItems(countryOfNationalityItems);
    super.onInit();
  }

  void checkIfEmailIsValid(String email) {
    isEmailValid = Utility.emailValidator(email);
    emailId = email;
    emailErrorText =
        isEmailValid ? null : StringConstants.pleaseEnterValidEmail;
    update();
  }

  List<DropdownMenuItem<RelationshipList>> buildRelationshipMenuItems(
      List<RelationshipList> listItems) {
    var items = <DropdownMenuItem<RelationshipList>>[];
    for (var listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Text(listItem.name),
          value: listItem,
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem<CountryOfNationalityList>>
      buildCountryOfNationalityItems(List<CountryOfNationalityList> listItems) {
    var items = <DropdownMenuItem<CountryOfNationalityList>>[];
    for (var listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Text(listItem.name),
          value: listItem,
        ),
      );
    }
    return items;
  }

  List<DropdownMenuItem<PlaceOfBirthList>> buildPlaceOfBirthItems(
      List<PlaceOfBirthList> listItems) {
    var items = <DropdownMenuItem<PlaceOfBirthList>>[];
    for (var listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Text(listItem.name),
          value: listItem,
        ),
      );
    }
    return items;
  }
}

class RelationshipList {
  RelationshipList(this.value, this.name);

  int value;
  String name;
}

class PlaceOfBirthList {
  PlaceOfBirthList(this.value, this.name);

  int value;
  String name;
}

class CountryOfNationalityList {
  CountryOfNationalityList(this.value, this.name);

  int value;
  String name;
}
