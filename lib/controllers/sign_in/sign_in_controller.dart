import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:investkarr/lib.dart';

class SignInController extends GetxController {
  var apiClient = ApiClient();

  String? emailErrorText;

  final pinPutController2 = TextEditingController();

  final pinPutController3 = TextEditingController();

  String phoneNumber = '';

  String emailId = '';

  String password = '';

  bool phoneNumberValid = false;

  bool isEmailValid = false;

  bool isPasswordVisible = false;

  bool isPasswordValid = false;

  bool isOTPValid = false;

  bool isAppPinValid = false;

  void checkIfEmailIsValid(String email) {
    isEmailValid = Utility.emailValidator(email);
    emailId = email;
    emailErrorText =
        isEmailValid ? null : StringConstants.pleaseEnterValidEmail;
    update();
  }

  void checkIfPhoneIsValid(String name) {
    phoneNumber = name;
    if (phoneNumber.isNotEmpty && phoneNumber.length == 10) {
      phoneNumberValid = true;
    } else {
      phoneNumberValid = false;
    }
    update();
  }

  void onOTPChanged2(String value) {
    if (pinPutController2.text.length == 6) {
      isOTPValid = true;
    } else {
      isOTPValid = false;
    }
    update();
  }

  void onOTPChanged3(String value) {
    if (pinPutController3.text.length == 4) {
      isAppPinValid = true;
    } else {
      isAppPinValid = false;
    }
    update();
  }

  void checkIfPasswordIsValid(String password) {
    this.password = password;
    if (password.isNotEmpty) {
      isPasswordValid = true;
    } else {
      isPasswordValid = false;
    }
    update();
  }

  void updatePasswordVisibility() {
    isPasswordVisible = !isPasswordVisible;
    update();
  }

  Future<void> loginWithEmail() async {
    var res = await apiClient.loginWithEmail(
        email: emailId, password: password, type: 'email', isLoading: true);
    if (!res.hasError) {
      print(res.data);
      var response = signUpResponseFromJson(res.data);
      await GetStorage('appData').write('token', response.result!.accessToken);
      RouteManagement.goToSignInEnterAppPin();
    } else {
      Get.snackbar('Error', 'Invalid Email or Password');
    }
  }

  Future<void> loginWithPhone() async {
    var res = await apiClient.sendOTP(
        keyType1: 'mobile_number',
        value1: phoneNumber,
        typeValue: 'mobile',
        isLoading: true);
    if (!res.hasError) {
      var response = sendOtpResponseFromJson(res.data);
      if (response.statusCode == 200 && response.success == true) {
        Get.snackbar('Success', response.result!.otp.toString(),
            duration: const Duration(seconds: 5));
// sms otp
        var res1 = await apiClient.sendSms(
            phone: phoneNumber,
            isLoading: true,
            otp: response.result!.otp.toString());
        print('dataxxx ${res1.data}');
        final dt = sendSmsOtpResponseFromJson(res1.data);
        print('dataxxx ${dt.data.status}');
        if (dt.data.status == 'success') {
          RouteManagement.goToSignInWithPhoneVerifyOTP();
        } else {
          Get.snackbar('Error', "SmsGetwayError",
              duration: const Duration(seconds: 5));
        }
      }
    } else {
      print(res.data);
    }
  }

  Future<void> verifyPhoneOTP() async {
    var res = await apiClient.verifyOTP(
        keyType1: 'mobile_number',
        value1: phoneNumber,
        typeValue: 'mobile',
        isLogin: true,
        otp: pinPutController2.text,
        isLoading: true);
    if (!res.hasError) {
      print(res.data);
      var response = signUpResponseFromJson(res.data);
      if (response.statusCode == 200 && response.success == true) {
        await GetStorage('appData')
            .write('token', response.result!.accessToken);
        RouteManagement.goToSignInEnterAppPin();
      }
    } else {
      print(res.data);
    }
  }

  Future<void> verifyPin() async {
    var token = await GetStorage('appData').read('token');
    var res = await apiClient.loginWithPin(
        pin: pinPutController3.text, isLoading: true, token: token.toString());
    if (!res.hasError) {
      print(res.data);
      var response = signUpResponseFromJson(res.data);
      if (response.statusCode == 200 && response.success == true) {
        await GetStorage('appData')
            .write('token', response.result!.accessToken);
        await GetStorage('appData')
            .write('email', response.result!.user!.email);
        await GetStorage('appData').write('name',
            '${response.result!.user!.firstName} ${response.result!.user!.lastName}');
        RouteManagement.goToOffAllKyc();
      }
    } else {
      Get.snackbar('Error', 'Invalid Pin');
    }
  }
}
