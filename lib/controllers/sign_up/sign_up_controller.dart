import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:investkarr/lib.dart';

class SignUpController extends GetxController {
  String? emailErrorText;

  var apiClient = ApiClient();

  final pinPutController1 = TextEditingController();
  final pinPutController2 = TextEditingController();
  final pinPutController3 = TextEditingController();
  final pinPutController4 = TextEditingController();

  bool onPasswordListening = false;

  bool isEmailValid = false;

  bool isEmailOTPValid = false;

  bool isNamesValid = false;

  bool phoneNumberValid = false;

  bool isPasswordVisible = false;

  bool isPasswordValid = false;

  bool isPanValid = false;

  bool isDOBValid = false;

  bool isAppPinValid = false;

  bool isConfirmAppPinValid = false;

  List<bool> passwordErrors = Utility.passwordValidator('');
  
  String emailId = '';

  String firstName = '';

  String lastName = '';

  String password = '';

  String phoneNumber = '';

  String panNumber = '';

  /// selected date
  DateTime? selectedDate;

  var dob = TextEditingController();

  /// Check if the email is valid or not.
  void checkIfEmailIsValid(String email) {
    isEmailValid = Utility.emailValidator(email);
    emailId = email;
    emailErrorText =
        isEmailValid ? null : StringConstants.pleaseEnterValidEmail;
    update();
  }

  void selectDate(BuildContext context) async => await showDatePicker(
        context: context,
        initialDate: selectedDate != null
            ? selectedDate!
            : DateTime.now().subtract(const Duration(days: 6574)),
        firstDate: DateTime(1900, 1),
        lastDate: DateTime.now().subtract(const Duration(days: 6574)),
        builder: (context, picker) => Theme(
          data: Theme.of(context).copyWith(
            colorScheme: ColorScheme.dark(
              primary: ColorsValue.primaryColor.withOpacity(.5),
              onSurface: Theme.of(context).scaffoldBackgroundColor,
            ),
            dialogBackgroundColor: ColorsValue.primaryColor.withOpacity(.1),
          ),
          child: picker!,
        ),
      ).then((value) {
        if (value != null) {
          dob.text = Utility.getDayMonthYear(value);
          debugPrint(dob.text);
          isDOBValid = true;
          update();
        }
        update();
      });

  void onOTPChanged1(String value) {
    if (pinPutController1.text.length == 6) {
      isEmailOTPValid = true;
    } else {
      isEmailOTPValid = false;
    }
    update();
  }

  void onOTPChanged2(String value) {
    if (pinPutController2.text.length == 6) {
      isEmailOTPValid = true;
    } else {
      isEmailOTPValid = false;
    }
    update();
  }

  void onOTPChanged3(String value) {
    if (pinPutController3.text.length == 4) {
      isAppPinValid = true;
    } else {
      isAppPinValid = false;
    }
    update();
  }

  void onOTPChanged4(String value) {
    if (pinPutController4.text.length == 4) {
      isConfirmAppPinValid = true;
    } else {
      isConfirmAppPinValid = false;
    }
    update();
  }

  void checkIfNamesAreValid(String name) {
    if (firstName.isNotEmpty && lastName.isNotEmpty) {
      isNamesValid = true;
    } else {
      isNamesValid = false;
    }
    update();
  }

  void checkIfPhoneIsValid(String name) {
    phoneNumber = name;
    if (phoneNumber.isNotEmpty && phoneNumber.length == 10) {
      phoneNumberValid = true;
    } else {
      phoneNumberValid = false;
    }
    update();
  }

  void checkIfPanIsValid(String name) {
    panNumber = name;
    if (panNumber.isNotEmpty && panNumber.length == 10) {
      isPanValid = true;
    } else {
      isPanValid = false;
    }
    update();
  }

  void checkIfDateIsValid(String name) {
    if (dob.text.isNotEmpty) {
      isDOBValid = true;
    } else {
      isDOBValid = false;
    }
    update();
  }

  /// Check if the password is valid or not.
  void checkIfPasswordIsValid(String password) {
    this.password = password;
    passwordErrors = Utility.passwordValidator(password);
    isPasswordValid = !passwordErrors.contains(false);
    update();
  }

  void updatePasswordVisibility() {
    isPasswordVisible = !isPasswordVisible;
    update();
  }

  Future<void> getOTPOnMail() async {
    var res = await apiClient.sendOTP(
        keyType1: 'email',
        value1: emailId,
        typeValue: 'email',
        isLoading: true);
    if (!res.hasError) {
      print(res.data);
      var response = sendOtpResponseFromJson(res.data);
      if (response.statusCode == 200 && response.success == true) {
        Get.snackbar('Success', response.result!.otp.toString(),
            duration: const Duration(seconds: 5));
        RouteManagement.goToSignUpWithEmailVerifyOTP();
      }
    } else {
      Utility.closeDialog();
      Get.snackbar('Error', 'Error while getting OTP on this email');
      print(res.data);
    }
  }

  Future<void> getOTPOnPhone() async {
    var res = await apiClient.sendOTP(
        keyType1: 'mobile_number',
        value1: phoneNumber,
        typeValue: 'mobile',
        isLoading: true);
    if (!res.hasError) {
      print(res.data);
      var response = sendOtpResponseFromJson(res.data);
      if (response.statusCode == 200 && response.success == true) {
        Get.snackbar('Success', response.result!.otp.toString(),
            duration: const Duration(seconds: 5));
        RouteManagement.goToSignUpWithEmailPhoneNumberVerifyOTP();
      }
    } else {
      Utility.closeDialog();
      Get.snackbar('Error', 'Error while getting OTP on this email');
      print(res.data);
    }
  }

  Future<void> verifyEmailOTP() async {
    var res = await apiClient.verifyOTP(
        keyType1: 'email',
        value1: emailId,
        typeValue: 'email',
        isLogin: false,
        otp: pinPutController1.text,
        isLoading: true);
    if (!res.hasError) {
      print(res.data);
      var response = sendOtpResponseFromJson(res.data);
      if (response.statusCode == 200 && response.success == true) {
        RouteManagement.goToSignUpWithEmailUserName();
      }
    } else {
      Get.snackbar('Error',
          'Invalid OTP, please check your inbox and enter the correct OTP');
      print(res.data);
    }
  }

  Future<void> verifyPhoneOTP(BuildContext context) async {
    FocusScope.of(context).unfocus();
    var res = await apiClient.verifyOTP(
        keyType1: 'mobile_number',
        value1: phoneNumber,
        typeValue: 'mobile',
        isLogin: false,
        otp: pinPutController2.text,
        isLoading: true);
    if (!res.hasError) {
      print(res.data);
      var response = sendOtpResponseFromJson(res.data);
      Get.snackbar('', '',
          titleText: Dimens.box0,
          borderRadius: 0,
          margin: Dimens.edgeInsets0,
          snackPosition: SnackPosition.BOTTOM,
          messageText: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.check_circle,
                color: Colors.white,
                size: Dimens.twenty,
              ),
              Dimens.boxWidth10,
              Text(
                StringConstants.otpVerifiedSuccessfully,
                style: Styles.medium14Black.copyWith(color: Colors.white),
              )
            ],
          ),
          backgroundColor: Colors.green);
      Future.delayed(const Duration(seconds: 4), () {
        if (response.statusCode == 200 && response.success == true) {
          RouteManagement.goToSignUpWithEmailPanCardVerification();
        }
      });
    } else {
      Get.snackbar('', '',
          titleText: Dimens.box0,
          borderRadius: 0,
          margin: Dimens.edgeInsets0,
          snackPosition: SnackPosition.BOTTOM,
          messageText: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                Icons.cancel,
                color: Colors.white,
                size: Dimens.twenty,
              ),
              Dimens.boxWidth10,
              Text(
                StringConstants.pleaseEnterAValidOtp,
                style: Styles.medium14Black.copyWith(color: Colors.white),
              )
            ],
          ),
          backgroundColor: ColorsValue.redColor);
      // Get.snackbar('Error',
      //     'Invalid OTP, please check your inbox and enter the correct OTP');
      print(res.data);
    }
  }

  Future<void> signUp() async {
    if (pinPutController3.text == pinPutController4.text) {
      var res = await apiClient.signUp(
          email: emailId,
          firstName: firstName,
          lastName: lastName,
          panNumber: panNumber,
          dob: dob.text,
          password: password,
          deviceId: 'deviceId',
          mobileNumber: phoneNumber,
          appPin: pinPutController4.text,
          isLoading: true);
      if (!res.hasError) {
        var data = signUpResponseFromJson(res.data);
        await GetStorage('appData').write('token', data.result!.accessToken);
        await GetStorage('appData').write('email', data.result!.user!.email);
        await GetStorage('appData').write('name',
            '${data.result!.user!.firstName} ${data.result!.user!.lastName}');
        RouteManagement.goToSignUpWithEmailSuccess();
      } else {
        print(res.data);
        Get.snackbar(
            'Error', 'User already exist with this email or phone number');
      }
    } else {
      Get.snackbar('Error', 'App Pins do not match');
    }
  }
}
