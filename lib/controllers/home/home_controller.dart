import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/dashboard/dashboard.dart';
import 'package:investkarr/lib.dart';

class HomeController extends GetxController {
  int selectedIndex = 0;

  bool isExpandedPaned = true;

  bool taxproofExpanded = false;

  bool capitalGainsExpanded = false;

  bool orderHistoryExpanded = false;

  bool isLoanDetailsExpanded = true;

  bool kycOne = false;

  bool kycTwo = false;

  bool kycThree = false;

  bool kycFour = false;

  bool kycFive = false;

  bool isAutoPayEnabled = false;
  bool isScreenLockEnabled = true;
  bool isWhatsappEnabled = false;
  bool isSmsEnabled = true;

  String selectedPaymentMethod = '';
  String typeOfInvestment = '';

  bool isAddInitialAmount = false;

  TextEditingController goalController = TextEditingController();

  bool goalEntered = false;

  void makeGoal(String v) {
    if (goalController.text.isNotEmpty) {
      goalEntered = true;
    } else {
      goalEntered = false;
    }
    update();
  }

  bool fullPaymentMethod = true;

  bool loanPaymentMethod = false;

  bool lumpsumInvesment = true;

  bool sipInvestment = false;

  double buyYearsPlan = 0.0;

  double loanAmount = 0.0;

  double inflowAmount = 0.0;

  double initialSipAmount = 0.0;

  double inflowYear = 0.0;

  double totalAmount = 0.0;

  double inflowRate = 0.0;

  double expectedReturnSlider = 0.0;

  double sipAnnualBasis = 0.0;

  bool isFutureInflows = false;

  bool wishToInitialSipAmount = false;

  void onItemTapped(int index) {
    selectedIndex = index;

    update();
  }

  Widget getItemWidget(int pos) {
    switch (pos) {
      case 0:
        return ExploreWidget();
      case 1:
        return const Dashboard();
      case 2:
        return ProfileView();
      default:
        return const Text('Error');
    }
  }

  /// Dashboard Screen =========================================================

  bool isOverViewNewUser = true;

  bool isInvestYourselfNewUser = true;

  var consolidatePassword = TextEditingController();

  var uploadConsolidatePassword = TextEditingController();

  bool isConsolidatePasswordShown = false;

  bool consolidatedFileSelectToUpload = false;

  List<String> analysisTitle = ['All', 'Equity', 'Debt', 'Money Market'];
  List<String> analysisSubtitle = ['', '95.9%', '4.5%', '4.0%'];
  int? selectedAnalysisIndex = 0;

  int? assetAnalysisIndex = 0;

  int? activeFundSortedCheck = 0;

  int? myGoalsSortedCheck = 0;

  bool? isNonMemberProfile = true;

  void activeFundSortWith(int index) {
    switch (index) {
      case 0:
        activeFundSortedCheck = 0;
        break;
      case 1:
        activeFundSortedCheck = 1;
        break;
      case 2:
        activeFundSortedCheck = 2;
        break;
      case 3:
        activeFundSortedCheck = 3;
        break;
      case 4:
        activeFundSortedCheck = 4;
        break;
      default:
    }
    update();
  }

  void myGoalsSortWith(int index) {
    switch (index) {
      case 0:
        myGoalsSortedCheck = 0;
        break;
      case 1:
        myGoalsSortedCheck = 1;
        break;
      case 2:
        myGoalsSortedCheck = 2;
        break;
      default:
    }
    update();
  }

  void selectIndexForAssetAnalysis(int index) {
    assetAnalysisIndex = index;
    update();
  }

  void selectIndexForAnalysisInvestment(int index) {
    selectedAnalysisIndex = index;
    update();
  }

  /// selected date
  DateTime? selectedDate;

  var from = TextEditingController();

  var to = TextEditingController();

  void selectDateFrom(BuildContext context) async => await showDatePicker(
        context: context,
        initialDate: selectedDate != null
            ? selectedDate!
            : DateTime.now().subtract(const Duration(days: 6574)),
        firstDate: DateTime(1900, 1),
        lastDate: DateTime.now().subtract(const Duration(days: 6574)),
        builder: (context, picker) => Theme(
          data: Theme.of(context).copyWith(
            colorScheme: ColorScheme.dark(
              primary: ColorsValue.primaryColor.withOpacity(.5),
              onSurface: ColorsValue.primaryColor,
            ),
            dialogBackgroundColor: Colors.white,
          ),
          child: picker!,
        ),
      ).then((value) {
        if (value != null) {
          from.text = Utility.getMonthYear(value);
          debugPrint(from.text);
          update();
        }
        update();
      });

  void selectDateTo(BuildContext context) async => await showDatePicker(
        context: context,
        initialDate: selectedDate != null
            ? selectedDate!
            : DateTime.now().subtract(const Duration(days: 6574)),
        firstDate: DateTime(1900, 1),
        lastDate: DateTime.now().subtract(const Duration(days: 6574)),
        builder: (context, picker) => Theme(
          data: Theme.of(context).copyWith(
            colorScheme: ColorScheme.dark(
              primary: ColorsValue.primaryColor.withOpacity(.5),
              onSurface: ColorsValue.primaryColor,
            ),
            dialogBackgroundColor: Colors.white,
          ),
          child: picker!,
        ),
      ).then((value) {
        if (value != null) {
          to.text = Utility.getMonthYear(value);
          update();
        }
        update();
      });

  var contactSupportController = TextEditingController();

  bool contactSupportAttachmentSelected = false;

  /// Dashboard Screen ends here ===============================================
}
