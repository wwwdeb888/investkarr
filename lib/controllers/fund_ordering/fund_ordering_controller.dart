import 'package:flutter/material.dart';
import 'package:get/get.dart';

class FundOrderingController extends GetxController {
  var checkBoxValue = -1;

  List<DropdownMenuItem<InvestmentList>>? investmentList = [];

  var selectedInvestment = Rx<InvestmentList?>(null);

  List<InvestmentList> investmentItems = [
    InvestmentList(1, 'SIP'),
    InvestmentList(2, 'Lumpsum'),
  ];

  bool isSipDateVisible=true;

  var selectedDate=-1;

  var datesList = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    '10',
    '11',
    '12',
    '13',
    '14',
    '15',
    '16',
    '17',
    '18',
    '19',
    '20',
    '21',
    '22',
    '23',
    '24',
    '25',
    '26',
    '27',
    '28'
  ];

  @override
  void onInit() {
    investmentList = buildInvestmentMenuItems(investmentItems);
    super.onInit();
  }

  List<DropdownMenuItem<InvestmentList>> buildInvestmentMenuItems(
      List<InvestmentList> listItems) {
    var items = <DropdownMenuItem<InvestmentList>>[];
    for (var listItem in listItems) {
      items.add(
        DropdownMenuItem(
          child: Text(listItem.name),
          value: listItem,
        ),
      );
    }
    return items;
  }
}

class InvestmentList {
  InvestmentList(this.value, this.name);

  int value;
  String name;
}
