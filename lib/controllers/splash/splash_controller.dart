import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:investkarr/lib.dart';

class SplashController extends GetxController {
  // late VideoPlayerController controller;

  @override
  void onInit() async {
    goToHomePage(true);
    super.onInit();
  }

  /// Go to onBoarding
  ///
  /// [shouldWait] : Tells if there should be delay before going to
  /// onBoarding or not.
  void goToHomePage(bool shouldWait) async {
    var token = await GetStorage('appData').read('token');
    if (token != null && token != '') {
      Future.delayed(
        Duration(seconds: shouldWait ? 2 : 0),
        () {
          // Get.off(BioMetricScreen());
          // RouteManagement.goToBioMetricScreen();
          Navigator.of(Get.context!)
              .push(MaterialPageRoute(builder: (context) => BioMetricScreen()));
        },
      );
    } else {
      Future.delayed(
        Duration(seconds: shouldWait ? 2 : 0),
        RouteManagement.goToLoginOrRegister,
      );
    }
  }
}
