import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class ColumnTile extends StatelessWidget {
  ColumnTile({
    Key? key,
    this.title,
    this.value,
    this.color,
    this.valueAlignment,
    this.editColor,
    this.isEditText = false,
    this.valueStyle,
    this.onTapEdit,
    this.onTapTitle,
    this.titleStyle,
  }) : super(key: key);

  final String? title;
  final String? value;
  final Color? color;
  final Color? editColor;
  final bool? isEditText;
  final TextStyle? valueStyle;
  final CrossAxisAlignment? valueAlignment;
  final TextStyle? titleStyle;
  final void Function()? onTapEdit;
  final void Function()? onTapTitle;

  @override
  Widget build(BuildContext context) => Column(
        crossAxisAlignment: valueAlignment ?? CrossAxisAlignment.start,
        children: [
          GestureDetector(
            onTap: () {
              onTapTitle!();
            },
            child: Text(
              '$title',
              style:titleStyle?? Styles.regular12Grey,
            ),
          ),
          Dimens.boxHeight10,
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              GestureDetector(
                onTap: () {},
                child: Text(
                  '$value',
                  style: valueStyle ??
                      Styles.medium16Black
                          .copyWith(color: color ?? Colors.black),
                ),
              ),
              Dimens.boxWidth5,
              if (isEditText!)
                GestureDetector(
                  onTap: () {
                    onTapEdit!();
                  },
                  child: Text(
                    'edit'.tr,
                    style: Styles.medium12PrimaryUnderline
                        .copyWith(color: editColor ?? ColorsValue.primaryColor),
                  ),
                ),
            ],
          )
        ],
      );
}
