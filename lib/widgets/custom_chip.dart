
import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class CustomChip extends StatelessWidget {
  const CustomChip({
    Key? key,
    this.title,
  }) : super(key: key);

  final String? title;

  @override
  Widget build(BuildContext context) => Row(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(
                Radius.circular(50),
              ),
              border: Border.all(
                width: 1,
                color: ColorsValue.primaryColor,
              ),
            ),
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 2,
            ),
            child: Text(
              '$title',
              style: Styles.primary15,
            ),
          ),
          Dimens.boxWidth10,
        ],
      );
}
