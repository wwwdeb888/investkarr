import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class ContainerType1 extends StatelessWidget {
  const ContainerType1({
    Key? key,
    this.color,
    this.title,
    this.value,
  }) : super(key: key);

  final Color? color;
  final String? title;
  final String? value;

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Container(
                height: Dimens.eight,
                width: Dimens.eight,
                decoration: BoxDecoration(
                  color: color!,
                  borderRadius: BorderRadius.all(
                    Radius.circular(Dimens.eight),
                  ),
                ),
              ),
              Dimens.boxWidth5,
              Text(
                '$title',
                style: Styles.medium12Black,
              ),
            ],
          ),
          Text(
            '$value%',
            style: Styles.medium12Grey,
          ),
        ],
      );
}
