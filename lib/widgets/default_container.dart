import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/theme/dimens.dart';

class DefaultContainer extends StatelessWidget {
  const DefaultContainer({
    Key? key,
    this.child,
    this.padding,
    this.width,
    this.border,
    this.color,
  }) : super(key: key);

  final Widget? child;
  final EdgeInsetsGeometry? padding;
  final double? width;
  final Border? border;
  final Color? color;

  @override
  Widget build(BuildContext context) => Container(
        width: width ?? double.infinity,
        decoration: BoxDecoration(
          color: color ?? Colors.white,
          borderRadius: const BorderRadius.all(
            Radius.circular(16),
          ),
          border: border,
          boxShadow: Styles.cardShadow,
        ),
        padding: padding ?? const EdgeInsets.all(16),
        child: child,
      );
}
