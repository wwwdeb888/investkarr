import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class GlobalContainer extends StatelessWidget {
  const GlobalContainer({
    Key? key,
    this.borderColor,
    this.borderRadius,
    this.borderWidth,
    this.child,
    this.color,
    this.height,
    this.width,
    this.padding,
    this.isBorderOnly = false,
    this.rBottomLeft,
    this.rBottomRight,
    this.rTopLeft,
    this.rTopRight,
    this.constraints,
    this.onTap,
  }) : super(key: key);

  final double? width;
  final double? height;
  final double? borderWidth;
  final Color? borderColor;
  final Color? color;
  final double? borderRadius;
  final Widget? child;
  final EdgeInsetsGeometry? padding;
  final bool isBorderOnly;
  final double? rBottomLeft;
  final double? rBottomRight;
  final double? rTopLeft;
  final double? rTopRight;
  final BoxConstraints? constraints;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () {
          onTap!();
        },
        child: Container(
          padding: padding ?? Dimens.edgeInsets0,
          width: width ?? Dimens.fifty,
          height: height ?? Dimens.fifty,
          constraints: constraints,
          decoration: BoxDecoration(
            border: Border.all(
              width: borderWidth ?? Dimens.zero,
              color: borderColor ?? ColorsValue.backgroundColor,
            ),
            borderRadius: isBorderOnly
                ? BorderRadius.only(
                    bottomLeft: Radius.circular(rBottomLeft ?? Dimens.twelve),
                    bottomRight: Radius.circular(rBottomRight ?? Dimens.twelve),
                    topLeft: Radius.circular(rTopLeft ?? Dimens.twelve),
                    topRight: Radius.circular(rTopRight ?? Dimens.twelve),
                  )
                : BorderRadius.circular(borderRadius ?? Dimens.twelve),
            color: color ?? ColorsValue.lightBgContainerColor,
          ),
          child: child ?? Dimens.box0,
        ),
      );
}
