
import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class TableHeading extends StatelessWidget {
  const TableHeading({
    Key? key,
    this.title,
  }) : super(key: key);

  final String? title;

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          '$title',
          textScaleFactor: 1,
          textAlign: TextAlign.center,
          style: Styles.boldBlack15,
        ),
      );
}
