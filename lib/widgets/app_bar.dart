import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  const CustomAppBar({
    Key? key,
    this.title,
    this.onLeadingTap,
    this.actions,
  }) : super(key: key);

  final String? title;
  final Function()? onLeadingTap;
  final Widget? actions;

  @override
  Widget build(BuildContext context) => SafeArea(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                IconButton(
                  onPressed: onLeadingTap ?? Get.back,
                  icon: Icon(
                    Icons.arrow_back_ios,
                    size: Dimens.twenty,
                  ),
                ),
                Dimens.boxWidth10,
                Text(
                  title!,
                  style: Styles.bold16Black,
                ),
              ],
            ),
            actions ?? Dimens.box0,
          ],
        ),
      );

  @override
  Size get preferredSize => const Size.fromHeight(100);
}
