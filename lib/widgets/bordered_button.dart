import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class BorderedButton extends StatelessWidget {
  BorderedButton({Key? key, this.text, this.padding, this.height, this.onTap})
      : super(key: key);

  final String? text;
  final double? height;
  void Function()? onTap;
  final EdgeInsetsGeometry? padding;

  @override
  Widget build(BuildContext context) => Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
          padding: padding ?? Dimens.edgeInsets5,
          child: SizedBox(
            width: Dimens.percentWidth(1),
            height: height ?? Dimens.percentHeight(.07),
            child: ElevatedButton(
              style: Styles.elevatedButtonTheme.style!.copyWith(
                elevation: MaterialStateProperty.all(0),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(Dimens.fifty),
                        side: BorderSide(
                            width: Dimens.two,
                            color: ColorsValue.primaryColor))),
                backgroundColor: MaterialStateProperty.resolveWith<Color>(
                  (Set<MaterialState> states) => (0 == 0)
                      ? ColorsValue.backgroundColor
                      : ColorsValue.primaryColor.withOpacity(.5),
                ),
                textStyle: MaterialStateProperty.all(
                  Styles.bold16White,
                ),
              ),
              onPressed: () {
                onTap!();
              },
              child: Text(
                text ?? '',
                style: Styles.bold16Primary,
              ),
            ),
          ),
        ),
      );
}
