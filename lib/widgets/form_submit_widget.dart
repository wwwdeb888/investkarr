import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

/// A widget which will be used in the forms to allow user to submit the
/// details which were filled by the user.
///
/// [opacity] : The opacity of the widget to show that the widget is not enabled.
/// [disableColor] : The disable color of the widget.
/// [padding] : The padding around the content of the widget.
/// [onTap] : The tap event which will be triggered.
/// [text] : The text which will be shown.
/// [textStyle] : The style of the [text].
class FormSubmitWidget extends StatelessWidget {
  FormSubmitWidget({
    Key? key,
    this.opacity = 1,
    this.disableColor = ColorsValue.greyColor,
    this.padding,
    required this.backgroundColor,
    this.onTap,
    required this.text,
    this.textStyle,
    this.autoFocus = false,
  }) : super(key: key);

  static const formSubmitWidgetKey = 'form-submit-widget-key';

  final double opacity;
  final Color disableColor;
  final Color backgroundColor;
  final EdgeInsets? padding;
  final void Function()? onTap;
  final String text;
  final TextStyle? textStyle;
  final bool autoFocus;

  @override
  Widget build(BuildContext context) => Opacity(
        key: const Key(formSubmitWidgetKey),
        opacity: opacity,
        child: ElevatedButton(
          autofocus: autoFocus,
          style: Styles.elevatedButtonTheme.style!.copyWith(
            backgroundColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) =>
                  states.contains(MaterialState.disabled)
                      ? disableColor
                      : backgroundColor,
            ),
            textStyle: MaterialStateProperty.all(
              textStyle,
            ),
            padding: MaterialStateProperty.all(
              padding,
            ),
          ),
          onPressed: onTap,
          child: Text(
            text,
            style: textStyle,
          ),
        ),
      );
}

class GreyFormSubmitWidget extends StatelessWidget {
  GreyFormSubmitWidget({
    Key? key,
    this.opacity = 1,
    this.disableColor = ColorsValue.greyColor,
    this.padding,
    this.onTap,
    required this.text,
    this.textStyle,
    this.autoFocus = false,
  }) : super(key: key);

  static const formSubmitWidgetKey = 'form-submit-widget-key';

  final double opacity;
  final Color disableColor;
  final EdgeInsets? padding;
  final void Function()? onTap;
  final String text;
  final TextStyle? textStyle;
  final bool autoFocus;

  @override
  Widget build(BuildContext context) => Opacity(
        key: const Key(formSubmitWidgetKey),
        opacity: opacity,
        child: ElevatedButton(
          autofocus: autoFocus,
          style: Styles.elevatedButtonTheme.style!.copyWith(
            backgroundColor: MaterialStateProperty.resolveWith<Color>(
              (Set<MaterialState> states) =>
                  states.contains(MaterialState.disabled)
                      ? disableColor
                      : ColorsValue.greyColor,
            ),
            textStyle: MaterialStateProperty.all(
              textStyle,
            ),
            padding: MaterialStateProperty.all(
              padding,
            ),
          ),
          onPressed: onTap,
          child: Text(
            text,
            style: textStyle,
          ),
        ),
      );
}
