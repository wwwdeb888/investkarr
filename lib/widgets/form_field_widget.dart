import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

/// A form field widget which will handle form ui.
///
/// [focusNode] : FocusNode for the form field.
/// [autoFocus] : Allow auto focus for the form field if true.
/// [textEditingController] : Text editing controller for the form field
///                           to handle the text change and other stuff.
/// [isObscureText] : If true it will make the form text secure.
/// [obscureCharacter] : If [isObscureText] true this will be used for the
///                      character which will be shown.
/// [textCapitalization] : Type of text capitalization for the form field.
/// [isFilled] : If true the decoration colors will be filled.
/// [contentPadding] : Padding for the form field between the content and
///                    boundary of the form.
/// [hintText] : The hint text of the form field.
/// [hintStyle] : The hint style for the the form field.
/// [errorStyle] : The error style for the the form field.
/// [formBorder] : The border for the form field.
/// [errorText] : The error text of the form field.
/// [suffixIcon] : The suffix widget of the form field.
/// [prefixIcon] : The prefix widget of the form field.
/// [textInputAction] : The text input action for the form filed.
/// [textInputType] : The keyboard type of the form field.
/// [formStyle] : The style of the form field. This will be used for the style
///               of the content.
class FormFieldWidget extends StatelessWidget {
  FormFieldWidget({
    this.focusNode,
    this.autoFocus = false,
    this.textEditingController,
    this.fillColor,
    this.isObscureText = false,
    this.obscureCharacter = ' ',
    this.textCapitalization = TextCapitalization.none,
    this.isFilled,
    this.contentPadding = EdgeInsets.zero,
    this.hintText,
    this.hintStyle,
    this.errorStyle,
    this.formBorder,
    required this.borderColor,
    this.errorText,
    this.suffixIcon,
    this.prefixIcon,
    this.textInputAction = TextInputAction.done,
    this.textInputType = TextInputType.text,
    this.formStyle,
    required this.onChange,
    this.isReadOnly = false,
    this.onTap,
    this.suffixIconConstraints,
    this.elevation = 10.0,
    this.borderRadius,
    this.maxLength,
    this.minLines,
    this.maxLines = 1,
    this.initialValue,
    this.labelText,
    this.labelStyle,
    this.textAlign,
  });

  final FocusNode? focusNode;
  final bool autoFocus;
  final TextEditingController? textEditingController;
  final bool isObscureText;
  final String obscureCharacter;
  final TextCapitalization textCapitalization;
  final bool? isFilled;
  final EdgeInsets? contentPadding;
  final String? hintText;
  final TextStyle? hintStyle;
  final TextStyle? errorStyle;
  final OutlineInputBorder? formBorder;
  final String? errorText;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final TextInputAction? textInputAction;
  final TextInputType? textInputType;
  final TextStyle? formStyle;
  final void Function(String value) onChange;
  final bool isReadOnly;
  final Function()? onTap;
  final Color? fillColor;
  final Color borderColor;
  final BoxConstraints? suffixIconConstraints;
  final double elevation;
  final double? borderRadius;
  final int? maxLength;
  final int? minLines;
  final int? maxLines;
  final String? initialValue;
  final String? labelText;
  final TextStyle? labelStyle;
  final TextAlign? textAlign;

  @override
  Widget build(BuildContext context) => Material(
        elevation: elevation,
        borderRadius: BorderRadius.circular(borderRadius ?? Dimens.ten),
        child: TextFormField(
          key: const Key('text-form-field'),
          readOnly: isReadOnly,
          autofocus: autoFocus,
          focusNode: focusNode,
          controller: textEditingController,
          obscureText: isObscureText,
          obscuringCharacter: obscureCharacter,
          textCapitalization: textCapitalization,
          onTap: onTap,
          textAlign: textAlign ?? TextAlign.start,
          initialValue: initialValue,
          decoration: InputDecoration(
            labelText: labelText,
            labelStyle: labelStyle,
            suffixIconConstraints: suffixIconConstraints,
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: borderColor),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: borderColor),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: borderColor),
            ),
            counterText: '',
            focusedErrorBorder: const UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.red),
            ),
            filled: true,
            contentPadding: contentPadding,
            fillColor: ColorsValue.formFieldColor,
            // border: formBorder,
            hintText: hintText,
            hintStyle: hintStyle,
            errorText: errorText,
            errorStyle: errorStyle,
            suffixIcon: suffixIcon,
            prefixIcon: prefixIcon,
          ),
          onChanged: onChange,
          textInputAction: textInputAction,
          keyboardType: textInputType,
          style: formStyle,
          maxLength: maxLength,
          maxLines: maxLines,
          minLines: minLines,
        ),
      );
}
