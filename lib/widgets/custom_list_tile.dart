import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:investkarr/lib.dart';

class CustomListTile extends StatelessWidget {
  const CustomListTile(
      {Key? key, this.leading, this.title, this.subtitle, this.onTap})
      : super(key: key);

  final String? leading;
  final String? title;
  final String? subtitle;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) => ListTile(
    onTap: onTap,
        tileColor: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
        ),
        contentPadding: Dimens.edgeInsets16,
        leading: SvgPicture.asset(leading!),
        title: Padding(
          padding: Dimens.edgeInsets0_0_0_10,
          child: Text(
            '$title',
            style: Styles.bold16Black,
          ),
        ),
        subtitle: Text(
          '$subtitle',
          style: Styles.grey15,
        ),
        trailing: SvgPicture.asset('assets/home/Group 30722.svg'),
      );
}
