import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class CuratedPacksSummary extends StatelessWidget {
  const CuratedPacksSummary({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: const CustomAppBar(
          title: '',
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: Dimens.edgeInsets16,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Tax Saver',
                        style: Styles.bold16Primary.copyWith(fontSize: 35),
                      ),
                      Dimens.boxHeight25,
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Text(
                            '17.22%',
                            style: Styles.boldBlack35.copyWith(height: .5),
                          ),
                          Dimens.boxWidth12,
                          Text(
                            '3Y annualised ',
                            style: Styles.grey15,
                          ),
                        ],
                      ),
                      Dimens.boxHeight35,
                      SizedBox(
                        width: Get.width,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              ' NAV : 65.19 ',
                              style: Styles.boldBlack15,
                            ),
                            Text(
                              '24th Jan 2020',
                              style: Styles.boldBlack15,
                            ),
                          ],
                        ),
                      ),
                      Dimens.boxHeight10,
                      Container(
                        height: 200,
                        width: Get.width,
                        color: Colors.pink.withOpacity(.3),
                      ),
                      Dimens.boxHeight10,
                      const Divider(),
                      Dimens.boxHeight10,
                      Text(
                        'Funds in the basket',
                        style: Styles.bookBoldBlack20,
                      ),
                      Container(
                        margin: const EdgeInsets.only(
                          left: 16,
                          top: 16,
                        ),
                        child: DefaultTabController(
                          length: 4,
                          child: Column(
                            children: [
                              Container(
                                padding: Dimens.edgeInsets7,
                                decoration: BoxDecoration(
                                  color:
                                      ColorsValue.primaryColor.withOpacity(.1),
                                  borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(50),
                                    bottomLeft: Radius.circular(50),
                                  ),
                                ),
                                child: TabBar(
                                  labelPadding: Dimens.edgeInsets16,
                                  indicator: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(50),
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.1),
                                        spreadRadius: 4,
                                        blurRadius: 3,
                                        offset: const Offset(0, 2),
                                      ),
                                    ],
                                  ),
                                  tabs: [
                                    Text(
                                      'all'.tr,
                                      style: Styles.bold13Primary,
                                    ),
                                    Column(
                                      children: [
                                        Text(
                                          'equity'.tr,
                                          style: Styles.bold13Primary,
                                        ),
                                        Text(
                                          '95.9%'.tr,
                                          style: Styles.black12,
                                        ),
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Text(
                                          'equity'.tr,
                                          style: Styles.bold13Primary,
                                        ),
                                        Text(
                                          '95.9%'.tr,
                                          style: Styles.black12,
                                        ),
                                      ],
                                    ),
                                    Column(
                                      children: [
                                        Text(
                                          'equity'.tr,
                                          style: Styles.bold13Primary,
                                        ),
                                        Text(
                                          '95.9%'.tr,
                                          style: Styles.black12,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                height: 100,
                                margin: const EdgeInsets.symmetric(
                                  vertical: 16,
                                ),
                                child: TabBarView(
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            height: 200,
                                            decoration: BoxDecoration(
                                              color:
                                                  Colors.amber.withOpacity(.3),
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(12),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Dimens.boxWidth8,
                                        Expanded(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              const ContainerType1(
                                                color: Colors.purple,
                                                title: 'Goal Based MF',
                                                value: '90.0',
                                              ),
                                              const ContainerType1(
                                                color: Colors.red,
                                                title: 'Our Solutions',
                                                value: '4',
                                              ),
                                              const ContainerType1(
                                                color: Colors.yellow,
                                                title: 'Direct MF',
                                                value: '4',
                                              ),
                                              const ContainerType1(
                                                color: Colors.yellow,
                                                title: 'Wealth Creation',
                                                value: '4',
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            height: 200,
                                            decoration: BoxDecoration(
                                              color:
                                                  Colors.amber.withOpacity(.3),
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(12),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Dimens.boxWidth8,
                                        Expanded(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              const ContainerType1(
                                                color: Colors.purple,
                                                title: 'Goal Based MF',
                                                value: '90.0',
                                              ),
                                              const ContainerType1(
                                                color: Colors.red,
                                                title: 'Our Solutions',
                                                value: '4',
                                              ),
                                              const ContainerType1(
                                                color: Colors.yellow,
                                                title: 'Direct MF',
                                                value: '4',
                                              ),
                                              const ContainerType1(
                                                color: Colors.yellow,
                                                title: 'Wealth Creation',
                                                value: '4',
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            height: 200,
                                            decoration: BoxDecoration(
                                              color:
                                                  Colors.amber.withOpacity(.3),
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(12),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Dimens.boxWidth8,
                                        Expanded(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              const ContainerType1(
                                                color: Colors.purple,
                                                title: 'Goal Based MF',
                                                value: '90.0',
                                              ),
                                              const ContainerType1(
                                                color: Colors.red,
                                                title: 'Our Solutions',
                                                value: '4',
                                              ),
                                              const ContainerType1(
                                                color: Colors.yellow,
                                                title: 'Direct MF',
                                                value: '4',
                                              ),
                                              const ContainerType1(
                                                color: Colors.yellow,
                                                title: 'Wealth Creation',
                                                value: '4',
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            height: 200,
                                            decoration: BoxDecoration(
                                              color:
                                                  Colors.amber.withOpacity(.3),
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(12),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Dimens.boxWidth8,
                                        Expanded(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              const ContainerType1(
                                                color: Colors.purple,
                                                title: 'Goal Based MF',
                                                value: '90.0',
                                              ),
                                              const ContainerType1(
                                                color: Colors.red,
                                                title: 'Our Solutions',
                                                value: '4',
                                              ),
                                              const ContainerType1(
                                                color: Colors.yellow,
                                                title: 'Direct MF',
                                                value: '4',
                                              ),
                                              const ContainerType1(
                                                color: Colors.yellow,
                                                title: 'Wealth Creation',
                                                value: '4',
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                ListView.builder(
                  itemCount: 20,
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) => Container(
                    padding: Dimens.edgeInsets7,
                    child: DefaultContainer(
                      child: Row(
                        children: [
                          Container(
                            margin: Dimens.edgeInsets7,
                            height: 50,
                            width: 50,
                            color: Colors.pink.withOpacity(.3),
                          ),
                          Expanded(
                            child: Text(
                              'Quant Tax Plan Direct Growth Plan',
                              style: Styles.black15,
                            ),
                          ),
                          const Icon(
                            Icons.arrow_forward_ios_rounded,
                            size: 20,
                          ),
                          Dimens.boxWidth15,
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                '40%',
                                style: Styles.primaryBold18,
                              ),
                              Text(
                                'Technology',
                                style: Styles.black16,
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: Dimens.edgeInsets16,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Dimens.boxHeight10,
                      const Divider(),
                      Dimens.boxHeight10,
                      Text(
                        'Return Calculator',
                        style: Styles.bookBoldBlack20,
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: Dimens.edgeInsets16,
                  child: DefaultContainer(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Dimens.boxHeight25,
                        Text(
                          'Total investment of 65K',
                          style: Styles.black15,
                        ),
                        Dimens.boxHeight10,
                        Text(
                          'Would have become 1.19 (37.85%)',
                          style: Styles.black15,
                        ),
                        Dimens.boxHeight10,
                        Row(
                          children: [
                            Chip(
                              backgroundColor:
                                  ColorsValue.primaryColor.withOpacity(.3),
                              label: Text(
                                'Monthly SIP',
                                style: Styles.boldBlack15,
                              ),
                            ),
                            Dimens.boxWidth10,
                            Chip(
                              label: Text(
                                'Lumpsum',
                                style: Styles.boldBlack15,
                              ),
                            ),
                          ],
                        ),
                        Dimens.boxHeight25,
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(
                              '2,000',
                              style: Styles.boldBlack22.copyWith(height: .5),
                            ),
                            Dimens.boxWidth10,
                            Text(
                              'per month',
                              style: Styles.black13,
                            ),
                          ],
                        ),
                        Dimens.boxHeight10,
                        Slider(
                          value: 20,
                          min: 0,
                          max: 100,
                          thumbColor: ColorsValue.primaryColor,
                          activeColor: ColorsValue.primaryColor,
                          onChanged: (_) {},
                        ),
                        Dimens.boxHeight10,
                        Text(
                          'No.of years',
                          style: Styles.bookGrey20,
                        ),
                        Dimens.boxHeight10,
                        Row(
                          children: [
                            Chip(
                              backgroundColor:
                                  ColorsValue.primaryColor.withOpacity(.3),
                              label: Text(
                                '1Y',
                                style: Styles.boldBlack15,
                              ),
                            ),
                            Dimens.boxWidth10,
                            Chip(
                              label: Text(
                                '3Y',
                                style: Styles.boldBlack15,
                              ),
                            ),
                            Dimens.boxWidth10,
                            Chip(
                              label: Text(
                                '5Y',
                                style: Styles.boldBlack15,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: Dimens.edgeInsets16,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Dimens.boxHeight10,
                      const Divider(),
                      Dimens.boxHeight10,
                      Text(
                        'Other similar solutions',
                        style: Styles.bookBoldBlack20,
                      ),
                    ],
                  ),
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      ...List.generate(
                        5,
                        (index) => SizedBox(
                          width: 200,
                          child: Card(
                            elevation: 2,
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.circular(Dimens.twenty),
                            ),
                            child: Padding(
                              padding: Dimens.edgeInsets20,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    StringConstants.emergency,
                                    style: Styles.boldBlack18,
                                  ),
                                  Dimens.boxHeight10,
                                  Text(
                                    StringConstants.riskFunds,
                                    style: Styles.black12,
                                  ),
                                  Dimens.boxHeight30,
                                  IconButton(
                                    onPressed: () {},
                                    icon: CircleAvatar(
                                      radius: Dimens.fifteen,
                                      child: Icon(
                                        Icons.arrow_forward_ios,
                                        color: ColorsValue.primaryColor,
                                        size: Dimens.ten,
                                      ),
                                      backgroundColor: const Color(0xffF9F6FE),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ).toList(),
                    ],
                  ),
                ),
                Dimens.boxHeight25,
                Row(
                  children: [
                    Dimens.boxWidth10,
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 35,
                          vertical: 10,
                        ),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(50),
                          ),
                          color: ColorsValue.primaryColor,
                        ),
                        child: Column(
                          children: [
                            Text(
                              'Monthy SIP',
                              style: Styles.boldBlack18.copyWith(color: Colors.white),
                            ),
                            Text(
                              'Min. 500',
                              style: Styles.grey13,
                            )
                          ],
                        ),
                      ),
                    ),
                    Dimens.boxWidth10,
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 35,
                          vertical: 10,
                        ),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(
                            Radius.circular(50),
                          ),
                          color: ColorsValue.primaryColor,
                        ),
                        child: Column(
                          children: [
                            Text(
                              'Lumpsum',
                              style: Styles.boldBlack18.copyWith(color: Colors.white),
                            ),
                            Text(
                              'Min. 500',
                              style: Styles.grey13,
                            )
                          ],
                        ),
                      ),
                    ),
                    Dimens.boxWidth10,
                  ],
                ),
                Dimens.boxHeight20,
              ],
            ),
          ),
        ),
      );
}
