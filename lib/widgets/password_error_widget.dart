import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class PassWordErrorWidget extends StatelessWidget {
  PassWordErrorWidget({
    Key? key,
    required this.error,
    required this.iconTextStyle,
  }) : super(key: key);

  final List<bool> error;
  final TextStyle iconTextStyle;

  @override
  Widget build(BuildContext context) => Wrap(
        alignment: WrapAlignment.start,
        runSpacing: Dimens.ten,
        children: [
          IconTextWidget(
            isCorrect: error[0],
            iconTextStyle: iconTextStyle,
            message: StringConstants.shouldBe8Characters,
          ),
          Dimens.boxWidth10,
          IconTextWidget(
            isCorrect: error[3],
            iconTextStyle: iconTextStyle,
            message: StringConstants.oneSpecial,
          ),
          Dimens.boxWidth10,
          IconTextWidget(
            isCorrect: error[1],
            iconTextStyle: iconTextStyle,
            message: StringConstants.oneUppercase,
          ),
          Dimens.boxWidth10,
          IconTextWidget(
            isCorrect: error[2],
            iconTextStyle: iconTextStyle,
            message: StringConstants.oneDigit,
          ),
        ],
      );
}
