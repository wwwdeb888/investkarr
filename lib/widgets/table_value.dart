
import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class TableValue extends StatelessWidget {
  const TableValue({
    Key? key,
    this.title,
  }) : super(key: key);

  final String? title;

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          '$title',
          textScaleFactor: 1,
          style: Styles.black15,
          textAlign: TextAlign.center,
        ),
      );
}
