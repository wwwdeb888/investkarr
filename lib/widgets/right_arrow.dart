import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:investkarr/lib.dart';

class RightArrow extends StatelessWidget {
  const RightArrow({
    Key? key,
    this.onTap,
  }) : super(key: key);
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () {
          onTap!();
        },
        child: SvgPicture.asset(
          AssetConstants.rightArrow,
          height: Dimens.eighteen,
          width: Dimens.eighteen,
        ),
      );
}
