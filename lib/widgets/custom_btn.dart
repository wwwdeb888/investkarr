import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({
    Key? key,
    this.title,
    this.onTap,
    this.icon,
  }) : super(key: key);

  final String? title;
  final Function()? onTap;
  final IconData? icon;

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: onTap,
        child: Container(
          padding: Dimens.edgeInsets32_12,
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(50)),
            color: ColorsValue.primaryColor,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (icon != null)
                Icon(
                  icon,
                  color: Colors.white,
                ),
              Text(
                '$title',
                style: Styles.boldWhite16,
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      );
}
