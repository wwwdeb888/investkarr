import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:investkarr/lib.dart';

class CustomListTile2 extends StatelessWidget {
  const CustomListTile2({
    Key? key,
    this.title,
    this.subtitle,
    this.trailing,
    this.subTrailing,
    this.showSuggested = false,
    this.onTap,
  }) : super(key: key);

  final String? title;
  final List<String>? subtitle;
  final String? trailing;
  final String? subTrailing;
  final bool? showSuggested;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) => GestureDetector(
    onTap: onTap,
    child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
          child: Stack(
            children: [
              DefaultContainer(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 30),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      height: 40,
                      width: 40,
                      decoration: const BoxDecoration(
                        color: Colors.purple,
                        borderRadius: BorderRadius.all(
                          Radius.circular(12),
                        ),
                      ),
                    ),
                    Dimens.boxWidth8,
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            '$title',
                            style: Styles.bold16Black,
                          ),
                          Dimens.boxHeight10,
                          Row(
                            children: [
                              ...List.generate(
                                subtitle!.length,
                                (index) => CustomChip(
                                  title: '${subtitle![index]}',
                                ),
                              ).toList(),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Dimens.boxWidth10,
                    Column(
                      children: [
                        Text(
                          '$trailing',
                          style: Styles.boldBlack15,
                        ),
                        Dimens.boxHeight3,
                        Text(
                          '$subTrailing',
                          style: Styles.black12,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              if (showSuggested!)
                SvgPicture.asset('assets/home/suggested_by_expert.svg')
              // if(showSuggested!)  Image.asset('assets/home/suggested_by_expert.png')
            ],
          ),
        ),
  );
}
