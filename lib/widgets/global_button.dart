import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class GlobalButton extends StatelessWidget {
  GlobalButton({
    Key? key,
    this.text,
    this.enable = true,
    this.onTap,
  }) : super(key: key);
  final String? text;
  final bool? enable;
  void Function()? onTap;

  @override
  Widget build(BuildContext context) => Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
          padding: Dimens.edgeInsets5,
          child: SizedBox(
            width: Dimens.percentWidth(1),
            height: Dimens.percentHeight(.07),
            child: ElevatedButton(
              style: Styles.elevatedButtonTheme.style!.copyWith(
                elevation: MaterialStateProperty.all(0),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(Dimens.fifty),
                    side: BorderSide(
                      width: enable! ? Dimens.two : Dimens.zero,
                      color: (enable!)
                          ? ColorsValue.primaryColor
                          : ColorsValue.primaryColor.withOpacity(.5),
                    ),
                  ),
                ),
                backgroundColor: MaterialStateProperty.resolveWith<Color>(
                  (Set<MaterialState> states) => (enable!)
                      ? ColorsValue.primaryColor
                      : ColorsValue.primaryColor.withOpacity(.5),
                ),
                textStyle: MaterialStateProperty.all(
                  Styles.bold16White,
                ),
              ),
              onPressed: () {
                if (enable!) {
                  onTap!();
                } else {}
              },
              child: Text(
                text ?? '',
                style: Styles.bold16White,
              ),
            ),
          ),
        ),
      );
}
