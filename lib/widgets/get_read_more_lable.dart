import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class GetReadMoreLable extends StatelessWidget {
  GetReadMoreLable(
      {Key? key,
      required this.text,
      this.moreStyle = const TextStyle(fontSize: 14, color: Colors.blue),
      this.textStyle = const TextStyle(fontSize: 14, color: Colors.white),
      this.maxLength = 20,
      this.moreText = 'read more',
      this.onTap})
      : super(key: key);
  final String text;
  final void Function()? onTap;
  final int maxLength;
  final String? moreText;
  final TextStyle? textStyle;
  final TextStyle? moreStyle;

  @override
  Widget build(BuildContext context) => RichText(
        text: TextSpan(
          children: [
            TextSpan(
              text: text.length > maxLength
                  ? '${text.substring(0, maxLength)}...'
                  : text,
              style: textStyle,
            ),
            if (text.length > maxLength)
              TextSpan(
                text: moreText,
                recognizer: TapGestureRecognizer()
                  ..onTap = () {
                    onTap!();
                  },
                style: moreStyle,
              ),
          ],
        ),
      );
}