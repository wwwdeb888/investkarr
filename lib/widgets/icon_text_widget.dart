import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

/// A widget to show an icon with text.
///
/// [isCorrect] : show selected icon color if true.
/// [message] : message need to be shown with the icon.
class IconTextWidget extends StatelessWidget {
  IconTextWidget({
    Key? key,
    this.isCorrect = false,
    required this.iconTextStyle,
    required this.message,
  }) : super(key: key);

  final bool isCorrect;
  final TextStyle iconTextStyle;
  final String message;

  final ButtonStyle flatButtonStyle = TextButton.styleFrom(
    padding: EdgeInsets.zero,
  );

  @override
  Widget build(BuildContext context) => Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Dimens.hundred),
            color:
                isCorrect ? const Color(0xffF1EDFD) : const Color(0xffFFF5F5)),
        child: Padding(
          padding: Dimens.edgeInsets5_0_5_0,
          child: TextButton.icon(
            style: flatButtonStyle,
            onPressed: null,
            icon: Icon(
              isCorrect ? Icons.done : Icons.close,
              color: isCorrect ? ColorsValue.primaryColor : Colors.redAccent,
            ),
            label: Text(
              message,
              style: isCorrect ? Styles.bold12Primary : iconTextStyle,
              maxLines: 1,
            ),
          ),
        ),
      );
}
