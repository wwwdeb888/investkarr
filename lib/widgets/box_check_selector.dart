import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class BoxCheckSelector extends StatelessWidget {
  BoxCheckSelector({
    Key? key,
    this.isChecked = false,
    required this.onTap,
    required this.title,
    this.height,
    this.width,
  }) : super(key: key);

  final bool? isChecked;
  final String? title;
  final double? width;
  final double? height;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () {
          onTap!();
        },
        child: Container(
          width: width,
          height: height,
          child: Stack(
            children: [
              Align(
                alignment: Alignment.center,
                child: Text(
                  title!,
                  style:
                      isChecked! ? Styles.bold14Primary : Styles.medium14Black,
                ),
              ),
              Align(
                alignment: Alignment.topRight,
                child: Padding(
                  padding: Dimens.edgeInsets5,
                  child: Visibility(
                    visible: isChecked! ? true : false,
                    child: Container(
                      width: Dimens.twelve,
                      height: Dimens.twelve,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(Dimens.ten),
                        color: ColorsValue.primaryColor,
                      ),
                      child: Icon(
                        Icons.done,
                        color: Colors.white,
                        size: Dimens.ten,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(Dimens.ten),
              color: isChecked!
                  ? const Color(0xffF5F1FE)
                  : ColorsValue.backgroundColor,
              border: Border.all(
                  color: isChecked! ? ColorsValue.primaryColor : Colors.grey)),
        ),
      );
}
