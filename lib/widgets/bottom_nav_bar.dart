import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

/// Bottom navigation bar widget
class BottomNavBar extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (controller) => BottomNavigationBar(
          selectedLabelStyle: Styles.primaryBold13,
          unselectedLabelStyle: Styles.bottomNav13,
          selectedItemColor: ColorsValue.primaryColor,
          onTap: controller.onItemTapped,
          currentIndex: controller.selectedIndex,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Padding(
                padding: Dimens.edgeInsets5,
                child: Image.asset(
                  controller.selectedIndex != 0
                      ? AssetConstants.greyExplore
                      : AssetConstants.explore,
                  width: Dimens.twentyFive,
                  height: Dimens.twentyFive,
                  color: controller.selectedIndex == 0
                      ? null
                      : const Color(0xffA99DBE),
                ),
              ),
              label: StringConstants.explore,
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: Dimens.edgeInsets5,
                child: Image.asset(
                  AssetConstants.dashboard,
                  width: Dimens.twentyFive,
                  height: Dimens.twentyFive,
                  color: controller.selectedIndex == 1
                      ? ColorsValue.primaryColor
                      : null,
                ),
              ),
              label: StringConstants.dashboard,
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: Dimens.edgeInsets5,
                child: Image.asset(
                  AssetConstants.profile,
                  width: Dimens.twentyFive,
                  height: Dimens.twentyFive,
                  color: controller.selectedIndex == 2
                      ? ColorsValue.primaryColor
                      : null,
                ),
              ),
              label: StringConstants.profile,
            ),
          ],
        ),
      );
}
