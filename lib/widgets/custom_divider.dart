import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class CustomDivider extends StatelessWidget {
  const CustomDivider({
    this.opacity,
    Key? key,
  }) : super(key: key);
  final double? opacity;

  @override
  Widget build(BuildContext context) => Container(
        width: Dimens.percentWidth(1),
        height: 0.3,
        decoration: BoxDecoration(
          color: ColorsValue.greyColor.withOpacity(opacity ?? 0.5),
        ),
      );
}
