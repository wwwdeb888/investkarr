import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class SplashBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(SplashController.new);
  }
}
