import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class SplashView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => GetBuilder<SplashController>(
      builder: (_controller) => Scaffold(
            backgroundColor: Colors.white,
            body: Stack(
              children: [
                Center(
                  child: Container(
                    width: Dimens.percentWidth(1),
                    height: Dimens.percentHeight(1),
                    decoration: const BoxDecoration(
                        gradient: LinearGradient(
                      colors: [
                        Colors.white,
                        Color(0xffF2EBFF),
                        Color(0xffFAF8FE),
                      ],
                      stops: [0.27, 0.63, 114.5],
                      begin: FractionalOffset.topCenter,
                      end: FractionalOffset.bottomCenter,
                    )),
                  ),
                ),
                Center(
                  child: Image.asset(
                    AssetConstants.splashLogo1,
                    width: Dimens.sixty,
                    height: Dimens.sixty,
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: Dimens.percentWidth(.7),
                        height: Dimens.percentHeight(.2),
                        child: Image.asset(
                          AssetConstants.splashLogo2,
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ));
}
