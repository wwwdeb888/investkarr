import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class SignUpWithEmailUserName extends StatelessWidget {
  const SignUpWithEmailUserName({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: ColorfulSafeArea(
          color: ColorsValue.backgroundColor,
          child: GetBuilder<SignUpController>(
            builder: (_controller) => Stack(
              children: [
                Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            SizedBox(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              child: IconButton(
                                constraints: const BoxConstraints(),
                                padding: EdgeInsets.zero,
                                onPressed: () {
                                  Get.back<dynamic>();
                                },
                                icon: const Icon(
                                  Icons.arrow_back_ios,
                                  color: ColorsValue.primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Image.asset(
                          AssetConstants.splashLogo1,
                          width: Dimens.thirtyTwo,
                          height: Dimens.thirtyTwo,
                        ),
                        Row(
                          children: [
                            Text(
                              '2/6',
                              style: Styles.bold14Primary,
                            ),
                            Dimens.boxWidth20,
                          ],
                        )
                      ],
                    ),
                    Dimens.boxHeight10,
                    const LinearProgressIndicator(
                      backgroundColor: Color(0xffEFE8FC),
                      valueColor: AlwaysStoppedAnimation<Color>(
                        ColorsValue.primaryColor,
                      ),
                      value: 0.14,
                    ),
                  ],
                ),
                Padding(
                  padding: Dimens.edgeInsets20,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Dimens.boxHeight80,
                      Text(
                        StringConstants.whatDoWeCallYou,
                        style: Styles.bold24Black,
                      ),
                      Dimens.boxHeight15,
                      Text(
                        StringConstants.nameMatches,
                        style: Styles.textFieldHintColor14,
                        textAlign: TextAlign.center,
                      ),
                      Dimens.boxHeight50,
                      FormFieldWidget(
                        elevation: 0,
                        autoFocus: true,
                        isFilled: true,
                        textInputAction: TextInputAction.next,
                        textCapitalization: TextCapitalization.words,
                        textInputType: TextInputType.text,
                        borderColor: ColorsValue.formFieldBorderColor,
                        fillColor: ColorsValue.backgroundColor,
                        hintText: StringConstants.firstName,
                        hintStyle: Styles.textFieldHintColor16,
                        contentPadding: Dimens.edgeInsets10,
                        onChange: (String v) {
                          _controller.firstName = v;
                          _controller.checkIfNamesAreValid(v);
                        },
                      ),
                      Dimens.boxHeight40,
                      FormFieldWidget(
                        elevation: 0,
                        autoFocus: true,
                        isFilled: true,
                        textCapitalization: TextCapitalization.words,
                        textInputType: TextInputType.text,
                        borderColor: ColorsValue.formFieldBorderColor,
                        fillColor: ColorsValue.backgroundColor,
                        hintText: StringConstants.lastName,
                        hintStyle: Styles.textFieldHintColor16,
                        contentPadding: Dimens.edgeInsets10,
                        onChange: (String v) {
                          _controller.lastName = v;
                          _controller.checkIfNamesAreValid(v);
                        },
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: Dimens.edgeInsets20,
                    child: SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) =>
                                _controller.isNamesValid
                                    ? ColorsValue.primaryColor
                                    : ColorsValue.primaryColor.withOpacity(.5),
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16White,
                          ),
                        ),
                        onPressed: _controller.isNamesValid
                            ? RouteManagement.goToSignUpWithEmailCreatePassword
                            : null,
                        child: Text(
                          StringConstants.next,
                          style: Styles.bold16White,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
