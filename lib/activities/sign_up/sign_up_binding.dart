import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class SignUpBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(SignUpController.new);
  }
}
