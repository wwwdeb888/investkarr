import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class SignUpWithEmailCreatePassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: ColorfulSafeArea(
          color: ColorsValue.backgroundColor,
          child: GetBuilder<SignUpController>(
            builder: (_controller) => Stack(
              children: [
                Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            SizedBox(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              child: IconButton(
                                constraints: const BoxConstraints(),
                                padding: EdgeInsets.zero,
                                onPressed: () {
                                  Get.back<dynamic>();
                                },
                                icon: const Icon(
                                  Icons.arrow_back_ios,
                                  color: ColorsValue.primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Image.asset(
                          AssetConstants.splashLogo1,
                          width: Dimens.thirtyTwo,
                          height: Dimens.thirtyTwo,
                        ),
                        Row(
                          children: [
                            Text(
                              '3/6',
                              style: Styles.bold14Primary,
                            ),
                            Dimens.boxWidth20,
                          ],
                        )
                      ],
                    ),
                    Dimens.boxHeight10,
                    const LinearProgressIndicator(
                      backgroundColor: Color(0xffEFE8FC),
                      valueColor: AlwaysStoppedAnimation<Color>(
                        ColorsValue.primaryColor,
                      ),
                      value: 0.28,
                    ),
                  ],
                ),
                Padding(
                  padding: Dimens.edgeInsets20,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Dimens.boxHeight80,
                      Text(
                        StringConstants.setAPassword,
                        style: Styles.bold24Black,
                      ),
                      Dimens.boxHeight15,
                      RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(
                            text: StringConstants.toAccessYourAccount,
                            style: Styles.textFieldHintColor14,
                            children: [
                              TextSpan(
                                  text: ' yourusername@domain.com ',
                                  style: Styles.medium14Black),
                              TextSpan(
                                  text: StringConstants.edit.toUpperCase(),
                                  style: Styles.medium12PrimaryUnderline)
                            ]),
                      ),
                      Dimens.boxHeight50,
                      FormFieldWidget(
                        elevation: 0,
                        isObscureText: !_controller.isPasswordVisible,
                        obscureCharacter: '*',
                        autoFocus: true,
                        isFilled: true,
                        textInputAction: TextInputAction.done,
                        textInputType: TextInputType.text,
                        borderColor: ColorsValue.formFieldBorderColor,
                        fillColor: ColorsValue.backgroundColor,
                        hintText: StringConstants.enterPassword,
                        hintStyle: Styles.textFieldHintColor16,
                        contentPadding: Dimens.edgeInsets10,
                        onChange: (String v) {
                          _controller.checkIfPasswordIsValid(v);
                        },
                        // errorText: _controller.passwordErrors,
                        suffixIcon: IconButton(
                          onPressed: _controller.updatePasswordVisibility,
                          icon: _controller.isPasswordVisible
                              ? Image.asset(
                                  AssetConstants.passwordVisible,
                                  width: Dimens.twenty,
                                  height: Dimens.twenty,
                                )
                              : Image.asset(
                                  AssetConstants.passwordHidden,
                                  width: Dimens.twenty,
                                  height: Dimens.twenty,
                                ),
                        ),
                      ),
                      Dimens.boxHeight10,
                      _controller.isPasswordValid
                          ? Dimens.box0
                          : PassWordErrorWidget(
                              error: _controller.passwordErrors,
                              iconTextStyle: Styles.red13,
                            ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: Dimens.edgeInsets20,
                    child: SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) =>
                                _controller.isPasswordValid
                                    ? ColorsValue.primaryColor
                                    : ColorsValue.primaryColor.withOpacity(.5),
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16White,
                          ),
                        ),
                        onPressed: _controller.isPasswordValid
                            ? RouteManagement.goToSignUpWithEmailPhoneNumber
                            : null,
                        child: Text(
                          StringConstants.confirm,
                          style: Styles.bold16White,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
