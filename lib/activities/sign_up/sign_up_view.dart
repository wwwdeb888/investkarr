import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';
import 'package:lottie/lottie.dart';

class SignUpView extends StatelessWidget {
  const SignUpView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Stack(
          children: [
            Container(
              width: Dimens.percentWidth(1),
              height: Dimens.percentHeight(1),
              decoration: const BoxDecoration(
                  gradient: LinearGradient(
                colors: [
                  Colors.white,
                  Color(0xffF2EBFF),
                  Color(0xffFAF8FE),
                ],
                stops: [0.20, 0.60, 114.5],
                begin: FractionalOffset.topCenter,
                end: FractionalOffset.bottomCenter,
              )),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: Stack(
                children: [
                  Lottie.asset(AssetConstants.signUpLottie),
                  Container(
                    width: Dimens.percentWidth(1),
                    height: Dimens.percentHeight(.5),
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          colors: [
                            const Color(0xffF2EBFF).withOpacity(.1),
                            const Color(0xffFAF8FE).withOpacity(1)
                          ]),
                      // color: Colors.white.withOpacity(.6),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: Dimens.edgeInsets20,
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Dimens.boxHeight20,
                    Image.asset(
                      AssetConstants.splashLogo1,
                      width: Dimens.fourtyEight,
                      height: Dimens.fourtyEight,
                    ),
                    Dimens.boxHeight10,
                    SizedBox(
                      width: Dimens.percentWidth(.7),
                      child: Text(
                        StringConstants.smarterWay,
                        style: Styles.bold24Black,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Dimens.boxHeight30,
                    SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) => Colors.white,
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16Black,
                          ),
                        ),
                        onPressed:
                            RouteManagement.goToSignUpWithEmailPhoneNumber,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Image.asset(
                              AssetConstants.google,
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                            ),
                            Text(
                              StringConstants.signUpWithGoogle,
                              style: Styles.bold16Black,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Dimens.boxHeight16,
                    SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) => Colors.black,
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16White,
                          ),
                        ),
                        onPressed:
                            RouteManagement.goToSignUpWithEmailPhoneNumber,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Image.asset(
                              AssetConstants.apple,
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                            ),
                            Text(
                              StringConstants.signUpWithApple,
                              style: Styles.bold16White,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Dimens.boxHeight16,
                    SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) =>
                                ColorsValue.primaryColor,
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16White,
                          ),
                        ),
                        onPressed: RouteManagement.goToSignUpWithEmail,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Image.asset(
                              AssetConstants.mail,
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                            ),
                            Text(
                              StringConstants.signUpWithEmail,
                              style: Styles.bold16White,
                            ),
                          ],
                        ),
                      ),
                    ),
                    Dimens.boxHeight16,
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: Dimens.edgeInsets30,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      StringConstants.alreadyHaveAnAccount,
                      style: Styles.bold12Black,
                    ),
                    GestureDetector(
                      onTap: RouteManagement.goToOffSignIn,
                      child: Text(
                        StringConstants.login,
                        style: Styles.bold12Primary,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
}
