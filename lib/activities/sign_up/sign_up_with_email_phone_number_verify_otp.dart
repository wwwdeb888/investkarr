import 'dart:async';

import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:pinput/pin_put/pin_put.dart';

class SignUpWithEmailPhoneNumberVerifyOTP extends StatefulWidget {
  const SignUpWithEmailPhoneNumberVerifyOTP({Key? key}) : super(key: key);

  @override
  State<SignUpWithEmailPhoneNumberVerifyOTP> createState() =>
      _SignUpWithEmailPhoneNumberVerifyOTPState();
}

class _SignUpWithEmailPhoneNumberVerifyOTPState
    extends State<SignUpWithEmailPhoneNumberVerifyOTP> {
  @override
  void initState() {
    counter = 50;
    startTimer();
    super.initState();
  }

  /// expiry time
  int? counter = 60;

  /// method to start timer
  void startTimer() {
    Timer.periodic(
      const Duration(seconds: 1),
      (Timer timer) {
        setState(() {
          if (counter != 0) {
            counter = counter! - 1;
            if (counter == 0) {
              timer.cancel();
            }
          }
        });

        setState(() {});
      },
    );
  }

  /// method to restart time
  void restartTimer() {
    setState(() {
      counter = 50;
    });
    startTimer();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<SignUpController>(
          builder: (_controller) => ColorfulSafeArea(
            color: ColorsValue.backgroundColor,
            child: Stack(
              children: [
                Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            SizedBox(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              child: IconButton(
                                constraints: const BoxConstraints(),
                                padding: EdgeInsets.zero,
                                onPressed: () {
                                  Get.back<dynamic>();
                                },
                                icon: const Icon(
                                  Icons.arrow_back_ios,
                                  color: ColorsValue.primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Image.asset(
                          AssetConstants.splashLogo1,
                          width: Dimens.thirtyTwo,
                          height: Dimens.thirtyTwo,
                        ),
                        Row(
                          children: [
                            Text(
                              '4/6',
                              style: Styles.bold14Primary,
                            ),
                            Dimens.boxWidth20,
                          ],
                        )
                      ],
                    ),
                    Dimens.boxHeight10,
                    const LinearProgressIndicator(
                      backgroundColor: Color(0xffEFE8FC),
                      valueColor: AlwaysStoppedAnimation<Color>(
                        ColorsValue.primaryColor,
                      ),
                      value: 0.35,
                    ),
                  ],
                ),
                Padding(
                  padding: Dimens.edgeInsets20,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Dimens.boxHeight80,
                      Text(
                        StringConstants.verifyWithOTP,
                        style: Styles.bold24Black,
                      ),
                      Dimens.boxHeight15,
                      SizedBox(
                        width: Dimens.percentWidth(.7),
                        child: Text(
                          StringConstants.waiting,
                          style: Styles.textFieldHintColor14,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Dimens.boxHeight5,
                      Text(
                        '+91 ${_controller.phoneNumber.substring(0, 5)}${_controller.phoneNumber.substring(5, 10)}',
                        style: Styles.black15,
                        textAlign: TextAlign.center,
                      ),
                      Dimens.boxHeight50,
                      PinPut(
                        autofocus: true,
                        // eachFieldMargin: Dimens.edgeInsets0_0_20_0,
                        mainAxisSize: MainAxisSize.max,
                        fieldsCount: 6,
                        controller: _controller.pinPutController2,
                        enabled: true,
                        onSubmit: (_) {
                          // controller.onVerifiy();
                        },
                        onChanged: _controller.onOTPChanged2,
                        eachFieldHeight: Dimens.fourty + Dimens.five,
                        eachFieldWidth: Dimens.thirty + Dimens.five,
                        textStyle: Styles.boldBlack22,
                        submittedFieldDecoration: Styles.pinPutDecoration,
                        selectedFieldDecoration: Styles.enabledPinPutDecoration,
                        followingFieldDecoration: Styles.pinPutDecoration,
                      ),
                      Dimens.boxHeight32,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              if (counter != 0) {
                              } else {
                                restartTimer();
                              }
                            },
                            child: Text(
                              StringConstants.resendOTP,
                              style: counter != 0
                                  ? Styles.bold12Black
                                  : Styles.bold12PrimaryUnderline,
                            ),
                          ),
                          if (counter != 0) Dimens.boxWidth8,
                          if (counter != 0)
                            Text(
                              '\00:${counter.toString().padLeft(2, '0')}',
                              style: Styles.bold12Primary,
                            ),
                        ],
                      )
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: Dimens.edgeInsets20,
                    child: SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) =>
                                _controller.isEmailOTPValid
                                    ? ColorsValue.primaryColor
                                    : ColorsValue.primaryColor.withOpacity(.5),
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16White,
                          ),
                        ),
                        onPressed: () {
                          _controller.isEmailOTPValid
                              ? _controller.verifyPhoneOTP(context)
                              : null;
                        },
                        child: Text(
                          StringConstants.verify,
                          style: Styles.bold16White,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
