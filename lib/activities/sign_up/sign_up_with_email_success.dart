import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:lottie/lottie.dart';

class SignUpWithEmailSuccess extends StatefulWidget {
  const SignUpWithEmailSuccess({Key? key}) : super(key: key);

  @override
  State<SignUpWithEmailSuccess> createState() => _SignUpWithEmailSuccessState();
}

class _SignUpWithEmailSuccessState extends State<SignUpWithEmailSuccess> {
  @override
  void initState() {
    getData();
    super.initState();
  }

  void getData() async {
    Future.delayed(const Duration(seconds: 3), () async {
      await Get.bottomSheet<dynamic>(
        Container(
          height: Dimens.percentHeight(.45),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(
                Dimens.twenty,
              ),
              topRight: Radius.circular(
                Dimens.twenty,
              ),
            ),
          ),
          child: Padding(
            padding: Dimens.edgeInsets10,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Dimens.boxHeight20,
                Image.asset(
                  AssetConstants.doubleProtection,
                  width: Dimens.eighty,
                  height: Dimens.eighty,
                ),
                Dimens.boxHeight20,
                Text(
                  StringConstants.doubleProtection,
                  style: Styles.bold24Black,
                ),
                Dimens.boxHeight10,
                Text(
                  StringConstants.phoneLockEnabled,
                  style: Styles.grey15,
                  textAlign: TextAlign.center,
                ),
                Dimens.boxHeight30,
                SizedBox(
                  width: Dimens.percentWidth(1),
                  height: Dimens.percentHeight(.07),
                  child: ElevatedButton(
                    style: Styles.elevatedButtonTheme.style!.copyWith(
                      backgroundColor: MaterialStateProperty.resolveWith<Color>(
                        (Set<MaterialState> states) => ColorsValue.primaryColor,
                      ),
                      textStyle: MaterialStateProperty.all(
                        Styles.bold16White,
                      ),
                    ),
                    onPressed: RouteManagement.goToOffAllKyc,
                    child: Text(
                      StringConstants.done,
                      style: Styles.bold16White,
                    ),
                  ),
                ),
                Dimens.boxHeight10,
                Text(
                  StringConstants.canBeChanged,
                  style: Styles.grey12,
                )
              ],
            ),
          ),
        ),
        isDismissible: false,
      );
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: ColorfulSafeArea(
          color: ColorsValue.backgroundColor,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Align(
                alignment: Alignment.center,
                child: Lottie.asset(
                  AssetConstants.signUpSuccess,
                ),
              ),
              Container(
                transform: Matrix4.translationValues(0, -Dimens.fifty, 0),
                child: Text(
                  StringConstants.appPinSuccess,
                  style: Styles.boldBlack18,
                ),
              )
            ],
          ),
        ),
      );
}
