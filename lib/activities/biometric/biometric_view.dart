import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:investkarr/lib.dart';
import 'package:local_auth/local_auth.dart';
import 'package:pinput/pin_put/pin_put.dart';

class BioMetricScreen extends StatefulWidget {
  @override
  _BioMetricScreenState createState() => _BioMetricScreenState();
}

class _BioMetricScreenState extends State<BioMetricScreen> {
  final LocalAuthentication auth = LocalAuthentication();
  dynamic authorized = 'Not Authorized';
  bool isAuthenticating = false;
  var apiClient = ApiClient();
  dynamic email;
  dynamic name;
  bool isOTPValid = false;
  final pinPutController2 = TextEditingController();

  void onOTPChanged2(String value) {
    if (pinPutController2.text.length == 6) {
      isOTPValid = true;
    } else {
      isOTPValid = false;
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    getData();
    _authenticate();
  }

  Future<void> verifyPin() async {
    var token = await GetStorage('appData').read('token');
    var res = await apiClient.loginWithPin(
        pin: pinPutController2.text, isLoading: true, token: token.toString());
    if (!res.hasError) {
      print(res.data);
      var response = signUpResponseFromJson(res.data);
      if (response.statusCode == 200 && response.success == true) {
        await GetStorage('appData')
            .write('token', response.result!.accessToken);
        await GetStorage('appData')
            .write('email', response.result!.user!.email);
        await GetStorage('appData').write('name',
            '${response.result!.user!.firstName} ${response.result!.user!.lastName}');
        RouteManagement.goToOffAllKyc();
      }
    } else {
      Get.snackbar('Error', 'Invalid Pin');
    }
  }

  void getData() async {
    email = await GetStorage('appData').read('email');
    name = await GetStorage('appData').read('name');
    setState(() {});
  }

  Future<void> _authenticate() async {
    var authenticated = false;
    try {
      setState(() {
        isAuthenticating = true;
        authorized = 'Authenticating';
      });
      authenticated = await auth.authenticate(
        biometricOnly: true,
        localizedReason: 'Login to Investkarr',
        useErrorDialogs: true,
        stickyAuth: true,
      );
      setState(() {
        if (authenticated) {
          RouteManagement.goToHome();
        }
      });
    } on PlatformException catch (e) {
      setState(() => authorized = e.message.toString());
    } finally {
      setState(() => isAuthenticating = false);
    }
  }

  @override
  Widget build(BuildContext context) => MaterialApp(
        home: Scaffold(
          backgroundColor: ColorsValue.backgroundColor,
          body: ColorfulSafeArea(
            color: ColorsValue.backgroundColor,
            child: Column(
              children: [
                Dimens.boxHeight5,
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Dimens.boxWidth20,
                        SizedBox(
                          width: Dimens.twenty,
                          height: Dimens.twenty,
                        ),
                      ],
                    ),
                    Image.asset(
                      AssetConstants.splashLogo1,
                      width: Dimens.thirtyTwo,
                      height: Dimens.thirtyTwo,
                    ),
                    Row(
                      children: [
                        Dimens.boxWidth20,
                        Container(
                          width: Dimens.twenty,
                          height: Dimens.twenty,
                          color: Colors.transparent,
                        ),
                      ],
                    ),
                  ],
                ),
                Dimens.boxHeight100,
                Column(
                  children: [
                    Padding(
                      padding: Dimens.edgeInsets20,
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              width: Dimens.fourtyEight,
                              height: Dimens.fourtyEight,
                              decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.circular(Dimens.twentyFive),
                                  color: ColorsValue.secondaryColor),
                              child: Center(
                                child: Text(
                                  '${name.toString().split(' ')[0][0]}${name.toString().split(' ')[1][0]}',
                                  style: Styles.bold18Black,
                                ),
                              ),
                            ),
                            Dimens.boxHeight10,
                            Text(
                              'Hi $name',
                              style: Styles.boldBlack18,
                            ),
                            Dimens.boxHeight10,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  '$email',
                                  style: Styles.black15,
                                ),
                                Dimens.boxWidth10,
                                GestureDetector(
                                  onTap: () async {
                                    await GetStorage('appData').erase();
                                    RouteManagement.goToLoginOrRegister();
                                  },
                                  child: Text(
                                    'Logout',
                                    style: Styles.bold12GreyUnderline,
                                  ),
                                ),
                              ],
                            ),
                            Dimens.boxHeight20,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                SizedBox(
                                  width: Dimens.percentWidth(.5),
                                  child: PinPut(
                                    autofocus: false,
                                    mainAxisSize: MainAxisSize.max,
                                    fieldsCount: 4,
                                    controller: pinPutController2,
                                    enabled: true,
                                    onSubmit: (_) {
                                      verifyPin();
                                    },
                                    onChanged: onOTPChanged2,
                                    eachFieldHeight:
                                        Dimens.fourty + Dimens.fifteen,
                                    textStyle: Styles.boldBlack22,
                                    submittedFieldDecoration:
                                        Styles.pinPutDecoration,
                                    selectedFieldDecoration:
                                        Styles.enabledPinPutDecoration,
                                    followingFieldDecoration:
                                        Styles.pinPutDecoration,
                                  ),
                                ),
                              ],
                            ),
                            Dimens.boxHeight40,
                            GestureDetector(
                              onTap: RouteManagement.goToForgotPinOrPasswordPAN,
                              child: Text(
                                StringConstants.forgotAppPin,
                                style: Styles.bold14Grey,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      );
}
