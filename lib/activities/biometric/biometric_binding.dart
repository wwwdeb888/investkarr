import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class BiometricBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(BioMetricController.new);
  }
}
