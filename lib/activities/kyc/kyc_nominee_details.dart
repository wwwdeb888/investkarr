import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class KycNomineeDetails extends StatelessWidget {
  const KycNomineeDetails({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<KycController>(
          builder: (_controller) => ColorfulSafeArea(
            color: ColorsValue.backgroundColor,
            child: Column(
              children: [
                Dimens.boxHeight10,
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Dimens.boxWidth20,
                        SizedBox(
                          width: Dimens.twenty,
                          height: Dimens.twenty,
                          child: IconButton(
                            constraints: const BoxConstraints(),
                            padding: EdgeInsets.zero,
                            onPressed: () {
                              Get.back<dynamic>();
                            },
                            icon: Icon(
                              Icons.arrow_back_ios,
                              size: Dimens.eighteen,
                              color: ColorsValue.primaryColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          '2/4',
                          style: Styles.bold14Primary,
                        ),
                        Dimens.boxWidth20,
                      ],
                    )
                  ],
                ),
                Dimens.boxHeight20,
                const LinearProgressIndicator(
                  backgroundColor: Color(0xffEFE8FC),
                  valueColor: AlwaysStoppedAnimation<Color>(
                    ColorsValue.primaryColor,
                  ),
                  value: 0.50,
                ),
                Expanded(
                  child: Padding(
                    padding: Dimens.edgeInsets20,
                    child: ListView(
                      children: [
                        Dimens.boxHeight10,
                        Row(
                          children: [
                            Text(
                              StringConstants.nomineeDetailsRequired,
                              style: Styles.boldBlack18,
                            ),
                          ],
                        ),
                        Dimens.boxHeight15,
                        FormFieldWidget(
                          elevation: 0,
                          autoFocus: true,
                          isFilled: true,
                          textInputAction: TextInputAction.next,
                          textCapitalization: TextCapitalization.words,
                          textInputType: TextInputType.text,
                          borderColor: ColorsValue.blackColor.withOpacity(0.5),
                          fillColor: ColorsValue.backgroundColor,
                          hintText: StringConstants.name,
                          hintStyle: Styles.textFieldHintColor16,
                          contentPadding: Dimens.edgeInsets10,
                          onChange: (String v) {
                            _controller.firstName = v;
                          },
                        ),
                        Dimens.boxHeight30,
                        SizedBox(
                          width: Dimens.percentWidth(.9),
                          child: DropdownButtonFormField(
                              isExpanded: true,
                              decoration: InputDecoration(
                                hintText: StringConstants.relationship,
                                hintStyle: Styles.textFieldHintColor16,
                                contentPadding: Dimens.edgeInsets10_0_0_0,
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color:
                                        ColorsValue.blackColor.withOpacity(0.5),
                                  ),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                    color:
                                        ColorsValue.blackColor.withOpacity(0.5),
                                  ),
                                ),
                              ),
                              value: _controller.selectedRelationship.value,
                              icon: const Icon(
                                Icons.arrow_drop_down,
                                color: ColorsValue.textColor,
                              ),
                              items: _controller.relationshipList,
                              onChanged: (value) {
                                _controller.selectedRelationship.value =
                                    value as RelationshipList?;
                                _controller.update();
                              }),
                        ),
                        Dimens.boxHeight20,
                        FormFieldWidget(
                          elevation: 0,
                          autoFocus: true,
                          isFilled: true,
                          textInputType: TextInputType.emailAddress,
                          borderColor: ColorsValue.blackColor.withOpacity(0.5),
                          fillColor: ColorsValue.backgroundColor,
                          hintText: StringConstants.enterEmailOrPhone,
                          hintStyle: Styles.textFieldHintColor16,
                          contentPadding: Dimens.edgeInsets10,
                          errorText: _controller.emailErrorText,
                          errorStyle: Styles.red13,
                          onChange: (String v) {
                            _controller.checkIfEmailIsValid(v);
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: Dimens.edgeInsets20_0_20_20,
                  child: Row(
                    children: [
                      SizedBox(
                        width: Dimens.twentyFive,
                        height: Dimens.twentyFive,
                        child: Checkbox(
                          value: _controller.checkBoxValue,
                          shape: const CircleBorder(),
                          side:
                              const BorderSide(color: ColorsValue.primaryColor),
                          checkColor: Colors.white,
                          activeColor: Colors.greenAccent,
                          onChanged: (value) {
                            _controller.checkBoxValue = value!;
                            _controller.update();
                          },
                        ),
                      ),
                      Dimens.boxWidth10,
                      Text(
                        StringConstants.sendNotificationToNominee,
                        style: Styles.secondary12,
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: Dimens.edgeInsets20_0_20_20,
                  child: SizedBox(
                    width: Dimens.percentWidth(1),
                    height: Dimens.percentHeight(.07),
                    child: ElevatedButton(
                      style: Styles.elevatedButtonTheme.style!.copyWith(
                        backgroundColor:
                            MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) =>
                              _controller.isEmailValid
                                  ? ColorsValue.primaryColor
                                  : ColorsValue.primaryColor.withOpacity(.5),
                        ),
                        textStyle: MaterialStateProperty.all(
                          Styles.bold16White,
                        ),
                      ),
                      onPressed: _controller.isEmailValid
                          ? RouteManagement.goToKycFatcaDeclaration
                          : null,
                      child: Text(
                        StringConstants.next,
                        style: Styles.bold16White,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
