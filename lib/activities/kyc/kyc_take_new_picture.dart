import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class KycTakeNewPicture extends StatelessWidget {
  const KycTakeNewPicture({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<KycController>(
          builder: (_controller) => ColorfulSafeArea(
            color: ColorsValue.backgroundColor,
            child: Column(
              children: [
                Dimens.boxHeight10,
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Dimens.boxWidth20,
                        SizedBox(
                          width: Dimens.twenty,
                          height: Dimens.twenty,
                          child: IconButton(
                            constraints: const BoxConstraints(),
                            padding: EdgeInsets.zero,
                            onPressed: () {
                              Get.back<dynamic>();
                            },
                            icon: Icon(
                              Icons.arrow_back_ios,
                              size: Dimens.eighteen,
                              color: ColorsValue.primaryColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          '4/4',
                          style: Styles.bold14Primary,
                        ),
                        Dimens.boxWidth20,
                      ],
                    )
                  ],
                ),
                Dimens.boxHeight20,
                const LinearProgressIndicator(
                  backgroundColor: Color(0xffEFE8FC),
                  valueColor: AlwaysStoppedAnimation<Color>(
                    ColorsValue.primaryColor,
                  ),
                  value: 0.88,
                ),
                Dimens.boxHeight10,
                Expanded(
                  child: Padding(
                    padding: Dimens.edgeInsets15,
                    child: ListView(
                      children: [
                        Container(
                          padding: Dimens.edgeInsets20_10_20_10,
                          width: Dimens.percentWidth(1),
                          height: Dimens.sixtyFour,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(Dimens.eight),
                            border: Border.all(
                              width: Dimens.one,
                              color: ColorsValue.redColor,
                            ),
                            color: ColorsValue.lightYellow,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              SizedBox(
                                width: Dimens.thirtyFour,
                                height: Dimens.thirtyFour,
                                child: Image.asset(
                                  AssetConstants.warning,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Dimens.boxWidth20,
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    StringConstants.pictureIsNotClear,
                                    style: Styles.bold12Black,
                                  ),
                                  Dimens.boxHeight5,
                                  Text(
                                    StringConstants
                                        .makeSureThereIsNoBlurOrGlare,
                                    style: Styles.black12,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Dimens.boxHeight30,
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: SizedBox(
                            width: double.infinity,
                            child: Image.asset(
                              AssetConstants.cancelledCheque,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Dimens.boxHeight10,
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: Dimens.edgeInsets20_0_20_20,
                  child: SizedBox(
                    width: Dimens.percentWidth(1),
                    height: Dimens.percentHeight(.07),
                    child: ElevatedButton(
                      style: Styles.elevatedButtonTheme.style!.copyWith(
                        backgroundColor:
                            MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) => 0 == 0
                              ? ColorsValue.primaryColor
                              : ColorsValue.primaryColor.withOpacity(.5),
                        ),
                        textStyle: MaterialStateProperty.all(
                          Styles.bold16White,
                        ),
                      ),
                      onPressed: () {},
                      child: Text(
                        StringConstants.takeNewPicture,
                        style: Styles.bold16White,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
