import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class KycScanCheque extends StatelessWidget {
  const KycScanCheque({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<KycController>(
          builder: (_controller) => ColorfulSafeArea(
            color: ColorsValue.backgroundColor,
            child: Column(
              children: [
                Dimens.boxHeight10,
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Dimens.boxWidth20,
                        SizedBox(
                          width: Dimens.twenty,
                          height: Dimens.twenty,
                          child: IconButton(
                            constraints: const BoxConstraints(),
                            padding: EdgeInsets.zero,
                            onPressed: () {
                              Get.back<dynamic>();
                            },
                            icon: Icon(
                              Icons.arrow_back_ios,
                              size: Dimens.eighteen,
                              color: ColorsValue.primaryColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          '4/4',
                          style: Styles.bold14Primary,
                        ),
                        Dimens.boxWidth20,
                      ],
                    )
                  ],
                ),
                Dimens.boxHeight20,
                const LinearProgressIndicator(
                  backgroundColor: Color(0xffEFE8FC),
                  valueColor: AlwaysStoppedAnimation<Color>(
                    ColorsValue.primaryColor,
                  ),
                  value: 0.88,
                ),
                Dimens.boxHeight10,
                Expanded(
                  child: Padding(
                    padding: Dimens.edgeInsets15,
                    child: ListView(
                      children: [
                        Row(
                          children: [
                            Text(
                              StringConstants.scanCheque,
                              style: Styles.boldBlack18,
                            ),
                          ],
                        ),
                        Dimens.boxHeight20,
                        Text(
                          StringConstants.yourbankCancelledChequeYouUpload,
                          style: Styles.black12,
                        ),
                        Dimens.boxHeight30,
                        Align(
                          alignment: Alignment.bottomLeft,
                          child: SizedBox(
                            width: double.infinity,
                            child: Image.asset(
                              AssetConstants.cancelledCheque,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Dimens.boxHeight30,
                        Container(
                          padding: Dimens.edgeInsets10,
                          width: Dimens.percentWidth(1),
                          height: Dimens.fiftyFive,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(Dimens.eight),
                            color: ColorsValue.lightPink,
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Icon(
                                Icons.info_outline_rounded,
                                size: Dimens.fifteen,
                              ),
                              Dimens.boxWidth10,
                              Expanded(
                                child: Text(
                                  StringConstants
                                      .pleaseMakeSureIsClearlyVisible,
                                  style: Styles.black12,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Dimens.boxHeight10,
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: Dimens.edgeInsets20_0_20_20,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: SizedBox(
                          width: Dimens.percentWidth(1),
                          height: Dimens.percentHeight(.07),
                          child: ElevatedButton(
                            style: Styles.elevatedButtonTheme.style!.copyWith(
                              backgroundColor:
                                  MaterialStateProperty.resolveWith<Color>(
                                (Set<MaterialState> states) =>
                                    ColorsValue.backgroundColor,
                              ),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(Dimens.thirty),
                                      side: BorderSide(
                                          width: Dimens.two,
                                          color: ColorsValue.primaryColor))),
                              elevation: MaterialStateProperty.all(Dimens.zero),
                              textStyle: MaterialStateProperty.all(
                                Styles.bold16White,
                              ),
                            ),
                            onPressed: RouteManagement.goToKycTakeNewPicture,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(AssetConstants.gallery),
                                Dimens.boxWidth10,
                                Text(
                                  StringConstants.gallery,
                                  style: Styles.bold16White.copyWith(
                                      color: ColorsValue.primaryColor),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Dimens.boxWidth15,
                      Expanded(
                        child: SizedBox(
                          width: Dimens.percentWidth(1),
                          height: Dimens.percentHeight(.07),
                          child: ElevatedButton(
                            style: Styles.elevatedButtonTheme.style!.copyWith(
                              backgroundColor:
                                  MaterialStateProperty.resolveWith<Color>(
                                (Set<MaterialState> states) =>
                                    ColorsValue.backgroundColor,
                              ),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(Dimens.thirty),
                                      side: BorderSide(
                                          width: Dimens.two,
                                          color: ColorsValue.primaryColor))),
                              elevation: MaterialStateProperty.all(Dimens.zero),
                              textStyle: MaterialStateProperty.all(
                                Styles.bold16White,
                              ),
                            ),
                            onPressed: RouteManagement.goToKycTakeNewPicture,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(AssetConstants.camera),
                                Dimens.boxWidth10,
                                Text(
                                  StringConstants.camera,
                                  style: Styles.bold16White.copyWith(
                                      color: ColorsValue.primaryColor),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
