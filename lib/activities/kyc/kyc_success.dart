import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';
import 'package:lottie/lottie.dart';

class KYCSuccess extends StatefulWidget {
  const KYCSuccess({Key? key}) : super(key: key);

  @override
  State<KYCSuccess> createState() => _KYCSuccessState();
}

class _KYCSuccessState extends State<KYCSuccess> {

  @override
  void initState() {
    getData();
    super.initState();
  }

  void getData() async {
    Future.delayed(const Duration(seconds: 3), () async {
      RouteManagement.goToHome();
    });
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: ColorfulSafeArea(
          color: ColorsValue.backgroundColor,
          child: Stack(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Lottie.asset(
                    AssetConstants.signUpSuccess,
                  ),
                  Text(
                    StringConstants.bankIsVerified,
                    style: Styles.boldBlack18,
                  ),
                  Text(
                    StringConstants.allSet,
                    style: Styles.black15,
                  ),
                ],
              ),
            ],
          ),
        ),
      );
}
