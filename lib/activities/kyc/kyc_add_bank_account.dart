import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class KycAddBankAccount extends StatelessWidget {
  const KycAddBankAccount({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<KycController>(
          builder: (_controller) => ColorfulSafeArea(
            color: ColorsValue.backgroundColor,
            child: Column(
              children: [
                Dimens.boxHeight10,
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Dimens.boxWidth20,
                        SizedBox(
                          width: Dimens.twenty,
                          height: Dimens.twenty,
                          child: IconButton(
                            constraints: const BoxConstraints(),
                            padding: EdgeInsets.zero,
                            onPressed: () {
                              if (_controller.pageNumber == 1) {
                                Get.back<dynamic>();
                              } else if (_controller.pageNumber == 2) {
                                _controller.pageNumber = 1;
                              } else if (_controller.pageNumber == 3) {
                                _controller.pageNumber = 2;
                              } else if (_controller.pageNumber == 4) {
                                _controller.pageNumber = 3;
                              }
                              _controller.update();
                            },
                            icon: Icon(
                              Icons.arrow_back_ios,
                              size: Dimens.eighteen,
                              color: ColorsValue.primaryColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          '4/4',
                          style: Styles.bold14Primary,
                        ),
                        Dimens.boxWidth20,
                      ],
                    )
                  ],
                ),
                Dimens.boxHeight20,
                const LinearProgressIndicator(
                  backgroundColor: Color(0xffEFE8FC),
                  valueColor: AlwaysStoppedAnimation<Color>(
                    ColorsValue.primaryColor,
                  ),
                  value: 0.88,
                ),
                Dimens.boxHeight10,
                Expanded(
                  child: Padding(
                    padding: Dimens.edgeInsets20,
                    child: ListView(
                      children: [
                        Row(
                          children: [
                            Text(
                              _controller.pageNumber == 1
                                  ? StringConstants.addBankAccount
                                  : _controller.pageNumber == 2
                                      ? StringConstants.chooseYourBranch
                                      : _controller.pageNumber == 3 ||
                                              _controller.pageNumber == 4
                                          ? StringConstants.confirmBankDetails
                                          : '',
                              style: Styles.boldBlack18,
                            ),
                          ],
                        ),
                        if (_controller.pageNumber == 1 ||
                            _controller.pageNumber == 2 ||
                            _controller.pageNumber == 3 ||
                            _controller.pageNumber == 4)
                          Dimens.boxHeight20,
                        (_controller.pageNumber == 3 ||
                                _controller.pageNumber == 4)
                            ? Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Card(
                                    color: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(Dimens.thirty),
                                    ),
                                    child: Container(
                                      width: Dimens.percentWidth(1),
                                      constraints: const BoxConstraints(
                                          maxHeight: double.infinity),
                                      // height: _controller.pageNumber == 4
                                      //     ? Dimens.percentHeight(.32)
                                      //     : Dimens.percentHeight(.28),
                                      child: Padding(
                                        padding: Dimens.edgeInsets20,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Image.asset(
                                                  AssetConstants.hdfcLogo,
                                                  width: Dimens.thirty,
                                                  height: Dimens.thirty,
                                                ),
                                                Dimens.boxWidth10,
                                                Text(
                                                  'HDFC Bank',
                                                  style: Styles.medium16Black,
                                                ),
                                                const Spacer(),
                                                if (_controller.pageNumber == 4)
                                                  const Icon(
                                                    Icons.edit,
                                                    color: ColorsValue
                                                        .primaryColor,
                                                  ),
                                              ],
                                            ),
                                            Dimens.boxHeight10,
                                            const Divider(),
                                            Dimens.boxHeight10,
                                            _controller.pageNumber == 4
                                                ? Row(
                                                    children: [
                                                      SizedBox(
                                                        width:
                                                            Dimens.percentWidth(
                                                                .33),
                                                        child: Text(
                                                          '${StringConstants.accountNumber} :',
                                                          style: Styles
                                                              .regular14Grey,
                                                        ),
                                                      ),
                                                      SizedBox(
                                                        width:
                                                            Dimens.percentWidth(
                                                                .35),
                                                        child: Text(
                                                          '1234*********',
                                                          style: Styles
                                                              .medium14DarkGrey,
                                                        ),
                                                      ),
                                                    ],
                                                  )
                                                : const SizedBox(),
                                            Dimens.boxHeight10,
                                            Row(
                                              children: [
                                                SizedBox(
                                                  width:
                                                      Dimens.percentWidth(.33),
                                                  child: Text(
                                                    StringConstants.branchName,
                                                    style: Styles.regular14Grey,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width:
                                                      Dimens.percentWidth(.35),
                                                  child: Text(
                                                    'Avinashi',
                                                    style:
                                                        Styles.medium14DarkGrey,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Dimens.boxHeight10,
                                            Row(
                                              children: [
                                                SizedBox(
                                                  width:
                                                      Dimens.percentWidth(.33),
                                                  child: Text(
                                                    StringConstants.iFSCCode,
                                                    style: Styles.regular14Grey,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width:
                                                      Dimens.percentWidth(.35),
                                                  child: Text(
                                                    'HDFC000000',
                                                    style:
                                                        Styles.medium14DarkGrey,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Dimens.boxHeight10,
                                            Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(
                                                  width:
                                                      Dimens.percentWidth(.33),
                                                  child: Text(
                                                    StringConstants.address,
                                                    style: Styles.regular14Grey,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width:
                                                      Dimens.percentWidth(.4),
                                                  child: Text(
                                                    '2972 Westheimer Rd. Santa Ana, Illinois 85486 ',
                                                    style:
                                                        Styles.medium14DarkGrey,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Dimens.boxHeight25,
                                  if (_controller.pageNumber == 4)
                                    SizedBox(
                                      width: Dimens.percentWidth(1),
                                      height: Dimens.percentHeight(.07),
                                      child: ElevatedButton(
                                        style: Styles.elevatedButtonTheme.style!
                                            .copyWith(
                                          backgroundColor: MaterialStateProperty
                                              .resolveWith<Color>(
                                            (Set<MaterialState> states) =>
                                                ColorsValue.backgroundColor,
                                          ),
                                          shape: MaterialStateProperty.all<
                                                  RoundedRectangleBorder>(
                                              RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          Dimens.thirty),
                                                  side: BorderSide(
                                                      width: Dimens.one,
                                                      color: ColorsValue
                                                          .primaryColor))),
                                          elevation: MaterialStateProperty.all(
                                              Dimens.zero),
                                          textStyle: MaterialStateProperty.all(
                                            Styles.bold16White,
                                          ),
                                        ),
                                        onPressed: () {},
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              StringConstants.addAnotherAccount,
                                              style: Styles.bold16White
                                                  .copyWith(
                                                      color: ColorsValue
                                                          .primaryColor),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  if (_controller.pageNumber == 4)
                                    Dimens.boxHeight10,
                                  if (_controller.pageNumber == 4)
                                    Center(
                                      child: Text(
                                        StringConstants.youCanAdd,
                                        style: Styles.regular12Grey,
                                      ),
                                    ),
                                  _controller.pageNumber == 3
                                      ? Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              StringConstants
                                                  .enterBankAccountNumber,
                                              style: Styles.boldBlack16,
                                            ),
                                            Dimens.boxHeight15,
                                            SizedBox(
                                              height: Dimens.fourtyThree,
                                              child: TextFormField(
                                                controller:
                                                    _controller.accountNumber,
                                                onChanged: (v) {
                                                  _controller
                                                      .enterAccountNumber();
                                                },
                                                obscureText: !_controller
                                                    .isAccountNumberShown,
                                                obscuringCharacter: '*',
                                                decoration: InputDecoration(
                                                  enabledBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                      Dimens.twelve,
                                                    ),
                                                    borderSide: BorderSide(
                                                      color: ColorsValue
                                                          .primaryColor
                                                          .withOpacity(0.4),
                                                    ),
                                                  ),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                      Dimens.twelve,
                                                    ),
                                                    borderSide: BorderSide(
                                                      color: ColorsValue
                                                          .primaryColor
                                                          .withOpacity(0.4),
                                                    ),
                                                  ),
                                                  filled: true,
                                                  fillColor: Colors.white,
                                                  hintStyle: Styles
                                                      .textFieldHintColor16
                                                      .copyWith(
                                                    color: ColorsValue.greyColor
                                                        .withOpacity(0.5),
                                                  ),
                                                  contentPadding:
                                                      Dimens.edgeInsets10,
                                                ),
                                              ),
                                            ),
                                            Dimens.boxHeight10,
                                            Row(
                                              children: [
                                                SizedBox(
                                                  width: Dimens.twentyFour,
                                                  height: Dimens.twentyFour,
                                                  child: Checkbox(
                                                    value: _controller
                                                        .isAccountNumberShown,
                                                    checkColor: Colors.white,
                                                    side: const BorderSide(
                                                        color: ColorsValue
                                                            .primaryColor),
                                                    activeColor:
                                                        Colors.greenAccent,
                                                    shape: const CircleBorder(),
                                                    onChanged: (value) {
                                                      _controller
                                                              .isAccountNumberShown =
                                                          value!;
                                                      _controller.update();
                                                    },
                                                  ),
                                                ),
                                                Dimens.boxWidth10,
                                                Text(
                                                  StringConstants
                                                      .showBankAccountNumber,
                                                  style: Styles.regular12Grey,
                                                ),
                                              ],
                                            ),
                                          ],
                                        )
                                      : const SizedBox(),
                                ],
                              )
                            : const SizedBox(),
                        if (_controller.pageNumber == 1 ||
                            _controller.pageNumber == 2)
                          Text(
                            StringConstants.loremIpsum,
                            style: Styles.black12,
                          ),
                        if (_controller.pageNumber == 1) Dimens.boxHeight20,
                        if (_controller.pageNumber == 1)
                          Text(
                            StringConstants.accountShouldBelongTo,
                            style: Styles.medium14Black,
                          ),
                        if (_controller.pageNumber == 1) Dimens.boxHeight10,
                        if (_controller.pageNumber == 1)
                          Text(
                            'Name Surname',
                            style: Styles.bold14Black,
                          ),
                        _controller.pageNumber == 1
                            ? Dimens.boxHeight40
                            : _controller.pageNumber == 2
                                ? Dimens.boxHeight30
                                : Dimens.box0,
                        _controller.pageNumber == 1
                            ? GestureDetector(
                                onTap: RouteManagement.goToKycScanCheque,
                                child: Container(
                                  height: Dimens.fourtyFive,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                        Dimens.twentyFive),
                                    border: Border.all(
                                      color: ColorsValue.greyColor
                                          .withOpacity(0.3),
                                    ),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Image.asset('assets/kyc/cheque.png'),
                                      Dimens.boxWidth20,
                                      Text(
                                        StringConstants.scanChequeLeaf,
                                        style: Styles.bold18Black.copyWith(
                                            color: ColorsValue.primaryColor),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                            : _controller.pageNumber == 2
                                ? Container(
                                    width: Dimens.percentWidth(.9),
                                    height: Dimens.fiftySix,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: ColorsValue.primaryColor),
                                      borderRadius: BorderRadius.circular(
                                        Dimens.ten,
                                      ),
                                      color: Colors.white,
                                    ),
                                    child: Padding(
                                      padding: EdgeInsets.fromLTRB(
                                          Dimens.twenty,
                                          Dimens.zero,
                                          Dimens.zero,
                                          Dimens.zero),
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            AssetConstants.hdfcLogo,
                                            width: Dimens.thirty,
                                            height: Dimens.thirty,
                                          ),
                                          Dimens.boxWidth10,
                                          Text(
                                            'HDFC Bank',
                                            style: Styles.boldBlack15,
                                          ),
                                        ],
                                      ),
                                    ),
                                  )
                                : Dimens.box0,
                        if (_controller.pageNumber == 1) Dimens.boxHeight20,
                        if (_controller.pageNumber == 1)
                          Center(
                            child: Text(
                              'Or',
                              style: Styles.black12,
                            ),
                          ),
                        Dimens.boxHeight20,
                        if (_controller.pageNumber == 1 ||
                            _controller.pageNumber == 2)
                          SizedBox(
                            height: Dimens.fourtyThree,
                            child: TextFormField(
                              controller: _controller.searchFormField,
                              decoration: InputDecoration(
                                enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(
                                    Dimens.twentyFive,
                                  ),
                                  borderSide: BorderSide(
                                    color: Colors.grey.withOpacity(0.4),
                                  ),
                                ),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(
                                    Dimens.twentyFive,
                                  ),
                                  borderSide: BorderSide(
                                    color: Colors.grey.withOpacity(0.4),
                                  ),
                                ),
                                filled: true,
                                fillColor: Colors.white,
                                prefixIcon: Icon(
                                  Icons.search,
                                  color: ColorsValue.greyColor.withOpacity(0.5),
                                ),
                                hintText: 'Search',
                                hintStyle: Styles.textFieldHintColor16.copyWith(
                                  color: ColorsValue.greyColor.withOpacity(0.5),
                                ),
                                contentPadding: Dimens.edgeInsets10,
                              ),
                            ),
                          ),
                        Dimens.boxHeight20,
                        if (_controller.pageNumber == 1)
                          Text(
                            StringConstants.popularBanks,
                            style: Styles.medium14Black,
                          ),
                        if (_controller.pageNumber == 1) Dimens.boxHeight10,
                        _controller.pageNumber == 1
                            ? ListView.builder(
                                itemCount: 4,
                                physics: const NeverScrollableScrollPhysics(),
                                shrinkWrap: true,
                                itemBuilder: (context, index) => Column(
                                  children: [
                                    Dimens.boxHeight10,
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          children: [
                                            Container(
                                              width: Dimens.thirtyTwo,
                                              height: Dimens.thirtyTwo,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        Dimens.eight),
                                              ),
                                              child: Image.asset(
                                                AssetConstants.hdfcLogo,
                                                width: Dimens.thirty,
                                                height: Dimens.thirty,
                                              ),
                                            ),
                                            Dimens.boxWidth10,
                                            Text(
                                              'HDFC Bank',
                                              overflow: TextOverflow.ellipsis,
                                              style: Styles.black15,
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          width: Dimens.twentyFive,
                                          height: Dimens.twentyFive,
                                          child: Checkbox(
                                            value: _controller
                                                        .selectedPopularBank ==
                                                    index
                                                ? true
                                                : false,
                                            shape: const CircleBorder(),
                                            side: const BorderSide(
                                                color:
                                                    ColorsValue.primaryColor),
                                            checkColor: Colors.white,
                                            activeColor: Colors.greenAccent,
                                            onChanged: (value) {
                                              _controller.selectedPopularBank =
                                                  index;
                                              _controller.update();
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              )
                            : _controller.pageNumber == 2
                                ? SizedBox(
                                    width: Dimens.percentWidth(.9),
                                    height: Dimens.percentHeight(.55),
                                    child: ListView.builder(
                                      itemCount: 7,
                                      physics:
                                          const NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      itemBuilder: (context, index) =>
                                          GestureDetector(
                                        onTap: () {
                                          _controller.pageNumber = 3;
                                          _controller.update();
                                        },
                                        child: Column(
                                          children: [
                                            Text(
                                              'DELHI - dwarika sect 12, delhi (CNRB0002783)',
                                              style: Styles.grey14,
                                            ),
                                            Dimens.boxHeight10,
                                            const Divider(),
                                          ],
                                        ),
                                      ),
                                    ),
                                  )
                                : Dimens.box0,
                        Dimens.boxHeight20,
                      ],
                    ),
                  ),
                ),
                if (_controller.pageNumber != 2)
                  Padding(
                    padding: Dimens.edgeInsets20_0_20_20,
                    child: SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) =>
                                (_controller.isAccountNumberValid &&
                                            _controller.pageNumber == 3) ||
                                        (_controller.pageNumber == 1 &&
                                            _controller.selectedPopularBank !=
                                                -1) ||
                                        _controller.pageNumber == 2 ||
                                        _controller.pageNumber == 4
                                    ? ColorsValue.primaryColor
                                    : ColorsValue.primaryColor.withOpacity(.5),
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16White,
                          ),
                        ),
                        onPressed: () {
                          if (_controller.pageNumber == 1 &&
                              _controller.selectedPopularBank != -1) {
                            _controller.pageNumber = 2;
                          } else if (_controller.pageNumber == 3 &&
                              _controller.isAccountNumberValid) {
                            _controller.pageNumber = 4;
                          } else if (_controller.pageNumber == 4) {
                            RouteManagement.goToKYCSuccess();
                          }
                          _controller.update();
                        },
                        child: Text(
                          _controller.pageNumber == 4
                              ? StringConstants.verifyBank
                              : StringConstants.next,
                          style: Styles.bold16White,
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ),
      );
}
