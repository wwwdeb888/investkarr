import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class KycIncome extends StatelessWidget {
  const KycIncome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<KycController>(
          builder: (_controller) => ColorfulSafeArea(
            color: ColorsValue.backgroundColor,
            child: Column(
              children: [
                Dimens.boxHeight10,
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Dimens.boxWidth20,
                        SizedBox(
                          width: Dimens.twenty,
                          height: Dimens.twenty,
                          child: IconButton(
                            constraints: const BoxConstraints(),
                            padding: EdgeInsets.zero,
                            onPressed: () {
                              Get.back<dynamic>();
                            },
                            icon: Icon(
                              Icons.arrow_back_ios,
                              size: Dimens.eighteen,
                              color: ColorsValue.primaryColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                    // CircleAvatar(
                    //   radius: Dimens.seventeen,
                    //   backgroundColor: Colors.white,
                    //   child: SizedBox(
                    //     width: Dimens.seventeen,
                    //     height: Dimens.seventeen,
                    //     child: Image.asset(AssetConstants.splashLogo1),
                    //   ),
                    // ),

                    Row(
                      children: [
                        Text(
                          '1/4',
                          style: Styles.bold14Primary,
                        ),
                        Dimens.boxWidth20,
                      ],
                    )
                  ],
                ),
                Dimens.boxHeight20,
                const LinearProgressIndicator(
                  backgroundColor: Color(0xffEFE8FC),
                  valueColor: AlwaysStoppedAnimation<Color>(
                    ColorsValue.primaryColor,
                  ),
                  value: 0.25,
                ),
                Expanded(
                  child: ListView(
                    children: [
                      Dimens.boxHeight10,
                      Padding(
                        padding: Dimens.edgeInsets20,
                        child: Column(
                          children: [
                            Row(
                              children: [
                                Text(
                                  StringConstants.income,
                                  style: Styles.boldBlack18,
                                ),
                              ],
                            ),
                            Dimens.boxHeight7,
                            Row(
                              children: [
                                Text(
                                  StringConstants.selectOne,
                                  style: Styles.regular12Grey,
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: Dimens.edgeInsets20,
                        child: SizedBox(
                          width: Dimens.percentWidth(1),
                          // height: Dimens.percentHeight(.48),
                          child: GridView.builder(
                            itemCount: _controller.listOfIncome.length,
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (context, index) => BoxCheckSelector(
                              onTap: () {
                                _controller.selectedIndex = index;
                                _controller.update();
                              },
                              title: _controller.listOfIncome[index],
                              isChecked: _controller.selectedIndex == index,
                            ),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              childAspectRatio: 2 / 0.7,
                              crossAxisCount: 2,
                              crossAxisSpacing: Dimens.twenty,
                              mainAxisSpacing: Dimens.twenty,
                            ),
                          ),
                        ),
                      ),

                      /// =====================

                      if (_controller.selectedIndex != -1)
                        Padding(
                          padding: Dimens.edgeInsets20,
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Text(
                                    StringConstants.occupation,
                                    style: Styles.boldBlack18,
                                  ),
                                ],
                              ),
                              Dimens.boxHeight7,
                              Row(
                                children: [
                                  Text(
                                    StringConstants.selectOne,
                                    style: Styles.regular12Grey,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      if (_controller.selectedIndex != -1)
                        Padding(
                          padding: Dimens.edgeInsets20,
                          child: SizedBox(
                            width: Dimens.percentWidth(1),
                            // height: Dimens.percentHeight(.48),
                            child: GridView.builder(
                              itemCount: _controller.listOfOccupation.length,
                              physics: const NeverScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemBuilder: (context, index) => GestureDetector(
                                onTap: () {
                                  _controller.selectedOccupationIndex = index;
                                  _controller.update();
                                },
                                child: Container(
                                  child: Stack(
                                    children: [
                                      Align(
                                        alignment: Alignment.center,
                                        child: Text(
                                          _controller.listOfOccupation[index],
                                          style: _controller
                                                      .selectedOccupationIndex ==
                                                  index
                                              ? Styles.bold14Primary
                                              : Styles.medium14Black,
                                        ),
                                      ),
                                      Align(
                                        alignment: Alignment.topRight,
                                        child: Padding(
                                          padding: Dimens.edgeInsets5,
                                          child: Visibility(
                                            visible: _controller
                                                        .selectedOccupationIndex ==
                                                    index
                                                ? true
                                                : false,
                                            child: Container(
                                              width: Dimens.twelve,
                                              height: Dimens.twelve,
                                              decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(
                                                        Dimens.ten),
                                                color: ColorsValue.primaryColor,
                                              ),
                                              child: Icon(
                                                Icons.done,
                                                color: Colors.white,
                                                size: Dimens.ten,
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  decoration: BoxDecoration(
                                      borderRadius:
                                          BorderRadius.circular(Dimens.ten),
                                      color:
                                          _controller.selectedOccupationIndex ==
                                                  index
                                              ? const Color(0xffF5F1FE)
                                              : ColorsValue.backgroundColor,
                                      border: Border.all(
                                          color: _controller
                                                      .selectedOccupationIndex ==
                                                  index
                                              ? ColorsValue.primaryColor
                                              : Colors.grey)),
                                ),
                              ),
                              gridDelegate:
                                  SliverGridDelegateWithFixedCrossAxisCount(
                                childAspectRatio: 2 / 0.7,
                                crossAxisCount: 2,
                                crossAxisSpacing: Dimens.twenty,
                                mainAxisSpacing: Dimens.twenty,
                              ),
                            ),
                          ),
                        )
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: Dimens.edgeInsets20,
                    child: SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) =>
                                (_controller.selectedIndex != -1 &&
                                        _controller.selectedOccupationIndex !=
                                            -1)
                                    ? ColorsValue.primaryColor
                                    : ColorsValue.primaryColor.withOpacity(.5),
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16White,
                          ),
                        ),
                        onPressed: (_controller.selectedIndex != -1 &&
                                _controller.selectedOccupationIndex != -1)
                            ? RouteManagement.goToKycNomineeDetails
                            : null,
                        child: Text(
                          StringConstants.confirm,
                          style: Styles.bold16White,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}

