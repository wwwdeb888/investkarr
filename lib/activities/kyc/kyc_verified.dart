import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class KycVerified extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: ColorfulSafeArea(
          color: ColorsValue.backgroundColor,
          child: Stack(
            children: [
              Column(
                children: [
                  Dimens.boxHeight15,
                  Padding(
                    padding: Dimens.edgeInsets15_0_15_0,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Icon(
                          Icons.keyboard_arrow_left,
                          size: Dimens.thirty,
                        ),
                        Container(
                          width: Dimens.fiftySeven,
                          height: Dimens.thirtyTwo,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(Dimens.twenty),
                            color: ColorsValue.secondaryColor,
                          ),
                          child: Center(
                            child: Text(
                              StringConstants.skip,
                              style: Styles.black12
                                  .copyWith(color: ColorsValue.primaryColor),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  // Row(
                  //   crossAxisAlignment: CrossAxisAlignment.center,
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: [
                  //     Row(
                  //       children: [
                  //         Dimens.boxWidth20,
                  //         SizedBox(
                  //           width: Dimens.twenty,
                  //           height: Dimens.twenty,
                  //           child: IconButton(
                  //             constraints: const BoxConstraints(),
                  //             padding: EdgeInsets.zero,
                  //             onPressed: () {
                  //               Get.back<dynamic>();
                  //             },
                  //             icon: const Icon(
                  //               Icons.arrow_back_ios,
                  //               color: ColorsValue.primaryColor,
                  //             ),
                  //           ),
                  //         ),
                  //       ],
                  //     ),
                  //     CircleAvatar(
                  //       radius: Dimens.seventeen,
                  //       backgroundColor: Colors.white,
                  //       child: SizedBox(
                  //         width: Dimens.seventeen,
                  //         height: Dimens.seventeen,
                  //         child: Image.asset(AssetConstants.splashLogo1),
                  //       ),
                  //     ),
                  //     Row(
                  //       children: [
                  //         Dimens.boxWidth20,
                  //         Container(
                  //           width: Dimens.twenty,
                  //           height: Dimens.twenty,
                  //           color: Colors.transparent,
                  //         ),
                  //       ],
                  //     ),
                  //   ],
                  // ),
                  Padding(
                    padding: Dimens.edgeInsets20,
                    child: SizedBox(
                      width: Dimens.percentWidth(.9),
                      child: Text(
                        'Hey Name, looks like you are already KYC verified',
                        style: Styles.boldBlack18,
                      ),
                    ),
                  ),
                ],
              ),
              Align(
                alignment: Alignment.center,
                child: Image.asset(
                  AssetConstants.kyc,
                  width: Dimens.percentWidth(.6),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: Dimens.edgeInsets20,
                  child: SizedBox(
                    width: Dimens.percentWidth(1),
                    height: Dimens.percentHeight(.07),
                    child: ElevatedButton(
                      style: Styles.elevatedButtonTheme.style!.copyWith(
                        backgroundColor:
                            MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) =>
                              ColorsValue.primaryColor,
                        ),
                        textStyle: MaterialStateProperty.all(
                          Styles.bold16White,
                        ),
                      ),
                      onPressed: RouteManagement.goToKycIncome,
                      child: Text(
                        StringConstants.next,
                        style: Styles.bold16White,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
}
