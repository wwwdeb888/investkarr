import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class KycChooseBankAccount extends StatelessWidget {
  const KycChooseBankAccount({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: ColorfulSafeArea(
          color: ColorsValue.backgroundColor,
          child: GetBuilder<KycController>(
            builder: (_controller) => Stack(
              children: [
                Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            SizedBox(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              child: IconButton(
                                constraints: const BoxConstraints(),
                                padding: EdgeInsets.zero,
                                onPressed: () {
                                  Get.back<dynamic>();
                                },
                                icon: const Icon(
                                  Icons.arrow_back_ios,
                                  color: ColorsValue.primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                        CircleAvatar(
                          radius: Dimens.seventeen,
                          backgroundColor: Colors.white,
                          child: SizedBox(
                            width: Dimens.seventeen,
                            height: Dimens.seventeen,
                            child: Image.asset(AssetConstants.splashLogo1),
                          ),
                        ),
                        Row(
                          children: [
                            Text(
                              '4/4',
                              style: Styles.bold14Primary,
                            ),
                            Dimens.boxWidth20,
                          ],
                        )
                      ],
                    ),
                    Dimens.boxHeight10,
                    const LinearProgressIndicator(
                      backgroundColor: Color(0xffEFE8FC),
                      valueColor: AlwaysStoppedAnimation<Color>(
                        ColorsValue.primaryColor,
                      ),
                      value: 0.66,
                    ),
                    Dimens.boxHeight10,
                    Padding(
                      padding: Dimens.edgeInsets20,
                      child: Column(
                        children: [
                          Row(
                            children: [
                              _controller.pageNumber == 1
                                  ? Text(
                                      StringConstants.addBankAccount,
                                      style: Styles.boldBlack18,
                                    )
                                  : _controller.pageNumber == 2
                                      ? Text(
                                          StringConstants.chooseYourBranch,
                                          style: Styles.boldBlack18,
                                        )
                                      : Text(
                                          StringConstants.confirmBankDetails,
                                          style: Styles.boldBlack18,
                                        ),
                            ],
                          ),
                          Dimens.boxHeight20,
                          _controller.pageNumber == 1
                              ? Column(
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                                          StringConstants.accountShouldBelongTo,
                                          style: Styles.black15,
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          'Name Surname',
                                          style: Styles.boldBlack15,
                                        ),
                                      ],
                                    ),
                                  ],
                                )
                              : const SizedBox(),
                          Dimens.boxHeight10,
                          if (_controller.pageNumber == 2)
                            Container(
                              width: Dimens.percentWidth(.9),
                              height: Dimens.fifty,
                              decoration: BoxDecoration(
                                border: Border.all(color: Colors.grey),
                                borderRadius: BorderRadius.circular(
                                  Dimens.ten,
                                ),
                                color: Colors.white,
                              ),
                              child: Padding(
                                padding: EdgeInsets.fromLTRB(Dimens.twenty,
                                    Dimens.zero, Dimens.zero, Dimens.zero),
                                child: Row(
                                  children: [
                                    Image.asset(
                                      AssetConstants.hdfcLogo,
                                      width: Dimens.thirty,
                                      height: Dimens.thirty,
                                    ),
                                    Dimens.boxWidth10,
                                    Text(
                                      'HDFC Bank',
                                      style: Styles.boldBlack15,
                                    ),
                                  ],
                                ),
                              ),
                            )
                          else
                            const SizedBox(),
                          (_controller.pageNumber == 3 ||
                                  _controller.pageNumber == 4)
                              ? Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Card(
                                      color: Colors.white,
                                      shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(
                                            Dimens.thirty),
                                      ),
                                      child: SizedBox(
                                        width: Dimens.percentWidth(.9),
                                        height: _controller.pageNumber == 4
                                            ? Dimens.percentHeight(.35)
                                            : Dimens.percentHeight(.3),
                                        child: Padding(
                                          padding: Dimens.edgeInsets20,
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Row(
                                                children: [
                                                  Image.asset(
                                                    AssetConstants.hdfcLogo,
                                                    width: Dimens.thirty,
                                                    height: Dimens.thirty,
                                                  ),
                                                  Dimens.boxWidth10,
                                                  Text(
                                                    'HDFC Bank',
                                                    style: Styles.black18,
                                                  ),
                                                  const Spacer(),
                                                  const Icon(
                                                    Icons.edit,
                                                    color: ColorsValue
                                                        .primaryColor,
                                                  ),
                                                ],
                                              ),
                                              Dimens.boxHeight10,
                                              const Divider(),
                                              Dimens.boxHeight10,
                                              _controller.pageNumber == 4
                                                  ? Row(
                                                      children: [
                                                        SizedBox(
                                                          width: Dimens
                                                              .percentWidth(.3),
                                                          child: Text(
                                                            StringConstants
                                                                .accountNumber,
                                                            style:
                                                                Styles.black15,
                                                          ),
                                                        ),
                                                        SizedBox(
                                                          width: Dimens
                                                              .percentWidth(.4),
                                                          child: Text(
                                                            'Avinashi',
                                                            style:
                                                                Styles.black15,
                                                          ),
                                                        ),
                                                      ],
                                                    )
                                                  : const SizedBox(),
                                              Dimens.boxHeight10,
                                              Row(
                                                children: [
                                                  SizedBox(
                                                    width:
                                                        Dimens.percentWidth(.3),
                                                    child: Text(
                                                      StringConstants
                                                          .branchName,
                                                      style: Styles.black15,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width:
                                                        Dimens.percentWidth(.4),
                                                    child: Text(
                                                      'Avinashi',
                                                      style: Styles.black15,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Dimens.boxHeight10,
                                              Row(
                                                children: [
                                                  SizedBox(
                                                    width:
                                                        Dimens.percentWidth(.3),
                                                    child: Text(
                                                      StringConstants.iFSCCode,
                                                      style: Styles.black15,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width:
                                                        Dimens.percentWidth(.4),
                                                    child: Text(
                                                      'HDFC000000',
                                                      style: Styles.black15,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              Dimens.boxHeight10,
                                              Row(
                                                children: [
                                                  SizedBox(
                                                    width:
                                                        Dimens.percentWidth(.3),
                                                    child: Text(
                                                      StringConstants.address,
                                                      style: Styles.black15,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width:
                                                        Dimens.percentWidth(.4),
                                                    child: Text(
                                                      '2972 Westheimer Rd. Santa Ana, Illinois 85486 ',
                                                      style: Styles.black15,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                    _controller.pageNumber == 3
                                        ? Column(
                                            children: [
                                              Dimens.boxHeight10,
                                              Text(
                                                StringConstants
                                                    .enterBankAccountNumber,
                                                style: Styles.boldBlack15,
                                              ),
                                              Dimens.boxHeight10,
                                              FormFieldWidget(
                                                elevation: 0,
                                                isFilled: true,
                                                obscureCharacter: '*',
                                                isObscureText: _controller
                                                        .showAccountNumber
                                                    ? false
                                                    : true,
                                                textInputAction:
                                                    TextInputAction.done,
                                                textCapitalization:
                                                    TextCapitalization
                                                        .characters,
                                                textInputType:
                                                    TextInputType.text,
                                                borderColor: ColorsValue
                                                    .formFieldBorderColor,
                                                fillColor:
                                                    ColorsValue.backgroundColor,
                                                hintStyle:
                                                    Styles.textFieldHintColor16,
                                                contentPadding:
                                                    Dimens.edgeInsets10,
                                                onChange: (String v) {
                                                  _controller.bankNumber = v;
                                                },
                                              ),
                                              Dimens.boxHeight10,
                                              Row(
                                                children: [
                                                  SizedBox(
                                                    width: Dimens.twentyFour,
                                                    height: Dimens.twentyFour,
                                                    child: Checkbox(
                                                      value: _controller
                                                          .showAccountNumber,
                                                      checkColor: Colors.white,
                                                      activeColor:
                                                          Colors.greenAccent,
                                                      onChanged: (value) {
                                                        _controller
                                                                .showAccountNumber =
                                                            value!;
                                                        _controller.update();
                                                      },
                                                    ),
                                                  ),
                                                  Dimens.boxWidth10,
                                                  Text(
                                                    StringConstants
                                                        .showBankAccountNumber,
                                                    style: Styles.secondary12,
                                                  ),
                                                ],
                                              ),
                                            ],
                                          )
                                        : const SizedBox(),
                                  ],
                                )
                              : const SizedBox(),
                          _controller.pageNumber == 4
                              ? Column(
                                  children: [
                                    Dimens.boxHeight10,
                                    Container(
                                      width: Dimens.percentWidth(.9),
                                      height: Dimens.fifty,
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                            color: ColorsValue.primaryColor),
                                        borderRadius:
                                            BorderRadius.circular(Dimens.fifty),
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Text(
                                            StringConstants.addAnotherAccount,
                                            style: Styles.bold16Primary,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Dimens.boxHeight10,
                                    Text(
                                      StringConstants.youCanAdd,
                                      style: Styles.black12,
                                    )
                                  ],
                                )
                              : const SizedBox(),
                          Dimens.boxHeight10,
                          (_controller.pageNumber == 1 ||
                                  _controller.pageNumber == 2)
                              ? FormFieldWidget(
                                  elevation: 0,
                                  autoFocus: false,
                                  prefixIcon: const Icon(
                                    Icons.search,
                                    color: Colors.grey,
                                  ),
                                  isFilled: true,
                                  textInputAction: TextInputAction.next,
                                  textCapitalization: TextCapitalization.words,
                                  textInputType: TextInputType.text,
                                  borderColor: ColorsValue.formFieldBorderColor,
                                  fillColor: ColorsValue.backgroundColor,
                                  hintText: StringConstants.searchBank,
                                  hintStyle: Styles.textFieldHintColor16,
                                  contentPadding: Dimens.edgeInsets10,
                                  onChange: (String v) {
                                    _controller.firstName = v;
                                  },
                                )
                              : const SizedBox(),
                          Dimens.boxHeight10,
                          _controller.pageNumber == 1
                              ? Row(
                                  children: [
                                    Text(
                                      StringConstants.popularBanks,
                                      style: Styles.black15,
                                    ),
                                  ],
                                )
                              : const SizedBox(),
                          _controller.pageNumber == 1
                              ? SizedBox(
                                  width: Dimens.percentWidth(.9),
                                  height: Dimens.percentHeight(.5),
                                  child: ListView.builder(
                                    itemCount: 4,
                                    itemBuilder: (context, index) => Column(
                                      children: [
                                        Dimens.boxHeight10,
                                        Row(
                                          children: [
                                            Row(
                                              children: [
                                                Image.asset(
                                                  AssetConstants.hdfcLogo,
                                                  width: Dimens.thirty,
                                                  height: Dimens.thirty,
                                                ),
                                                Dimens.boxWidth10,
                                                Text(
                                                  'HDFC Bank',
                                                  style: Styles.black15,
                                                ),
                                              ],
                                            ),
                                            const Spacer(),
                                            SizedBox(
                                              width: Dimens.twentyFour,
                                              height: Dimens.twentyFour,
                                              child: Checkbox(
                                                value: _controller
                                                            .selectedPopularBank ==
                                                        index
                                                    ? true
                                                    : false,
                                                checkColor: Colors.white,
                                                activeColor:
                                                    ColorsValue.primaryColor,
                                                onChanged: (value) {
                                                  _controller
                                                          .selectedPopularBank =
                                                      index;
                                                  _controller.update();
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              : const SizedBox(),
                          _controller.pageNumber == 2
                              ? SizedBox(
                                  width: Dimens.percentWidth(.9),
                                  height: Dimens.percentHeight(.55),
                                  child: ListView.builder(
                                    itemCount: 4,
                                    itemBuilder: (context, index) =>
                                        GestureDetector(
                                      onTap: () {
                                        _controller.pageNumber = 3;
                                        _controller.update();
                                      },
                                      child: Column(
                                        children: [
                                          Text(
                                            'DELHI - dwarika sect 12, delhi (CNRB0002783)',
                                            style: Styles.black15,
                                          ),
                                          Dimens.boxHeight10,
                                          const Divider(),
                                        ],
                                      ),
                                    ),
                                  ),
                                )
                              : const SizedBox(),
                        ],
                      ),
                    ),
                  ],
                ),
                (_controller.pageNumber == 1)
                    ? Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: Dimens.edgeInsets20,
                          child: SizedBox(
                            width: Dimens.percentWidth(1),
                            height: Dimens.percentHeight(.07),
                            child: ElevatedButton(
                              style: Styles.elevatedButtonTheme.style!.copyWith(
                                backgroundColor:
                                    MaterialStateProperty.resolveWith<Color>(
                                  (Set<MaterialState> states) =>
                                      ColorsValue.primaryColor,
                                ),
                                textStyle: MaterialStateProperty.all(
                                  Styles.bold16White,
                                ),
                              ),
                              onPressed: () {
                                _controller.pageNumber = 2;
                                _controller.update();
                              },
                              child: Text(
                                StringConstants.next,
                                style: Styles.bold16White,
                              ),
                            ),
                          ),
                        ),
                      )
                    : const SizedBox(),
                (_controller.pageNumber == 3)
                    ? Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: Dimens.edgeInsets20,
                          child: SizedBox(
                            width: Dimens.percentWidth(1),
                            height: Dimens.percentHeight(.07),
                            child: ElevatedButton(
                              style: Styles.elevatedButtonTheme.style!.copyWith(
                                backgroundColor:
                                    MaterialStateProperty.resolveWith<Color>(
                                  (Set<MaterialState> states) =>
                                      ColorsValue.primaryColor,
                                ),
                                textStyle: MaterialStateProperty.all(
                                  Styles.bold16White,
                                ),
                              ),
                              onPressed: () {
                                _controller.pageNumber = 4;
                                _controller.update();
                              },
                              child: Text(
                                StringConstants.next,
                                style: Styles.bold16White,
                              ),
                            ),
                          ),
                        ),
                      )
                    : const SizedBox(),
                (_controller.pageNumber == 4)
                    ? Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: Dimens.edgeInsets20,
                          child: SizedBox(
                            width: Dimens.percentWidth(1),
                            height: Dimens.percentHeight(.07),
                            child: ElevatedButton(
                              style: Styles.elevatedButtonTheme.style!.copyWith(
                                backgroundColor:
                                    MaterialStateProperty.resolveWith<Color>(
                                  (Set<MaterialState> states) =>
                                      ColorsValue.primaryColor,
                                ),
                                textStyle: MaterialStateProperty.all(
                                  Styles.bold16White,
                                ),
                              ),
                              onPressed: RouteManagement.goToKYCSuccess,
                              child: Text(
                                StringConstants.verifyBank,
                                style: Styles.bold16White,
                              ),
                            ),
                          ),
                        ),
                      )
                    : const SizedBox(),
              ],
            ),
          ),
        ),
      );
}
