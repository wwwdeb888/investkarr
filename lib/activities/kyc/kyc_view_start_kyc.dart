import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class KycViewStartKYC extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: SafeArea(
          child: Stack(
            children: [
              Container(
                height: Dimens.percentHeight(.35),
                width: Dimens.percentWidth(1),
                decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage(
                        AssetConstants.frameOne,
                      ),
                      fit: BoxFit.cover),
                ),
                // color: const Color(0xffEBE3FD),
              ),
              Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding: Dimens.edgeInsets15,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(
                        Icons.keyboard_arrow_left,
                        size: Dimens.thirty,
                      ),
                      GestureDetector(
                        onTap: RouteManagement.goToHome,
                        child: Container(
                          width: Dimens.fiftySeven,
                          height: Dimens.thirtyTwo,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(Dimens.twenty),
                            color: Colors.white,
                          ),
                          child: Center(
                            child: Text(
                              StringConstants.skip,
                              style: Styles.black12
                                  .copyWith(color: ColorsValue.primaryColor),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(
                    Dimens.twenty, Dimens.eighty, Dimens.twenty, Dimens.zero),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      StringConstants.kycReady,
                      style: Styles.boldBlack18,
                    ),
                    Dimens.boxHeight5,
                    Text(
                      StringConstants.smoothReg,
                      style: Styles.secondary15,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.fromLTRB(Dimens.twenty,
                    Dimens.percentHeight(.23), Dimens.twenty, Dimens.zero),
                child: Card(
                  color: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius:
                          BorderRadius.all(Radius.circular(Dimens.twenty))),
                  child: SizedBox(
                    width: Dimens.percentWidth(1),
                    height: Dimens.percentHeight(.55),
                    child: Padding(
                      padding: Dimens.edgeInsets30,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Row(
                            children: [
                              CircleAvatar(
                                radius: Dimens.twenty,
                                backgroundColor: const Color(0xffFDF4F7),
                                child: const Icon(
                                  Icons.person_add_alt,
                                  color: Color(0xffE194AE),
                                ),
                              ),
                              Dimens.boxWidth20,
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    StringConstants.proofOfIdentity,
                                    style: Styles.boldBlack15,
                                  ),
                                  SizedBox(
                                    width: Dimens.percentWidth(.55),
                                    child: Text(
                                      StringConstants.mandatoryPAN,
                                      style: Styles.secondary12,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Dimens.boxHeight30,
                          Row(
                            children: [
                              CircleAvatar(
                                radius: Dimens.twenty,
                                backgroundColor: const Color(0xffFFF0EB),
                                child: const Icon(
                                  Icons.edit_location,
                                  color: Color(0xffE194AE),
                                ),
                              ),
                              Dimens.boxWidth20,
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    StringConstants.proofOfAddress,
                                    style: Styles.boldBlack15,
                                  ),
                                  SizedBox(
                                    width: Dimens.percentWidth(.55),
                                    child: Text(
                                      StringConstants.aadhaar,
                                      style: Styles.secondary12,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Dimens.boxHeight30,
                          Row(
                            children: [
                              CircleAvatar(
                                radius: Dimens.twenty,
                                backgroundColor: const Color(0xffE9F5F3),
                                child: const Icon(
                                  Icons.credit_card,
                                  color: Color(0xff91CEC3),
                                ),
                              ),
                              Dimens.boxWidth20,
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    StringConstants.chequeBook,
                                    style: Styles.boldBlack15,
                                  ),
                                  SizedBox(
                                    width: Dimens.percentWidth(.55),
                                    child: Text(
                                      StringConstants.iFSC,
                                      style: Styles.secondary12,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Dimens.boxHeight30,
                          Row(
                            children: [
                              CircleAvatar(
                                radius: Dimens.twenty,
                                child: const Icon(
                                  Icons.edit,
                                  color: Color(0xffFFDBA0),
                                ),
                                backgroundColor: const Color(0xffFFFBF5),
                              ),
                              Dimens.boxWidth20,
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    StringConstants.penPaper,
                                    style: Styles.boldBlack15,
                                  ),
                                  SizedBox(
                                    width: Dimens.percentWidth(.55),
                                    child: Text(
                                      StringConstants.signature,
                                      style: Styles.secondary12,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: Dimens.edgeInsets20,
                  child: SizedBox(
                    width: Dimens.percentWidth(1),
                    height: Dimens.percentHeight(.07),
                    child: ElevatedButton(
                      style: Styles.elevatedButtonTheme.style!.copyWith(
                        backgroundColor:
                            MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) =>
                              ColorsValue.primaryColor,
                        ),
                        textStyle: MaterialStateProperty.all(
                          Styles.bold16White,
                        ),
                      ),
                      onPressed: RouteManagement.goToKycVerified,
                      child: Text(
                        StringConstants.startKYC,
                        style: Styles.bold16White,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
}
