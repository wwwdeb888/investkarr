import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class KycBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(KycController.new);
  }

}