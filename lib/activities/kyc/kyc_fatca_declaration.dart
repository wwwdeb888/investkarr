import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class KycFatcaDeclaration extends StatelessWidget {
  const KycFatcaDeclaration({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<KycController>(
          builder: (_controller) => ColorfulSafeArea(
            color: ColorsValue.backgroundColor,
            child: Column(
              children: [
                Dimens.boxHeight10,
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Dimens.boxWidth20,
                        SizedBox(
                          width: Dimens.twenty,
                          height: Dimens.twenty,
                          child: IconButton(
                            constraints: const BoxConstraints(),
                            padding: EdgeInsets.zero,
                            onPressed: () {
                              Get.back<dynamic>();
                            },
                            icon: Icon(
                              Icons.arrow_back_ios,
                              size: Dimens.eighteen,
                              color: ColorsValue.primaryColor,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        Text(
                          '3/4',
                          style: Styles.bold14Primary,
                        ),
                        Dimens.boxWidth20,
                      ],
                    )
                  ],
                ),
                Dimens.boxHeight20,
                const LinearProgressIndicator(
                  backgroundColor: Color(0xffEFE8FC),
                  valueColor: AlwaysStoppedAnimation<Color>(
                    ColorsValue.primaryColor,
                  ),
                  value: 0.75,
                ),
                Dimens.boxHeight10,
                Expanded(
                  child: Padding(
                    padding: Dimens.edgeInsets20,
                    child: ListView(
                      children: [
                        Row(
                          children: [
                            Text(
                              StringConstants.fatcaDeclaration,
                              style: Styles.boldBlack18,
                            ),
                          ],
                        ),
                        Dimens.boxHeight20,
                        Text(
                          StringConstants.areYouAPoliticallyExposedPerson,
                          style: Styles.medium14Black,
                        ),
                        Dimens.boxHeight10,
                        YesNoRadioButton(
                          valuesList: ['yes', 'no'],
                          groupValue: _controller.politicallyExposedPerson!,
                          onChanged: (v) {
                            _controller.isPoliticallyExposedPerson(v);
                          },
                        ),
                        Dimens.boxHeight20,
                        Text(
                          StringConstants
                              .areYouRelatedToAPoliticallyExposedPerson,
                          style: Styles.medium14Black,
                        ),
                        Dimens.boxHeight10,
                        YesNoRadioButton(
                          valuesList: ['yes', 'no'],
                          groupValue:
                              _controller.relatedToPoliticallyExposedPerson!,
                          onChanged: (v) {
                            _controller.isRelatedToPoliticallyExposedPerson(v);
                          },
                        ),
                        Dimens.boxHeight20,
                        Text(
                          StringConstants.areYouATaxResidentOfAnyOtherCountry,
                          style: Styles.medium14Black,
                        ),
                        Dimens.boxHeight10,
                        YesNoRadioButton(
                          valuesList: ['yes', 'no'],
                          groupValue: _controller.taxResidentOfAnyCountry!,
                          onChanged: (v) {
                            _controller.istaxResidentOfAnyCountry(v);
                          },
                        ),
                        Dimens.boxHeight20,
                        Text(
                          StringConstants.areYouACitizenOfIndia,
                          style: Styles.medium14Black,
                        ),
                        Dimens.boxHeight10,
                        YesNoRadioButton(
                          valuesList: ['yes', 'no'],
                          groupValue: _controller.citizenOfIndia!,
                          onChanged: (v) {
                            _controller.isCitizenOfIndia(v);
                          },
                        ),
                        Dimens.boxHeight20,
                        SizedBox(
                          width: Dimens.percentWidth(.9),
                          child: DropdownButton(
                              isExpanded: true,
                              value: _controller.selectedPlaceOfBirth.value,
                              icon: const Icon(
                                Icons.arrow_drop_down,
                                color: ColorsValue.textColor,
                              ),
                              items: _controller.placeOfBirthList,
                              hint: Padding(
                                padding: Dimens.edgeInsets10,
                                child: Text(
                                  StringConstants.selectYourPlaceOfBirth,
                                  style: Styles.textFieldHintColor16,
                                ),
                              ),
                              onChanged: (value) {
                                _controller.selectedPlaceOfBirth.value =
                                    value as PlaceOfBirthList?;
                                _controller.update();
                              }),
                        ),
                        Dimens.boxHeight20,
                        SizedBox(
                          width: Dimens.percentWidth(.9),
                          child: DropdownButton(
                              isExpanded: true,
                              value: _controller
                                  .selectedCountryOfNationality.value,
                              icon: const Icon(
                                Icons.arrow_drop_down,
                                color: ColorsValue.textColor,
                              ),
                              items: _controller.countryOfNationalityList,
                              hint: Padding(
                                padding: Dimens.edgeInsets10,
                                child: Text(
                                  StringConstants
                                      .selectYourCountryOfNationality,
                                  style: Styles.textFieldHintColor16,
                                ),
                              ),
                              onChanged: (value) {
                                _controller.selectedCountryOfNationality.value =
                                    value as CountryOfNationalityList?;
                                _controller.update();
                              }),
                        ),
                        Dimens.boxHeight20,
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: Dimens.edgeInsets20_0_20_20,
                  child: Row(
                    children: [
                      SizedBox(
                        width: Dimens.twentyFive,
                        height: Dimens.twentyFive,
                        child: Checkbox(
                          value: _controller.checkBoxValue,
                          shape: const CircleBorder(),
                          side:
                              const BorderSide(color: ColorsValue.primaryColor),
                          checkColor: Colors.white,
                          activeColor: Colors.greenAccent,
                          onChanged: (value) {
                            _controller.checkBoxValue = value!;
                            _controller.update();
                          },
                        ),
                      ),
                      Dimens.boxWidth10,
                      Text(
                        StringConstants.iAgreeToAllTAndCAndFatcaDeclarations,
                        style: Styles.secondary12,
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: Dimens.edgeInsets20_0_20_20,
                  child: SizedBox(
                    width: Dimens.percentWidth(1),
                    height: Dimens.percentHeight(.07),
                    child: ElevatedButton(
                      style: Styles.elevatedButtonTheme.style!.copyWith(
                        backgroundColor:
                            MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) => 0 == 0
                              ? ColorsValue.primaryColor
                              : ColorsValue.primaryColor.withOpacity(.5),
                        ),
                        textStyle: MaterialStateProperty.all(
                          Styles.bold16White,
                        ),
                      ),
                      onPressed: RouteManagement.goToKycAddBankAccount,
                      child: Text(
                        StringConstants.next,
                        style: Styles.bold16White,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
