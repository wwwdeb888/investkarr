import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class YesNoRadioButton extends StatelessWidget {
  YesNoRadioButton(
      {Key? key,
      required this.valuesList,
      required this.groupValue,
      required this.onChanged})
      : super(key: key);

  final List<String> valuesList;
  String groupValue;
  void Function(String?)? onChanged;

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            children: [
              Radio<String>(
                value: valuesList[0],
                groupValue: groupValue,
                fillColor: MaterialStateProperty.all(ColorsValue.primaryColor),
                materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                splashRadius: 0,
                onChanged: (v) {
                  onChanged!(v);
                },
              ),
              Text(
                StringConstants.yes,
                style: Styles.medium14Black,
              ),
            ],
          ),
          Dimens.boxWidth40,
          Row(
            children: [
              Radio<String>(
                value: valuesList[1],
                groupValue: groupValue,
                fillColor: MaterialStateProperty.all(ColorsValue.primaryColor),
                onChanged: (v) {
                  onChanged!(v);
                },
              ),
              Text(
                StringConstants.no,
                style: Styles.medium14Black,
              ),
            ],
          ),
        ],
      );
}
