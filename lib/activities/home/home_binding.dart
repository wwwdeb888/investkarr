import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class HomeBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(HomeController.new);
  }

}