import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class NotificationsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: SafeArea(
          child: DefaultTabController(
            length: 2,
            child: SizedBox(
              width: Get.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      IconButton(
                        onPressed: Get.back,
                        icon: Icon(
                          Icons.arrow_back_ios,
                          size: Dimens.twenty,
                        ),
                      ),
                      Text(
                        'Notifications Center',
                        style: Styles.bold14Black,
                      ),
                    ],
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: Colors.grey.withOpacity(0.2), width: 1)),
                    ),
                    child: TabBar(
                      padding: Dimens.edgeInsets0_10_0_0,
                      indicatorSize: TabBarIndicatorSize.tab,
                      unselectedLabelColor: ColorsValue.greyColor,
                      unselectedLabelStyle: Styles.medium16Black,
                      indicatorColor: ColorsValue.primaryColor,
                      labelColor: ColorsValue.primaryColor,
                      labelStyle: Styles.medium16Black,
                      tabs: [
                        Column(
                          children: [
                            const Text('Message'),
                            Dimens.boxHeight10,
                          ],
                        ),
                        Column(
                          children: [
                            const Text('Alerts'),
                            Dimens.boxHeight10,
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: TabBarView(
                      children: [
                        ListView.separated(
                          itemCount: 10,
                          separatorBuilder: (context, index) => Column(
                            children: [
                              Dimens.boxHeight10,
                              const CustomDivider(),
                              Dimens.boxHeight10,
                            ],
                          ),
                          itemBuilder: (context, index) => Container(
                            padding: Dimens.edgeInsets10,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                CircleIcon(
                                  width: Dimens.thirtyTwo,
                                  height: Dimens.thirtyTwo,
                                  isSvg: false,
                                  color: Colors.white,
                                  image: AssetConstants.noti,
                                ),
                                Dimens.boxWidth10,
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'News Update',
                                          style: Styles.medium14Black,
                                        ),
                                        SizedBox(
                                          width: Dimens.percentWidth(.42),
                                        ),
                                        Row(
                                          children: [
                                            SvgPicture.asset(
                                                AssetConstants.time),
                                            Dimens.boxWidth5,
                                            Text(
                                              '1 hr ago',
                                              style: Styles.regular12Grey,
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    Dimens.boxHeight10,
                                    SizedBox(
                                        width: Dimens.percentWidth(.8),
                                        child: Text(
                                          'What is Lorem Ipsum Lorem Ipsum is simply dumm text of the printing and typesetting ',
                                          style: Styles.regular12Black,
                                        )),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                        ListView.separated(
                          itemCount: 10,
                          separatorBuilder: (context, index) => Column(
                            children: [
                              Dimens.boxHeight10,
                              const CustomDivider(),
                              Dimens.boxHeight10,
                            ],
                          ),
                          itemBuilder: (context, index) => Container(
                            padding: Dimens.edgeInsets10,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                CircleIcon(
                                  width: Dimens.thirtyTwo,
                                  height: Dimens.thirtyTwo,
                                  isSvg: false,
                                  color: Colors.white,
                                  image: AssetConstants.noti2,
                                ),
                                Dimens.boxWidth10,
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'SIP Updates',
                                          style: Styles.medium14Black,
                                        ),
                                        SizedBox(
                                          width: Dimens.percentWidth(.45),
                                        ),
                                        Row(
                                          children: [
                                            SvgPicture.asset(
                                                AssetConstants.time),
                                            Dimens.boxWidth5,
                                            Text(
                                              '1 hr ago',
                                              style: Styles.regular12Grey,
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    Dimens.boxHeight10,
                                    SizedBox(
                                        width: Dimens.percentWidth(.8),
                                        child: Text(
                                          'What is Lorem Ipsum Lorem Ipsum is simply dumm text of the printing and typesetting ',
                                          style: Styles.regular12Black,
                                        )),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
}
