import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class HomeView extends StatelessWidget {
  const HomeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: GetBuilder<HomeController>(
          builder: (_controller) => Scaffold(
            backgroundColor: ColorsValue.backgroundColor,
            body: SafeArea(
              child: _controller.getItemWidget(_controller.selectedIndex),
            ),
            bottomNavigationBar: BottomNavBar(),
          ),
        ),
      );
}
