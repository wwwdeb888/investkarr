import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/esg_pack.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/goal_based_investing.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/medium_term_pack.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/short_term_pack.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/tax_saver_pack.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/wealth_creation/wealth_creation_screen.dart';
import 'package:investkarr/lib.dart';
import 'widgets/emergency_pack.dart';

class ExploreWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) => SafeArea(
        child: Column(
          children: [
            Expanded(
              child: ListView(
                padding: Dimens.edgeInsets0,
                children: [
                  Container(
                    // transform: Matrix4.translationValues(
                    //     -Dimens.five, -Dimens.fifteen, Dimens.zero),
                    child: Padding(
                      padding: Dimens.edgeInsets10,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          SizedBox(
                            width: Dimens.oneHundredTwentyFour,
                            height: Dimens.twentyFour,
                            child: Image.asset(
                              AssetConstants.investkarLogo,
                              fit: BoxFit.cover,
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              Get.to(NotificationsView());
                            },
                            child: Stack(
                              children: [
                                Container(
                                  width: Dimens.thirtyTwo,
                                  height: Dimens.thirtyTwo,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(
                                          Dimens.twentyFive),
                                      color: ColorsValue.pinkColor),
                                  child: Image.asset(
                                    AssetConstants.notification,
                                    scale: 2.9,
                                  ),
                                ),
                                Positioned(
                                    right: 0,
                                    top: 0,
                                    child: Container(
                                      width: Dimens.fourteen,
                                      height: Dimens.fourteen,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(Dimens.fifty),
                                        color: ColorsValue.redColor,
                                      ),
                                      child: Center(
                                        child: Text(
                                          '10',
                                          style: Styles.bold8White,
                                        ),
                                      ),
                                    )),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Dimens.boxHeight10,
                  Align(
                    alignment: Alignment.bottomLeft,
                    child: Padding(
                      padding: Dimens.edgeInsets0,
                      child: CarouselSlider(
                        options: CarouselOptions(
                          autoPlay: false,
                          viewportFraction: .90,
                          enableInfiniteScroll: false,
                          initialPage: 0,
                        ),
                        items: [
                          Padding(
                            padding: Dimens.edgeInsets0_0_10_0,
                            child: FirstCarouselBanner(
                              heading: StringConstants.completeRiskProfile,
                              subHeading: StringConstants.toGetMore,
                              onTap: () {},
                            ),
                          ),
                          FirstCarouselBanner(
                            heading: StringConstants.rebalanceAvailable,
                            subHeading: StringConstants.toGetMore,
                            onTap: () {},
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: Dimens.edgeInsets20,
                    child: Text(
                      StringConstants.mutualFunds,
                      style: Styles.bold16Black,
                    ),
                  ),
                  Padding(
                    padding: Dimens.edgeInsets20_0_20_0,
                    child: Column(
                      children: [
                        ExploreFundCard(
                          heading: StringConstants.wealthCreation,
                          subHeading: StringConstants.jumpStart,
                          assetImage: AssetConstants.wealthCreation,
                          onPressed: () {
                            Get.to(const WealthCreationScreen());
                          },
                        ),
                        Dimens.boxHeight15,
                        ExploreFundCard(
                          heading: StringConstants.goalBasedInvesting,
                          subHeading: StringConstants.investLifeGoal,
                          assetImage: AssetConstants.goalBasedInvesting,
                          onPressed: () {
                            Get.to(ExploreGoalBasedInvesting.new);
                          },
                        ),
                        Dimens.boxHeight15,
                        ExploreFundCard(
                          heading: StringConstants.investYourself,
                          subHeading: StringConstants.exploreAll,
                          assetImage: AssetConstants.investYourself,
                          onPressed: RouteManagement.goToInvestYourself,
                        ),
                      ],
                    ),
                  ),
                  Dimens.boxHeight15,
                  Padding(
                    padding: Dimens.edgeInsets20,
                    child: Text(
                      StringConstants.curatedPacks,
                      style: Styles.bold16Black,
                    ),
                  ),
                  Padding(
                    padding: Dimens.edgeInsets20_0_20_0,
                    child: SizedBox(
                      width: Get.width,
                      height: Dimens.hundred + Dimens.ninety,
                      child: ListView(
                        physics: const AlwaysScrollableScrollPhysics(),
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        children: [
                          CuratedPackItem(
                            heading: StringConstants.emergency,
                            subHeading: StringConstants.riskFunds,
                            bgAssetImage: AssetConstants.curatedOne,
                            onPressed: () {
                              Get.to(const EmergencyPack());
                            },
                          ),
                          Dimens.boxWidth10,
                          CuratedPackItem(
                            heading: StringConstants.taxSaver,
                            subHeading: StringConstants.offersELSS,
                            bgAssetImage: AssetConstants.curatedTwo,
                            onPressed: () {
                              Get.to(const TaxSaverPack());
                            },
                          ),
                          Dimens.boxWidth10,
                          CuratedPackItem(
                            heading: StringConstants.esg,
                            subHeading: 'ESG Pack',
                            bgAssetImage: AssetConstants.curatedOne,
                            onPressed: () {
                              Get.to(const ESGPack());
                            },
                          ),
                          Dimens.boxWidth10,
                          CuratedPackItem(
                            heading: 'Short Term Pack',
                            subHeading: StringConstants.offersELSS,
                            bgAssetImage: AssetConstants.curatedTwo,
                            onPressed: () {
                              Get.to(const ShortTermPack());
                            },
                          ),
                          Dimens.boxWidth10,
                          CuratedPackItem(
                            heading: 'Medium Term Pack',
                            subHeading: StringConstants.offersELSS,
                            bgAssetImage: AssetConstants.curatedOne,
                            onPressed: () {
                              Get.to(const MediumTermPack());
                            },
                          ),
                        ],
                      ),
                    ),
                  ),
                  Dimens.boxHeight20,
                  Padding(
                    padding: Dimens.edgeInsets20_0_20_0,
                    child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(Dimens.fifteen),
                        color: Colors.white,
                        boxShadow: Styles.cardShadow,
                      ),
                      child: SizedBox(
                        width: Dimens.percentWidth(1),
                        height: Dimens.percentHeight(.22),
                        child: Padding(
                          padding: Dimens.edgeInsets20,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Expense Calculator',
                                        style: Styles.medium18CardHeading,
                                      ),
                                      Dimens.boxHeight10,
                                      Text(
                                        'Keep an eye on your spending',
                                        overflow: TextOverflow.ellipsis,
                                        style: Styles.regular12Grey,
                                      ),
                                      Dimens.boxHeight30,
                                      SizedBox(
                                        width: Dimens.percentWidth(.36),
                                        height: Dimens.fourty,
                                        child: FormSubmitWidget(
                                          opacity: 1,
                                          backgroundColor:
                                              ColorsValue.primaryColor,
                                          text: StringConstants.calculate,
                                          textStyle: Styles.bold16White,
                                          onTap: () {},
                                        ),
                                      ),
                                    ],
                                  ),
                                  Expanded(
                                    child: Image.asset(
                                      AssetConstants.ec,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: Dimens.edgeInsets20,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          StringConstants.recommendedVideos,
                          style: Styles.boldBlack16,
                        ),
                        Text(
                          StringConstants.viewAll,
                          style: Styles.medium12Primary,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: Dimens.edgeInsets20_0_20_0,
                    child: CarouselSlider(
                      options: CarouselOptions(
                        disableCenter: true,
                        autoPlay: true,
                        viewportFraction: .45,
                        height: Dimens.percentHeight(.15),
                      ),
                      items: [
                        const VideoCardWidget(index: 8),
                        const VideoCardWidget(index: 6),
                        const VideoCardWidget(index: 5),
                        const VideoCardWidget(index: 7),
                      ],
                    ),
                  ),
                  Padding(
                    padding: Dimens.edgeInsets20,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          StringConstants.recommendedReads,
                          style: Styles.boldBlack16,
                        ),
                        Text(
                          StringConstants.viewAll,
                          style: Styles.medium12Primary,
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: Dimens.edgeInsets20_0_20_0,
                    child: CarouselSlider(
                      options: CarouselOptions(
                        disableCenter: true,
                        autoPlay: true,
                        viewportFraction: .45,
                      ),
                      items: [
                        CardTextWidget(index: 11),
                        CardTextWidget(index: 12),
                        CardTextWidget(index: 13),
                        CardTextWidget(index: 14),
                        CardTextWidget(index: 15),
                      ],
                    ),
                  ),
                  Padding(
                    padding: Dimens.edgeInsets20,
                    child: Text(
                      StringConstants.tools,
                      style: Styles.boldBlack16,
                    ),
                  ),
                  Padding(
                    padding: Dimens.edgeInsets20_0_20_0,
                    child: CarouselSlider(
                      options: CarouselOptions(
                          disableCenter: true,
                          autoPlay: true,
                          viewportFraction: .45,
                          aspectRatio: 3),
                      items: [
                        Card(
                          elevation: 0,
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(Dimens.twenty),
                          ),
                          child: Padding(
                            padding: Dimens.edgeInsets20,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Image.asset(
                                  AssetConstants.sipCalculator,
                                  width: Dimens.thirtyTwo,
                                  height: Dimens.thirtyTwo,
                                ),
                                Dimens.boxHeight10,
                                Text(
                                  StringConstants.sipCalculator,
                                  style: Styles.medium14Black,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Dimens.boxHeight20,
                ],
              ),
            ),
          ],
        ),
      );
}

class VideoCardWidget extends StatelessWidget {
  const VideoCardWidget({
    Key? key,
    required this.index,
  }) : super(key: key);
  final int index;

  @override
  Widget build(BuildContext context) => Card(
        elevation: 2,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Dimens.twelve),
        ),
        child: Container(
          height: Dimens.oneHundredTwenty,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Dimens.twelve),
            image: DecorationImage(
                image: NetworkImage('https://picsum.photos/id/$index/200/300'),
                fit: BoxFit.cover),
          ),
          child: Center(
            child: Container(
              width: Dimens.thirtyTwo,
              height: Dimens.thirtyTwo,
              decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.7),
                  borderRadius: BorderRadius.circular(Dimens.fifty)),
              child: const Icon(
                Icons.play_arrow,
                color: ColorsValue.primaryColor,
              ),
            ),
          ),
        ),
      );
}

class CardTextWidget extends StatelessWidget {
  CardTextWidget({
    Key? key,
    required this.index,
  }) : super(key: key);
  final int index;

  @override
  Widget build(BuildContext context) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            height: Dimens.percentHeight(.16),
            child: Card(
              elevation: 0,
              color: Colors.white,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(Dimens.twelve),
              ),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(Dimens.twelve),
                  image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                      'https://picsum.photos/id/$index/200/300',
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: Dimens.edgeInsets10_0_10_0,
            child: Text(
              'Card Text',
              style: Styles.medium14Black,
            ),
          ),
          Expanded(
            child: Padding(
              padding: Dimens.edgeInsets10_0_10_0,
              child: Container(
                constraints: const BoxConstraints(maxHeight: double.infinity),
                child: Text(
                  'Lorem ipsum dolor sit amet, consectetur.',
                  maxLines: 2,
                  overflow: TextOverflow.fade,
                  softWrap: true,
                  style: Styles.regular12Grey,
                ),
              ),
            ),
          ),
        ],
      );
}

class CuratedPackItem extends StatelessWidget {
  CuratedPackItem({
    Key? key,
    this.heading = 'heading',
    this.subHeading = 'subHeading',
    this.bgAssetImage = '',
    required this.onPressed,
  }) : super(key: key);

  String heading;
  String subHeading;
  String bgAssetImage;
  void Function()? onPressed;
  @override
  Widget build(BuildContext context) => Container(
        width: Dimens.hundred + Dimens.fifty,
        constraints: const BoxConstraints(maxHeight: double.infinity),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Dimens.fifteen),
            color: Colors.white,
            image: DecorationImage(
                image: AssetImage(bgAssetImage),
                alignment: Alignment.bottomRight),
            boxShadow: [
              BoxShadow(
                  blurRadius: Dimens.two,
                  offset: const Offset(0, 2),
                  color: Colors.grey.withOpacity(0.1),
                  spreadRadius: Dimens.one),
              BoxShadow(
                  blurRadius: Dimens.two,
                  offset: const Offset(-2, 0),
                  color: Colors.white.withOpacity(0.5),
                  spreadRadius: Dimens.one),
              BoxShadow(
                  blurRadius: Dimens.two,
                  offset: const Offset(2, 0),
                  color: Colors.white.withOpacity(0.5),
                  spreadRadius: Dimens.one)
            ]),
        child: Padding(
          padding: Dimens.edgeInsets20,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                heading,
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: Styles.medium18Black,
              ),
              Dimens.boxHeight10,
              Text(
                subHeading,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                style: Styles.regular12Grey,
              ),
              const Spacer(),
              GestureDetector(
                onTap: () {
                  onPressed!();
                },
                child: SvgPicture.asset(
                  AssetConstants.rightArrow,
                  height: Dimens.eighteen,
                  width: Dimens.eighteen,
                ),
              ),
              // GestureDetector(
              //   onTap: () {
              //     onPressed!();
              //   },
              //   child: CircleAvatar(
              //     radius: Dimens.fifteen,
              //     child: Icon(
              //       Icons.arrow_forward_ios,
              //       color: ColorsValue.primaryColor,
              //       size: Dimens.ten,
              //     ),
              //     backgroundColor: const Color(0xffF9F6FE),
              //   ),
              // ),
            ],
          ),
        ),
      );
}

class FirstCarouselBanner extends StatelessWidget {
  FirstCarouselBanner({
    Key? key,
    this.heading = 'heading',
    this.subHeading = 'subHeading',
    this.onTap,
  }) : super(key: key);

  String heading;
  String subHeading;
  void Function()? onTap;

  @override
  Widget build(BuildContext context) => Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Dimens.fifteen),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  blurRadius: Dimens.two,
                  offset: const Offset(0, 2),
                  color: Colors.grey.withOpacity(0.1),
                  spreadRadius: Dimens.one),
              BoxShadow(
                  blurRadius: Dimens.two,
                  offset: const Offset(-2, 0),
                  color: Colors.white.withOpacity(0.5),
                  spreadRadius: Dimens.one),
              BoxShadow(
                  blurRadius: Dimens.two,
                  offset: const Offset(2, 0),
                  color: Colors.white.withOpacity(0.5),
                  spreadRadius: Dimens.one)
            ]),
        child: Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
              alignment: Alignment.bottomRight,
              image: AssetImage(
                AssetConstants.circularDesign,
              ),
              scale: 0.8,
            ),
          ),
          width: Dimens.percentWidth(1),
          height: Dimens.percentHeight(.25),
          child: Padding(
            padding: Dimens.edgeInsets20,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  heading,
                  style: Styles.medium18Black,
                ),
                Dimens.boxHeight10,
                Text(
                  subHeading,
                  style: Styles.regular12Grey,
                ),
                Dimens.boxHeight20,
                SizedBox(
                  width: Dimens.percentWidth(.36),
                  child: FormSubmitWidget(
                    opacity: 1,
                    backgroundColor: ColorsValue.primaryColor,
                    padding: Dimens.edgeInsets15,
                    text: StringConstants.calculate,
                    textStyle: Styles.bold16White,
                    onTap: () {
                      onTap!();
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}

class ExploreFundCard extends StatelessWidget {
  ExploreFundCard({
    Key? key,
    this.heading = 'heading',
    this.subHeading = 'subHeading',
    this.assetImage = '',
    required this.onPressed,
  }) : super(key: key);

  final String heading;
  final String subHeading;
  final String assetImage;
  void Function()? onPressed;

  @override
  Widget build(BuildContext context) => Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Dimens.fifteen),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                  blurRadius: Dimens.two,
                  offset: const Offset(0, 2),
                  color: Colors.grey.withOpacity(0.1),
                  spreadRadius: Dimens.one),
              BoxShadow(
                  blurRadius: Dimens.two,
                  offset: const Offset(-2, 0),
                  color: Colors.white.withOpacity(0.5),
                  spreadRadius: Dimens.one),
              BoxShadow(
                  blurRadius: Dimens.two,
                  offset: const Offset(2, 0),
                  color: Colors.white.withOpacity(0.5),
                  spreadRadius: Dimens.one)
            ]),
        child: SizedBox(
          width: Dimens.percentWidth(1),
          height: Dimens.percentHeight(.20),
          child: Padding(
            padding: Dimens.edgeInsets20,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.asset(
                  assetImage,
                  width: Dimens.thirty,
                  height: Dimens.thirty,
                ),
                Dimens.boxHeight10,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      heading,
                      style: Styles.medium18CardHeading,
                    ),
                    const Spacer(),
                    GestureDetector(
                      onTap: () {
                        onPressed!();
                      },
                      child: SvgPicture.asset(
                        AssetConstants.rightArrow,
                        height: Dimens.eighteen,
                        width: Dimens.eighteen,
                      ),
                    ),
                  ],
                ),
                Dimens.boxHeight10,
                SizedBox(
                  width: Dimens.percentWidth(0.69),
                  child: Text(
                    subHeading,
                    style: Styles.regular12Grey,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
