import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class SectorAndThemes extends StatelessWidget {
  const SectorAndThemes({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        appBar: CustomAppBar(
          title: StringConstants.sectorAndThemes,
          actions: Row(
            children: [],
          ),
        ),
        body: Column(
          children: [
            GetBuilder<HomeController>(
              builder: (_controller) => Expanded(
                child: ListView(
                  children: [
                    const Divider(),
                    Padding(
                      padding: Dimens.edgeInsets10,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CategoryOfMutualFundCard(
                                title: StringConstants.esg,
                                subTitle:
                                    'Funds primarily invest in stocks of listed cut',
                                iconImage: AssetConstants.esg,
                                bgAssetImage: AssetConstants.looperOne,
                              ),
                              CategoryOfMutualFundCard(
                                title: 'Bank & Financial',
                                subTitle:
                                    'Funds invest in more than one asset class',
                                iconImage: AssetConstants.bank,
                                isWithoutCircleIconImage: true,
                                bgAssetImage: AssetConstants.looperOne,
                              ),
                            ],
                          ),
                          Dimens.boxHeight15,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CategoryOfMutualFundCard(
                                title: 'Technology',
                                subTitle:
                                    'Funds invest in more than one asset class',
                                iconImage: AssetConstants.technology,
                                bgAssetImage: AssetConstants.looperTwo,
                              ),
                              CategoryOfMutualFundCard(
                                title: 'Pharma',
                                subTitle: 'Funds invest in companies location',
                                iconImage: AssetConstants.shoppingBag,
                                isWithoutCircleIconImage: true,
                                bgAssetImage: AssetConstants.looperTwo,
                              ),
                            ],
                          ),
                          Dimens.boxHeight15,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CategoryOfMutualFundCard(
                                title: 'PSU',
                                subTitle:
                                    'Funds invest in physical gold, ETFs & stock market',
                                iconImage: AssetConstants.subway,
                                isWithoutCircleIconImage: true,
                                bgAssetImage: AssetConstants.looperThree,
                              ),
                              CategoryOfMutualFundCard(
                                title: 'Consumption',
                                subTitle:
                                    'Fund categories include index funds, fund and ',
                                iconImage: AssetConstants.box,
                                bgAssetImage: AssetConstants.looperThree,
                              ),
                            ],
                          ),
                          Dimens.boxHeight15,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CategoryOfMutualFundCard(
                                title: 'Energy & Power',
                                subTitle:
                                    'Funds primarily invest in stocks of listed cut',
                                iconImage: AssetConstants.renewableEnergy,
                                isWithoutCircleIconImage: true,
                                bgAssetImage: AssetConstants.looperOne,
                              ),
                              CategoryOfMutualFundCard(
                                title: 'Infrastructure',
                                subTitle:
                                    'Funds invest in more than one asset class',
                                iconImage: AssetConstants.road,
                                isWithoutCircleIconImage: true,
                                bgAssetImage: AssetConstants.looperOne,
                              ),
                            ],
                          ),
                          Dimens.boxHeight15,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CategoryOfMutualFundCard(
                                title: 'MNC',
                                subTitle:
                                    'Funds invest in more than one asset class',
                                iconImage: AssetConstants.global,
                                isWithoutCircleIconImage: true,
                                bgAssetImage: AssetConstants.looperTwo,
                              ),
                              CategoryOfMutualFundCard(
                                title: 'Service Industry',
                                subTitle: 'Funds invest in companies location',
                                iconImage: AssetConstants.easyInstallation,
                                isWithoutCircleIconImage: true,
                                bgAssetImage: AssetConstants.looperTwo,
                              ),
                            ],
                          ),
                          Dimens.boxHeight15,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CategoryOfMutualFundCard(
                                title: 'Innovation',
                                subTitle:
                                    'Funds invest in physical gold, ETFs & stock market',
                                iconImage: AssetConstants.innovation,
                                isWithoutCircleIconImage: true,
                                bgAssetImage: AssetConstants.looperThree,
                              ),
                              CategoryOfMutualFundCard(
                                title: 'Auto',
                                subTitle:
                                    'Fund categories include index funds, fund and ',
                                iconImage: AssetConstants.auto,
                                bgAssetImage: AssetConstants.looperThree,
                              ),
                            ],
                          ),
                          Dimens.boxHeight15,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CategoryOfMutualFundCard(
                                title: 'Value',
                                subTitle:
                                    'Funds primarily invest in stocks of listed cut',
                                iconImage: AssetConstants.coin,
                                isWithoutCircleIconImage: true,
                                bgAssetImage: AssetConstants.looperOne,
                              ),
                              CategoryOfMutualFundCard(
                                title: 'Business Cycle',
                                subTitle:
                                    'Funds invest in more than one asset class',
                                iconImage: AssetConstants.exchange,
                                isWithoutCircleIconImage: true,
                                bgAssetImage: AssetConstants.looperOne,
                              ),
                            ],
                          ),
                          Dimens.boxHeight15,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CategoryOfMutualFundCard(
                                title: 'Contra',
                                subTitle:
                                    'Funds invest in more than one asset class',
                                iconImage: AssetConstants.wage,
                                isWithoutCircleIconImage: true,
                                bgAssetImage: AssetConstants.looperTwo,
                              ),
                              CategoryOfMutualFundCard(
                                title: 'Dividend Yield',
                                subTitle: 'Funds invest in companies location',
                                iconImage: AssetConstants.pieChart,
                                isWithoutCircleIconImage: true,
                                bgAssetImage: AssetConstants.looperTwo,
                              ),
                            ],
                          ),
                          Dimens.boxHeight15,
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
}
