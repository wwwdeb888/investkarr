import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class AllMutualFunds extends StatefulWidget {
  const AllMutualFunds({Key? key}) : super(key: key);

  @override
  State<AllMutualFunds> createState() => _AllMutualFundsState();
}

class _AllMutualFundsState extends State<AllMutualFunds>
    with SingleTickerProviderStateMixin {
  TabController? tabController;

  @override
  void initState() {
    tabController = TabController(vsync: this, length: 6);
    super.initState();
  }

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<HomeController>(
          builder: (_controller) => DefaultTabController(
            length: 6,
            child: ColorfulSafeArea(
              color: ColorsValue.backgroundColor,
              child: Column(
                children: [
                  CustomAppBar(
                    title: StringConstants.mutualFunds,
                    actions: Row(
                      children: [
                        CircleAvatar(
                          radius: Dimens.sixTeen,
                          backgroundColor: const Color(0xffEFE8FC),
                          child: Icon(
                            Icons.search,
                            size: Dimens.twenty,
                            color: ColorsValue.primaryColor,
                          ),
                        ),
                        Dimens.boxWidth15,
                      ],
                    ),
                  ),
                  // const Divider(),
                  Container(
                    decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(
                              color: Colors.grey.withOpacity(0.2), width: 1)),
                    ),
                    child: TabBar(
                      padding: Dimens.edgeInsets0_10_0_0,
                      isScrollable: true,
                      indicatorSize: TabBarIndicatorSize.label,
                      unselectedLabelColor: ColorsValue.greyColor,
                      unselectedLabelStyle: Styles.medium16Black,
                      indicatorColor: ColorsValue.primaryColor,
                      labelColor: ColorsValue.primaryColor,
                      labelStyle: Styles.medium16Black,
                      controller: tabController,
                      tabs: [
                        Column(
                          children: [
                            Text(StringConstants.equity),
                            Dimens.boxHeight10,
                          ],
                        ),
                        Column(
                          children: [
                            Text(StringConstants.debts),
                            Dimens.boxHeight10,
                          ],
                        ),
                        Column(
                          children: [
                            Text(StringConstants.hybrid),
                            Dimens.boxHeight10,
                          ],
                        ),
                        Column(
                          children: [
                            Text(StringConstants.international),
                            Dimens.boxHeight10,
                          ],
                        ),
                        Column(
                          children: [
                            Text(StringConstants.goldFunds),
                            Dimens.boxHeight10,
                          ],
                        ),
                        Column(
                          children: [
                            Text(StringConstants.others),
                            Dimens.boxHeight10,
                          ],
                        ),
                      ],
                    ),
                  ),
                  Dimens.boxHeight20,
                  SizedBox(
                    width: Dimens.percentWidth(1),
                    height: Dimens.percentHeight(.75),
                    child: TabBarView(
                      controller: tabController,
                      children: [
                        const MutualFundsEquity(),
                        SingleChildScrollView(
                          child: SizedBox(
                            width: Dimens.percentWidth(1),
                            child: Padding(
                              padding: Dimens.edgeInsets10_0_10_0,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    StringConstants.passive,
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight20,
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                CircleAvatar(
                                                  radius: Dimens.twenty,
                                                  backgroundColor:
                                                      const Color(0xffFDF4F7),
                                                  child: Image.asset(
                                                    AssetConstants.indexFunds,
                                                    width: Dimens.twentyFive,
                                                    height: Dimens.twentyFive,
                                                  ),
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  StringConstants.equity,
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds primarily invest in stocks of listed c...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                CircleAvatar(
                                                  radius: Dimens.twenty,
                                                  backgroundColor:
                                                      const Color(0xffFDF4F7),
                                                  child: Image.asset(
                                                    AssetConstants.fofs,
                                                    width: Dimens.twentyFive,
                                                    height: Dimens.twentyFive,
                                                  ),
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  StringConstants.debts,
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds invest in more than one asset class',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight20,
                                  Text(
                                    'Short Duration',
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight20,
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.equity,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Overnight',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds lend companies for 1 business day',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.debt,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Low Duration Fund',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds lend companies for 6 - 12 months',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight20,
                                  Text(
                                    'Medium to Long Duration',
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight20,
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.equity,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Medium Fund',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds lend companies for  3 - 4 years',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.debt,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Medium to Long..',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds lend companies for 4 - 7 years',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight20,
                                  Text(
                                    'Credit Specific',
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight20,
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.equity,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Credit Risk Fund',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds lend  to not so high rated companies.',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.debt,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Corporate Fund',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds lend to corporates that...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight20,
                                  Text(
                                    'Issuer Specific',
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight20,
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.equity,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Gilt',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds primarily invest in government s...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.debt,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Gilt with 10 yr c..',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds primarily invest in government s...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight20,
                                  Text(
                                    'Others',
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight20,
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.equity,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Dynamic',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds invest across all duration funds',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.debt,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Floater',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds invest at least 65% of their mon...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight10,
                                  Text(
                                    'Top Equity Funds',
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight10,
                                  ListView.separated(
                                    itemCount: 3,
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    separatorBuilder: (context, index) =>
                                        Dimens.boxHeight10,
                                    itemBuilder: (context, index) => SmallFundCard(
                                        title:
                                            'DSP Tax Saver Direct Plan growth',
                                        bankIcon: AssetConstants.hdfcLogo,
                                        percentage: '24.3',
                                        isGrowth: true,
                                        returns: '3Y returns'),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SingleChildScrollView(
                          child: SizedBox(
                            width: Dimens.percentWidth(1),
                            child: Padding(
                              padding: Dimens.edgeInsets10_0_10_0,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Risk Based',
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight20,
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                CircleAvatar(
                                                  radius: Dimens.twenty,
                                                  backgroundColor:
                                                      const Color(0xffFDF4F7),
                                                  child: Image.asset(
                                                    AssetConstants.indexFunds,
                                                    width: Dimens.twentyFive,
                                                    height: Dimens.twentyFive,
                                                  ),
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Conservative Funds',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds primarily invest in debt instruments',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                CircleAvatar(
                                                  radius: Dimens.twenty,
                                                  backgroundColor:
                                                      const Color(0xffFDF4F7),
                                                  child: Image.asset(
                                                    AssetConstants.fofs,
                                                    width: Dimens.twentyFive,
                                                    height: Dimens.twentyFive,
                                                  ),
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Balanced Funds',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds primarily invest in equity and de...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight20,
                                  Text(
                                    'Dynamic',
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight20,
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.equity,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Dynamic Asset All..',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds dynamically change allocatio...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.debt,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Balanced Advanta..',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds invest equally in equity and debt ...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight20,
                                  Text(
                                    'Others',
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight20,
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.equity,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Arbitrage',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds buy and sell securities simult...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.debt,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Equity Savings',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds invest in equity, debti and arbitra...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight20,
                                  Text(
                                    'Hybrid - FOFs',
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight20,
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.equity,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Index Funds',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds are track/replicate a partic...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.equity,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'FOFs',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Invest other investment funds rather than...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight10,
                                  Text(
                                    'Top Equity Funds',
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight10,
                                  ListView.separated(
                                    itemCount: 3,
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    separatorBuilder: (context, index) =>
                                        Dimens.boxHeight10,
                                    itemBuilder: (context, index) => SmallFundCard(
                                        title:
                                            'DSP Tax Saver Direct Plan growth',
                                        bankIcon: AssetConstants.hdfcLogo,
                                        percentage: '24.3',
                                        isGrowth: true,
                                        returns: '3Y returns'),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SingleChildScrollView(
                          child: SizedBox(
                            width: Dimens.percentWidth(1),
                            child: Padding(
                              padding: Dimens.edgeInsets10_0_10_0,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    StringConstants.passive,
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight20,
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                CircleAvatar(
                                                  radius: Dimens.twenty,
                                                  backgroundColor:
                                                      const Color(0xffFDF4F7),
                                                  child: Image.asset(
                                                    AssetConstants.indexFunds,
                                                    width: Dimens.twentyFive,
                                                    height: Dimens.twentyFive,
                                                  ),
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Index Funds',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds are track/replicate a partic...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                CircleAvatar(
                                                  radius: Dimens.twenty,
                                                  backgroundColor:
                                                      const Color(0xffFDF4F7),
                                                  child: Image.asset(
                                                    AssetConstants.fofs,
                                                    width: Dimens.twentyFive,
                                                    height: Dimens.twentyFive,
                                                  ),
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'FOFs',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Invest other investment funds rather than...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight20,
                                  Text(
                                    'Sector & Themes',
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight20,
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.equity,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'ESG',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds invest in banking companies',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Image.asset(
                                                  AssetConstants.debt,
                                                  width: Dimens.fourty,
                                                  height: Dimens.fourty,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Banking',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds invest in dividend yielding stocks ',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight10,
                                  Text(
                                    'Top Equity Funds',
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight10,
                                  ListView.separated(
                                    itemCount: 3,
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    separatorBuilder: (context, index) =>
                                        Dimens.boxHeight10,
                                    itemBuilder: (context, index) => SmallFundCard(
                                        title:
                                            'DSP Tax Saver Direct Plan growth',
                                        bankIcon: AssetConstants.hdfcLogo,
                                        percentage: '24.3',
                                        isGrowth: true,
                                        returns: '3Y returns'),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SingleChildScrollView(
                          child: SizedBox(
                            width: Dimens.percentWidth(1),
                            child: Padding(
                              padding: Dimens.edgeInsets10_0_10_0,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Gold - FOFs',
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight20,
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                CircleAvatar(
                                                  radius: Dimens.twenty,
                                                  backgroundColor:
                                                      const Color(0xffFDF4F7),
                                                  child: Image.asset(
                                                    AssetConstants.indexFunds,
                                                    width: Dimens.twentyFive,
                                                    height: Dimens.twentyFive,
                                                  ),
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Index Funds',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds are track/replicate a partic...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                CircleAvatar(
                                                  radius: Dimens.twenty,
                                                  backgroundColor:
                                                      const Color(0xffFDF4F7),
                                                  child: Image.asset(
                                                    AssetConstants.fofs,
                                                    width: Dimens.twentyFive,
                                                    height: Dimens.twentyFive,
                                                  ),
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'FOFs',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Invest other investment funds rather than...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight10,
                                  Text(
                                    'Top Equity Funds',
                                    style: Styles.bold16Black,
                                  ),
                                  ListView.separated(
                                    itemCount: 10,
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    separatorBuilder: (context, index) =>
                                        Dimens.boxHeight10,
                                    itemBuilder: (context, index) => SmallFundCard(
                                        title:
                                            'DSP Tax Saver Direct Plan growth',
                                        bankIcon: AssetConstants.hdfcLogo,
                                        percentage: '24.3',
                                        isGrowth: true,
                                        returns: '3Y returns'),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SingleChildScrollView(
                          child: SizedBox(
                            width: Dimens.percentWidth(1),
                            child: Padding(
                              padding: Dimens.edgeInsets10_0_10_0,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                CircleAvatar(
                                                  radius: Dimens.twenty,
                                                  backgroundColor:
                                                      const Color(0xffFDF4F7),
                                                  child: Image.asset(
                                                    AssetConstants.indexFunds,
                                                    width: Dimens.twentyFive,
                                                    height: Dimens.twentyFive,
                                                  ),
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Index Funds',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds are track/replicate a partic...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                CircleAvatar(
                                                  radius: Dimens.twenty,
                                                  backgroundColor:
                                                      const Color(0xffFDF4F7),
                                                  child: Image.asset(
                                                    AssetConstants.fofs,
                                                    width: Dimens.twentyFive,
                                                    height: Dimens.twentyFive,
                                                  ),
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'FOFs',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Invest other investment funds rather than...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight20,
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                CircleAvatar(
                                                  radius: Dimens.twenty,
                                                  backgroundColor:
                                                      const Color(0xffFDF4F7),
                                                  child: Image.asset(
                                                    AssetConstants.indexFunds,
                                                    width: Dimens.twentyFive,
                                                    height: Dimens.twentyFive,
                                                  ),
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Retirement Funds',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds have a lock in for at least 5 years o...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Card(
                                        color: Colors.white,
                                        shape: RoundedRectangleBorder(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10,
                                          child: SizedBox(
                                            width: Dimens.percentWidth(.39),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                CircleAvatar(
                                                  radius: Dimens.twenty,
                                                  backgroundColor:
                                                      const Color(0xffFDF4F7),
                                                  child: Image.asset(
                                                    AssetConstants.fofs,
                                                    width: Dimens.twentyFive,
                                                    height: Dimens.twentyFive,
                                                  ),
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Children’s Fund',
                                                  style: Styles.boldBlack15,
                                                ),
                                                Dimens.boxHeight10,
                                                Text(
                                                  'Funds act as a solution oriented plan for ...',
                                                  style: Styles.black12,
                                                  maxLines: 2,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight10,
                                  Text(
                                    'Top Equity Funds',
                                    style: Styles.bold16Black,
                                  ),
                                  Dimens.boxHeight10,
                                  ListView.separated(
                                    itemCount: 10,
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    separatorBuilder: (context, index) =>
                                        Dimens.boxHeight10,
                                    itemBuilder: (context, index) => SmallFundCard(
                                        title:
                                            'DSP Tax Saver Direct Plan growth',
                                        bankIcon: AssetConstants.hdfcLogo,
                                        percentage: '24.3',
                                        isGrowth: true,
                                        returns: '3Y returns'),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
}
