import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/goal_summary.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';
import 'increase_annual_basis.dart';

class SipCreate extends StatelessWidget {
  SipCreate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          appBar: CustomAppBar(
            title: 'settingAGoal'.tr,
          ),
          body: SafeArea(
            child: Container(
              padding: Dimens.edgeInsets16,
              width: Get.width,
              child: CustomScrollView(
                slivers: [
                  SliverFillRemaining(
                    hasScrollBody: false,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Dimens.boxHeight30,
                        CircleIcon(
                          image: 'assets/home/car-compact 1.svg',
                          color: Colors.orange.withOpacity(.2),
                        ),
                        Dimens.boxHeight20,
                        SizedBox(
                          width: Dimens.percentWidth(0.7),
                          child: Text(
                            'addAnInitialAmountToYourGoalSIP'.tr,
                            textAlign: TextAlign.center,
                            style: Styles.bold24Black,
                          ),
                        ),
                        Dimens.boxHeight50,

                        Padding(
                          padding: Dimens.edgeInsets5_0_5_0 +
                              Dimens.edgeInsets2_0_2_0 +
                              Dimens.edgeInsets0_0_2_0,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Do you wish to add initial amount?',
                                style: Styles.medium14Black
                                    .copyWith(color: ColorsValue.textColor),
                              ),
                              FlutterSwitch(
                                padding: Dimens.three,
                                activeColor: ColorsValue.primaryColor,
                                inactiveColor: const Color(0xffEBDDF8),
                                width: Dimens.fourtyFive,
                                toggleColor: _controller.wishToInitialSipAmount
                                    ? Colors.white
                                    : ColorsValue.primaryColor,
                                height: Dimens.twentyFour,
                                toggleSize: Dimens.twenty,
                                value: _controller.wishToInitialSipAmount,
                                onToggle: (val) {
                                  _controller.wishToInitialSipAmount = val;
                                  _controller.update();
                                },
                              ),
                            ],
                          ),
                        ),
                        if (_controller.wishToInitialSipAmount)
                          Dimens.boxHeight40,

                        /// next slider
                        if (_controller.wishToInitialSipAmount)
                          Column(
                            children: [
                              Padding(
                                padding: Dimens.edgeInsets5_0_5_0 +
                                    Dimens.edgeInsets5_0_5_0 +
                                    Dimens.edgeInsets0_0_5_0,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Amount'.tr,
                                      style: Styles.medium14Black.copyWith(
                                          color: ColorsValue.textColor),
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          '\u{20B9}',
                                          style: Styles.medium16Black,
                                        ),
                                        Dimens.boxWidth10,
                                        SizedBox(
                                          width: Dimens.percentWidth(0.2),
                                          child: FormFieldWidget(
                                            onChange: (value) {},
                                            elevation: 0,
                                            initialValue: '25,000',
                                            textAlign: TextAlign.end,
                                            formStyle: Styles.medium16Black,
                                            borderColor: ColorsValue.greyColor
                                                .withOpacity(0.5),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Dimens.boxHeight10,
                              SizedBox(
                                width: Dimens.percentWidth(1),
                                child: SliderTheme(
                                  data: SliderTheme.of(context).copyWith(
                                    activeTrackColor:
                                        ColorsValue.activeTrackColor,
                                    inactiveTrackColor:
                                        ColorsValue.inactiveTrackColor,
                                    trackHeight: 5,
                                    thumbShape: const SliderThumbShape(),
                                    overlayShape: const RoundSliderOverlayShape(
                                        overlayRadius: 15.0),
                                  ),
                                  child: Slider(
                                    activeColor: ColorsValue.primaryColor,
                                    min: 0,
                                    max: 10,
                                    value: _controller.initialSipAmount,
                                    onChanged: (value) {
                                      _controller.initialSipAmount = value;
                                      _controller.update();
                                      debugPrint(value.toString());
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        Dimens.boxHeight25,
                        const Spacer(),
                        Container(
                          padding: Dimens.edgeInsets10,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Icon(
                                Icons.info_outline_rounded,
                                size: Dimens.fifteen,
                              ),
                              Dimens.boxWidth10,
                              Expanded(
                                child: Text(
                                  'Try adding a small amount initially to reduce the monthly SIP',
                                  style: Styles.regular12Grey,
                                ),
                              ),
                            ],
                          ),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            color: ColorsValue.lightBgContainerColor,
                          ),
                        ),
                        Dimens.boxHeight10,
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                            padding: Dimens.edgeInsets5,
                            child: SizedBox(
                              width: Dimens.percentWidth(1),
                              height: Dimens.percentHeight(.07),
                              child: ElevatedButton(
                                style:
                                    Styles.elevatedButtonTheme.style!.copyWith(
                                  backgroundColor:
                                      MaterialStateProperty.resolveWith<Color>(
                                    (Set<MaterialState> states) =>
                                        (_controller.isFutureInflows)
                                            ? ColorsValue.primaryColor
                                            : ColorsValue.primaryColor
                                                .withOpacity(.5),
                                  ),
                                  textStyle: MaterialStateProperty.all(
                                    Styles.bold16White,
                                  ),
                                ),
                                onPressed: () {
                                  if (_controller.sipInvestment) {
                                    Get.to(const GoalSummary());
                                  } else {
                                    Get.to(IncreaseAnnualBasic());
                                  }
                                },
                                child: Text(
                                  'Continue'.tr,
                                  style: Styles.bold16White,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
