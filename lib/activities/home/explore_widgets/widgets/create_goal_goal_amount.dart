import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/create_goal_plan_to_buy.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';
import 'create_goal_goal_amount_types_of_investment.dart';

class CreateGoalAmount extends StatelessWidget {
  CreateGoalAmount({Key? key, this.isEditingFromSummary = false})
      : super(key: key);
  final bool isEditingFromSummary;

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          appBar: CustomAppBar(
            title:
                isEditingFromSummary ? 'Edit Goal Summary' : 'settingAGoal'.tr,
          ),
          body: SafeArea(
            child: Container(
              padding: Dimens.edgeInsets16,
              width: Get.width,
              child: CustomScrollView(
                slivers: [
                  SliverFillRemaining(
                    hasScrollBody: false,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Dimens.boxHeight30,
                        CircleIcon(
                          image: 'assets/home/car-compact 1.svg',
                          color: Colors.orange.withOpacity(.2),
                        ),
                        Dimens.boxHeight20,
                        Text(
                          'goalAmount'.tr,
                          style: Styles.regular14Grey,
                        ),
                        Dimens.boxHeight15,
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              '\u{20b9}'.tr,
                              style: Styles.medium26Black,
                            ),
                            Dimens.boxWidth5,
                            Padding(
                              padding: Dimens.edgeInsets0_0_0_5,
                              child: Text(
                                '5,35,000'.tr,
                                style: Styles.bold24Black,
                              ),
                            ),
                          ],
                        ),
                        Dimens.boxHeight10,
                        Container(
                          padding: Dimens.edgeInsets10,
                          child: Text(
                            'Current value + 7% Inflation',
                            style: Styles.regular12Grey,
                          ),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            color: ColorsValue.lightBgContainerColor,
                          ),
                        ),
                        Dimens.boxHeight30,
                        Row(
                          children: [
                            Text(
                              'selectPaymentMethod'.tr,
                              style: Styles.medium14Grey,
                            ),
                            Dimens.boxWidth2,
                            Icon(
                              Icons.info_outline_rounded,
                              size: Dimens.fifteen,
                            ),
                          ],
                        ),
                        Dimens.boxHeight20,
                        Row(
                          children: [
                            Expanded(
                              child: BoxCheckSelector(
                                  height: Dimens.fifty,
                                  onTap: () {
                                    _controller.fullPaymentMethod = true;
                                    _controller.loanPaymentMethod = false;
                                    _controller.update();
                                  },
                                  isChecked: _controller.fullPaymentMethod,
                                  title: 'Full Payment'),
                            ),
                            Dimens.boxWidth15,
                            Expanded(
                              child: BoxCheckSelector(
                                  height: Dimens.fifty,
                                  onTap: () {
                                    _controller.fullPaymentMethod = false;
                                    _controller.loanPaymentMethod = true;
                                    _controller.update();
                                  },
                                  isChecked: _controller.loanPaymentMethod,
                                  title: 'Loan'),
                            ),
                          ],
                        ),
                        Dimens.boxHeight30,
                        if (_controller.loanPaymentMethod)
                          Column(
                            children: [
                              Padding(
                                padding: Dimens.edgeInsets5_0_5_0 +
                                    Dimens.edgeInsets5_0_5_0 +
                                    Dimens.edgeInsets0_0_5_0,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Loan amount'.tr,
                                      style: Styles.medium14Black.copyWith(
                                          color: ColorsValue.textColor),
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          '\u{20B9}',
                                          style: Styles.medium16Black,
                                        ),
                                        Dimens.boxWidth10,
                                        SizedBox(
                                          width: Dimens.percentWidth(0.2),
                                          child: FormFieldWidget(
                                            onChange: (value) {},
                                            elevation: 0,
                                            initialValue: '1,00,000',
                                            textAlign: TextAlign.end,
                                            formStyle: Styles.medium16Black,
                                            borderColor: ColorsValue.greyColor
                                                .withOpacity(0.5),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Dimens.boxHeight10,
                              SizedBox(
                                width: Dimens.percentWidth(1),
                                child: SliderTheme(
                                  data: SliderTheme.of(context).copyWith(
                                    activeTrackColor:
                                        ColorsValue.activeTrackColor,
                                    inactiveTrackColor:
                                        ColorsValue.inactiveTrackColor,
                                    trackHeight: 5,
                                    thumbShape: const SliderThumbShape(),
                                    overlayShape: const RoundSliderOverlayShape(
                                        overlayRadius: 15.0),
                                  ),
                                  child: Slider(
                                    activeColor: ColorsValue.primaryColor,
                                    min: 0,
                                    max: 10,
                                    value: _controller.loanAmount,
                                    onChanged: (value) {
                                      _controller.loanAmount = value;
                                      _controller.update();
                                      debugPrint(value.toString());
                                    },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        const Spacer(),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                            padding: Dimens.edgeInsets5,
                            child: SizedBox(
                              width: Dimens.percentWidth(1),
                              height: Dimens.percentHeight(.07),
                              child: ElevatedButton(
                                style:
                                    Styles.elevatedButtonTheme.style!.copyWith(
                                  backgroundColor:
                                      MaterialStateProperty.resolveWith<Color>(
                                    (Set<MaterialState> states) => (0 == 0)
                                        ? ColorsValue.primaryColor
                                        : ColorsValue.primaryColor
                                            .withOpacity(.5),
                                  ),
                                  textStyle: MaterialStateProperty.all(
                                    Styles.bold16White,
                                  ),
                                ),
                                onPressed: () {
                                  if (isEditingFromSummary) {
                                    Get.back<void>();
                                  } else {
                                    Get.to(
                                      CreateGoalAmountTypeOfInvestment(
                                          isLoan:
                                              _controller.loanPaymentMethod),
                                    );
                                  }
                                },
                                child: Text(
                                  isEditingFromSummary ? 'Save' : 'Continue'.tr,
                                  style: Styles.bold16White,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
