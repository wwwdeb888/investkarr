import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class WealthCreationScreen extends StatelessWidget {
  const WealthCreationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Stack(
          children: [
            Container(
              height: Get.height * .35,
              width: Dimens.percentWidth(1),
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    AssetConstants.goalBasedBg,
                  ),
                  alignment: Alignment.centerRight,
                ),
                color: ColorsValue.secondaryColor,
              ),
            ),
            Scaffold(
              backgroundColor: Colors.transparent,
              appBar: const CustomAppBar(
                title: '',
              ),
              body: SafeArea(
                child: Padding(
                  padding: Dimens.edgeInsets16,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Wealth Creation'.tr,
                        style: Styles.bold24Black,
                      ),
                      Dimens.boxHeight10,
                      Text(
                        'Let us jump-start your wealth creation journey with our research-backed solution.'
                            .tr,
                        style: Styles.regular14Grey,
                      ),
                      Dimens.boxHeight25,
                      DefaultContainer(
                        child: Column(
                          children: [
                            SvgPicture.asset(
                                'assets/home/wealth_creation_screen.svg'),
                            Dimens.boxHeight25,
                            const PointBulletsWidget(
                              title:
                                  'Different asset classes react differently in response to changes in market and economic conditions',
                            ),
                            Dimens.boxHeight25,
                            const PointBulletsWidget(
                              title:
                                  'Having an appropriate asset allocation can help manage fluctuations in financial.',
                            ),
                            Dimens.boxHeight25,
                            const PointBulletsWidget(
                              title:
                                  'It\'s time we help you focus on dividing your investments among different asset classes that work best for you.',
                            ),
                          ],
                        ),
                      ),
                      const Spacer(),
                      SizedBox(
                        width: Get.width,
                        child: CustomButton(
                          title: 'Continue'.tr,
                          onTap: () {
                            Get.to(const WealthCreationUpgradePremium());
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}
