import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class WealthCreationSUmmary extends StatelessWidget {
  const WealthCreationSUmmary({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
      builder: (_controller) => Scaffold(
            appBar: const CustomAppBar(
              title: '',
            ),
            body: SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: Dimens.edgeInsets16,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Wealth creation',
                            style: Styles.bold24Black
                                .copyWith(color: ColorsValue.primaryColor),
                          ),
                          Dimens.boxHeight25,
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(
                                '17.22%',
                                style: Styles.medium24Black,
                              ),
                              Dimens.boxWidth12,
                              Padding(
                                padding: Dimens.edgeInsets0_0_0_5,
                                child: Text(
                                  '3Y annualised ',
                                  style: Styles.regular12Grey,
                                ),
                              ),
                            ],
                          ),
                          Dimens.boxHeight35,
                          Align(
                            alignment: Alignment.center,
                            child: Container(
                              width: Dimens.percentWidth(0.48),
                              padding: Dimens.edgeInsets2,
                              alignment: Alignment.center,
                              color: Colors.white,
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    ' NAV : 65.19 ',
                                    style: Styles.medium12Black,
                                  ),
                                  Dimens.boxWidth5,
                                  Container(
                                    width: Dimens.one,
                                    height: Dimens.ten,
                                    color: ColorsValue.greyColor,
                                  ),
                                  Dimens.boxWidth5,
                                  Text(
                                    '24th Jan 2020',
                                    style: Styles.regular12Black,
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Dimens.boxHeight10,
                          Container(
                            height: 200,
                            width: Get.width,
                            color: Colors.pink.withOpacity(.3),
                          ),
                          Dimens.boxHeight10,
                          const Divider(),
                          Dimens.boxHeight10,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Funds in the basket',
                                style: Styles.bold16Black,
                              ),
                              GestureDetector(
                                onTap: () {
                                  showModalBottomSheet(
                                    isScrollControlled: true,
                                    backgroundColor: Colors.transparent,
                                    context: context,
                                    builder: (ctx) => Container(
                                      decoration: const BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(25.0),
                                          topRight: Radius.circular(25.0),
                                        ),
                                      ),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Dimens.boxHeight25,
                                          SvgPicture.asset(
                                            'assets/home/settings.svg',
                                          ),
                                          Dimens.boxHeight10,
                                          Text(
                                            'Customize the asset classes',
                                            style: Styles.bold16Black,
                                          ),
                                          Dimens.boxHeight10,
                                          Padding(
                                            padding:
                                                Dimens.edgeInsets16_0_16_0 +
                                                    Dimens.edgeInsets16_0_16_0 +
                                                    Dimens.edgeInsets16_0_16_0 +
                                                    Dimens.edgeInsets16_0_16_0,
                                            child: Text(
                                              'Not satisfied? Increase or decrease the allocation to any asset class',
                                              style: Styles.regular12Black,
                                              textAlign: TextAlign.center,
                                            ),
                                          ),
                                          const Tile(
                                            icon: 'assets/home/equity.svg',
                                            title: 'Equity',
                                            amount: '15',
                                          ),
                                          const Tile(
                                            icon: 'assets/home/equity.svg',
                                            title: 'Debt',
                                            amount: '15',
                                          ),
                                          const Tile(
                                            icon: 'assets/home/equity.svg',
                                            title: 'Money Market',
                                            amount: '15',
                                          ),
                                          const Tile(
                                            icon: 'assets/home/equity.svg',
                                            title: 'Gold',
                                            amount: '15',
                                          ),
                                          Row(
                                            children: [
                                              Expanded(
                                                child: BorderedButton(
                                                  text: 'Cancel',
                                                  onTap: () {},
                                                ),
                                              ),
                                              Dimens.boxWidth10,
                                              Expanded(
                                                  child: GlobalButton(
                                                text: 'Save',
                                                onTap: () {},
                                              )),
                                            ],
                                          ),
                                          Dimens.boxHeight25,
                                        ],
                                      ),
                                    ),
                                  );
                                },
                                child: Container(
                                  padding: const EdgeInsets.symmetric(
                                    vertical: 6,
                                    horizontal: 20,
                                  ),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 1,
                                      color: ColorsValue.primaryColor,
                                    ),
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(50),
                                    ),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.settings_outlined,
                                        size: Dimens.fifteen,
                                        color: ColorsValue.primaryColor,
                                      ),
                                      Dimens.boxWidth8,
                                      Text(
                                        'Customize'.tr,
                                        style: Styles.bold14Primary,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Container(
                            margin: const EdgeInsets.only(
                              left: 16,
                              top: 16,
                            ),
                            child: DefaultTabController(
                              length: 4,
                              child: Column(
                                children: [
                                  Container(
                                    padding: Dimens.edgeInsets7,
                                    decoration: BoxDecoration(
                                      color: ColorsValue.primaryColor
                                          .withOpacity(.1),
                                      borderRadius: const BorderRadius.only(
                                        topLeft: Radius.circular(50),
                                        bottomLeft: Radius.circular(50),
                                      ),
                                    ),
                                    child: TabBar(
                                      labelPadding: Dimens.edgeInsets16,
                                      indicator: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: const BorderRadius.all(
                                          Radius.circular(50),
                                        ),
                                        boxShadow: [
                                          BoxShadow(
                                            color: Colors.grey.withOpacity(0.1),
                                            spreadRadius: 4,
                                            blurRadius: 3,
                                            offset: const Offset(0, 2),
                                          ),
                                        ],
                                      ),
                                      tabs: [
                                        Text(
                                          'all'.tr,
                                          style: Styles.bold13Primary,
                                        ),
                                        Column(
                                          children: [
                                            Text(
                                              'equity'.tr,
                                              style: Styles.bold13Primary,
                                            ),
                                            Text(
                                              '95.9%'.tr,
                                              style: Styles.black12,
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: [
                                            Text(
                                              'Debt'.tr,
                                              style: Styles.bold13Primary,
                                            ),
                                            Text(
                                              '95.9%'.tr,
                                              style: Styles.black12,
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: [
                                            Text(
                                              'Money Market'.tr,
                                              overflow: TextOverflow.ellipsis,
                                              style: Styles.bold13Primary,
                                            ),
                                            Text(
                                              '95.9%'.tr,
                                              style: Styles.black12,
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  Dimens.boxHeight10,
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding: Dimens.edgeInsets10_0_10_0,
                            child: Row(
                              children: [
                                Expanded(
                                  child: Container(
                                    height: Dimens.oneHundredFifty,
                                    decoration: BoxDecoration(
                                      color: Colors.amber.withOpacity(.3),
                                      borderRadius: const BorderRadius.all(
                                        Radius.circular(12),
                                      ),
                                    ),
                                  ),
                                ),
                                Dimens.boxWidth20,
                                Expanded(
                                  child: Column(
                                    children: [
                                      const ContainerType1(
                                        color: Colors.purple,
                                        title: 'Goal Based MF',
                                        value: '90.0',
                                      ),
                                      Dimens.boxHeight10,
                                      const ContainerType1(
                                        color: Colors.red,
                                        title: 'Our Solutions',
                                        value: '4',
                                      ),
                                      Dimens.boxHeight10,
                                      const ContainerType1(
                                        color: Colors.yellow,
                                        title: 'Direct MF',
                                        value: '4',
                                      ),
                                      Dimens.boxHeight10,
                                      const ContainerType1(
                                        color: Colors.yellow,
                                        title: 'Wealth Creation',
                                        value: '4',
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    ListView.builder(
                      itemCount: 3,
                      physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemBuilder: (context, index) => Container(
                        padding: Dimens.edgeInsets7,
                        child: DefaultContainer(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                height: Dimens.fourty,
                                width: Dimens.fourty,
                                child: Image.asset(
                                  AssetConstants.hdfcLogo,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              Dimens.boxWidth10,
                              Expanded(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      width: Dimens.percentWidth(0.4),
                                      child: Expanded(
                                        child: Text(
                                          'Quant Tax Plan Direct Growth Plan',
                                          style: Styles.medium14Black,
                                        ),
                                      ),
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios_rounded,
                                      size: Dimens.fifteen,
                                    ),
                                  ],
                                ),
                              ),
                              Dimens.boxWidth15,
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    '40%',
                                    style: Styles.medium16Primary,
                                  ),
                                  Text(
                                    'Technology',
                                    style: Styles.regular12Black,
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: Dimens.edgeInsets16,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Divider(),
                          Dimens.boxHeight10,
                          Text(
                            'Return Calculator',
                            style: Styles.bold16Black,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: Dimens.edgeInsets16_0_16_0,
                      child: DefaultContainer(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Dimens.boxHeight25,
                            Text(
                              'Total investment of 65K',
                              style: Styles.regular12Black,
                            ),
                            Dimens.boxHeight10,
                            Text(
                              'Would have become 1.19 (37.85%)',
                              style: Styles.regular14Black,
                            ),
                            Dimens.boxHeight10,
                            Row(
                              children: [
                                Chip(
                                  backgroundColor: Color(0xffF3EDFC),
                                  label: Text(
                                    'Monthly SIP',
                                    style: Styles.medium12Primary,
                                  ),
                                ),
                                Dimens.boxWidth10,
                                Chip(
                                  backgroundColor: Colors.white,
                                  label: Text(
                                    'Lumpsum',
                                    style: Styles.medium12Primary,
                                  ),
                                ),
                              ],
                            ),
                            Dimens.boxHeight25,
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                Text(
                                  '2,000',
                                  style:
                                      Styles.bold16Black.copyWith(height: .5),
                                ),
                                Dimens.boxWidth10,
                                Text(
                                  'per month',
                                  style: Styles.medium12Black,
                                ),
                              ],
                            ),
                            Dimens.boxHeight10,
                            SizedBox(
                              width: Dimens.percentWidth(1),
                              child: SliderTheme(
                                data: SliderTheme.of(context).copyWith(
                                  activeTrackColor:
                                      ColorsValue.activeTrackColor,
                                  inactiveTrackColor:
                                      ColorsValue.inactiveTrackColor,
                                  trackHeight: 5,
                                  thumbShape: const SliderThumbShape(),
                                  overlayShape: const RoundSliderOverlayShape(
                                      overlayRadius: 15.0),
                                ),
                                child: Slider(
                                  activeColor: ColorsValue.primaryColor,
                                  min: 0,
                                  max: 10,
                                  value: _controller.buyYearsPlan,
                                  onChanged: (value) {
                                    _controller.buyYearsPlan = value;
                                    _controller.update();
                                    debugPrint(value.toString());
                                  },
                                ),
                              ),
                            ),
                            Dimens.boxHeight10,
                            Text(
                              'No.of years',
                              style: Styles.medium14Black,
                            ),
                            Dimens.boxHeight10,
                            Row(
                              children: [
                                Expanded(
                                  child: Chip(
                                    backgroundColor: Color(0xffF3EDFC),
                                    label: Text(
                                      '1Y',
                                      style: Styles.medium12Primary,
                                    ),
                                  ),
                                ),
                                Dimens.boxWidth10,
                                Expanded(
                                  child: Chip(
                                    backgroundColor: Colors.white,
                                    label: Text(
                                      '3Y',
                                      style: Styles.medium12Primary,
                                    ),
                                  ),
                                ),
                                Dimens.boxWidth10,
                                Expanded(
                                  child: Chip(
                                    backgroundColor: Colors.white,
                                    label: Text(
                                      '5Y',
                                      style: Styles.medium12Primary,
                                    ),
                                  ),
                                ),
                                Dimens.boxWidth10,
                                Expanded(
                                  child: Chip(
                                    backgroundColor: Colors.white,
                                    label: Text(
                                      '10Y',
                                      style: Styles.medium12Primary,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    Dimens.boxHeight25,
                    Row(
                      children: [
                        Dimens.boxWidth10,
                        Expanded(
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 35,
                              vertical: 10,
                            ),
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(50),
                              ),
                              color: ColorsValue.primaryColor,
                            ),
                            child: Column(
                              children: [
                                Text(
                                  'Monthy SIP',
                                  style: Styles.bold16White,
                                ),
                                Dimens.boxHeight3,
                                Text(
                                  'Min. 500',
                                  style: Styles.medium11White,
                                )
                              ],
                            ),
                          ),
                        ),
                        Dimens.boxWidth10,
                        Expanded(
                          child: Container(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 35,
                              vertical: 10,
                            ),
                            decoration: const BoxDecoration(
                              borderRadius: BorderRadius.all(
                                Radius.circular(50),
                              ),
                              color: ColorsValue.primaryColor,
                            ),
                            child: Column(
                              children: [
                                Text('Lumpsum', style: Styles.bold16White),
                                Dimens.boxHeight3,
                                Text(
                                  'Min. 500',
                                  style: Styles.medium11White,
                                )
                              ],
                            ),
                          ),
                        ),
                        Dimens.boxWidth10,
                      ],
                    ),
                    Dimens.boxHeight20,
                  ],
                ),
              ),
            ),
          ));
}

class Tile extends StatelessWidget {
  const Tile({
    Key? key,
    this.icon,
    this.title,
    this.amount,
  }) : super(key: key);

  final String? icon;
  final String? title;
  final String? amount;

  @override
  Widget build(BuildContext context) => Padding(
        padding: Dimens.edgeInsets7,
        child: Row(
          children: [
            Dimens.boxWidth10,
            SvgPicture.asset(icon!),
            Dimens.boxWidth10,
            Text(
              '$title',
              style:
                  Styles.medium16Black.copyWith(color: ColorsValue.textColor),
            ),
            const Spacer(),
            Expanded(
              child: DefaultContainer(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 6),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text(
                      '-',
                      style: Styles.boldBlack22,
                    ),
                    Text(
                      '$amount%',
                      style: Styles.bold14Grey,
                    ),
                    Text(
                      '+',
                      style: Styles.boldBlack22,
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      );
}
