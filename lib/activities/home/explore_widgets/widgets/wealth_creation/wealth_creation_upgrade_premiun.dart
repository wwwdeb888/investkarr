import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/wealth_creation/wealth_creation_summary.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';
import 'package:investkarr/widgets/curated_packs_summary.dart';

class WealthCreationUpgradePremium extends StatelessWidget {
  const WealthCreationUpgradePremium({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Stack(
          children: [
            Container(
              height: Get.height * .35,
              color: ColorsValue.primaryColor.withOpacity(.2),
            ),
            Scaffold(
              backgroundColor: Colors.transparent,
              appBar: const CustomAppBar(
                title: '',
              ),
              bottomNavigationBar: GestureDetector(
                onTap: () {
                  Get.to(const WealthCreationSUmmary());
                },
                child: SizedBox(
                  // padding: Dimens.edgeInsets16,
                  width: Get.width,
                  child: SvgPicture.asset('assets/home/subscrib_now.svg'),
                ),
              ),
              body: SafeArea(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: double.infinity,
                        child: Column(
                          children: [
                            SvgPicture.asset(
                              'assets/home/upgrade_to_premium.svg',
                            ),
                            Dimens.boxHeight10,
                            SvgPicture.asset(
                              'assets/home/Swipe to meet new fr.svg',
                            ),
                          ],
                        ),
                      ),
                      Dimens.boxHeight25,
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            ...List.generate(
                              5,
                              (index) => const PriceTile(
                                time: '3',
                                price: '129',
                                isBestValue: true,
                                isChecked: true,
                              ),
                            ).toList()
                          ],
                        ),
                      ),
                      Container(
                        padding: Dimens.edgeInsets10,
                        child: DefaultContainer(
                          child: Column(
                            children: [
                              Dimens.boxHeight10,
                              const Point(
                                title:
                                    'Track your various short term or long term life goals.',
                              ),
                              Dimens.boxHeight25,
                              const Point(
                                title:
                                    'Invest In expert curated investment packs',
                              ),
                              Dimens.boxHeight25,
                              const Point(
                                title:
                                    'Invest in a core portfolio that is specially designed for you.',
                              ),
                              Dimens.boxHeight25,
                              const Point(
                                title:
                                    'Access to invest in expert suggested funds',
                              ),
                              Dimens.boxHeight15,
                            ],
                          ),
                        ),
                      ),
                      
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}

class PriceTile extends StatelessWidget {
  const PriceTile({
    Key? key,
    this.price,
    this.time,
    this.isBestValue,
    this.isChecked = false,
  }) : super(key: key);

  final String? price;
  final String? time;
  final bool? isBestValue;
  final bool? isChecked;

  @override
  Widget build(BuildContext context) => Container(
        padding: Dimens.edgeInsets16,
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            DefaultContainer(
              border: isChecked!
                  ? Border.all(
                      width: 1,
                      color: ColorsValue.primaryColor,
                    )
                  : null,
              width: 150,
              child: Column(
                children: [
                  Dimens.boxHeight10,
                  SizedBox(
                    height: 25,
                    child: Row(
                      children: [
                        Column(
                          children: [
                            Dimens.boxHeight7,
                            SvgPicture.asset('assets/home/rupee.svg'),
                            const Spacer(),
                          ],
                        ),
                        Text(
                          ' $price',
                          style: Styles.primaryBold24,
                        ),
                        Column(
                          children: [
                            const Spacer(),
                            Text(
                              ' / Month',
                              style: Styles.black12,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  Dimens.boxHeight10,
                  Text(
                    '$time months Subscription',
                    style: Styles.medium14Black,
                  ),
                ],
              ),
            ),
            if (isBestValue!)
              Positioned(
                top: -9,
                left: 0,
                right: 0,
                child: Container(
                  margin: const EdgeInsets.symmetric(
                    horizontal: 25,
                  ),
                  decoration: const BoxDecoration(
                    color: Color.fromRGBO(255, 231, 221, 1),
                    borderRadius: BorderRadius.all(
                      Radius.circular(50),
                    ),
                  ),
                  padding: Dimens.edgeInsets2,
                  child: Center(
                    child: Text(
                      'BEST VALUE',
                      style: Styles.black13,
                    ),
                  ),
                ),
              ),
          ],
        ),
      );
}

class Point extends StatelessWidget {
  const Point({
    Key? key,
    this.title,
  }) : super(key: key);

  final String? title;

  @override
  Widget build(BuildContext context) => Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SvgPicture.asset('assets/home/goal_based_point.svg'),
          Dimens.boxWidth10,
          Expanded(
            child: Text(
              '$title',
              style: Styles.black16,
            ),
          ),
        ],
      );
}
