import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class ExpandedCustomTile extends StatelessWidget {
  const ExpandedCustomTile({
    Key? key,
    this.title,
    this.value,
    this.needMargin = true,
    this.needPadding = true,
    this.needBorder = true,
    this.onTap,
  }) : super(key: key);

  final String? title;
  final String? value;
  final bool? needMargin;
  final bool? needPadding;
  final bool? needBorder;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) => Container(
        padding: needPadding! ? Dimens.edgeInsets0_0_0_20 : Dimens.edgeInsets0,
        margin: needMargin! ? Dimens.edgeInsets0_0_0_20 : Dimens.edgeInsets0,
        decoration: BoxDecoration(
          border: Border(
            bottom: needBorder!
                ? BorderSide(
                    color: Colors.grey.withOpacity(0.2),
                    width: Dimens.one,
                  )
                : BorderSide.none,
          ),
        ),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  '$title'.tr,
                  style: Styles.regular14Grey,
                ),
                const Spacer(),
                GestureDetector(
                  onTap: () {
                    onTap!();
                  },
                  child: Row(
                    children: [
                      Text(
                        '$value',
                        style: Styles.medium14Black,
                      ),
                      Icon(
                        Icons.keyboard_arrow_right,
                        size: Dimens.twentyTwo,
                        color: ColorsValue.greyColor,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      );
}