import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/global_button.dart';

class ExpectedReturnsDialogContent extends StatelessWidget {
  const ExpectedReturnsDialogContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
      builder: (_controller) => StatefulBuilder(
            builder: (context, setState) => Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0),
                ),
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Dimens.boxHeight10,
                    Container(
                      width: Dimens.fourty,
                      height: Dimens.five,
                      decoration: BoxDecoration(
                        color: ColorsValue.greyColor.withOpacity(.2),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(50),
                        ),
                      ),
                    ),
                    Dimens.boxHeight20,
                    Image.asset(AssetConstants.settingsCircle),
                    Dimens.boxHeight10,
                    Text(
                      'Expected Returns',
                      style: Styles.bold16Black,
                    ),
                    Dimens.boxHeight5,
                    SizedBox(
                      width: Dimens.percentWidth(0.6),
                      child: Text(
                        'Lorem ipsum dolor sit amet, sit amet, consectetur adipiscing elit. ',
                        textAlign: TextAlign.center,
                        style: Styles.regular12Grey,
                      ),
                    ),
                    Dimens.boxHeight20,
                    Padding(
                      padding: Dimens.edgeInsets15_0_15_0,
                      child: Container(
                        padding: Dimens.edgeInsets5 +
                            Dimens.edgeInsets2 +
                            Dimens.edgeInsets2,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(
                            Dimens.eight,
                          ),
                          border: Border.all(
                            color: Colors.grey.withOpacity(0.4),
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ColumnTile(
                              title: 'Monthly SIP'.tr,
                              value: '₹ 12,000',
                            ),
                            ColumnTile(
                              title: 'Goal Amount'.tr,
                              valueAlignment: CrossAxisAlignment.end,
                              value: '₹ 4,82,000',
                            ),
                          ],
                        ),
                      ),
                    ),
                    Dimens.boxHeight40,
                    SizedBox(
                      width: Dimens.percentWidth(0.95),
                      child: SliderTheme(
                        data: SliderTheme.of(context).copyWith(
                          showValueIndicator: ShowValueIndicator.always,
                          valueIndicatorColor: Colors.white,
                          valueIndicatorTextStyle:
                              const TextStyle(color: ColorsValue.blackColor),
                          activeTrackColor: ColorsValue.activeTrackColor,
                          inactiveTrackColor: ColorsValue.inactiveTrackColor,
                          trackHeight: 2,
                          thumbShape: const SliderThumbShape(),
                          overlayShape: const RoundSliderOverlayShape(
                              overlayRadius: 10.0),
                        ),
                        child: Slider(
                          activeColor: ColorsValue.primaryColor,
                          min: 0,
                          max: 10,
                          value: _controller.expectedReturnSlider,
                          label:
                              '${'${(_controller.expectedReturnSlider / 10) * 100}'.split('.')[0]}%',
                          onChanged: (value) {
                            setState(() {
                              _controller.expectedReturnSlider = value;
                              _controller.update();
                            });

                            debugPrint(value.toString());
                          },
                        ),
                      ),
                    ),
                    Dimens.boxHeight35,
                    Padding(
                      padding: Dimens.edgeInsets15_0_15_0,
                      child: Container(
                        padding: Dimens.edgeInsets10,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Icon(
                              Icons.info_outline_rounded,
                              size: Dimens.fifteen,
                            ),
                            Dimens.boxWidth10,
                            Expanded(
                              child: RichText(
                                text: TextSpan(
                                    text:
                                        'Investors  understand  that  their  principal  will  be  at ',
                                    style: Styles.regular12Grey,
                                    children: [
                                      TextSpan(
                                        text: ' Very Low ',
                                        style: Styles.regular12Grey
                                            .copyWith(color: Colors.green),
                                      ),
                                      TextSpan(
                                        text: ' risk',
                                        style: Styles.regular12Grey,
                                      )
                                    ]),
                              ),
                            ),
                          ],
                        ),
                        decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(12)),
                          color: ColorsValue.lightBgContainerColor,
                        ),
                      ),
                    ),
                    Dimens.boxHeight10,
                    Padding(
                      padding: Dimens.edgeInsets15_0_15_0,
                      child: Row(
                        children: [
                          Expanded(
                            child: BorderedButton(
                              text: 'Cancel'.tr,
                              onTap: () {
                                Get.back<void>();
                              },
                            ),
                          ),
                          Expanded(
                            child: GlobalButton(
                              text: 'Save'.tr,
                              onTap: () {
                                Get.back<void>();
                                showModalBottomSheet(
                                    backgroundColor: Colors.transparent,
                                    context: context,
                                    isScrollControlled: true,
                                    builder: (ctx) =>
                                        const RiskProfileDialogContent());
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Dimens.boxHeight15,
                  ],
                ),
              ),
            ),
          ));
}
