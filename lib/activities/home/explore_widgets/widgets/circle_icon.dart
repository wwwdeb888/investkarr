import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:investkarr/lib.dart';

class CircleIcon extends StatelessWidget {
  CircleIcon({
    Key? key,
    this.width,
    this.height,
    this.image,
    this.isSvg = true,
    this.color = const Color(0xffEBE3FD),
    this.svgColor,
  }) : super(key: key);

  final double? width;
  final double? height;
  final String? image;
  final bool? isSvg;
  final Color? color;
  final Color? svgColor;

  @override
  Widget build(BuildContext context) => Container(
        padding: Dimens.edgeInsets7,
        width: width ?? Dimens.fourty,
        height: height ?? Dimens.fourty,
        decoration: BoxDecoration(
          color: color,
          shape: BoxShape.circle,
        ),
        child: Center(
          child: isSvg!
              ? SvgPicture.asset(
                  image!,
                  color: svgColor,
                )
              : Image.asset(image!),
        ),
      );
}
