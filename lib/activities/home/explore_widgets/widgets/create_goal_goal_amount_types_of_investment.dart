import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/create_goal_plan_to_buy.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/goal_summary.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/sip_create.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class CreateGoalAmountTypeOfInvestment extends StatelessWidget {
  CreateGoalAmountTypeOfInvestment({Key? key, this.isLoan = false})
      : super(key: key);
  final bool? isLoan;

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          appBar: CustomAppBar(
            title: 'settingAGoal'.tr,
          ),
          body: SafeArea(
            child: Container(
              padding: Dimens.edgeInsets16,
              width: Get.width,
              child: CustomScrollView(
                slivers: [
                  SliverFillRemaining(
                    hasScrollBody: false,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Dimens.boxHeight30,
                        CircleIcon(
                          image: 'assets/home/car-compact 1.svg',
                          color: Colors.orange.withOpacity(.2),
                        ),
                        Dimens.boxHeight20,
                        Text(
                          'goalAmount'.tr,
                          style: Styles.regular14Grey,
                        ),
                        Dimens.boxHeight15,
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              '\u{20b9}'.tr,
                              style: Styles.medium26Black,
                            ),
                            Dimens.boxWidth5,
                            Padding(
                              padding: Dimens.edgeInsets0_0_0_5,
                              child: Text(
                                isLoan! ? '3,35,000' : '5,35,000',
                                style: Styles.bold24Black,
                              ),
                            ),
                          ],
                        ),
                        Dimens.boxHeight10,
                        Container(
                          padding: Dimens.edgeInsets10,
                          child: Text(
                            'Current value + 7% Inflation',
                            style: Styles.regular12Grey,
                          ),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(12)),
                            color: ColorsValue.lightBgContainerColor,
                          ),
                        ),
                        Dimens.boxHeight30,
                        Row(
                          children: [
                            Text(
                              'Type of investment'.tr,
                              style: Styles.medium14Grey,
                            ),
                            Dimens.boxWidth2,
                            Icon(
                              Icons.info_outline_rounded,
                              size: Dimens.fifteen,
                            ),
                          ],
                        ),
                        Dimens.boxHeight20,
                        Row(
                          children: [
                            Expanded(
                              child: BoxCheckSelector(
                                  height: Dimens.fifty,
                                  onTap: () {
                                    _controller.lumpsumInvesment = true;
                                    _controller.sipInvestment = false;
                                    _controller.update();
                                  },
                                  isChecked: _controller.lumpsumInvesment,
                                  title: 'Lumpsum'),
                            ),
                            Dimens.boxWidth15,
                            Expanded(
                              child: BoxCheckSelector(
                                  height: Dimens.fifty,
                                  onTap: () {
                                    _controller.lumpsumInvesment = false;
                                    _controller.sipInvestment = true;
                                    _controller.update();
                                  },
                                  isChecked: _controller.sipInvestment,
                                  title: 'sip'.toUpperCase()),
                            ),
                          ],
                        ),
                        const Spacer(),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                            padding: Dimens.edgeInsets5,
                            child: SizedBox(
                              width: Dimens.percentWidth(1),
                              height: Dimens.percentHeight(.07),
                              child: ElevatedButton(
                                style:
                                    Styles.elevatedButtonTheme.style!.copyWith(
                                  backgroundColor:
                                      MaterialStateProperty.resolveWith<Color>(
                                    (Set<MaterialState> states) => (0 == 0)
                                        ? ColorsValue.primaryColor
                                        : ColorsValue.primaryColor
                                            .withOpacity(.5),
                                  ),
                                  textStyle: MaterialStateProperty.all(
                                    Styles.bold16White,
                                  ),
                                ),
                                onPressed: () {
                                  Get.to(SipCreate());
                                },
                                child: Text(
                                  'Continue'.tr,
                                  style: Styles.bold16White,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
