import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class MutualFundsEquity extends StatelessWidget {
  const MutualFundsEquity({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        child: SizedBox(
          width: Dimens.percentWidth(1),
          child: Padding(
            padding: Dimens.edgeInsets15_0_15_0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      StringConstants.passive,
                      style: Styles.bold16Black,
                    ),
                    Dimens.boxWidth10,
                    Image.asset(
                      AssetConstants.infoOutlined,
                    ),
                  ],
                ),
                Dimens.boxHeight10,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CategoryOfMutualFundCard(
                      title: StringConstants.equity,
                      width: Dimens.percentWidth(0.39),
                      subTitle:
                          'Funds primarily invest in stocks of listed cut',
                      bgAssetImage: AssetConstants.looperOne,
                      iconImage: AssetConstants.indexFunds,
                    ),
                    CategoryOfMutualFundCard(
                      title: StringConstants.debts,
                      width: Dimens.percentWidth(0.39),
                      subTitle: 'Funds invest in more than one asset class',
                      bgAssetImage: AssetConstants.looperOne,
                      iconImage: AssetConstants.fofs,
                    ),
                  ],
                ),
                Dimens.boxHeight20,
                Text(
                  StringConstants.marketCap,
                  style: Styles.bold16Black,
                ),
                Dimens.boxHeight20,
                SingleChildScrollView(
                  physics: const AlwaysScrollableScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CategoryOfMutualFundCard(
                        title: StringConstants.largeCap,
                        width: Dimens.percentWidth(0.36),
                        subTitle:
                            'Funds invest primarily in the Largest list of funds',
                        bgAssetImage: AssetConstants.looperTwo,
                        iconImage: AssetConstants.equity,
                      ),
                      Dimens.boxWidth10,
                      CategoryOfMutualFundCard(
                        title: StringConstants.multiCap,
                        width: Dimens.percentWidth(0.36),
                        trimTextLength: 28,
                        subTitle:
                            'Funds invest in companies acroding to the yearly returs',
                        bgAssetImage: AssetConstants.looperTwo,
                        iconImage: AssetConstants.debt,
                      ),
                      Dimens.boxWidth10,
                      CategoryOfMutualFundCard(
                        title: StringConstants.largeCap,
                        width: Dimens.percentWidth(0.38),
                        subTitle:
                            'Funds invest primarily in the Largest list of funds',
                        bgAssetImage: AssetConstants.looperTwo,
                        iconImage: AssetConstants.equity,
                      ),
                    ],
                  ),
                ),
                Dimens.boxHeight20,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      StringConstants.sectorAndThemes,
                      style: Styles.bold16Black,
                    ),
                    GestureDetector(
                      onTap: () {
                        RouteManagement.goToSectorAndThemes();
                      },
                      child: Text(
                        StringConstants.viewAll.toUpperCase(),
                        style: Styles.regular14Grey
                            .copyWith(color: ColorsValue.primaryColor),
                      ),
                    ),
                  ],
                ),
                Dimens.boxHeight20,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CategoryOfMutualFundCard(
                      title: StringConstants.esg,
                      width: Dimens.percentWidth(0.39),
                      subTitle: 'Funds invest in banking companies',
                      bgAssetImage: AssetConstants.looperOne,
                      iconImage: AssetConstants.equity,
                    ),
                    CategoryOfMutualFundCard(
                      title: StringConstants.banking,
                      width: Dimens.percentWidth(0.39),
                      subTitle: 'Funds invest in dividend yielding stocks ',
                      bgAssetImage: AssetConstants.looperOne,
                      iconImage: AssetConstants.debt,
                    ),
                  ],
                ),
                Dimens.boxHeight20,
                Text(
                  StringConstants.taxSaving,
                  style: Styles.bold16Black,
                ),
                Dimens.boxHeight20,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CategoryOfMutualFundCard(
                      title: StringConstants.elss,
                      width: Dimens.percentWidth(0.39),
                      subTitle: 'Funds are elegible for tax benefits under the',
                      bgAssetImage: AssetConstants.looperOne,
                      iconImage: AssetConstants.equity,
                    ),
                  ],
                ),
                Dimens.boxHeight20,
                Text(
                  'Top Equity Funds',
                  style: Styles.bold16Black,
                ),
                Dimens.boxHeight15,
                ListView.separated(
                  itemCount: 3,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  separatorBuilder: (context, index) => Dimens.boxHeight15,
                  itemBuilder: (context, index) => SmallFundCard(
                      bankIcon: AssetConstants.hdfcLogo,
                      title: 'DSP Tax Saver Direct Plan growth',
                      returns: '3Y return',
                      percentage: '24.27%',
                      isGrowth: true),
                ),
                Dimens.boxHeight20,
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'All Equity Funds',
                      style: Styles.bold16Black,
                    ),
                    GestureDetector(
                      child: Text(
                        StringConstants.viewAll.toUpperCase(),
                        style: Styles.regular14Grey
                            .copyWith(color: ColorsValue.primaryColor),
                      ),
                    ),
                  ],
                ),
                Dimens.boxHeight15,
                ListView.separated(
                  itemCount: 3,
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  separatorBuilder: (context, index) => Dimens.boxHeight15,
                  itemBuilder: (context, index) => SmallFundCard(
                      bankIcon: AssetConstants.hdfcLogo,
                      title: 'DSP Tax Saver Direct Plan growth',
                      returns: '3Y return',
                      percentage: '24.27%',
                      isGrowth: true),
                ),
              ],
            ),
          ),
        ),
      );
}
