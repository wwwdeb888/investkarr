
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:investkarr/lib.dart';

class PointBulletsWidget extends StatelessWidget {
  const PointBulletsWidget({
    Key? key,
    this.title,
  }) : super(key: key);

  final String? title;

  @override
  Widget build(BuildContext context) => Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SvgPicture.asset('assets/home/goal_based_point.svg'),
          Dimens.boxWidth10,
          Expanded(
            child: Text(
              '$title',
              style: Styles.regular14Grey,
            ),
          ),
        ],
      );
}
