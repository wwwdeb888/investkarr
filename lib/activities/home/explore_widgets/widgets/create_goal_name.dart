import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/create_goal_plan_to_buy.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class CreateGoalName extends StatelessWidget {
  const CreateGoalName({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
      builder: (_controller) => Scaffold(
            appBar: CustomAppBar(
              title: !_controller.goalEntered
                  ? 'Create a goal'
                  : 'settingAGoal'.tr,
            ),
            body: SafeArea(
              child: Container(
                padding: Dimens.edgeInsets16,
                width: Get.width,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Dimens.boxHeight35,
                    CircleIcon(
                      image: !_controller.goalEntered
                          ? AssetConstants.flag
                          : 'assets/home/car-compact 1.svg',
                      color: !_controller.goalEntered
                          ? const Color(0xffEBE3FD)
                          : Colors.orange.withOpacity(.2),
                    ),
                    Dimens.boxHeight25,
                    Text(
                      'giveYourGoalAName'.tr,
                      style: Styles.bold24Black,
                    ),
                    const Spacer(
                      flex: 2,
                    ),
                    FormFieldWidget(
                      elevation: 0,
                      autoFocus: true,
                      isFilled: true,
                      textEditingController: _controller.goalController,
                      textInputAction: TextInputAction.done,
                      textInputType: TextInputType.text,
                      borderColor: ColorsValue.formFieldBorderColor,
                      fillColor: ColorsValue.backgroundColor,
                      labelText: 'goalName'.tr,
                      labelStyle:
                          const TextStyle(color: ColorsValue.blackColor),
                      hintText: 'Enter goal name',
                      onChange: (String v) {
                        _controller.makeGoal(v);
                      },
                    ),
                    const Spacer(
                      flex: 3,
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Padding(
                        padding: Dimens.edgeInsets5,
                        child: SizedBox(
                          width: Dimens.percentWidth(1),
                          height: Dimens.percentHeight(.07),
                          child: ElevatedButton(
                            style: Styles.elevatedButtonTheme.style!.copyWith(
                              backgroundColor:
                                  MaterialStateProperty.resolveWith<Color>(
                                (Set<MaterialState> states) => (_controller
                                        .goalEntered)
                                    ? ColorsValue.primaryColor
                                    : ColorsValue.primaryColor.withOpacity(.5),
                              ),
                              textStyle: MaterialStateProperty.all(
                                Styles.bold16White,
                              ),
                            ),
                            onPressed: () {
                              if (_controller.goalEntered) {
                                Get.to(const CreateGoalPlanToBuy());
                              } else {}
                            },
                            child: Text(
                              'Continue'.tr,
                              style: Styles.bold16White,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ));
}
