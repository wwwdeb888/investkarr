import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/goal_summary.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

import 'goal_summary_emergency.dart';

class SipCreateEmergency extends StatelessWidget {
  SipCreateEmergency({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: CustomAppBar(
          title: 'Emergency Pack'.tr,
        ),
        body: GetBuilder<HomeController>(
          builder: (_controller) => SafeArea(
            child: Container(
              padding: Dimens.edgeInsets16,
              width: Get.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Dimens.boxHeight35,
                  SvgPicture.asset(
                    'assets/home/emerygency.svg',
                  ),
                  Dimens.boxHeight25,
                  Text(
                    'addAnInitialAmountToYourGoalSIP'.tr,
                    style: Styles.bold24Black,
                  ),
                  const Spacer(
                    flex: 1,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'doYouWishToAddInitialAmount'.tr,
                        style: Styles.mediumBlack15,
                      ),
                      CupertinoSwitch(
                        value: _controller.isAddInitialAmount,
                        onChanged: (_) {
                          _controller.isAddInitialAmount =
                              !_controller.isAddInitialAmount;
                          _controller.update();
                        },
                        activeColor: ColorsValue.primaryColor,
                      )
                    ],
                  ),
                  if (_controller.isAddInitialAmount)
                    Column(
                      children: [
                        Dimens.boxHeight26,
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'totalAmount',
                              style: Styles.mediumBlack15,
                            ),
                            Text(
                              '5 Years',
                              style: Styles.mediumBlack15,
                            ),
                          ],
                        ),
                        Slider(
                          activeColor: ColorsValue.primaryColor,
                          min: 0,
                          max: 100,
                          value: 60,
                          onChanged: (value) {
                            debugPrint(value.toString());
                          },
                        ),
                      ],
                    ),
                  const Spacer(
                    flex: 3,
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: ColorsValue.primaryColor.withOpacity(.05),
                      borderRadius: const BorderRadius.all(
                        Radius.circular(12),
                      ),
                    ),
                    padding: Dimens.edgeInsets16,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Icon(
                          Icons.info_outline_rounded,
                          size: 18,
                          color: Colors.grey,
                        ),
                        Dimens.boxWidth8,
                        Expanded(
                          child: Text(
                            'tryAddingSmallAmountInitiallyToLowerTheMonthlySIP'
                                .tr,
                            style: Styles.grey15,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Dimens.boxHeight10,
                  SizedBox(
                    width: Get.width,
                    child: CustomButton(
                      title: 'Continue'.tr,
                      onTap: () {
                        Get.to(const GoalSummaryEmergency());
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
}

class PaymentMethodButton extends StatelessWidget {
  const PaymentMethodButton({
    Key? key,
    this.title,
    this.isCheck,
    this.onTap,
  }) : super(key: key);

  final String? title;
  final bool? isCheck;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => GestureDetector(
          onTap: onTap,
          child: Stack(
            children: [
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(8),
                  ),
                  color: _controller.typeOfInvestment == title
                      ? ColorsValue.primaryColor.withOpacity(.05)
                      : Colors.transparent,
                  border: Border.all(
                    width: .5,
                    color: _controller.typeOfInvestment == title
                        ? ColorsValue.primaryColor
                        : Colors.grey,
                  ),
                ),
                child: Center(
                  child: Text(
                    '$title',
                    style: _controller.typeOfInvestment == title
                        ? Styles.primaryBold18
                        : Styles.boldBlack18.copyWith(
                            color: Colors.grey,
                          ),
                  ),
                ),
              ),
              if (_controller.typeOfInvestment == title)
                Positioned(
                  right: 3,
                  top: 2,
                  child: Container(
                    height: 18,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: ColorsValue.primaryColor,
                    ),
                    child: const Icon(
                      Icons.check,
                      color: Colors.white,
                      size: 13,
                    ),
                  ),
                )
            ],
          ),
        ),
      );
}
