import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class GoalSummaryEmergency extends StatelessWidget {
  const GoalSummaryEmergency({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leadingWidth: 50,
          leading: SvgPicture.asset(
            'assets/home/emerygency.svg',
          ),
          title: Text(
            'Emergency Pack ',
            style: Styles.boldBlack15,
          ),
          actions: [
            GestureDetector(
              onTap: Get.back,
              child: Container(
                margin: const EdgeInsets.only(right: 16),
                padding: Dimens.edgeInsets7,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  border: Border.all(
                    width: .5,
                    color: Colors.grey.withOpacity(.7),
                  ),
                ),
                child: const Icon(
                  Icons.close,
                  color: Colors.black,
                ),
              ),
            ),
          ],
        ),
        body: Padding(
          padding: Dimens.edgeInsets16,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Recommended For You'.tr,
                style: Styles.boldBlack22,
              ),
              Dimens.boxHeight25,
              Stack(
                children: [
                  DefaultContainer(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Text(
                              '₹40,000',
                              style: Styles.boldBlack20,
                            ),
                            Dimens.boxWidth8,
                            Text(
                              'monthlySip'.tr,
                              style: Styles.black13,
                            ),
                            const Spacer(),
                            Container(
                              padding: Dimens.edgeInsets7,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: ColorsValue.greyColor.withOpacity(.3),
                              ),
                              child: const Icon(
                                Icons.arrow_forward_ios_rounded,
                                size: 12,
                              ),
                            ),
                          ],
                        ),
                        Dimens.boxHeight32,
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            ColumnTile(
                              title: 'investmentAmount'.tr,
                              value: '₹4,00,000',
                            ),
                            ColumnTile(
                              title: 'expReturns'.tr,
                              value: '100%',
                              color: Colors.green,
                              valueAlignment: CrossAxisAlignment.end,
                            ),
                          ],
                        ),
                        Dimens.boxHeight25,
                      ],
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    child: Stack(
                      children: [
                        SvgPicture.asset('assets/home/bottom_bar.svg'),
                        Positioned.fill(
                          child: Row(
                            children: [
                              const SizedBox(
                                width: 16,
                              ),
                              Text(
                                'goalAmount'.tr,
                                style: Styles.black18,
                              ),
                              const Spacer(),
                              Text(
                                '₹5,35,000',
                                style: Styles.black18,
                              ),
                              const SizedBox(
                                width: 16,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Dimens.boxHeight25,
              const Divider(),
              Theme(
                data: Theme.of(context)
                    .copyWith(dividerColor: Colors.transparent),
                child: ExpansionTile(
                  title: Text(
                    'goalSummary'.tr,
                    style: Styles.boldBlack22,
                  ),
                  children: [
                    DefaultContainer(
                      child: Column(
                        children: [
                          CustomTile(
                            title: 'goalAmount'.tr,
                            value: '₹1,00,000',
                          ),
                          CustomTile(
                            title: 'goalAmount'.tr,
                            value: '₹1,00,000',
                          ),
                          CustomTile(
                            title: 'goalAmount'.tr,
                            value: '₹1,00,000',
                          ),
                          CustomTile(
                            title: 'goalAmount'.tr,
                            value: '₹1,00,000',
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              // SvgPicture.asset('assets/home/CTA.svg')
            ],
          ),
        ),
        bottomNavigationBar: Container(
          padding: Dimens.edgeInsets16,
          width: Get.width,
          child: CustomButton(
            title: 'Continue',
            onTap: () {
              showModalBottomSheet(
                isScrollControlled: true,
                backgroundColor: Colors.transparent,
                context: context,
                builder: (ctx) => Container(
                  height: Dimens.percentHeight(.6),
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25.0),
                      topRight: Radius.circular(25.0),
                    ),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Dimens.boxHeight25,
                      SvgPicture.asset('assets/home/settings.svg'),
                      Dimens.boxHeight10,
                      Text(
                        'Expected Returns',
                        style: Styles.boldBlack18,
                      ),
                      Dimens.boxHeight10,
                      Text(
                        'Lorem ipsum dolor sit amet, sit amet, consectetur adipiscing elit. ',
                        style: Styles.black15,
                        textAlign: TextAlign.center,
                      ),
                      Container(
                        margin: const EdgeInsets.all(16),
                        padding: const EdgeInsets.all(16),
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: .5,
                            color: Colors.grey.withOpacity(.2),
                          ),
                          borderRadius: const BorderRadius.all(
                            Radius.circular(12),
                          ),
                        ),
                        child: Row(
                          children: [
                            ColumnTile(
                              title: 'Monthly SIP',
                              value: '12,000',
                            ),
                            const Spacer(),
                            ColumnTile(
                              title: 'Goal Amount',
                              value: '12,000',
                            ),
                          ],
                        ),
                      ),
                      Slider(
                        min: 0,
                        max: 100,
                        value: 40,
                        thumbColor: ColorsValue.primaryColor,
                        activeColor: ColorsValue.primaryColor,
                        onChanged: (_) {},
                      ),
                      Container(
                        margin: const EdgeInsets.all(16),
                        decoration: BoxDecoration(
                          color: ColorsValue.primaryColor.withOpacity(.05),
                          borderRadius: const BorderRadius.all(
                            Radius.circular(12),
                          ),
                        ),
                        padding: Dimens.edgeInsets16,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Icon(
                              Icons.info_outline_rounded,
                              size: 18,
                              color: Colors.grey,
                            ),
                            Dimens.boxWidth8,
                            Expanded(
                              child: Text(
                                'tryAddingSmallAmountInitiallyToLowerTheMonthlySIP'
                                    .tr,
                                style: Styles.grey15,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: GestureDetector(
                              onTap: Get.back,
                              child: Container(
                                height: Dimens.fifty,
                                child: Center(
                                  child: Text(
                                    'Cancel',
                                    style: Styles.primaryBold18,
                                  ),
                                ),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: 1,
                                    color: ColorsValue.primaryColor,
                                  ),
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(50),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Dimens.boxWidth10,
                          Expanded(
                            child: GestureDetector(
                              onTap: () {},
                              child: Container(
                                height: Dimens.fifty,
                                child: Center(
                                  child: Text(
                                    'Save'.tr,
                                    style: Styles.boldBlack18.copyWith(
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                                padding: Dimens.edgeInsets16,
                                decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(50),
                                    ),
                                    color: ColorsValue.primaryColor),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Dimens.boxHeight25,
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      );
}

class CustomTile extends StatelessWidget {
  const CustomTile({
    Key? key,
    this.title,
    this.value,
  }) : super(key: key);

  final String? title;
  final String? value;

  @override
  Widget build(BuildContext context) => Container(
        margin: const EdgeInsets.only(top: 8),
        child: Column(
          children: [
            Row(
              children: [
                Text(
                  '$title'.tr,
                  style: Styles.black15,
                ),
                const Spacer(),
                Text(
                  '$value',
                  style: Styles.black16,
                )
              ],
            ),
            Divider(
              color: Colors.grey.withOpacity(.4),
            ),
          ],
        ),
      );
}
