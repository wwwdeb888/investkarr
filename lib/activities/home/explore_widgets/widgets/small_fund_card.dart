import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:investkarr/lib.dart';

class SmallFundCard extends StatelessWidget {
  SmallFundCard({
    Key? key,
    required this.title,
    required this.bankIcon,
    required this.percentage,
    required this.returns,
    this.isGrowth = true,
    this.tagsList = const [],
    this.currency = false,
    this.showSuggested = false,
    this.onTap,
  }) : super(key: key);
  final String? bankIcon;
  final String? title;
  final String? percentage;
  final String? returns;
  final bool? isGrowth;
  List<String>? tagsList;
  final bool? currency;
  final bool? showSuggested;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) => Stack(
        children: [
          GestureDetector(
            onTap: () {
              onTap!();
            },
            child: Container(
              padding: showSuggested!
                  ? Dimens.edgeInsets0_10_0_0 +
                      Dimens.edgeInsets0_10_0_0 +
                      Dimens.edgeInsets0_10_0_0
                  : null,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(Dimens.ten),
                color: Colors.white,
                boxShadow: Styles.cardShadow,
              ),
              height: showSuggested!
                  ? Dimens.hundred + Dimens.thirtyFive
                  : tagsList!.isNotEmpty
                      ? Dimens.hundred
                      : Dimens.seventy,
              child: Padding(
                padding: Dimens.edgeInsets10,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: Dimens.fourty,
                          height: Dimens.fourty,
                          child: Image.asset(
                            bankIcon!,
                            fit: BoxFit.cover,
                          ),
                        ),
                        Dimens.boxWidth10,
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: Dimens.percentWidth(.42),
                              child: Text(
                                title!,
                                style: Styles.medium16Black,
                              ),
                            ),
                            if (tagsList!.isNotEmpty) Dimens.boxHeight10,
                            if (tagsList!.isNotEmpty)
                              Row(
                                children: [
                                  ...List.generate(
                                    tagsList!.length,
                                    (index) => CustomChip(
                                      title: '${tagsList![index]}',
                                    ),
                                  ).toList(),
                                ],
                              ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Row(
                          children: [
                            if (!currency!)
                              Icon(
                                isGrowth!
                                    ? Icons.arrow_drop_up_outlined
                                    : Icons.arrow_drop_down_outlined,
                                size: Dimens.thirty,
                                color: isGrowth! ? Colors.green : Colors.red,
                              ),
                            Text(
                              currency!
                                  ? '\u{20b9} $percentage'
                                  : '$percentage%',
                              style: Styles.medium16Black.copyWith(
                                  color: isGrowth! ? Colors.green : Colors.red),
                            ),
                          ],
                        ),
                        Text(
                          currency! ? '(\u{20b9} $returns)' : returns!,
                          style: Styles.regular12Grey,
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          if (showSuggested!)
            ClipRRect(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(Dimens.ten),
                ),
                child: SvgPicture.asset('assets/home/suggested_by_expert.svg'))
        ],
      );
}
