import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/create_goal_plan_to_buy.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

import 'create_goal_goal_amount.dart';

class CreateGoalCostOfRide extends StatelessWidget {
  const CreateGoalCostOfRide({
    Key? key,
    this.isEditingFromSummary = false,
  }) : super(key: key);

  final bool isEditingFromSummary;

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          appBar: CustomAppBar(
            title:
                isEditingFromSummary ? 'Edit Goal Summary' : 'settingAGoal'.tr,
          ),
          body: SafeArea(
            child: Container(
              padding: Dimens.edgeInsets16,
              width: Get.width,
              child: CustomScrollView(
                slivers: [
                  SliverFillRemaining(
                    hasScrollBody: false,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Dimens.boxHeight30,
                        CircleIcon(
                          image: 'assets/home/car-compact 1.svg',
                          color: Colors.orange.withOpacity(.2),
                        ),
                        Dimens.boxHeight20,
                        SizedBox(
                          width: Dimens.percentWidth(0.8),
                          child: Text(
                            'whatIsTheCostOfYourRide'.tr,
                            textAlign: TextAlign.center,
                            style: Styles.bold24Black,
                          ),
                        ),
                        Dimens.boxHeight40,

                        /// next slider

                        Column(
                          children: [
                            Padding(
                              padding: Dimens.edgeInsets5_0_5_0 +
                                  Dimens.edgeInsets5_0_5_0 +
                                  Dimens.edgeInsets0_0_5_0,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    'Total amount'.tr,
                                    style: Styles.medium14Black
                                        .copyWith(color: ColorsValue.textColor),
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        '\u{20B9}',
                                        style: Styles.medium16Black,
                                      ),
                                      Dimens.boxWidth10,
                                      SizedBox(
                                        width: Dimens.percentWidth(0.2),
                                        child: FormFieldWidget(
                                          onChange: (value) {},
                                          elevation: 0,
                                          initialValue: '5,00,000',
                                          textAlign: TextAlign.end,
                                          formStyle: Styles.medium16Black,
                                          borderColor: ColorsValue.greyColor
                                              .withOpacity(0.5),
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Dimens.boxHeight10,
                            SizedBox(
                              width: Dimens.percentWidth(1),
                              child: SliderTheme(
                                data: SliderTheme.of(context).copyWith(
                                  activeTrackColor:
                                      ColorsValue.activeTrackColor,
                                  inactiveTrackColor:
                                      ColorsValue.inactiveTrackColor,
                                  trackHeight: 5,
                                  thumbShape: const SliderThumbShape(),
                                  overlayShape: const RoundSliderOverlayShape(
                                      overlayRadius: 15.0),
                                ),
                                child: Slider(
                                  activeColor: ColorsValue.primaryColor,
                                  min: 0,
                                  max: 10,
                                  value: _controller.totalAmount,
                                  onChanged: (value) {
                                    _controller.totalAmount = value;
                                    _controller.update();
                                    debugPrint(value.toString());
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        Dimens.boxHeight40,

                        Column(
                          children: [
                            Padding(
                              padding: Dimens.edgeInsets5_0_5_0 +
                                  Dimens.edgeInsets5_0_5_0 +
                                  Dimens.edgeInsets0_0_5_0,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                    'Inflow Rate'.tr,
                                    style: Styles.medium14Black
                                        .copyWith(color: ColorsValue.textColor),
                                  ),
                                ],
                              ),
                            ),
                            Dimens.boxHeight20,
                            SizedBox(
                              width: Dimens.percentWidth(1),
                              child: SliderTheme(
                                data: SliderTheme.of(context).copyWith(
                                  showValueIndicator: ShowValueIndicator.always,
                                  valueIndicatorColor: Colors.white,
                                  valueIndicatorTextStyle: const TextStyle(
                                      color: ColorsValue.blackColor),
                                  activeTrackColor:
                                      ColorsValue.activeTrackColor,
                                  inactiveTrackColor:
                                      ColorsValue.inactiveTrackColor,
                                  trackHeight: 5,
                                  thumbShape: const SliderThumbShape(),
                                  overlayShape: const RoundSliderOverlayShape(
                                      overlayRadius: 15.0),
                                ),
                                child: Slider(
                                  activeColor: ColorsValue.primaryColor,
                                  min: 0,
                                  max: 10,
                                  value: _controller.inflowRate,
                                  label:
                                      '${'${(_controller.inflowRate / 10) * 100}'.split('.')[0]}%',
                                  onChanged: (value) {
                                    _controller.inflowRate = value;
                                    _controller.update();
                                    debugPrint(value.toString());
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        Dimens.boxHeight26,
                        const Spacer(),

                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                            padding: Dimens.edgeInsets5,
                            child: SizedBox(
                              width: Dimens.percentWidth(1),
                              height: Dimens.percentHeight(.07),
                              child: ElevatedButton(
                                style:
                                    Styles.elevatedButtonTheme.style!.copyWith(
                                  backgroundColor:
                                      MaterialStateProperty.resolveWith<Color>(
                                    (Set<MaterialState> states) => (0 == 0)
                                        ? ColorsValue.primaryColor
                                        : ColorsValue.primaryColor
                                            .withOpacity(.5),
                                  ),
                                  textStyle: MaterialStateProperty.all(
                                    Styles.bold16White,
                                  ),
                                ),
                                onPressed: () {
                                  if (isEditingFromSummary) {
                                    Get.to(const CreateGoalPlanToBuy(
                                      isEditingFromSummary: true,
                                    ));
                                  } else {
                                    Get.to(CreateGoalAmount());
                                  }
                                },
                                child: Text(
                                  isEditingFromSummary ? 'Save' : 'Continue'.tr,
                                  style: Styles.bold16White,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
