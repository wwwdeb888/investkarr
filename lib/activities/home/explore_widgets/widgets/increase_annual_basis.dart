import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/utils/utils.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

import 'goal_summary.dart';

class IncreaseAnnualBasic extends StatelessWidget {
  IncreaseAnnualBasic({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          appBar: CustomAppBar(
            title: 'settingAGoal'.tr,
            actions: GestureDetector(
              onTap: () {
                Get.to(const GoalSummary());
              },
              child: Padding(
                padding: Dimens.edgeInsets0_0_10_0 + Dimens.edgeInsets0_0_5_0,
                child: Text(
                  'skip'.tr,
                  style: Styles.regular12GreyUnderline
                      .copyWith(color: ColorsValue.primaryColor),
                ),
              ),
            ),
          ),
          body: SafeArea(
            child: Container(
              padding: Dimens.edgeInsets16,
              width: Get.width,
              child: CustomScrollView(
                slivers: [
                  SliverFillRemaining(
                    hasScrollBody: false,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Dimens.boxHeight30,
                        CircleIcon(
                          image: 'assets/home/car-compact 1.svg',
                          color: Colors.orange.withOpacity(.2),
                        ),
                        Dimens.boxHeight20,
                        SizedBox(
                          width: Dimens.percentWidth(0.7),
                          child: Text(
                            'howMuchWouldYouLikeToIncreaseOnAnAnnualBasis'.tr,
                            textAlign: TextAlign.center,
                            style: Styles.bold24Black,
                          ),
                        ),
                        Dimens.boxHeight40,
                        Column(
                          children: [
                            SizedBox(
                              width: Dimens.percentWidth(1),
                              child: SliderTheme(
                                data: SliderTheme.of(context).copyWith(
                                  showValueIndicator: ShowValueIndicator.always,
                                  valueIndicatorColor: Colors.white,
                                  valueIndicatorTextStyle: const TextStyle(
                                      color: ColorsValue.blackColor),
                                  activeTrackColor:
                                      ColorsValue.activeTrackColor,
                                  inactiveTrackColor:
                                      ColorsValue.inactiveTrackColor,
                                  trackHeight: 5,
                                  thumbShape: const SliderThumbShape(),
                                  overlayShape: const RoundSliderOverlayShape(
                                      overlayRadius: 15.0),
                                ),
                                child: Slider(
                                  activeColor: ColorsValue.primaryColor,
                                  min: 0,
                                  max: 10,
                                  value: _controller.sipAnnualBasis,
                                  label:
                                      '${'${(_controller.sipAnnualBasis / 10) * 100}'.split('.')[0]}%',
                                  onChanged: (value) {
                                    _controller.sipAnnualBasis = value;
                                    _controller.update();
                                    debugPrint(value.toString());
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        Dimens.boxHeight26,
                        Padding(
                          padding: Dimens.edgeInsets20_5_20_5,
                          child: SingleChildScrollView(
                            physics: const AlwaysScrollableScrollPhysics(),
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const CustomList(
                                  title: '10',
                                ),
                                Dimens.boxWidth10,
                                const CustomList(
                                  title: '15',
                                ),
                                Dimens.boxWidth10,
                                const CustomList(
                                  title: '25',
                                ),
                              ],
                            ),
                          ),
                        ),
                        const Spacer(),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                            padding: Dimens.edgeInsets5,
                            child: SizedBox(
                              width: Dimens.percentWidth(1),
                              height: Dimens.percentHeight(.07),
                              child: ElevatedButton(
                                style:
                                    Styles.elevatedButtonTheme.style!.copyWith(
                                  backgroundColor:
                                      MaterialStateProperty.resolveWith<Color>(
                                    (Set<MaterialState> states) => (0 == 0)
                                        ? ColorsValue.primaryColor
                                        : ColorsValue.primaryColor
                                            .withOpacity(.5),
                                  ),
                                  textStyle: MaterialStateProperty.all(
                                    Styles.bold16White,
                                  ),
                                ),
                                onPressed: () {
                                  Get.to(const GoalSummary());
                                },
                                child: Text(
                                  'Continue'.tr,
                                  style: Styles.bold16White,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}

class CustomList extends StatelessWidget {
  const CustomList({
    Key? key,
    this.title,
  }) : super(key: key);

  final String? title;

  @override
  Widget build(BuildContext context) => Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 18,
          vertical: 5,
        ),
        constraints: const BoxConstraints(maxWidth: double.infinity),
        decoration: BoxDecoration(
          border: Border.all(
            width: .8,
            color: Colors.grey.withOpacity(.8),
          ),
          borderRadius: const BorderRadius.all(
            Radius.circular(50),
          ),
        ),
        child: Text(
          '$title%',
          style: Styles.medium14Black,
        ),
      );
}
