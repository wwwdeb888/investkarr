import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class GoalSummary extends StatelessWidget {
  const GoalSummary({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
      builder: (_controller) => Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              leadingWidth: 50,
              leading: Container(
                margin: const EdgeInsets.only(left: 16),
                width: Dimens.thirtyTwo,
                height: Dimens.thirtyTwo,
                decoration: BoxDecoration(
                  color: Colors.orange.withOpacity(.2),
                  shape: BoxShape.circle,
                ),
                child: Center(
                  child: SvgPicture.asset(
                    'assets/home/car-compact 1.svg',
                  ),
                ),
              ),
              title: Text(
                'Dream Car',
                style: Styles.boldBlack16,
              ),
              actions: [
                GestureDetector(
                  onTap: Get.back,
                  child: Padding(
                    padding:
                        Dimens.edgeInsets0_0_10_0 + Dimens.edgeInsets0_0_5_0,
                    child: Container(
                      width: Dimens.thirty,
                      height: Dimens.thirty,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(
                          width: 1,
                          color: Colors.grey.withOpacity(.7),
                        ),
                      ),
                      child: Icon(
                        Icons.close,
                        color: Colors.black,
                        size: Dimens.seventeen,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            body: SingleChildScrollView(
              child: Padding(
                padding: Dimens.edgeInsets16,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      _controller.sipInvestment
                          ? 'Suggested Portfolio'
                          : 'suggestedForYou'.tr,
                      style: Styles.boldBlack16,
                    ),
                    Dimens.boxHeight25,
                    Stack(
                      children: [
                        Stack(
                          children: [
                            DefaultContainer(
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Text(
                                        '₹40,000',
                                        style: Styles.boldBlack24,
                                      ),
                                      Dimens.boxWidth8,
                                      Text(
                                        'monthlySip'.tr,
                                        style: Styles.medium12Grey,
                                      ),
                                      const Spacer(),
                                      Container(
                                        padding: Dimens.edgeInsets5,
                                        width: Dimens.twenty,
                                        height: Dimens.twenty,
                                        decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: ColorsValue.circleArrowColor,
                                        ),
                                        child: Center(
                                          child: Icon(
                                            Icons.arrow_forward_ios_rounded,
                                            color: ColorsValue.primaryColor,
                                            size: Dimens.twelve,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight25,
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      ColumnTile(
                                        title: 'investmentAmount'.tr,
                                        value: '₹4,00,000',
                                      ),
                                      ColumnTile(
                                        title: 'expReturns'.tr,
                                        onTapTitle: () {
                                          showModalBottomSheet(
                                              backgroundColor:
                                                  Colors.transparent,
                                              context: context,
                                              isScrollControlled: true,
                                              builder: (ctx) =>
                                                  const ExpectedReturnsDialogContent());
                                        },
                                        value: '10.00%',
                                        valueStyle: Styles.regular16Grey
                                            .copyWith(color: Colors.green),
                                        isEditText: true,
                                        onTapEdit: () {
                                          Get.to(
                                            const CreateGoalCostOfRide(
                                              isEditingFromSummary: true,
                                            ),
                                          );
                                        },
                                        valueAlignment: CrossAxisAlignment.end,
                                      ),
                                    ],
                                  ),
                                  Dimens.boxHeight30,
                                ],
                              ),
                            ),
                            Positioned.fill(
                              child: Align(
                                alignment: Alignment.bottomCenter,
                                child: Container(
                                  padding: Dimens.edgeInsets15_0_15_0,
                                  width: Dimens.percentWidth(1),
                                  height: Dimens.twentyFive,
                                  decoration: BoxDecoration(
                                    color: const Color(0xffEBE3FD),
                                    borderRadius: BorderRadius.only(
                                      bottomLeft:
                                          Radius.circular(Dimens.fifteen),
                                      bottomRight:
                                          Radius.circular(Dimens.fifteen),
                                    ),
                                  ),
                                  child: SizedBox(
                                    width: Dimens.percentWidth(1),
                                    child: Row(
                                      children: [
                                        Text(
                                          'goalAmount'.tr,
                                          style: Styles.regular12Black,
                                        ),
                                        const Spacer(),
                                        Text(
                                          '₹ 5,35,000',
                                          style: Styles.medium14Black,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Dimens.boxHeight20,
                    Divider(
                      color: Colors.grey.withOpacity(0.6),
                    ),
                    Theme(
                      data: Theme.of(context)
                          .copyWith(dividerColor: Colors.transparent),
                      child: ExpansionTile(
                        title: Text(
                          'goalSummary'.tr,
                          style: Styles.boldBlack16,
                        ),
                        onExpansionChanged: (v) {
                          _controller.isExpandedPaned = v;
                          _controller.update();
                        },
                        initiallyExpanded: true,
                        trailing: !_controller.isExpandedPaned
                            ? const Icon(
                                Icons.keyboard_arrow_down,
                                color: ColorsValue.greyColor,
                              )
                            : const Icon(
                                Icons.keyboard_arrow_up,
                                color: ColorsValue.greyColor,
                              ),
                        children: [
                          DefaultContainer(
                            child: Column(
                              children: [
                                ExpandedCustomTile(
                                  title: 'goalAmount'.tr,
                                  value: '₹ 5,53,000',
                                  onTap: () {},
                                ),
                                ExpandedCustomTile(
                                  title: 'Goal duration'.tr,
                                  value: '5 Years',
                                  onTap: () {},
                                ),
                                ExpandedCustomTile(
                                  title: 'Initial Amount'.tr,
                                  value: '₹ 25,000',
                                  onTap: () {},
                                ),
                                ExpandedCustomTile(
                                  title: 'Inflation'.tr,
                                  value: '7%',
                                  onTap: () {},
                                ),
                                ExpandedCustomTile(
                                  title: 'Inflow'.tr,
                                  value: '₹ 1,00,000',
                                  onTap: () {},
                                ),
                                ExpandedCustomTile(
                                  title: 'Inflow Year'.tr,
                                  value: '2 Years',
                                  onTap: () {},
                                ),
                                ExpandedCustomTile(
                                  title: 'Payment method'.tr,
                                  value: _controller.sipInvestment
                                      ? 'Loan'
                                      : 'Full Payment',
                                  onTap: () {},
                                ),
                                ExpandedCustomTile(
                                  title: _controller.sipInvestment
                                      ? 'Loan Amount'
                                      : 'Type of investment'.tr,
                                  value: _controller.sipInvestment
                                      ? '₹ 2,00,000'
                                      : 'SIP',
                                  needPadding: false,
                                  needMargin: false,
                                  needBorder: false,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Dimens.boxHeight20,
                    if (_controller.sipInvestment)
                      Theme(
                        data: Theme.of(context)
                            .copyWith(dividerColor: Colors.transparent),
                        child: ExpansionTile(
                          title: Text(
                            'Loan Details'.tr,
                            style: Styles.boldBlack16,
                          ),
                          onExpansionChanged: (v) {
                            _controller.isLoanDetailsExpanded = v;
                            _controller.update();
                          },
                          initiallyExpanded: true,
                          trailing: !_controller.isLoanDetailsExpanded
                              ? const Icon(
                                  Icons.keyboard_arrow_down,
                                  color: ColorsValue.greyColor,
                                )
                              : const Icon(
                                  Icons.keyboard_arrow_up,
                                  color: ColorsValue.greyColor,
                                ),
                          children: [
                            DefaultContainer(
                              child: Column(
                                children: [
                                  ExpandedCustomTile(
                                    title: 'Down payment'.tr,
                                    value: '₹ 50,000',
                                  ),
                                  ExpandedCustomTile(
                                    title: 'Interest rate'.tr,
                                    value: '7%',
                                  ),
                                  ExpandedCustomTile(
                                    title: 'Loan Tenure'.tr,
                                    value: '2 Years',
                                    needPadding: false,
                                    needMargin: false,
                                    needBorder: false,
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    Dimens.boxHeight20,
                    SvgPicture.asset('assets/home/CTA.svg')
                  ],
                ),
              ),
            ),
          ));
}


