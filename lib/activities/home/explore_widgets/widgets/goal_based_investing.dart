import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

import 'goal_based_investing_screen.dart';

class ExploreGoalBasedInvesting extends StatelessWidget {
  const ExploreGoalBasedInvesting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: Stack(
          children: [
            Container(
              height: Get.height * .35,
              width: Dimens.percentWidth(1),
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    AssetConstants.goalBasedBg,
                  ),
                  alignment: Alignment.centerRight,
                ),
                color: ColorsValue.secondaryColor,
              ),
            ),
            Scaffold(
              backgroundColor: Colors.transparent,
              appBar: const CustomAppBar(
                title: '',
              ),
              body: SafeArea(
                child: Padding(
                  padding: Dimens.edgeInsets16,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'goalBasedInvesting'.tr,
                        style: Styles.bold24Black,
                      ),
                      Dimens.boxHeight10,
                      Text(
                        'investLifeGoal'.tr,
                        style: Styles.regular14Grey,
                      ),
                      Dimens.boxHeight25,
                      DefaultContainer(
                        child: Column(
                          children: [
                            SvgPicture.asset('assets/home/goal-based.svg'),
                            Dimens.boxHeight25,
                            const PointBulletsWidget(
                              title:
                                  'Pick from our suggested goals or create your own.',
                            ),
                            Dimens.boxHeight25,
                            const PointBulletsWidget(
                              title:
                                  'Objective is to measure the progress towards achieving life goals like buying a car or building a retirement nest egg.',
                            ),
                            Dimens.boxHeight25,
                          ],
                        ),
                      ),
                      const Spacer(),
                      SizedBox(
                        width: Get.width,
                        child: CustomButton(
                          title: 'continue'.tr,
                          onTap: () {
                            Get.to(const GoalBasedInvestingScreen());
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}
