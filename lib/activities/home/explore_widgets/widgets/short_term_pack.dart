import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class ShortTermPack extends StatelessWidget {
  const ShortTermPack({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: const CustomAppBar(title: 'Curated Packs'),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: Dimens.edgeInsets16,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Dimens.boxHeight26,
                  Text(
                    'Short Term Pack',
                    style: Styles.boldBlack35,
                  ),
                  Dimens.boxHeight15,
                  Text(
                    'Park your idle cash here instead of keeping it in your wallet',
                    style: Styles.black16,
                  ),
                  ...List.generate(
                    5,
                    (index) => const CuratedPackTile(
                      title: 'Why this pack ?',
                      data: [
                        {
                          'icon': 'assets/home/brif.svg',
                          'title': 'Brief',
                          'descp':
                              'Money invested in this pack offers a stress free life for your unexpected spends. It should typically be 3-6 months\' worth of your expenses'
                        },
                        {
                          'icon': 'assets/home/brif.svg',
                          'title': 'Brief',
                          'descp':
                              'Money invested in this pack offers a stress free life for your unexpected spends. It should typically be 3-6 months\' worth of your expenses'
                        }
                      ],
                    ),
                  ).toList(),
                  SvgPicture.asset('assets/home/CTA.svg'),
                ],
              ),
            ),
          ),
        ),
      );
}

class CuratedPackTile extends StatelessWidget {
  const CuratedPackTile({
    Key? key,
    this.title,
    this.data,
  }) : super(key: key);

  final String? title;
  final List<Map<String, String>>? data;

  @override
  Widget build(BuildContext context) => Container(
        margin: const EdgeInsets.only(top: 36),
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            DefaultContainer(
              border: Border.all(
                width: 1,
                color: ColorsValue.primaryColor.withOpacity(.4),
              ),
              child: Column(
                children: [
                  ...List.generate(
                    data!.length,
                    (index) => Column(
                      children: [
                        Dimens.boxHeight30,
                        SvgPicture.asset(
                          data![index]['icon']!,
                          height: 45,
                        ),
                        Dimens.boxHeight10,
                        Text(
                          '${data![index]['title']!}',
                          style: Styles.boldBlack22,
                        ),
                        Dimens.boxHeight10,
                        Text(
                          '${data![index]['descp']!}',
                          style: Styles.black16,
                          textAlign: TextAlign.center,
                        ),
                      ],
                    ),
                  ).toList(),
                ],
              ),
            ),
            Positioned(
              top: -20,
              left: 0,
              right: 0,
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 60),
                width: 140,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                    Radius.circular(50),
                  ),
                  color: ColorsValue.primaryColor.withOpacity(.2),
                ),
                child: Center(
                  child: Text(
                    '$title',
                    style: Styles.boldBlack15,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}
