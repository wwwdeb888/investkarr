import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/create_goal_cost_of_ride.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class CreateGoalPlanToBuy extends StatelessWidget {
  const CreateGoalPlanToBuy({Key? key, this.isEditingFromSummary = false})
      : super(key: key);

  final bool isEditingFromSummary;

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
      builder: (_controller) => Scaffold(
            appBar: CustomAppBar(
              title: isEditingFromSummary
                  ? 'Edit Goal Summary'
                  : 'settingAGoal'.tr,
            ),
            body: SafeArea(
              child: Container(
                padding: Dimens.edgeInsets16,
                width: Get.width,
                child: CustomScrollView(
                  slivers: [
                    SliverFillRemaining(
                      hasScrollBody: false,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Dimens.boxHeight30,
                          CircleIcon(
                            image: 'assets/home/car-compact 1.svg',
                            color: Colors.orange.withOpacity(.2),
                          ),
                          Dimens.boxHeight20,
                          SizedBox(
                            width: Dimens.percentWidth(0.7),
                            child: Text(
                              'whenDoYouPlanToBuyYourRide'.tr,
                              textAlign: TextAlign.center,
                              style: Styles.bold24Black,
                            ),
                          ),
                          Dimens.boxHeight20,
                          Padding(
                            padding: Dimens.edgeInsets5_0_5_0 +
                                Dimens.edgeInsets5_0_5_0 +
                                Dimens.edgeInsets0_0_5_0,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  'In How Many Years'.tr,
                                  style: Styles.medium14Black
                                      .copyWith(color: ColorsValue.textColor),
                                ),
                                SizedBox(
                                  width: Dimens.percentWidth(0.15),
                                  child: FormFieldWidget(
                                    onChange: (value) {},
                                    elevation: 0,
                                    initialValue: '5 Yrs',
                                    textAlign: TextAlign.end,
                                    formStyle: Styles.medium16Black,
                                    borderColor:
                                        ColorsValue.greyColor.withOpacity(0.5),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Dimens.boxHeight10,
                          SizedBox(
                            width: Dimens.percentWidth(1),
                            child: SliderTheme(
                              data: SliderTheme.of(context).copyWith(
                                activeTrackColor: ColorsValue.activeTrackColor,
                                inactiveTrackColor:
                                    ColorsValue.inactiveTrackColor,
                                trackHeight: 5,
                                thumbShape: const SliderThumbShape(),
                                overlayShape: const RoundSliderOverlayShape(
                                    overlayRadius: 15.0),
                              ),
                              child: Slider(
                                activeColor: ColorsValue.primaryColor,
                                min: 0,
                                max: 10,
                                value: _controller.buyYearsPlan,
                                onChanged: (value) {
                                  _controller.buyYearsPlan = value;
                                  _controller.update();
                                  debugPrint(value.toString());
                                },
                              ),
                            ),
                          ),
                          Dimens.boxHeight20,
                          Padding(
                            padding: Dimens.edgeInsets5_0_0_0 +
                                Dimens.edgeInsets2_0_0_0,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  _controller.isFutureInflows
                                      ? 'Are there any future inflows?'
                                      : 'Will there be any future inflows?',
                                  style: Styles.medium14Black
                                      .copyWith(color: ColorsValue.textColor),
                                ),
                                FlutterSwitch(
                                  padding: Dimens.three,
                                  activeColor: ColorsValue.primaryColor,
                                  inactiveColor: const Color(0xffEBDDF8),
                                  width: Dimens.fourtyFive,
                                  toggleColor: _controller.isFutureInflows
                                      ? Colors.white
                                      : ColorsValue.primaryColor,
                                  height: Dimens.twentyFour,
                                  toggleSize: Dimens.twenty,
                                  value: _controller.isFutureInflows,
                                  onToggle: (val) {
                                    _controller.isFutureInflows = val;
                                    _controller.update();
                                  },
                                ),
                              ],
                            ),
                          ),
                          if (_controller.isFutureInflows) Dimens.boxHeight20,

                          /// next slider
                          if (_controller.isFutureInflows)
                            Column(
                              children: [
                                Padding(
                                  padding: Dimens.edgeInsets5_0_5_0 +
                                      Dimens.edgeInsets5_0_5_0 +
                                      Dimens.edgeInsets0_0_5_0,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Inflow amount'.tr,
                                        style: Styles.medium14Black.copyWith(
                                            color: ColorsValue.textColor),
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            '\u{20B9}',
                                            style: Styles.medium16Black,
                                          ),
                                          Dimens.boxWidth10,
                                          SizedBox(
                                            width: Dimens.percentWidth(0.2),
                                            child: FormFieldWidget(
                                              onChange: (value) {},
                                              elevation: 0,
                                              initialValue: '1,00,000',
                                              textAlign: TextAlign.end,
                                              formStyle: Styles.medium16Black,
                                              borderColor: ColorsValue.greyColor
                                                  .withOpacity(0.5),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                Dimens.boxHeight10,
                                SizedBox(
                                  width: Dimens.percentWidth(1),
                                  child: SliderTheme(
                                    data: SliderTheme.of(context).copyWith(
                                      activeTrackColor:
                                          ColorsValue.activeTrackColor,
                                      inactiveTrackColor:
                                          ColorsValue.inactiveTrackColor,
                                      trackHeight: 5,
                                      thumbShape: const SliderThumbShape(),
                                      overlayShape:
                                          const RoundSliderOverlayShape(
                                              overlayRadius: 15.0),
                                    ),
                                    child: Slider(
                                      activeColor: ColorsValue.primaryColor,
                                      min: 0,
                                      max: 10,
                                      value: _controller.inflowAmount,
                                      onChanged: (value) {
                                        _controller.inflowAmount = value;
                                        _controller.update();
                                        debugPrint(value.toString());
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          if (_controller.isFutureInflows) Dimens.boxHeight20,

                          /// next slider
                          if (_controller.isFutureInflows)
                            Column(
                              children: [
                                Padding(
                                  padding: Dimens.edgeInsets5_0_5_0 +
                                      Dimens.edgeInsets5_0_5_0 +
                                      Dimens.edgeInsets0_0_5_0,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        'Inflow Year'.tr,
                                        style: Styles.medium14Black.copyWith(
                                            color: ColorsValue.textColor),
                                      ),
                                      SizedBox(
                                        width: Dimens.percentWidth(0.15),
                                        child: FormFieldWidget(
                                          onChange: (value) {},
                                          elevation: 0,
                                          initialValue: '2 Yrs',
                                          textAlign: TextAlign.end,
                                          formStyle: Styles.medium16Black,
                                          borderColor: ColorsValue.greyColor
                                              .withOpacity(0.5),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Dimens.boxHeight10,
                                SizedBox(
                                  width: Dimens.percentWidth(1),
                                  child: SliderTheme(
                                    data: SliderTheme.of(context).copyWith(
                                      activeTrackColor:
                                          ColorsValue.activeTrackColor,
                                      inactiveTrackColor:
                                          ColorsValue.inactiveTrackColor,
                                      trackHeight: 5,
                                      thumbShape: const SliderThumbShape(),
                                      overlayShape:
                                          const RoundSliderOverlayShape(
                                              overlayRadius: 15.0),
                                    ),
                                    child: Slider(
                                      activeColor: ColorsValue.primaryColor,
                                      min: 0,
                                      max: 10,
                                      value: _controller.inflowYear,
                                      onChanged: (value) {
                                        _controller.inflowYear = value;
                                        _controller.update();
                                        debugPrint(value.toString());
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          Dimens.boxHeight26,
                          const Spacer(),

                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Padding(
                              padding: Dimens.edgeInsets5,
                              child: SizedBox(
                                width: Dimens.percentWidth(1),
                                height: Dimens.percentHeight(.07),
                                child: ElevatedButton(
                                  style: Styles.elevatedButtonTheme.style!
                                      .copyWith(
                                    backgroundColor: MaterialStateProperty
                                        .resolveWith<Color>(
                                      (Set<MaterialState> states) =>
                                          (_controller.isFutureInflows)
                                              ? ColorsValue.primaryColor
                                              : ColorsValue.primaryColor
                                                  .withOpacity(.5),
                                    ),
                                    textStyle: MaterialStateProperty.all(
                                      Styles.bold16White,
                                    ),
                                  ),
                                  onPressed: () {
                                    if (isEditingFromSummary) {
                                      Get.back<void>();
                                      Get.back<void>();
                                    } else {
                                      if (_controller.isFutureInflows) {
                                        Get.to(const CreateGoalCostOfRide());
                                      } else {}
                                    }
                                  },
                                  child: Text(
                                    isEditingFromSummary
                                        ? 'Save'
                                        : 'Continue'.tr,
                                    style: Styles.bold16White,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ));
}
