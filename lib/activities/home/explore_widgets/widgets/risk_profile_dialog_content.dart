import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class RiskProfileDialogContent extends StatelessWidget {
  const RiskProfileDialogContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => StatefulBuilder(
          builder: (context, setState) => Container(
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(25.0),
                topRight: Radius.circular(25.0),
              ),
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Dimens.boxHeight10,
                  Container(
                    width: Dimens.fourty,
                    height: Dimens.five,
                    decoration: BoxDecoration(
                      color: ColorsValue.greyColor.withOpacity(.2),
                      borderRadius: const BorderRadius.all(
                        Radius.circular(50),
                      ),
                    ),
                  ),
                  Dimens.boxHeight30,
                  CircleIcon(
                    image: AssetConstants.danger,
                    svgColor: ColorsValue.iconBlueColor,
                    color: ColorsValue.pinkColor,
                    isSvg: true,
                  ),
                  Dimens.boxHeight20,
                  Text(
                    'Complete your Risk Profile',
                    style: Styles.bold24Black,
                  ),
                  Dimens.boxHeight5,
                  SizedBox(
                    width: Dimens.percentWidth(0.75),
                    child: Text(
                      'You need to complete your risk profile to edit the expected returns',
                      textAlign: TextAlign.center,
                      style: Styles.regular14Grey,
                    ),
                  ),
                  Dimens.boxHeight30,
                  Padding(
                    padding: Dimens.edgeInsets15_0_15_0,
                    child: Expanded(
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: Dimens.edgeInsets5,
                          child: SizedBox(
                            width: Dimens.percentWidth(1),
                            height: Dimens.percentHeight(.07),
                            child: ElevatedButton(
                              style: Styles.elevatedButtonTheme.style!.copyWith(
                                elevation: MaterialStateProperty.all(0),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(Dimens.fifty),
                                        side: BorderSide(
                                            width: Dimens.two,
                                            color: ColorsValue.primaryColor))),
                                backgroundColor:
                                    MaterialStateProperty.resolveWith<Color>(
                                  (Set<MaterialState> states) => (0 == 0)
                                      ? ColorsValue.primaryColor
                                      : ColorsValue.primaryColor
                                          .withOpacity(.5),
                                ),
                                textStyle: MaterialStateProperty.all(
                                  Styles.bold16White,
                                ),
                              ),
                              onPressed: () {
                                Get.back<void>();
                              },
                              child: Text(
                                'Continue'.tr,
                                style: Styles.bold16White,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  Dimens.boxHeight15,
                ],
              ),
            ),
          ),
        ),
      );
}
