import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';
import 'package:investkarr/widgets/curated_tile.dart';

import 'tax_saver_pack_screen.dart';

class MediumTermPack extends StatelessWidget {
  const MediumTermPack({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: const CustomAppBar(title: 'Curated Packs'),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: Dimens.edgeInsets16,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Dimens.boxHeight26,
                  Text(
                    'Medium Term Pack',
                    style: Styles.boldBlack35,
                  ),
                  Dimens.boxHeight15,
                  Text(
                    'For all your short term needs this pack is more than enough.',
                    style: Styles.black16,
                  ),
                  ...List.generate(
                    5,
                    (index) => const CuratedPackTile(
                      title: 'Why this pack ?',
                      data: [
                        {
                          'icon': 'assets/home/brif.svg',
                          'title': 'Brief',
                          'descp':
                              'Money invested in this pack offers a stress free life for your unexpected spends. It should typically be 3-6 months\' worth of your expenses'
                        },
                        {
                          'icon': 'assets/home/brif.svg',
                          'title': 'Brief',
                          'descp':
                              'Money invested in this pack offers a stress free life for your unexpected spends. It should typically be 3-6 months\' worth of your expenses'
                        }
                      ],
                    ),
                  ).toList(),
                  GestureDetector(
                    onTap: () {
                      Get.to(TaxSaverPackScreen());
                    },
                    child: SvgPicture.asset('assets/home/CTA.svg'),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
