import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/sip_create.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

import 'create_goal_name.dart';

class GoalBasedInvestingScreen extends StatelessWidget {
  const GoalBasedInvestingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: CustomAppBar(
          title: StringConstants.goalBasedInvesting,
          actions: Row(
            children: [
              CircleAvatar(
                radius: Dimens.sixTeen,
                backgroundColor: const Color(0xffEFE8FC),
                child: Icon(
                  Icons.search,
                  size: Dimens.twenty,
                  color: ColorsValue.primaryColor,
                ),
              ),
              Dimens.boxWidth15,
            ],
          ),
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: Dimens.edgeInsets16,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'suggestedGoalsForYou'.tr,
                        style: Styles.boldBlack16,
                      ),
                      Dimens.boxHeight20,
                      GoalTile(
                        title: 'retirement'.tr,
                        cardColor: Color(0xffFFF0EB),
                        cardIcon: AssetConstants.retirement,
                        onTap: () {
                          Get.to(SipCreate());
                        },
                        subTitle:
                            'Lorem ipsum dolor sit amet, asseyut qer consectetur adipiscing elit. ',
                      ),
                      Dimens.boxHeight20,
                      GoalTile(
                        title: 'Marriage'.tr,
                        cardColor: Color(0xffEFA9C1).withOpacity(0.5),
                        cardIcon: AssetConstants.weddingRings,
                        subTitle:
                            'Lorem ipsum dolor sit amet, asseyut qer consectetur adipiscing elit. ',
                      ),
                      Dimens.boxHeight20,
                      GoalTile(
                        isShortTerm: true,
                        title: 'Education'.tr,
                        cardColor: Color(0xffE9F5F3),
                        cardIcon: AssetConstants.education,
                        subTitle:
                            'Lorem ipsum dolor sit amet, asseyut qer consectetur adipiscing elit. ',
                      ),
                      Dimens.boxHeight20,
                      Text(
                        'shortTermGoals'.tr,
                        style: Styles.boldBlack16,
                      ),
                    ],
                  ),
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      ...List.generate(
                        10,
                        (index) => const GoalTileColumn(
                          icon: 'assets/home/gadgets.svg',
                          title: 'Gadgets',
                          bgImage: AssetConstants.looperFour,
                          subTitle:
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ',
                        ),
                      ).toList(),
                    ],
                  ),
                ),
                Dimens.boxHeight20,
                Padding(
                  padding: Dimens.edgeInsets16,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'longTermGoals'.tr,
                            style: Styles.boldBlack16,
                          ),
                          GestureDetector(
                            onTap: () {
                              Get.to(const CreateGoalName());
                            },
                            child: Container(
                              padding: const EdgeInsets.symmetric(
                                vertical: 6,
                                horizontal: 20,
                              ),
                              decoration: BoxDecoration(
                                border: Border.all(
                                  width: 1,
                                  color: ColorsValue.primaryColor,
                                ),
                                borderRadius: const BorderRadius.all(
                                  Radius.circular(50),
                                ),
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Container(
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      border: Border.all(
                                        width: .5,
                                        color: ColorsValue.primaryColor,
                                      ),
                                    ),
                                    child: Icon(
                                      Icons.add,
                                      size: Dimens.twelve,
                                      color: ColorsValue.primaryColor,
                                    ),
                                  ),
                                  Dimens.boxWidth8,
                                  Text(
                                    'create'.tr,
                                    style: Styles.bold14Primary,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      Dimens.boxHeight20,
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            GoalTile(
                              title: 'Child\'s Education'.tr,
                              cardColor: Color(0xffEFA9C1).withOpacity(0.5),
                              width: Dimens.percentWidth(0.8),
                              cardIcon: AssetConstants.readingBook,
                              subTitle:
                                  'Lorem ipsum dolor sit amet, asseyut qer consectetur adipiscing elit. ',
                            ),
                            Dimens.boxWidth10,
                            GoalTile(
                              title: 'retirement'.tr,
                              cardColor: Color(0xffFFE0AD).withOpacity(0.5),
                              width: Dimens.percentWidth(0.8),
                              cardIcon: AssetConstants.carCompact,
                              subTitle:
                                  'Lorem ipsum dolor sit amet, asseyut qer consectetur adipiscing elit. ',
                            ),
                          ],
                        ),
                      ),
                      Dimens.boxHeight20,
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            GoalTile(
                              title: 'Buying a Car'.tr,
                              cardColor: Color(0xffFFC5AD).withOpacity(0.5),
                              width: Dimens.percentWidth(0.8),
                              cardIcon: AssetConstants.carCompact,
                              subTitle:
                                  'Lorem ipsum dolor sit amet, asseyut qer consectetur adipiscing elit. ',
                            ),
                            Dimens.boxWidth10,
                            GoalTile(
                              title: 'retirement'.tr,
                              cardColor: Color(0xffEFA9C1).withOpacity(0.5),
                              width: Dimens.percentWidth(0.8),
                              cardIcon: AssetConstants.education,
                              subTitle:
                                  'Lorem ipsum dolor sit amet, asseyut qer consectetur adipiscing elit. ',
                            ),
                          ],
                        ),
                      ),
                      Dimens.boxHeight20,
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: [
                            GoalTile(
                              title: 'Buying a House'.tr,
                              cardColor: Color(0xffFFE0AD).withOpacity(0.5),
                              width: Dimens.percentWidth(0.8),
                              cardIcon: AssetConstants.hut,
                              subTitle:
                                  'Lorem ipsum dolor sit amet, asseyut qer consectetur adipiscing elit. ',
                            ),
                            Dimens.boxWidth10,
                            GoalTile(
                              title: 'retirement'.tr,
                              cardColor: Color(0xffFFC5AD).withOpacity(0.5),
                              width: Dimens.percentWidth(0.8),
                              cardIcon: AssetConstants.education,
                              subTitle:
                                  'Lorem ipsum dolor sit amet, asseyut qer consectetur adipiscing elit. ',
                            ),
                          ],
                        ),
                      ),
                      Dimens.boxHeight20,
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}

class GoalTileColumn extends StatelessWidget {
  const GoalTileColumn({
    Key? key,
    this.icon,
    this.title,
    this.subTitle,
    this.bgImage,
    this.onTap,
  }) : super(key: key);

  final String? icon;
  final String? title;
  final String? subTitle;
  final String? bgImage;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) => Padding(
        padding: Dimens.edgeInsets16_0_0_0,
        child: Container(
          padding: Dimens.edgeInsets12,
          width: Dimens.percentWidth(0.52),
          height: Dimens.percentHeight(0.27),
          decoration: BoxDecoration(
            boxShadow: Styles.cardShadow,
            borderRadius: BorderRadius.circular(Dimens.twelve),
            image: DecorationImage(
              image: AssetImage(
                bgImage!,
              ),
              alignment: Alignment.topRight,
            ),
            color: Colors.white,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SvgPicture.asset('$icon'),
              Dimens.boxHeight10,
              Text(
                '$title',
                style: Styles.bold16Black,
              ),
              Dimens.boxHeight15,
              Text(
                '$subTitle',
                overflow: TextOverflow.ellipsis,
                maxLines: 3,
                style: Styles.regular12Grey,
              ),
              Dimens.boxHeight25,
              SetGoalWidget(onTap: onTap),
            ],
          ),
        ),
      );
}

class GoalTile extends StatelessWidget {
  const GoalTile({
    Key? key,
    this.title,
    this.subTitle,
    this.isShortTerm = false,
    this.onTap,
    this.cardColor,
    this.width,
    this.height,
    this.cardIcon,
  }) : super(key: key);

  final String? title;
  final String? subTitle;
  final bool? isShortTerm;
  final Color? cardColor;
  final String? cardIcon;
  final double? width;
  final double? height;
  final Function()? onTap;

  @override
  Widget build(BuildContext context) => Stack(
        children: [
          Container(
            padding: Dimens.edgeInsets15,
            width: width ?? Dimens.percentWidth(1),
            height: height ?? Dimens.percentHeight(0.2),
            decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: Styles.cardShadow,
                borderRadius: BorderRadius.circular(Dimens.fifteen)),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: cardColor!,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(12),
                    ),
                  ),
                  width: Dimens.percentWidth(0.22),
                  child: Center(
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(Dimens.fifty)),
                      width: Dimens.fourty,
                      height: Dimens.fourty,
                      child: Center(
                        child: Image.asset(cardIcon!),
                      ),
                    ),
                  ),
                ),
                Dimens.boxWidth10,
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '$title',
                            style: Styles.bold16Black,
                          ),
                          Dimens.boxHeight10,
                          Text(
                            '$subTitle',
                            overflow: TextOverflow.ellipsis,
                            maxLines: 3,
                            style: Styles.regular12Grey,
                          ),
                        ],
                      ),
                      SetGoalWidget(onTap: onTap),
                    ],
                  ),
                )
              ],
            ),
          ),
          if (isShortTerm!)
            Positioned(
              right: 0,
              top: 0,
              child: SvgPicture.asset('assets/home/short_term_badge.svg'),
            ),
        ],
      );
}

class SetGoalWidget extends StatelessWidget {
  const SetGoalWidget({
    Key? key,
    required this.onTap,
  }) : super(key: key);

  final Function()? onTap;

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: onTap,
        child: Row(
          children: [
            Text(
              'setGoal'.tr,
              style: Styles.bold14Primary,
            ),
            Dimens.boxWidth8,
            Container(
              padding: Dimens.edgeInsets5,
              width: Dimens.twenty,
              height: Dimens.twenty,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: ColorsValue.circleArrowColor,
              ),
              child: Center(
                child: Icon(
                  Icons.arrow_forward_ios_rounded,
                  size: Dimens.twelve,
                ),
              ),
            ),
          ],
        ),
      );
}
