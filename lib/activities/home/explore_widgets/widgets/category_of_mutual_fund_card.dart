import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class CategoryOfMutualFundCard extends StatelessWidget {
  CategoryOfMutualFundCard({
    Key? key,
    required this.title,
    required this.subTitle,
    required this.bgAssetImage,
    required this.iconImage,
    this.width,
    this.trimTextLength,
    this.isWithoutCircleIconImage = false,
    this.circleFillColor,
  }) : super(key: key);
  final String? title;
  final String? subTitle;
  final String? bgAssetImage;
  final String? iconImage;
  double? width;
  int? trimTextLength;
  bool? isWithoutCircleIconImage;
  Color? circleFillColor;

  @override
  Widget build(BuildContext context) => Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(Dimens.fifteen),
          image: DecorationImage(
              image: AssetImage(
                bgAssetImage!,
              ),
              scale: 0.1,
              alignment: Alignment.topRight),
          color: Colors.white,
          boxShadow: Styles.cardShadow,
        ),
        child: Padding(
          padding: Dimens.edgeInsets10,
          child: SizedBox(
            width: width ?? Dimens.percentWidth(0.40),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                isWithoutCircleIconImage!
                    ? Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(Dimens.fifty),
                          color: circleFillColor ?? ColorsValue.lightYellow,
                        ),
                        width: Dimens.fourty,
                        height: Dimens.fourty,
                        child: Center(
                          child: Image.asset(
                            iconImage!,
                            scale: 0.9,
                          ),
                        ),
                      )
                    : SizedBox(
                        width: Dimens.fourty,
                        height: Dimens.fourty,
                        child: Image.asset(
                          iconImage!,
                          fit: BoxFit.cover,
                        ),
                      ),
                Dimens.boxHeight10,
                Text(
                  title!,
                  style: Styles.boldBlack14,
                ),
                Dimens.boxHeight10,
                GetReadMoreLable(
                  text: subTitle!,
                  maxLength: trimTextLength ?? 42,
                  moreText: 'More',
                  textStyle: Styles.regular12Grey,
                  moreStyle: Styles.regular12GreyUnderline,
                  onTap: () {
                    showModalBottomSheet(
                      backgroundColor: Colors.transparent,
                      context: context,
                      builder: (ctx) => Container(
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(25.0),
                            topRight: Radius.circular(25.0),
                          ),
                        ),
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Dimens.boxHeight20,
                              Container(
                                width: 50,
                                height: 6,
                                decoration: BoxDecoration(
                                  color: ColorsValue.greyColor.withOpacity(.2),
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(50),
                                  ),
                                ),
                              ),
                              Padding(
                                padding: Dimens.edgeInsets16,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Padding(
                                        padding: Dimens.edgeInsets16,
                                        child: Text(
                                          title!,
                                          style: Styles.boldBlack16,
                                        ),
                                      ),
                                    ),
                                    Align(
                                      alignment: Alignment.centerLeft,
                                      child: Padding(
                                        padding: Dimens.edgeInsets16,
                                        child: Text(
                                          subTitle!,
                                          style: Styles.regular12Grey,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      );
}
