import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class InvestYourself extends StatelessWidget {
  const InvestYourself({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        appBar: CustomAppBar(
          title: StringConstants.investYourself,
          actions: Row(
            children: [
              CircleAvatar(
                radius: Dimens.sixTeen,
                backgroundColor: const Color(0xffEFE8FC),
                child: Icon(
                  Icons.search,
                  size: Dimens.twenty,
                  color: ColorsValue.primaryColor,
                ),
              ),
              Dimens.boxWidth15,
            ],
          ),
        ),
        body: Column(
          children: [
            GetBuilder<HomeController>(
              builder: (_controller) => Expanded(
                child: ListView(
                  children: [
                    const Divider(),
                    Padding(
                      padding: Dimens.edgeInsets10,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: Dimens.edgeInsets5_0_0_0,
                            child: Text(
                              StringConstants.categoriesOfMF,
                              style: Styles.bold16Black,
                            ),
                          ),
                          Dimens.boxHeight20,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CategoryOfMutualFundCard(
                                title: StringConstants.equity,
                                subTitle:
                                    'Funds primarily invest in stocks of listed cut',
                                iconImage: AssetConstants.equity,
                                bgAssetImage: AssetConstants.looperOne,
                              ),
                              CategoryOfMutualFundCard(
                                title: StringConstants.debts,
                                subTitle:
                                    'Funds invest in more than one asset class',
                                iconImage: AssetConstants.debt,
                                bgAssetImage: AssetConstants.looperOne,
                              ),
                            ],
                          ),
                          Dimens.boxHeight15,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CategoryOfMutualFundCard(
                                title: StringConstants.hybrid,
                                subTitle:
                                    'Funds invest in more than one asset class',
                                iconImage: AssetConstants.hybrid,
                                bgAssetImage: AssetConstants.looperTwo,
                              ),
                              CategoryOfMutualFundCard(
                                title: StringConstants.international,
                                subTitle: 'Funds invest in companies location',
                                iconImage: AssetConstants.international,
                                bgAssetImage: AssetConstants.looperTwo,
                              ),
                            ],
                          ),
                          Dimens.boxHeight15,
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              CategoryOfMutualFundCard(
                                title: StringConstants.goldFunds,
                                subTitle:
                                    'Funds invest in physical gold, ETFs & stock market',
                                iconImage: AssetConstants.goldFunds,
                                bgAssetImage: AssetConstants.looperThree,
                              ),
                              CategoryOfMutualFundCard(
                                title: StringConstants.others,
                                subTitle:
                                    'Fund categories include index funds, fund and ',
                                iconImage: AssetConstants.others,
                                bgAssetImage: AssetConstants.looperThree,
                              ),
                            ],
                          ),
                          Dimens.boxHeight20,
                          Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.circular(Dimens.fifteen),
                              color: Colors.white,
                              boxShadow: Styles.cardShadow,
                            ),
                            child: Container(
                              padding: Dimens.edgeInsets10,
                              width: Dimens.percentWidth(1),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        StringConstants.myWatchlist,
                                        style: Styles.medium18Black,
                                      ),
                                      Dimens.boxHeight10,
                                      SizedBox(
                                        width: Dimens.percentWidth(.4),
                                        child: Text(
                                          StringConstants.keepTrack,
                                          style: Styles.black12,
                                        ),
                                      ),
                                      Dimens.boxHeight20,
                                      SizedBox(
                                        width: Dimens.percentWidth(.41),
                                        child: FormSubmitWidget(
                                          opacity: 1,
                                          backgroundColor:
                                              ColorsValue.primaryColor,
                                          padding: Dimens.edgeInsets15,
                                          text: StringConstants.startTracking,
                                          textStyle: Styles.bold16White,
                                          onTap: () {},
                                        ),
                                      ),
                                    ],
                                  ),
                                  Image.asset(AssetConstants.watchlist,
                                      height: Dimens.oneHundredFourty)
                                ],
                              ),
                            ),
                          ),
                          Dimens.boxHeight20,
                          InvestYourselfHeadings(
                            heading: 'Top Equity Funds',
                          ),
                          Dimens.boxHeight10,
                          ListView.separated(
                            itemCount: 3,
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            separatorBuilder: (context, index) =>
                                Dimens.boxHeight15,
                            itemBuilder: (context, index) => SmallFundCard(
                                bankIcon: AssetConstants.hdfcLogo,
                                title: 'DSP Tax Saver Direct Plan growth',
                                returns: '3Y return',
                                percentage: '24.27%',
                                tagsList: [],
                                isGrowth: true),
                          ),
                          Dimens.boxHeight20,
                          InvestYourselfHeadings(
                            heading: 'Top Debt Funds',
                          ),
                          Dimens.boxHeight10,
                          ListView.separated(
                            itemCount: 3,
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            separatorBuilder: (context, index) =>
                                Dimens.boxHeight15,
                            itemBuilder: (context, index) => SmallFundCard(
                                bankIcon: AssetConstants.hdfcLogo,
                                title: 'Aditya Birla Gold Fund- Direct Plan',
                                returns: '3Y return',
                                percentage: '24.27%',
                                tagsList: [],
                                isGrowth: true),
                          ),
                          Dimens.boxHeight20,
                          InvestYourselfHeadings(
                            heading: 'All Mutual Funds',
                            isViewAll: true,
                            onTap: RouteManagement.goToAllMutualFunds,
                          ),
                          Dimens.boxHeight10,
                          ListView.separated(
                            itemCount: 3,
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            separatorBuilder: (context, index) =>
                                Dimens.boxHeight15,
                            itemBuilder: (context, index) => SmallFundCard(
                                bankIcon: AssetConstants.hdfcLogo,
                                title: 'DSP Tax Saver Direct Plan growth',
                                returns: '3Y return',
                                percentage: '24.27%',
                                tagsList: [],
                                isGrowth: true),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
}

class InvestYourselfHeadings extends StatelessWidget {
  InvestYourselfHeadings(
      {Key? key, required this.heading, this.isViewAll = false, this.onTap})
      : super(key: key);
  final String? heading;
  final bool? isViewAll;
  void Function()? onTap;

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            heading!,
            style: Styles.bold16Black,
          ),
          if (isViewAll!)
            GestureDetector(
              onTap: () {
                onTap!();
              },
              child: Text(
                StringConstants.viewAll,
                style: Styles.primaryBold13,
              ),
            )
        ],
      );
}
