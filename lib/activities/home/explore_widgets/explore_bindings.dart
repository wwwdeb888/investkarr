import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class ExploreBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(ExploreWidget.new);
  }
}
