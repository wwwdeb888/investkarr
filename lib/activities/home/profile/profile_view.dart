import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class ProfileView extends StatelessWidget {
  final gradient = const LinearGradient(
    colors: [
      Color(0xff4313A1),
      Color(0xffBD4238),
      Color(0xffFF5C00),
    ],
  );

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => SafeArea(
          child: Column(
            children: [
              Expanded(
                child: Padding(
                  padding: Dimens.edgeInsets10_0_10_0,
                  child: ListView(
                    children: [
                      Dimens.boxHeight10,
                      Container(
                        child: Padding(
                          padding: Dimens.edgeInsets10_0_10_0,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                StringConstants.profile,
                                style: Styles.medium18Black,
                              ),
                              Stack(
                                children: [
                                  GestureDetector(
                                    onTap: () {
                                      Get.to(NotificationsView());
                                    },
                                    child: CircleAvatar(
                                      radius: Dimens.eighteen,
                                      backgroundColor: const Color(0xffEFE8FC),
                                      child: Center(
                                        child: Image.asset(
                                          AssetConstants.notification,
                                          width: Dimens.eighteen,
                                          height: Dimens.eighteen,
                                        ),
                                      ),
                                    ),
                                  ),
                                  Positioned(
                                    top: 0,
                                    right: 0,
                                    child: GlobalContainer(
                                      width: Dimens.fourteen,
                                      height: Dimens.fourteen,
                                      color: ColorsValue.redColor,
                                      borderRadius: Dimens.fifteen,
                                      child: Center(
                                        child: Text(
                                          '10',
                                          style: Styles.bold8White,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ),
                      Dimens.boxHeight15,
                      Center(
                        child: CircleAvatar(
                          backgroundColor: ColorsValue.profileAvatarColor,
                          radius: Dimens.twentyFive,
                          child: Text(
                            'RR',
                            style: Styles.bold18Black,
                          ),
                        ),
                      ),
                      Dimens.boxHeight10,
                      Center(
                        child: Text(
                          'Ronald Richards',
                          style: Styles.bold18Black,
                        ),
                      ),
                      Dimens.boxHeight25,
                      _controller.isNonMemberProfile!
                          ? GestureDetector(
                              onTap: () {
                                _controller.isNonMemberProfile = false;
                                _controller.update();
                              },
                              child: Container(
                                width: Dimens.percentWidth(1),
                                height: Dimens.percentHeight(0.08),
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.circular(Dimens.ten),
                                    gradient: gradient),
                                child: Padding(
                                  padding: Dimens.edgeInsets12_0_12_0,
                                  child: Row(
                                    children: [
                                      CircleIcon(
                                        width: Dimens.thirtyTwo,
                                        height: Dimens.thirtyTwo,
                                        color: Colors.white,
                                        isSvg: false,
                                        image: AssetConstants.oldLogoPng,
                                      ),
                                      Dimens.boxWidth12,
                                      Expanded(
                                        child: Text(
                                          'Check out all the expert funds with premium membership',
                                          style: Styles.medium14Black
                                              .copyWith(color: Colors.white),
                                        ),
                                      ),
                                      Dimens.boxWidth12,
                                      GlobalContainer(
                                        width: Dimens.twentyTwo,
                                        height: Dimens.twentyTwo,
                                        borderRadius: Dimens.fifty,
                                        color: Colors.white,
                                        child: const Icon(
                                          Icons.keyboard_arrow_right,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          : GestureDetector(
                              onTap: () {
                                _controller.isNonMemberProfile = true;
                                _controller.update();
                              },
                              child: Container(
                                width: Dimens.percentWidth(1),
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.circular(Dimens.ten),
                                  gradient: gradient,
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(1.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius:
                                          BorderRadius.circular(Dimens.ten),
                                    ),
                                    child: Padding(
                                      padding: Dimens.edgeInsets10,
                                      child: Row(
                                        children: [
                                          Image.asset(
                                            AssetConstants.gradientLogo,
                                            width: Dimens.twentyFive,
                                            height: Dimens.twentyFive,
                                          ),
                                          Dimens.boxWidth10,
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Wrap(
                                                crossAxisAlignment:
                                                    WrapCrossAlignment.end,
                                                children: [
                                                  ShaderMask(
                                                    blendMode: BlendMode.srcIn,
                                                    shaderCallback: (bounds) =>
                                                        gradient.createShader(
                                                      Rect.fromLTWH(
                                                          0,
                                                          0,
                                                          bounds.width,
                                                          bounds.height),
                                                    ),
                                                    child: Text(
                                                      StringConstants
                                                          .premiumMembership,
                                                      style: Styles.bold16Black,
                                                    ),
                                                  ),
                                                  Dimens.boxWidth5,
                                                  Text(
                                                    '(6 Mth)',
                                                    style:
                                                        Styles.regular12Black,
                                                  )
                                                ],
                                              ),
                                              Dimens.boxHeight5,
                                              Text(
                                                'Subscription valid till: 21 Apr 2022 ',
                                                style: Styles.regular12Grey,
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                      Dimens.boxHeight10,
                      const Divider(),
                      Dimens.boxHeight10,
                      const ProfileCard(
                        title: 'Personal Information',
                        subTitle: 'Complete KYC',
                        isChip: true,
                        iconImage: AssetConstants.user,
                        onTap: RouteManagement.goToPersonalInformation,
                      ),
                      Dimens.boxHeight10,
                      ProfileCard(
                        title: 'Bank and Autopay',
                        subTitle: 'Setup Autopay',
                        isChip: true,
                        iconImage: AssetConstants.bankAndPay,
                        chipColor: Colors.yellow[100],
                        subTitleColor: ColorsValue.textColor,
                        onTap: RouteManagement.goToBankAndAutoPay,
                      ),
                      Dimens.boxHeight10,
                      ProfileCard(
                        title: 'Risk Profile',
                        subTitle: 'Check your risk profile now',
                        isChip: false,
                        iconImage: AssetConstants.caution,
                        imageBg: const Color(0xffFFF0EB),
                        onTap: () {},
                      ),
                      Dimens.boxHeight10,
                      ProfileCard(
                        title: 'Reports',
                        subTitle: 'Check your order history',
                        isChip: false,
                        iconImage: AssetConstants.report,
                        imageBg: const Color(0xffFFF0EB),
                        onTap: () {
                          Get.to(const Report());
                        },
                      ),
                      Dimens.boxHeight10,
                      ProfileCard(
                        title: 'Manage Permissions',
                        subTitle: 'Secure your account',
                        isChip: false,
                        iconImage: AssetConstants.permissions,
                        imageBg: const Color(0xffFFFBF5),
                        onTap: () {
                          Get.to(ManagePermissions());
                        },
                      ),
                      Dimens.boxHeight10,
                      ProfileCard(
                        title: 'Password Settings',
                        subTitle: 'Send us your queries and feedback',
                        isChip: false,
                        iconImage: AssetConstants.padlock,
                        imageBg: const Color(0xffFFF0EB),
                        onTap: () {
                          Get.to(PasswordSettings());
                        },
                      ),
                      Dimens.boxHeight10,
                      ProfileCard(
                        title: 'Help & support',
                        subTitle: 'Check your risk profile now',
                        isChip: false,
                        iconImage: AssetConstants.helpAndSupport,
                        imageBg: const Color(0xffFDF4F7),
                        onTap: () {
                          Get.to(HelpAndSupport());
                        },
                      ),
                      Dimens.boxHeight10,
                      ProfileCard(
                        title: 'Investkarr secure and safe',
                        subTitle: 'Your data is safe with us',
                        isChip: false,
                        iconImage: AssetConstants.shield,
                        imageBg: const Color(0xffFFFBF5),
                        onTap: () {
                          Get.to(const InvestkarrSecureAndSafe());
                        },
                      ),
                      Dimens.boxHeight10,
                      ProfileCard(
                        title: 'Invite & earn',
                        subTitle: 'Send us your queries and feedback',
                        isChip: false,
                        iconImage: AssetConstants.badge,
                        imageBg: const Color(0xffFFF0EB),
                        onTap: () {
                          Get.to(InviteAndEarn());
                        },
                      ),
                      Dimens.boxHeight10,
                      ProfileCard(
                        title: 'More',
                        subTitle: 'Terms & conditions, privacy policy',
                        isChip: false,
                        iconImage: AssetConstants.agreement,
                        imageBg: const Color(0xffFFF0EB),
                        onTap: () {
                          Get.to(More());
                        },
                      ),
                      Dimens.boxHeight20,
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      );
}

class ProfileCard extends StatelessWidget {
  const ProfileCard({
    Key? key,
    required this.title,
    required this.subTitle,
    this.isChip = false,
    this.chipColor,
    this.subTitleColor,
    this.iconImage,
    this.imageBg,
    this.onTap,
  }) : super(key: key);

  final String? title;
  final String? subTitle;
  final Color? subTitleColor;
  final bool isChip;
  final Color? chipColor;
  final String? iconImage;
  final Color? imageBg;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () {
          onTap!();
        },
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Dimens.ten),
            color: Colors.white,
          ),
          width: Dimens.percentWidth(1),
          child: Padding(
            padding: Dimens.edgeInsets10,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    CircleIcon(
                      width: Dimens.fourty,
                      height: Dimens.fourty,
                      image: iconImage!,
                      isSvg: false,
                      color: imageBg ?? const Color(0xffFDF4F7),
                    ),
                    Dimens.boxWidth10,
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          title!,
                          style: Styles.medium16Black,
                        ),
                        Dimens.boxHeight5,
                        isChip
                            ? Container(
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.circular(Dimens.twenty),
                                    color:
                                        chipColor ?? const Color(0xffFFF5F5)),
                                child: Padding(
                                  padding: Dimens.edgeInsets5,
                                  child: Text(
                                    subTitle!,
                                    style: Styles.medium10Grey.copyWith(
                                      color: subTitleColor ??
                                          const Color(0xffFF7F7A),
                                    ),
                                  ),
                                ),
                              )
                            : Text(
                                subTitle!,
                                style: Styles.medium12Grey,
                              ),
                      ],
                    )
                  ],
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.arrow_forward_ios,
                    color: ColorsValue.primaryColor,
                    size: Dimens.fifteen,
                  ),
                )
              ],
            ),
          ),
        ),
      );
}
