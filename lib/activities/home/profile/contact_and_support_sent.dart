import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/theme/dimens.dart';
import 'package:investkarr/theme/theme.dart';
import 'package:investkarr/utils/utils.dart';

class ContactAndSupportSent extends StatelessWidget {
  const ContactAndSupportSent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        body: SafeArea(
          child: Padding(
            padding: Dimens.edgeInsets16,
            child: Stack(
              children: [
                Center(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: Dimens.oneHundredTwenty,
                        height: Dimens.oneHundredTwenty,
                        child: Image.asset(
                          AssetConstants.envolope,
                        ),
                      ),
                      Dimens.boxHeight30,
                      Padding(
                        padding: Dimens.edgeInsets16_0_16_0 +
                            Dimens.edgeInsets16_0_16_0,
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(
                            'We promise to get back to you within 24 hours on your email',
                            textAlign: TextAlign.center,
                            style: Styles.bold18Black,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Positioned(
                  top: 0,
                  right: 0,
                  child: GestureDetector(
                    onTap: () {
                      Get.back<void>();
                    },
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: Icon(
                        Icons.cancel_outlined,
                        size: Dimens.fourty,
                        color: ColorsValue.greyColor.withOpacity(0.5),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      );
}
