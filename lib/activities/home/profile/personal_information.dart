import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class PersonalInformation extends StatelessWidget {
  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          backgroundColor: ColorsValue.backgroundColor,
          appBar: const CustomAppBar(
            title: 'Personal Information',
          ),
          body: SafeArea(
            child: Padding(
              padding: Dimens.edgeInsets16_0_16_0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        Dimens.boxHeight10,
                        const PersonalInformationHeading(
                            prefixText: 'Personal Details'),
                        Dimens.boxHeight10,
                        const PersonalInformationDetailCard(
                          children: [
                            CardChild(
                              prefixText: 'Full Name (as on PAN)',
                              suffixText: 'Ronald Richards',
                            ),
                            CardChild(
                              prefixText: 'Date of Birth',
                              suffixText: '02/03/1988',
                            ),
                            CardChild(
                              prefixText: 'Mobile number',
                              suffixText: '99898 90990',
                            ),
                            CardChild(
                              prefixText: 'Email',
                              suffixText: 'michael.mitc@example.com',
                              divider: false,
                            ),
                          ],
                        ),
                        Dimens.boxHeight30,
                        const PersonalInformationHeading(
                            prefixText: 'KYC Details'),
                        Dimens.boxHeight10,
                        const PersonalInformationDetailCard(
                          children: [
                            CardChild(
                              prefixText: 'KYC Status',
                              suffixText: 'Completed',
                              checkOnSuffix: true,
                            ),
                            CardChild(
                              prefixText: 'KYC Type',
                              suffixText: 'Adhaar based eKYC',
                            ),
                            CardChild(
                              prefixText: 'PAN No',
                              suffixText: 'DDNGP9099H',
                              divider: false,
                            ),
                          ],
                        ),
                        Dimens.boxHeight30,
                        PersonalInformationHeading(
                          prefixText: 'Nominee Details',
                          suffixText: 'edit'.tr,
                        ),
                        Dimens.boxHeight10,
                        const PersonalInformationDetailCard(
                          children: [
                            CardChild(
                              prefixText: 'Name',
                              suffixText: 'Wade Warren',
                            ),
                            CardChild(
                              prefixText: 'Relationship',
                              suffixText: 'Father',
                            ),
                            CardChild(
                              prefixText: 'Email/Phone',
                              suffixText: 'michael.mitc@example.com',
                              divider: false,
                            ),
                          ],
                        ),
                        Dimens.boxHeight20,
                        RichText(
                          textAlign: TextAlign.center,
                          text: TextSpan(
                            children: <TextSpan>[
                              TextSpan(
                                text:
                                    'If you want to make any changes in the above information, Please ',
                                style: Styles.regular14Black,
                              ),
                              TextSpan(
                                text: 'Contact us',
                                style: Styles.primary15Underline,
                              ),
                            ],
                          ),
                        ),
                        Dimens.boxHeight20,
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
}

class PersonalInformationDetailCard extends StatelessWidget {
  const PersonalInformationDetailCard({
    Key? key,
    this.children,
  }) : super(key: key);

  final List<Widget>? children;

  @override
  Widget build(BuildContext context) => Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(Dimens.twenty),
        ),
        color: Colors.white,
        child: Padding(
          padding: Dimens.edgeInsets10,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: children ?? [],
          ),
        ),
      );
}

class CardChild extends StatelessWidget {
  const CardChild({
    Key? key,
    required this.prefixText,
    required this.suffixText,
    this.checkOnSuffix = false,
    this.divider = true,
  }) : super(key: key);

  final String? prefixText;
  final String? suffixText;
  final bool? checkOnSuffix;
  final bool? divider;

  @override
  Widget build(BuildContext context) => Column(
        children: [
          Dimens.boxHeight10,
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                prefixText!,
                style: Styles.regular12Grey,
              ),
              Row(
                children: [
                  Text(
                    suffixText!,
                    style: Styles.medium14Black,
                  ),
                  if (checkOnSuffix!) Dimens.boxWidth2,
                  if (checkOnSuffix!)
                    Icon(Icons.check_circle,
                        size: Dimens.eighteen,
                        color: ColorsValue.shineGreenColor)
                ],
              ),
            ],
          ),
          Dimens.boxHeight10,
          if (divider!) const Divider(),
        ],
      );
}

class PersonalInformationHeading extends StatelessWidget {
  const PersonalInformationHeading({
    Key? key,
    required this.prefixText,
    this.suffixText = '',
    this.onTapSuffix,
  }) : super(key: key);

  final String? prefixText;
  final String? suffixText;
  final void Function()? onTapSuffix;

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            prefixText!,
            style: Styles.bold16Black,
          ),
          if (suffixText!.isNotEmpty)
            Text(
              suffixText!,
              style: Styles.regular12PrimaryUnderline,
            ),
        ],
      );
}
