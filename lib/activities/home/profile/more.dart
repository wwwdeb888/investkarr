import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class More extends StatelessWidget {
  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          backgroundColor: ColorsValue.backgroundColor,
          body: Padding(
            padding: Dimens.edgeInsets10_0_10_0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: ListView(
                    children: [
                      Row(
                        children: [
                          IconButton(
                            onPressed: Get.back,
                            icon: const Icon(Icons.arrow_back_ios),
                          ),
                          Text(
                            'More',
                            style: Styles.darkGrey14,
                          ),
                        ],
                      ),
                      Dimens.boxHeight20,
                      Padding(
                        padding: Dimens.edgeInsets10,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundColor: const Color(0xffFDF4F7),
                                  radius: Dimens.twenty,
                                  child: Image.asset(
                                    AssetConstants.moreAboutUs,
                                    width: Dimens.twentyFive,
                                    height: Dimens.twentyFive,
                                  ),
                                ),
                                Dimens.boxWidth10,
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'About Investkarr',
                                      style: Styles.darkGrey14,
                                    ),
                                  ],
                                )
                              ],
                            ),
                            IconButton(
                              onPressed: () {},
                              icon: Icon(
                                Icons.arrow_forward_ios,
                                size: Dimens.twelve,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const Divider(),
                      Padding(
                        padding: Dimens.edgeInsets10,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundColor: const Color(0xffFDF4F7),
                                  radius: Dimens.twenty,
                                  child: Image.asset(
                                    AssetConstants.helpOthers,
                                    width: Dimens.twentyFive,
                                    height: Dimens.twentyFive,
                                    color: const Color(0xffE194AE),
                                  ),
                                ),
                                Dimens.boxWidth10,
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Terms & Conditions',
                                      style: Styles.darkGrey14,
                                    ),
                                  ],
                                )
                              ],
                            ),
                            IconButton(
                              onPressed: () {},
                              icon: Icon(
                                Icons.arrow_forward_ios,
                                size: Dimens.twelve,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const Divider(),
                      Padding(
                        padding: Dimens.edgeInsets10,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundColor: const Color(0xffFFFBF5),
                                  radius: Dimens.twenty,
                                  child: Image.asset(
                                    AssetConstants.privacy,
                                    width: Dimens.twentyFive,
                                    height: Dimens.twentyFive,
                                  ),
                                ),
                                Dimens.boxWidth10,
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Privacy Policy',
                                      style: Styles.darkGrey14,
                                    ),
                                  ],
                                )
                              ],
                            ),
                            IconButton(
                              onPressed: () {},
                              icon: Icon(
                                Icons.arrow_forward_ios,
                                size: Dimens.twelve,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const Divider(),
                      Padding(
                        padding: Dimens.edgeInsets10,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundColor: const Color(0xffFFFBF5),
                                  radius: Dimens.twenty,
                                  child: Image.asset(
                                    AssetConstants.howWeRecommend,
                                    width: Dimens.twentyFive,
                                    height: Dimens.twentyFive,
                                  ),
                                ),
                                Dimens.boxWidth10,
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'How we recommend',
                                      style: Styles.darkGrey14,
                                    ),
                                  ],
                                )
                              ],
                            ),
                            IconButton(
                              onPressed: () {},
                              icon: Icon(
                                Icons.arrow_forward_ios,
                                size: Dimens.twelve,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const Divider(),
                      Padding(
                        padding: Dimens.edgeInsets10,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundColor: const Color(0xffFFFBF5),
                                  radius: Dimens.twenty,
                                  child: Image.asset(
                                    AssetConstants.moreDelete,
                                    width: Dimens.twentyFive,
                                    height: Dimens.twentyFive,
                                  ),
                                ),
                                Dimens.boxWidth10,
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Delete Account',
                                      style: Styles.darkGrey14,
                                    ),
                                  ],
                                )
                              ],
                            ),
                            IconButton(
                              onPressed: () {},
                              icon: Icon(
                                Icons.arrow_forward_ios,
                                size: Dimens.twelve,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const Divider()
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );
}
