import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/profile/contact_and_support_sent.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class ContactSupportTeam extends StatelessWidget {
  const ContactSupportTeam({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          appBar: const CustomAppBar(title: 'Contact Support Team'),
          body: Padding(
            padding: Dimens.edgeInsets15,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: ListView(children: [
                    Dimens.boxHeight10,
                    Text(
                      'How can I get my full KYC done?',
                      style: Styles.bold20Black,
                    ),
                    Dimens.boxHeight35,
                    Row(
                      children: [
                        GlobalContainer(
                          width: Dimens.twenty,
                          height: Dimens.twenty,
                          borderRadius: Dimens.fifty,
                          color: ColorsValue.pinkColor,
                          child: Center(
                            child: Image.asset(
                              AssetConstants.user,
                              scale: 4,
                            ),
                          ),
                        ),
                        Dimens.boxWidth10,
                        Text(
                          'michael.mitc@example.com',
                          style: Styles.medium12Black,
                        ),
                      ],
                    ),
                    Dimens.boxHeight25,
                    Container(
                      constraints:
                          const BoxConstraints(maxHeight: double.infinity),
                      child: TextFormField(
                        maxLines: 10,
                        controller: _controller.contactSupportController,
                        onChanged: (v) {
                          _controller.update();
                        },
                        decoration: InputDecoration(
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(
                              Dimens.twelve,
                            ),
                            borderSide: BorderSide(
                              color: ColorsValue.textColor.withOpacity(0.4),
                            ),
                          ),
                          hintText: 'Describe your query in detail',
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(
                              Dimens.twelve,
                            ),
                            borderSide: BorderSide(
                              color: ColorsValue.textColor.withOpacity(0.4),
                            ),
                          ),
                          filled: true,
                          fillColor: Colors.white,
                          hintStyle: Styles.textFieldHintColor16.copyWith(
                            color: ColorsValue.greyColor.withOpacity(0.5),
                          ),
                          contentPadding: Dimens.edgeInsets12,
                        ),
                      ),
                    ),
                    Dimens.boxHeight30,
                    RichText(
                      text: TextSpan(
                        text: 'Attachments ',
                        style: Styles.bold16Black,
                        children: [
                          TextSpan(
                            text: '(optional)',
                            style: Styles.regular13Black,
                          ),
                        ],
                      ),
                    ),
                    Dimens.boxHeight16,
                    _controller.contactSupportAttachmentSelected
                        ? GestureDetector(
                            onTap: () {
                              _controller.contactSupportAttachmentSelected =
                                  false;
                              _controller.update();
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.circular(Dimens.twelve),
                                color: Colors.white,
                              ),
                              child: DottedBorder(
                                radius: Radius.circular(Dimens.twelve),
                                borderType: BorderType.RRect,
                                padding:
                                    Dimens.edgeInsets20 + Dimens.edgeInsets5,
                                color: ColorsValue.primaryColor,
                                strokeWidth: 1,
                                dashPattern: [5],
                                child: Center(
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      SvgPicture.asset(
                                        AssetConstants.uploadIc,
                                        color: ColorsValue.primaryColor,
                                        width: Dimens.twenty,
                                      ),
                                      Dimens.boxWidth10,
                                      Text(
                                        'Upload Attachments',
                                        style: Styles.regular14Primary,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          )
                        : GlobalContainer(
                            width: Dimens.percentWidth(1),
                            height: Dimens.percentHeight(0.28),
                            borderRadius: Dimens.twelve,
                            onTap: () {},
                            color: Colors.white,
                            child: Column(
                              children: [
                                Padding(
                                  padding: Dimens.edgeInsets15,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          SvgPicture.asset(
                                            AssetConstants.pageIc,
                                          ),
                                          Dimens.boxWidth15,
                                          Text(
                                            'Image.jpeg',
                                            style: Styles.regular14Grey,
                                          ),
                                        ],
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          _controller
                                                  .contactSupportAttachmentSelected =
                                              true;
                                          _controller.update();
                                        },
                                        child: Icon(
                                          Icons.cancel_outlined,
                                          size: Dimens.twenty,
                                          color: ColorsValue.primaryColor,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                Dimens.boxHeight10,
                                SizedBox(
                                    width: Dimens.percentWidth(1),
                                    height: Dimens.percentHeight(0.18),
                                    child: Image.asset(AssetConstants.adhar))
                              ],
                            ),
                          ),
                    Dimens.boxHeight25,
                    RichText(
                      text: TextSpan(
                        text: 'Note: ',
                        style: Styles.bold12Grey,
                        children: [
                          TextSpan(
                            text:
                                ' Please enclose the screenshot or other supporting   documents in attachments.',
                            style: Styles.regular12Grey,
                          ),
                        ],
                      ),
                    ),
                    Dimens.boxHeight40,
                  ]),
                ),
                GlobalButton(
                  text: 'Send',
                  enable: !_controller.contactSupportAttachmentSelected &&
                      _controller.contactSupportController.text.isNotEmpty,
                  onTap: () {
                    Get.to(const ContactAndSupportSent());
                  },
                ),
                Dimens.boxHeight10,
                Center(
                  child: Text(
                    'You\'ll receive a response within 24 hours ',
                    style: Styles.regular12Grey,
                  ),
                )
              ],
            ),
          ),
        ),
      );
}
