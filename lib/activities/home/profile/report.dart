import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/explore_widgets/widgets/expanded_custom_tile.dart';
import 'package:investkarr/controllers/controllers.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/theme/dimens.dart';
import 'package:investkarr/theme/theme.dart';
import 'package:investkarr/widgets/app_bar.dart';

class Report extends StatelessWidget {
  const Report({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          appBar: const CustomAppBar(title: 'Report'),
          body: SingleChildScrollView(
            child: Padding(
              padding: Dimens.edgeInsets16_0_16_0,
              child: Column(
                children: [
                  Dimens.boxHeight20,
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Tax Filling',
                      style: Styles.bold16Black,
                    ),
                  ),
                  Dimens.boxHeight15,
                  Card(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                    clipBehavior: Clip.antiAlias,
                    margin: EdgeInsets.zero,
                    child: Theme(
                      data: Theme.of(context)
                          .copyWith(dividerColor: Colors.white),
                      child: ExpansionTile(
                        title: Text(
                          'Tax Proof - 80C ELSS Statement',
                          style: Styles.medium16Black,
                        ),
                        collapsedBackgroundColor: Colors.white,
                        backgroundColor: Colors.white,
                        onExpansionChanged: (v) {
                          _controller.taxproofExpanded = v;
                          _controller.update();
                        },
                        initiallyExpanded: false,
                        trailing: !_controller.taxproofExpanded
                            ? const Icon(
                                Icons.keyboard_arrow_down,
                                color: ColorsValue.greyColor,
                              )
                            : const Icon(
                                Icons.keyboard_arrow_up,
                                color: ColorsValue.greyColor,
                              ),
                        children: [
                          DefaultContainer(
                            child: Column(
                              children: [
                                FormFieldWidget(
                                  elevation: 0,
                                  autoFocus: true,
                                  isFilled: true,
                                  maxLength: 10,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  textInputType: TextInputType.emailAddress,
                                  borderColor:
                                      ColorsValue.greyColor.withOpacity(0.4),
                                  fillColor: Colors.transparent,
                                  labelText: 'From',
                                  labelStyle: Styles.regular16Grey,
                                  contentPadding: Dimens.edgeInsets10,
                                  errorStyle: Styles.red13,
                                  isReadOnly: true,
                                  onTap: () {
                                    _controller.selectDateFrom(context);
                                  },
                                  suffixIcon: GestureDetector(
                                    onTap: () {},
                                    child: Image.asset(
                                      AssetConstants.calendar,
                                      scale: 2.5,
                                    ),
                                  ),
                                  textEditingController: _controller.from,
                                  onChange: (String value) {},
                                ),
                                Dimens.boxHeight15,
                                FormFieldWidget(
                                  elevation: 0,
                                  autoFocus: true,
                                  isFilled: true,
                                  maxLength: 10,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  textInputType: TextInputType.emailAddress,
                                  borderColor:
                                      ColorsValue.greyColor.withOpacity(0.4),
                                  fillColor: Colors.transparent,
                                  labelText: 'To',
                                  labelStyle: Styles.regular16Grey,
                                  contentPadding: Dimens.edgeInsets10,
                                  errorStyle: Styles.red13,
                                  isReadOnly: true,
                                  onTap: () {
                                    _controller.selectDateTo(context);
                                  },
                                  suffixIcon: GestureDetector(
                                    onTap: () {},
                                    child: Image.asset(
                                      AssetConstants.calendar,
                                      scale: 2.5,
                                    ),
                                  ),
                                  textEditingController: _controller.to,
                                  onChange: (String value) {},
                                ),
                                Dimens.boxHeight15,
                                GlobalButton(
                                  text: 'Download',
                                  onTap: () {},
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Dimens.boxHeight15,
                  Card(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                    clipBehavior: Clip.antiAlias,
                    margin: EdgeInsets.zero,
                    child: Theme(
                      data: Theme.of(context)
                          .copyWith(dividerColor: Colors.white),
                      child: ExpansionTile(
                        title: Text(
                          'Capital Gains - Mutual Funds',
                          style: Styles.medium16Black,
                        ),
                        collapsedBackgroundColor: Colors.white,
                        backgroundColor: Colors.white,
                        onExpansionChanged: (v) {
                          _controller.capitalGainsExpanded = v;
                          _controller.update();
                        },
                        initiallyExpanded: false,
                        trailing: !_controller.capitalGainsExpanded
                            ? const Icon(
                                Icons.keyboard_arrow_down,
                                color: ColorsValue.greyColor,
                              )
                            : const Icon(
                                Icons.keyboard_arrow_up,
                                color: ColorsValue.greyColor,
                              ),
                        children: [
                          DefaultContainer(
                            child: Column(
                              children: [
                                FormFieldWidget(
                                  elevation: 0,
                                  autoFocus: true,
                                  isFilled: true,
                                  maxLength: 10,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  textInputType: TextInputType.emailAddress,
                                  borderColor:
                                      ColorsValue.greyColor.withOpacity(0.4),
                                  fillColor: Colors.transparent,
                                  labelText: 'From',
                                  labelStyle: Styles.regular16Grey,
                                  contentPadding: Dimens.edgeInsets10,
                                  errorStyle: Styles.red13,
                                  isReadOnly: true,
                                  onTap: () {
                                    _controller.selectDateFrom(context);
                                  },
                                  suffixIcon: GestureDetector(
                                    onTap: () {},
                                    child: Image.asset(
                                      AssetConstants.calendar,
                                      scale: 2.5,
                                    ),
                                  ),
                                  textEditingController: _controller.from,
                                  onChange: (String value) {},
                                ),
                                Dimens.boxHeight15,
                                FormFieldWidget(
                                  elevation: 0,
                                  autoFocus: true,
                                  isFilled: true,
                                  maxLength: 10,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  textInputType: TextInputType.emailAddress,
                                  borderColor:
                                      ColorsValue.greyColor.withOpacity(0.4),
                                  fillColor: Colors.transparent,
                                  labelText: 'To',
                                  labelStyle: Styles.regular16Grey,
                                  contentPadding: Dimens.edgeInsets10,
                                  errorStyle: Styles.red13,
                                  isReadOnly: true,
                                  onTap: () {
                                    _controller.selectDateTo(context);
                                  },
                                  suffixIcon: GestureDetector(
                                    onTap: () {},
                                    child: Image.asset(
                                      AssetConstants.calendar,
                                      scale: 2.5,
                                    ),
                                  ),
                                  textEditingController: _controller.to,
                                  onChange: (String value) {},
                                ),
                                Dimens.boxHeight15,
                                GlobalButton(
                                  text: 'Download',
                                  onTap: () {},
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  Dimens.boxHeight20,
                  Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      'Mutual Funds',
                      style: Styles.bold16Black,
                    ),
                  ),
                  Dimens.boxHeight15,
                  Card(
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                    clipBehavior: Clip.antiAlias,
                    margin: EdgeInsets.zero,
                    child: Theme(
                      data: Theme.of(context)
                          .copyWith(dividerColor: Colors.white),
                      child: ExpansionTile(
                        title: Text(
                          'Order History',
                          style: Styles.medium16Black,
                        ),
                        collapsedBackgroundColor: Colors.white,
                        backgroundColor: Colors.white,
                        onExpansionChanged: (v) {
                          _controller.orderHistoryExpanded = v;
                          _controller.update();
                        },
                        initiallyExpanded: false,
                        trailing: !_controller.orderHistoryExpanded
                            ? const Icon(
                                Icons.keyboard_arrow_down,
                                color: ColorsValue.greyColor,
                              )
                            : const Icon(
                                Icons.keyboard_arrow_up,
                                color: ColorsValue.greyColor,
                              ),
                        children: [
                          DefaultContainer(
                            child: Column(
                              children: [
                                FormFieldWidget(
                                  elevation: 0,
                                  autoFocus: true,
                                  isFilled: true,
                                  maxLength: 10,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  textInputType: TextInputType.emailAddress,
                                  borderColor:
                                      ColorsValue.greyColor.withOpacity(0.4),
                                  fillColor: Colors.transparent,
                                  labelText: 'From',
                                  labelStyle: Styles.regular16Grey,
                                  contentPadding: Dimens.edgeInsets10,
                                  errorStyle: Styles.red13,
                                  isReadOnly: true,
                                  onTap: () {
                                    _controller.selectDateFrom(context);
                                  },
                                  suffixIcon: GestureDetector(
                                    onTap: () {},
                                    child: Image.asset(
                                      AssetConstants.calendar,
                                      scale: 2.5,
                                    ),
                                  ),
                                  textEditingController: _controller.from,
                                  onChange: (String value) {},
                                ),
                                Dimens.boxHeight15,
                                FormFieldWidget(
                                  elevation: 0,
                                  autoFocus: true,
                                  isFilled: true,
                                  maxLength: 10,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  textInputType: TextInputType.emailAddress,
                                  borderColor:
                                      ColorsValue.greyColor.withOpacity(0.4),
                                  fillColor: Colors.transparent,
                                  labelText: 'To',
                                  labelStyle: Styles.regular16Grey,
                                  contentPadding: Dimens.edgeInsets10,
                                  errorStyle: Styles.red13,
                                  isReadOnly: true,
                                  onTap: () {
                                    _controller.selectDateTo(context);
                                  },
                                  suffixIcon: GestureDetector(
                                    onTap: () {},
                                    child: Image.asset(
                                      AssetConstants.calendar,
                                      scale: 2.5,
                                    ),
                                  ),
                                  textEditingController: _controller.to,
                                  onChange: (String value) {},
                                ),
                                Dimens.boxHeight15,
                                GlobalButton(
                                  text: 'Download',
                                  onTap: () {},
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
