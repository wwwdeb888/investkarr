import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/profile/contact_support_team.dart';
import 'package:investkarr/controllers/controllers.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/theme/dimens.dart';
import 'package:investkarr/widgets/app_bar.dart';

class HelpSupportKyc extends StatelessWidget {
  const HelpSupportKyc({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          appBar: const CustomAppBar(title: 'KYC'),
          body: SingleChildScrollView(
            child: Padding(
              padding: Dimens.edgeInsets5,
              child: Column(
                children: [
                  Theme(
                    data: Theme.of(context)
                        .copyWith(dividerColor: Colors.transparent),
                    child: ExpansionTile(
                      title: Text(
                        'How can I get my full KYC done?',
                        style: Styles.medium14Black,
                      ),
                      backgroundColor: Colors.transparent,
                      onExpansionChanged: (v) {
                        _controller.kycOne = v;
                        _controller.update();
                      },
                      initiallyExpanded: false,
                      trailing: !_controller.kycOne
                          ? const Icon(
                              Icons.keyboard_arrow_down,
                              color: ColorsValue.primaryColor,
                            )
                          : const Icon(
                              Icons.keyboard_arrow_up,
                              color: ColorsValue.primaryColor,
                            ),
                      children: [
                        Container(
                          padding: Dimens.edgeInsets10,
                          child: Column(
                            children: [
                              Text(
                                'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
                                style: Styles.regular14Grey,
                              ),
                              Dimens.boxHeight30,
                              Row(
                                children: [
                                  Text(
                                    'Is your issue resolved?',
                                    style: Styles.medium12Black,
                                  ),
                                  Dimens.boxWidth40,
                                  Dimens.boxWidth40,
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        Get.to(const ContactSupportTeam());
                                      },
                                      child: Container(
                                        padding: Dimens.edgeInsets10,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(
                                              Dimens.fifty,
                                            ),
                                            border: Border.all(
                                                width: Dimens.two,
                                                color:
                                                    ColorsValue.primaryColor)),
                                        child: Center(
                                            child: Text(
                                          'Contact Us',
                                          style: Styles.bold14Primary,
                                        )),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Dimens.boxHeight10,
                  Padding(
                    padding:
                        Dimens.edgeInsets10_0_10_0 + Dimens.edgeInsets2_0_2_0,
                    child: const CustomDivider(),
                  ),
                  Dimens.boxHeight10,
                  Theme(
                    data: Theme.of(context)
                        .copyWith(dividerColor: Colors.transparent),
                    child: ExpansionTile(
                      title: Text(
                        'Do you charge for processing KYC?',
                        style: Styles.medium14Black,
                      ),
                      backgroundColor: Colors.transparent,
                      onExpansionChanged: (v) {
                        _controller.kycTwo = v;
                        _controller.update();
                      },
                      initiallyExpanded: false,
                      trailing: !_controller.kycTwo
                          ? const Icon(
                              Icons.keyboard_arrow_down,
                              color: ColorsValue.primaryColor,
                            )
                          : const Icon(
                              Icons.keyboard_arrow_up,
                              color: ColorsValue.primaryColor,
                            ),
                      children: [
                        Container(
                          padding: Dimens.edgeInsets10,
                          child: Column(
                            children: [
                              Text(
                                'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
                                style: Styles.regular14Grey,
                              ),
                              Dimens.boxHeight30,
                              Row(
                                children: [
                                  Text(
                                    'Is your issue resolved?',
                                    style: Styles.medium12Black,
                                  ),
                                  Dimens.boxWidth40,
                                  Dimens.boxWidth40,
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        Get.to(const ContactSupportTeam());
                                      },
                                      child: Container(
                                        padding: Dimens.edgeInsets10,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(
                                              Dimens.fifty,
                                            ),
                                            border: Border.all(
                                                width: Dimens.two,
                                                color:
                                                    ColorsValue.primaryColor)),
                                        child: Center(
                                            child: Text(
                                          'Contact Us',
                                          style: Styles.bold14Primary,
                                        )),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Dimens.boxHeight10,
                  Padding(
                    padding:
                        Dimens.edgeInsets10_0_10_0 + Dimens.edgeInsets2_0_2_0,
                    child: const CustomDivider(),
                  ),
                  Dimens.boxHeight10,
                  Theme(
                    data: Theme.of(context)
                        .copyWith(dividerColor: Colors.transparent),
                    child: ExpansionTile(
                      title: Text(
                        'Do you charge for processing KYC?',
                        style: Styles.medium14Black,
                      ),
                      backgroundColor: Colors.transparent,
                      onExpansionChanged: (v) {
                        _controller.kycThree = v;
                        _controller.update();
                      },
                      initiallyExpanded: false,
                      trailing: !_controller.kycThree
                          ? const Icon(
                              Icons.keyboard_arrow_down,
                              color: ColorsValue.primaryColor,
                            )
                          : const Icon(
                              Icons.keyboard_arrow_up,
                              color: ColorsValue.primaryColor,
                            ),
                      children: [
                        Container(
                          padding: Dimens.edgeInsets10,
                          child: Column(
                            children: [
                              Text(
                                'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
                                style: Styles.regular14Grey,
                              ),
                              Dimens.boxHeight30,
                              Row(
                                children: [
                                  Text(
                                    'Is your issue resolved?',
                                    style: Styles.medium12Black,
                                  ),
                                  Dimens.boxWidth40,
                                  Dimens.boxWidth40,
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        Get.to(const ContactSupportTeam());
                                      },
                                      child: Container(
                                        padding: Dimens.edgeInsets10,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(
                                              Dimens.fifty,
                                            ),
                                            border: Border.all(
                                                width: Dimens.two,
                                                color:
                                                    ColorsValue.primaryColor)),
                                        child: Center(
                                            child: Text(
                                          'Contact Us',
                                          style: Styles.bold14Primary,
                                        )),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Dimens.boxHeight10,
                  Padding(
                    padding:
                        Dimens.edgeInsets10_0_10_0 + Dimens.edgeInsets2_0_2_0,
                    child: const CustomDivider(),
                  ),
                  Dimens.boxHeight10,
                  Theme(
                    data: Theme.of(context)
                        .copyWith(dividerColor: Colors.transparent),
                    child: ExpansionTile(
                      title: Text(
                        'Do you charge for processing KYC?',
                        style: Styles.medium14Black,
                      ),
                      backgroundColor: Colors.transparent,
                      onExpansionChanged: (v) {
                        _controller.kycFour = v;
                        _controller.update();
                      },
                      initiallyExpanded: false,
                      trailing: !_controller.kycFour
                          ? const Icon(
                              Icons.keyboard_arrow_down,
                              color: ColorsValue.primaryColor,
                            )
                          : const Icon(
                              Icons.keyboard_arrow_up,
                              color: ColorsValue.primaryColor,
                            ),
                      children: [
                        Container(
                          padding: Dimens.edgeInsets10,
                          child: Column(
                            children: [
                              Text(
                                'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
                                style: Styles.regular14Grey,
                              ),
                              Dimens.boxHeight30,
                              Row(
                                children: [
                                  Text(
                                    'Is your issue resolved?',
                                    style: Styles.medium12Black,
                                  ),
                                  Dimens.boxWidth40,
                                  Dimens.boxWidth40,
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        Get.to(const ContactSupportTeam());
                                      },
                                      child: Container(
                                        padding: Dimens.edgeInsets10,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(
                                              Dimens.fifty,
                                            ),
                                            border: Border.all(
                                                width: Dimens.two,
                                                color:
                                                    ColorsValue.primaryColor)),
                                        child: Center(
                                            child: Text(
                                          'Contact Us',
                                          style: Styles.bold14Primary,
                                        )),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Dimens.boxHeight10,
                  Padding(
                    padding:
                        Dimens.edgeInsets10_0_10_0 + Dimens.edgeInsets2_0_2_0,
                    child: const CustomDivider(),
                  ),
                  Dimens.boxHeight10,
                  Theme(
                    data: Theme.of(context)
                        .copyWith(dividerColor: Colors.transparent),
                    child: ExpansionTile(
                      title: Text(
                        'Do you charge for processing KYC processing KYC?',
                        style: Styles.medium14Black,
                      ),
                      backgroundColor: Colors.transparent,
                      onExpansionChanged: (v) {
                        _controller.kycFive = v;
                        _controller.update();
                      },
                      initiallyExpanded: false,
                      trailing: !_controller.kycFive
                          ? const Icon(
                              Icons.keyboard_arrow_down,
                              color: ColorsValue.primaryColor,
                            )
                          : const Icon(
                              Icons.keyboard_arrow_up,
                              color: ColorsValue.primaryColor,
                            ),
                      children: [
                        Container(
                          padding: Dimens.edgeInsets10,
                          child: Column(
                            children: [
                              Text(
                                'Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet.',
                                style: Styles.regular14Grey,
                              ),
                              Dimens.boxHeight30,
                              Row(
                                children: [
                                  Text(
                                    'Is your issue resolved?',
                                    style: Styles.medium12Black,
                                  ),
                                  Dimens.boxWidth40,
                                  Dimens.boxWidth40,
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        Get.to(const ContactSupportTeam());
                                      },
                                      child: Container(
                                        padding: Dimens.edgeInsets10,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(
                                              Dimens.fifty,
                                            ),
                                            border: Border.all(
                                                width: Dimens.two,
                                                color:
                                                    ColorsValue.primaryColor)),
                                        child: Center(
                                            child: Text(
                                          'Contact Us',
                                          style: Styles.bold14Primary,
                                        )),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  Dimens.boxHeight10,
                  Padding(
                    padding:
                        Dimens.edgeInsets10_0_10_0 + Dimens.edgeInsets2_0_2_0,
                    child: const CustomDivider(),
                  ),
                  Dimens.boxHeight10,
                ],
              ),
            ),
          ),
        ),
      );
}
