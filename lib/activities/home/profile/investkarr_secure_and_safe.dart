import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/controllers/controllers.dart';
import 'package:investkarr/theme/dimens.dart';
import 'package:investkarr/theme/theme.dart';
import 'package:investkarr/utils/utils.dart';
import 'package:investkarr/widgets/app_bar.dart';
import 'package:investkarr/widgets/widgets.dart';

class InvestkarrSecureAndSafe extends StatelessWidget {
  const InvestkarrSecureAndSafe({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          appBar: CustomAppBar(title: 'Investkarr secure and safe'),
          body: Container(
            color: Color(0xffEBE3FD),
            width: Dimens.percentWidth(1),
            child: SingleChildScrollView(
              child: Padding(
                padding: Dimens.edgeInsets16,
                child: Column(
                  children: [
                    InvestkarrSecureSafeBox(
                      title: 'Data Security',
                      subTitle: 'Utmost sanctity of your data Stored in AWS.',
                      image: AssetConstants.aws,
                    ),
                    Dimens.boxHeight10,
                    InvestkarrSecureSafeBox(
                      title: 'Data Provider ',
                      image: AssetConstants.crisil,
                      subTitle: 'Accurate and trusted data provided by Crisil.',
                    ),
                    Dimens.boxHeight10,
                    InvestkarrSecureSafeBox(
                      title: 'Strict Privacy Policy',
                      image: AssetConstants.privacySvg,
                      subTitle:
                          'Do not share the data with third party marketers.',
                    ),
                    Dimens.boxHeight10,
                    InvestkarrSecureSafeBox(
                      title: 'SEBI Regulated ',
                      subTitle:  'SEBI ARN Code: xxxxxxxxxx.',
                      image: AssetConstants.sebi,
                    ),
                    Dimens.boxHeight10,
                    InvestkarrSecureSafeBox(
                      title: 'Zero Commission',
                      image: AssetConstants.getMoney,
                      subTitle:
                          'Invest into low cost direct plans of mutual funds schemes.',
                    ),
                    Dimens.boxHeight10,
                    InvestkarrSecureSafeBox(
                      title: 'SEBI Regulated ',
                      subTitle:
                          'SEBI Investor Advisor Code: MC100012190BSE Star Code: 23456',
                    ),
                    Dimens.boxHeight10,
                  ],
                ),
              ),
            ),
          ),
        ),
      );
}

class InvestkarrSecureSafeBox extends StatelessWidget {
  InvestkarrSecureSafeBox({
    Key? key,
    this.image,
    this.subTitle,
    this.title,
    this.onTap,
  }) : super(key: key);
  final String? title;
  final String? subTitle;
  final String? image;
  void Function()? onTap;

  @override
  Widget build(BuildContext context) => GlobalContainer(
        width: Dimens.percentWidth(0.95),
        height: Dimens.percentHeight(0.28),
        onTap: () {},
        color: Colors.white,
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              GlobalContainer(
                width: Dimens.fourtyEight,
                height: Dimens.fourtyEight,
                borderRadius: Dimens.fifty,
                borderColor: ColorsValue.primaryColor,
                borderWidth: Dimens.one,
                color: Colors.white,
                child: Center(
                  child: SvgPicture.asset(
                    image!,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Dimens.boxHeight10,
              Text(
                title ?? '',
                style: Styles.bold16Black,
              ),
              Dimens.boxHeight10,
              Padding(
                padding: Dimens.edgeInsets16_0_16_0 +
                    Dimens.edgeInsets16_0_16_0 +
                    Dimens.edgeInsets16_0_16_0 +
                    Dimens.edgeInsets16_0_16_0 +
                    Dimens.edgeInsets2_0_2_0,
                child: Text(
                  subTitle ?? '',
                  textAlign: TextAlign.center,
                  style: Styles.regular12Grey,
                ),
              ),
            ],
          ),
        ),
      );
}
