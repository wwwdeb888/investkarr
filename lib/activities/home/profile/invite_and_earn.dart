import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class InviteAndEarn extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        body: Stack(
          children: [
            Container(
              height: Get.height * .35,
              decoration: BoxDecoration(
                  color: ColorsValue.primaryColor.withOpacity(.2),
                  image: const DecorationImage(
                      image: AssetImage(
                        AssetConstants.looperFour,
                      ),
                      alignment: Alignment.centerRight)),
            ),
            Scaffold(
              backgroundColor: Colors.transparent,
              appBar: const CustomAppBar(
                title: 'Refer & Earn',
              ),
              body: SafeArea(
                child: Padding(
                  padding: Dimens.edgeInsets16,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(
                        AssetConstants.referandearn,
                        scale: 2,
                      ),
                      Dimens.boxHeight10,
                      Padding(
                        padding: Dimens.edgeInsets20_0_20_0 +
                            Dimens.edgeInsets20_0_20_0 +
                            Dimens.edgeInsets20_0_20_0,
                        child: Text(
                          'Send referral link to your friend via sms/email/whatsapp',
                          style: Styles.regular14Black,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Dimens.boxHeight25,
                      DefaultContainer(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'How it works?',
                              style: Styles.bold16Black,
                            ),
                            Dimens.boxHeight25,
                            Row(
                              children: [
                                CircleAvatar(
                                  radius: Dimens.ten,
                                  backgroundColor: const Color(0xffFDF4F7),
                                  child: Image.asset(
                                    AssetConstants.invite,
                                    width: Dimens.fifteen,
                                    height: Dimens.fifteen,
                                  ),
                                ),
                                Dimens.boxWidth10,
                                SizedBox(
                                  width: Dimens.percentWidth(.7),
                                  child: Text(
                                    'Invite your friend using your referral code',
                                    style: Styles.medium14Grey,
                                  ),
                                ),
                              ],
                            ),
                            Padding(
                              padding: Dimens.edgeInsets10_0_0_0,
                              child: Container(
                                width: Dimens.one,
                                height: Dimens.twentyFive,
                                color: ColorsValue.secondaryColor,
                              ),
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                CircleAvatar(
                                  radius: Dimens.ten,
                                  backgroundColor: const Color(0xffFFF0EB),
                                  child: Image.asset(
                                    AssetConstants.inviteMoney,
                                    width: Dimens.fifteen,
                                    height: Dimens.fifteen,
                                  ),
                                ),
                                Dimens.boxWidth10,
                                SizedBox(
                                  width: Dimens.percentWidth(.7),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'When they sign up on MC',
                                        style: Styles.medium14Grey,
                                      ),
                                      Dimens.boxHeight5,
                                      Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                          color: ColorsValue.primaryColor
                                              .withOpacity(.1),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10_0_10_0 +
                                              Dimens.edgeInsets0_5_0_5,
                                          child: Text(
                                            'You get ₹50',
                                            style: Styles.medium12Black,
                                          ),
                                        ),
                                      ),
                                      Dimens.boxHeight5,
                                      Container(
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(
                                              Dimens.twenty),
                                          color: ColorsValue.primaryColor
                                              .withOpacity(.1),
                                        ),
                                        child: Padding(
                                          padding: Dimens.edgeInsets10_0_10_0 +
                                              Dimens.edgeInsets0_5_0_5,
                                          child: Text(
                                            'They get ₹20',
                                            style: Styles.medium12Black,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            Dimens.boxHeight25,
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                CircleAvatar(
                                  radius: Dimens.ten,
                                  backgroundColor: const Color(0xffFFF0EB),
                                  child: Image.asset(
                                    AssetConstants.helpBadge,
                                    width: Dimens.fifteen,
                                    height: Dimens.fifteen,
                                  ),
                                ),
                                Dimens.boxWidth10,
                                SizedBox(
                                  width: Dimens.percentWidth(.7),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Rewards points can be used on your next order',
                                        style: Styles.medium14Grey,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Referral code',
                            style: Styles.medium14Grey,
                          ),
                          Dimens.boxHeight10,
                          DottedBorder(
                            color: ColorsValue.primaryColor,
                            radius: Radius.circular(Dimens.fifteen),
                            child: Padding(
                              padding: Dimens.edgeInsets10,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'http://drd.sh/zWzBCL/',
                                    style: Styles.bold12Primary,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Copy',
                                        style: Styles.bold12Primary,
                                      ),
                                      Dimens.boxWidth10,
                                      SvgPicture.asset(AssetConstants.copyIc)
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                          Dimens.boxHeight20,
                          SizedBox(
                            width: Get.width,
                            child: CustomButton(
                              title: 'Refer Now',
                              onTap: () {
                                // Get.to(const GoalBasedInvestingScreen());
                              },
                            ),
                          ),
                          Dimens.boxHeight5,
                        ],
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}
