import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class MakePrimarySheetContent extends StatelessWidget {
  const MakePrimarySheetContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
      builder: (_controller) => StatefulBuilder(
            builder: (context, setState) => Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0),
                ),
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Dimens.boxHeight10,
                    Container(
                      width: Dimens.fourty,
                      height: Dimens.five,
                      decoration: BoxDecoration(
                        color: ColorsValue.greyColor.withOpacity(.2),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(50),
                        ),
                      ),
                    ),
                    Dimens.boxHeight20,
                    GestureDetector(
                      onTap: () {},
                      child: Container(
                        padding: Dimens.edgeInsets10_0_10_0,
                        color: ColorsValue.transparent,
                        child: Row(
                          children: [
                            CircleAvatar(
                              radius: Dimens.sixTeen,
                              backgroundColor: ColorsValue.backgroundColor,
                              child: Icon(
                                Icons.edit,
                                color: ColorsValue.primaryColor,
                                size: Dimens.fifteen,
                              ),
                            ),
                            Dimens.boxWidth10,
                            Text(
                              'Make Primary',
                              style: Styles.regular16Black,
                            )
                          ],
                        ),
                      ),
                    ),
                    const Divider(),
                    GestureDetector(
                      onTap: () {
                        if (_controller.isAutoPayEnabled) {
                          Get.back<dynamic>();
                          showModalBottomSheet(
                            isScrollControlled: true,
                            backgroundColor: Colors.transparent,
                            context: context,
                            builder: (ctx) => DeleteBankAccountSheetContent(),
                          );
                        } else {
                          Get.back<dynamic>();
                          showModalBottomSheet(
                            isScrollControlled: true,
                            backgroundColor: Colors.transparent,
                            context: context,
                            builder: (ctx) =>
                                DeleteBankAccountSecondSheetContent(),
                          );
                        }
                      },
                      child: Container(
                        padding: Dimens.edgeInsets10_0_10_0,
                        color: ColorsValue.transparent,
                        child: Row(
                          children: [
                            CircleAvatar(
                              radius: Dimens.sixTeen,
                              backgroundColor: ColorsValue.backgroundColor,
                              child: Icon(
                                Icons.delete_outline,
                                color: ColorsValue.primaryColor,
                                size: Dimens.fifteen,
                              ),
                            ),
                            Dimens.boxWidth10,
                            Text(
                              'Delete Bank Account',
                              style: Styles.regular16Black,
                            )
                          ],
                        ),
                      ),
                    ),
                    Dimens.boxHeight20,
                  ],
                ),
              ),
            ),
          ));
}

class DeleteBankAccountSheetContent extends StatelessWidget {
  const DeleteBankAccountSheetContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
      builder: (_controller) => StatefulBuilder(
            builder: (context, setState) => Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0),
                ),
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Dimens.boxHeight10,
                    Container(
                      width: Dimens.fourty,
                      height: Dimens.five,
                      decoration: BoxDecoration(
                        color: ColorsValue.greyColor.withOpacity(.2),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(50),
                        ),
                      ),
                    ),
                    Dimens.boxHeight20,
                    GlobalContainer(
                      color: Color(0xffEFE8FC),
                      borderRadius: Dimens.fifty,
                      child: Center(
                        child: SvgPicture.asset(
                          AssetConstants.danger,
                          color: ColorsValue.primaryColor,
                        ),
                      ),
                    ),
                    Dimens.boxHeight20,
                    Padding(
                      padding: Dimens.edgeInsets16_0_16_0,
                      child: RichText(
                        text: TextSpan(
                            text: 'You need to',
                            style: Styles.medium16Black,
                            children: [
                              TextSpan(
                                text: ' disbale Auto-Pay ',
                                style: Styles.bold16Black,
                              ),
                              TextSpan(
                                text: 'before deleting bank account',
                                style: Styles.medium16Black,
                              ),
                            ]),
                      ),
                    ),
                    Dimens.boxHeight20,
                  ],
                ),
              ),
            ),
          ));
}

class DeleteBankAccountSecondSheetContent extends StatelessWidget {
  const DeleteBankAccountSecondSheetContent({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
      builder: (_controller) => StatefulBuilder(
            builder: (context, setState) => Container(
              decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  topRight: Radius.circular(25.0),
                ),
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Dimens.boxHeight10,
                    Container(
                      width: Dimens.fourty,
                      height: Dimens.five,
                      decoration: BoxDecoration(
                        color: ColorsValue.greyColor.withOpacity(.2),
                        borderRadius: const BorderRadius.all(
                          Radius.circular(50),
                        ),
                      ),
                    ),
                    Dimens.boxHeight20,
                    GlobalContainer(
                      color: Color(0xffEFE8FC),
                      borderRadius: Dimens.fifty,
                      child: Center(
                        child: SvgPicture.asset(
                          AssetConstants.delete,
                          color: ColorsValue.primaryColor,
                        ),
                      ),
                    ),
                    Dimens.boxHeight20,
                    Padding(
                      padding: Dimens.edgeInsets16_0_16_0,
                      child: Text(
                        'Are you sure you want to delete your bank account?',
                        style: Styles.bold16Black,
                      ),
                    ),
                    Dimens.boxHeight10,
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: Dimens.edgeInsets16_0_16_0,
                        child: Text(
                          'Amet minim mollit non deserunt ullamco est sit ',
                          style: Styles.regular14Grey,
                        ),
                      ),
                    ),
                    Dimens.boxHeight20,
                    Padding(
                      padding: Dimens.edgeInsets10_0_10_0,
                      child: Row(
                        children: [
                          Expanded(
                            child: BorderedButton(
                              text: 'Cancel'.tr,
                              onTap: () {
                                Get.back<void>();
                              },
                            ),
                          ),
                          Expanded(
                            child: GlobalButton(
                              text: 'Delete'.tr,
                              onTap: () {
                                Get.back<void>();
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Dimens.boxHeight10,
                  ],
                ),
              ),
            ),
          ));
}
