import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class ManagePermissions extends StatelessWidget {
  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          backgroundColor: ColorsValue.backgroundColor,
          appBar: const CustomAppBar(
            title: 'Manage permissions',
          ),
          body: Padding(
            padding: Dimens.edgeInsets10_0_10_0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: ListView(
                    children: [
                      Dimens.boxHeight20,
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(Dimens.twelve),
                          boxShadow: Styles.cardShadow,
                          color: Colors.white,
                        ),
                        child: Padding(
                          padding: Dimens.edgeInsets10,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  CircleIcon(
                                    color: const Color(0xffFDF4F7),
                                    image: AssetConstants.enableScreenLock,
                                    isSvg: false,
                                  ),
                                  Dimens.boxWidth10,
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Enable Screen Lock',
                                        style: Styles.bold16Black,
                                      ),
                                      Text(
                                        'Secure your account ',
                                        style: Styles.grey12,
                                      )
                                    ],
                                  )
                                ],
                              ),
                              FlutterSwitch(
                                padding: Dimens.three,
                                activeColor: ColorsValue.primaryColor,
                                inactiveColor: const Color(0xffEBDDF8),
                                width: Dimens.fourtyFive,
                                toggleColor: _controller.isScreenLockEnabled
                                    ? Colors.white
                                    : ColorsValue.primaryColor,
                                height: Dimens.twentyFour,
                                toggleSize: Dimens.twenty,
                                value: _controller.isScreenLockEnabled,
                                onToggle: (val) {
                                  _controller.isScreenLockEnabled = val;
                                  _controller.update();
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      Dimens.boxHeight10,
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(Dimens.twelve),
                          boxShadow: Styles.cardShadow,
                          color: Colors.white,
                        ),
                        child: Padding(
                          padding: Dimens.edgeInsets10,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  CircleIcon(
                                    color: const Color(0xffE9F5F3),
                                    isSvg: false,
                                    image: AssetConstants.whatsapp,
                                  ),
                                  Dimens.boxWidth10,
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Whatsapp Notification',
                                        style: Styles.bold16Black,
                                      ),
                                      Text(
                                        'Secure your account ',
                                        style: Styles.grey12,
                                      )
                                    ],
                                  )
                                ],
                              ),
                              FlutterSwitch(
                                padding: Dimens.three,
                                activeColor: ColorsValue.primaryColor,
                                inactiveColor: const Color(0xffEBDDF8),
                                width: Dimens.fourtyFive,
                                toggleColor: _controller.isWhatsappEnabled
                                    ? Colors.white
                                    : ColorsValue.primaryColor,
                                height: Dimens.twentyFour,
                                toggleSize: Dimens.twenty,
                                value: _controller.isWhatsappEnabled,
                                onToggle: (val) {
                                  _controller.isWhatsappEnabled = val;
                                  _controller.update();
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      Dimens.boxHeight10,
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(Dimens.twelve),
                          boxShadow: Styles.cardShadow,
                          color: Colors.white,
                        ),
                        child: Padding(
                          padding: Dimens.edgeInsets10,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  CircleIcon(
                                    color: const Color(0xffFDF4F7),
                                    isSvg: false,
                                    image: AssetConstants.smsPermissions,
                                  ),
                                  Dimens.boxWidth10,
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'SMS Permission',
                                        style: Styles.bold16Black,
                                      ),
                                      Text(
                                        'Secure your account ',
                                        style: Styles.grey12,
                                      )
                                    ],
                                  )
                                ],
                              ),
                              FlutterSwitch(
                                padding: Dimens.three,
                                activeColor: ColorsValue.primaryColor,
                                inactiveColor: const Color(0xffEBDDF8),
                                width: Dimens.fourtyFive,
                                toggleColor: _controller.isSmsEnabled
                                    ? Colors.white
                                    : ColorsValue.primaryColor,
                                height: Dimens.twentyFour,
                                toggleSize: Dimens.twenty,
                                value: _controller.isSmsEnabled,
                                onToggle: (val) {
                                  _controller.isSmsEnabled = val;
                                  _controller.update();
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );
}
