import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class HelpAndSupport extends StatelessWidget {
  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          backgroundColor: ColorsValue.backgroundColor,
          appBar: const CustomAppBar(
            title: 'Help & Support',
          ),
          body: Padding(
            padding: Dimens.edgeInsets10_0_10_0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: ListView(
                    children: [
                      Dimens.boxHeight10,
                      SizedBox(
                        height: Dimens.fourtyThree,
                        child: TextFormField(
                          onChanged: (v) {
                            _controller.update();
                          },
                          decoration: InputDecoration(
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(
                                Dimens.twelve,
                              ),
                              borderSide: BorderSide(
                                color: ColorsValue.textColor.withOpacity(0.4),
                              ),
                            ),
                            prefixIcon: Icon(
                              Icons.search,
                              color: ColorsValue.textColor.withOpacity(0.4),
                            ),
                            hintText: 'Search',
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(
                                Dimens.twelve,
                              ),
                              borderSide: BorderSide(
                                color: ColorsValue.textColor.withOpacity(0.4),
                              ),
                            ),
                            filled: true,
                            fillColor: Colors.white,
                            hintStyle: Styles.textFieldHintColor16.copyWith(
                              color: ColorsValue.greyColor.withOpacity(0.5),
                            ),
                            contentPadding: Dimens.edgeInsets10,
                          ),
                        ),
                      ),
                      Dimens.boxHeight20,
                      Row(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.circular(Dimens.thirty),
                              color: ColorsValue.primaryColor,
                            ),
                            child: Padding(
                              padding: Dimens.edgeInsets12,
                              child: Text(
                                'My Account',
                                style: Styles.white15,
                              ),
                            ),
                          ),
                          Dimens.boxWidth10,
                          Container(
                            decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.circular(Dimens.thirty),
                                color: Colors.white,
                                border: Border.all(
                                    color: ColorsValue.primaryColor)),
                            child: Padding(
                              padding: Dimens.edgeInsets12,
                              child: Text(
                                'Mutual Funds',
                                style: Styles.primary15,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Dimens.boxHeight20,
                      ProfileCard(
                        title: 'Payments',
                        subTitle: 'All Transactions',
                        iconImage: AssetConstants.paymentsHelp,
                        onTap: () {},
                      ),
                      Dimens.boxHeight10,
                      ProfileCard(
                        title: 'KYC',
                        subTitle: 'Documents, esign, PAN',
                        iconImage: AssetConstants.paymentsKyc,
                        onTap: () {
                          Get.to(const HelpSupportKyc());
                        },
                      ),
                      Dimens.boxHeight10,
                      ProfileCard(
                        title: 'Bank Accounts',
                        subTitle: 'Primary Bank, NFT, Bank Verification',
                        iconImage: AssetConstants.helpBank,
                        imageBg: const Color(0xffFDF4F7),
                        onTap: () {},
                      ),
                      Dimens.boxHeight10,
                      ProfileCard(
                        title: 'Investkarr Account',
                        subTitle: 'Login, Password, Mobile Number',
                        iconImage: AssetConstants.helpUser,
                        imageBg: const Color(0xffFDF4F7),
                        onTap: () {},
                      ),
                      Dimens.boxHeight10,
                      ProfileCard(
                        title: 'Referrals',
                        subTitle: 'Invite & Earn. Rewards',
                        iconImage: AssetConstants.helpBadge,
                        imageBg: const Color(0xffFFF0EB),
                        onTap: () {},
                      ),
                      Dimens.boxHeight10,
                      ProfileCard(
                        title: 'Others',
                        subTitle: 'Login, Password, Mobile Number',
                        iconImage: AssetConstants.helpOthers,
                        imageBg: const Color(0xffFFF0EB),
                        onTap: () {},
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );
}
