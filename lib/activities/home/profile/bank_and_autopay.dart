import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class BankAndAutoPay extends StatelessWidget {
  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          backgroundColor: ColorsValue.backgroundColor,
          appBar: CustomAppBar(
            title: 'Bank and Autopay',
          ),
          body: Padding(
            padding: Dimens.edgeInsets10_0_10_0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: ListView(
                    children: [
                      Dimens.boxHeight20,
                      Container(
                        width: Dimens.percentWidth(1),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(Dimens.ten),
                          color: const Color(0xffFFF5F5),
                          border: Border.all(
                            color: const Color(0xffFF6369),
                          ),
                        ),
                        child: Padding(
                          padding: Dimens.edgeInsets10,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      CircleAvatar(
                                        backgroundColor:
                                            const Color(0xffFF6369),
                                        radius: Dimens.sixTeen,
                                        child: CircleAvatar(
                                          radius: Dimens.ten,
                                          backgroundColor: Colors.white,
                                          child: Text(
                                            '!',
                                            style: Styles.red13,
                                          ),
                                        ),
                                      ),
                                      Dimens.boxWidth10,
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'Setup Autopay',
                                            style: Styles.medium12Black,
                                          ),
                                          Dimens.boxHeight5,
                                          Text(
                                            'To avoid failure of future SIP installments',
                                            style: Styles.regular12Grey,
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  IconButton(
                                    onPressed: () {},
                                    icon: Icon(
                                      Icons.arrow_forward_ios,
                                      size: Dimens.ten,
                                      color: ColorsValue.primaryColor,
                                    ),
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      Dimens.boxHeight20,
                      SavedBankWidget(
                        bankName: 'State Bank of India',
                        bankAccountNo: '******2679',
                        isPrimaryBank: true,
                        onTap: () {
                          Get.to(BankDetails());
                        },
                      ),
                      Dimens.boxHeight20,
                      SavedBankWidget(
                        bankName: 'State Bank of India',
                        bankAccountNo: '******2679',
                        onTap: () {
                          Get.to(BankDetails());
                        },
                      ),
                    ],
                  ),
                ),
                BorderedButton(
                  text: 'Add Another Account',
                  onTap: () {},
                ),
                Dimens.boxHeight10,
                Center(
                  child: Text(
                    'You can add up to 5 bank accounts',
                    style: Styles.regular12Grey,
                  ),
                ),
                Dimens.boxHeight10,
              ],
            ),
          ),
        ),
      );
}

class SavedBankWidget extends StatelessWidget {
  SavedBankWidget({
    Key? key,
    this.bankAccountNo,
    this.bankName,
    this.isPrimaryBank = false,
    this.onTap,
  }) : super(key: key);

  void Function()? onTap;
  final bool isPrimaryBank;
  final String? bankName;
  final String? bankAccountNo;

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () {
          onTap!();
        },
        child: Card(
          elevation: 0.6,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(Dimens.seven),
          ),
          color: Colors.white,
          child: Padding(
            padding: Dimens.edgeInsets0_15_0_15 + Dimens.edgeInsets10_0_10_0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    GlobalContainer(
                      width: Dimens.thirtyTwo,
                      height: Dimens.thirtyTwo,
                      child: Image.asset(
                        AssetConstants.hdfcLogo,
                        fit: BoxFit.cover,
                      ),
                    ),
                    Dimens.boxWidth10,
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              bankName ?? '',
                              style: Styles.medium16Black,
                            ),
                            Dimens.boxWidth5,
                            GlobalContainer(
                              borderRadius: Dimens.fifty,
                              color: const Color(0xff1ECD93),
                              width: Dimens.fifteen,
                              height: Dimens.fifteen,
                              child: Icon(
                                Icons.done,
                                size: Dimens.eight,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                        Dimens.boxHeight5,
                        Text(
                          bankAccountNo ?? '',
                          style: Styles.regular14Grey,
                        ),
                        if (isPrimaryBank) Dimens.boxHeight15,
                        if (isPrimaryBank)
                          Text(
                            '(Primary Bank)',
                            style: Styles.regular12Grey,
                          )
                      ],
                    )
                  ],
                ),
                IconButton(
                  onPressed: () {},
                  icon: Icon(
                    Icons.arrow_forward_ios,
                    size: Dimens.ten,
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
