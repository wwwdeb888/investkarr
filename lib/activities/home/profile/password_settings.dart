import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class PasswordSettings extends StatelessWidget {
  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          backgroundColor: ColorsValue.backgroundColor,
          appBar: const CustomAppBar(
            title: 'Password Setting',
          ),
          body: Padding(
            padding: Dimens.edgeInsets10_0_10_0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: ListView(
                    children: [
                      Dimens.boxHeight20,
                      Padding(
                        padding: Dimens.edgeInsets10,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundColor: const Color(0xffFDF4F7),
                                  radius: Dimens.twenty,
                                  child: Image.asset(
                                    AssetConstants.changePin,
                                    width: Dimens.twentyFive,
                                    height: Dimens.twentyFive,
                                  ),
                                ),
                                Dimens.boxWidth10,
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Change Pin',
                                      style: Styles.darkGrey14,
                                    ),
                                  ],
                                )
                              ],
                            ),
                            IconButton(
                              onPressed: () {},
                              icon: Icon(
                                Icons.arrow_forward_ios,
                                size: Dimens.twelve,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const CustomDivider(),
                      Padding(
                        padding: Dimens.edgeInsets10,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundColor: const Color(0xffFDF4F7),
                                  radius: Dimens.twenty,
                                  child: Image.asset(
                                    AssetConstants.enableScreenLock,
                                    width: Dimens.twentyFive,
                                    height: Dimens.twentyFive,
                                  ),
                                ),
                                Dimens.boxWidth10,
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Change Password',
                                      style: Styles.darkGrey14,
                                    ),
                                  ],
                                )
                              ],
                            ),
                            IconButton(
                              onPressed: () {},
                              icon: Icon(
                                Icons.arrow_forward_ios,
                                size: Dimens.twelve,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const CustomDivider(
                        opacity: 0.2,
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );
}
