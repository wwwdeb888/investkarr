import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class BankDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          backgroundColor: ColorsValue.backgroundColor,
          appBar: const CustomAppBar(
            title: 'Bank Account Details',
          ),
          body: Padding(
            padding: Dimens.edgeInsets10_0_10_0,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: ListView(
                    children: [
                      Dimens.boxHeight10,
                      Card(
                        elevation: 0.2,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(Dimens.twenty),
                        ),
                        color: Colors.white,
                        child: Padding(
                          padding: Dimens.edgeInsets10,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      GlobalContainer(
                                        width: Dimens.thirtyTwo,
                                        height: Dimens.thirtyTwo,
                                        child: Image.asset(
                                          AssetConstants.hdfcLogo,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                      Dimens.boxWidth10,
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            'HDFC Bank',
                                            style: Styles.medium16Black,
                                          ),
                                          Row(
                                            children: [
                                              Text(
                                                'Primary bank',
                                                style: Styles.regular14Grey,
                                              ),
                                              Dimens.boxWidth2,
                                              Image.asset(
                                                AssetConstants.infoOutlined,
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  GestureDetector(
                                    onTap: () {
                                      showModalBottomSheet(
                                          isScrollControlled: true,
                                          backgroundColor: Colors.transparent,
                                          context: context,
                                          builder: (ctx) =>
                                              MakePrimarySheetContent());
                                    },
                                    child: Icon(
                                      Icons.more_vert,
                                      color: ColorsValue.primaryColor,
                                      size: Dimens.twenty,
                                    ),
                                  ),
                                ],
                              ),
                              Dimens.boxHeight16,
                              const CustomDivider(),
                              Dimens.boxHeight16,
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Status',
                                    style: Styles.regular14Grey,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Verified',
                                        style: Styles.medium14Black,
                                      ),
                                      Dimens.boxWidth2,
                                      GlobalContainer(
                                        borderRadius: Dimens.fifty,
                                        color: const Color(0xff1ECD93),
                                        width: Dimens.fifteen,
                                        height: Dimens.fifteen,
                                        child: Icon(
                                          Icons.done,
                                          size: Dimens.ten,
                                          color: Colors.white,
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                              Dimens.boxHeight20,
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Account Number',
                                    style: Styles.regular14Grey,
                                  ),
                                  Text(
                                    '123********',
                                    style: Styles.medium14Black,
                                  ),
                                ],
                              ),
                              Dimens.boxHeight20,
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Branch Name',
                                    style: Styles.regular14Grey,
                                  ),
                                  Text(
                                    'Avinashi',
                                    style: Styles.medium14Black,
                                  ),
                                ],
                              ),
                              Dimens.boxHeight20,
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'IFSC Code',
                                    style: Styles.regular14Grey,
                                  ),
                                  Text(
                                    'FGSJHDSU',
                                    style: Styles.medium14Black,
                                  ),
                                ],
                              ),
                              Dimens.boxHeight20,
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Account Type',
                                    style: Styles.regular14Grey,
                                  ),
                                  Text(
                                    'Savings',
                                    style: Styles.medium14Black,
                                  ),
                                ],
                              ),
                              Dimens.boxHeight10,
                            ],
                          ),
                        ),
                      ),
                      Dimens.boxHeight20,
                      const CustomDivider(),
                      Dimens.boxHeight20,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Auto Pay',
                            style: Styles.bold16Black,
                          ),
                          FlutterSwitch(
                            padding: Dimens.three,
                            activeColor: ColorsValue.primaryColor,
                            inactiveColor: const Color(0xffEBDDF8),
                            width: Dimens.fourtyFive,
                            toggleColor: _controller.isAutoPayEnabled
                                ? Colors.white
                                : ColorsValue.primaryColor,
                            height: Dimens.twentyFour,
                            toggleSize: Dimens.twenty,
                            value: _controller.isAutoPayEnabled,
                            onToggle: (val) {
                              _controller.isAutoPayEnabled = val;
                              _controller.update();
                            },
                          ),
                        ],
                      ),
                      Dimens.boxHeight16,
                      Container(
                          width: Dimens.percentWidth(1),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(Dimens.ten),
                              color: const Color(0xffF7F2FC)),
                          child: Padding(
                            padding: Dimens.edgeInsets0_15_0_15 +
                                Dimens.edgeInsets10_0_10_0,
                            child: Row(
                              children: [
                                Image.asset(AssetConstants.infoOutlined),
                                Dimens.boxWidth10,
                                Text(
                                  'To avoid failure of future SIP installments',
                                  style: Styles.regular12Grey,
                                ),
                              ],
                            ),
                          ))
                    ],
                  ),
                ),
                BorderedButton(
                  text: 'Setup Autopay',
                  onTap: () {},
                ),
                Dimens.boxHeight10,
              ],
            ),
          ),
        ),
      );
}
