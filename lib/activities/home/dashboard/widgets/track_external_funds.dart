import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/dashboard/widgets/forward_consolidated_statemenet.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class TrackExternalFunds extends StatelessWidget {
  const TrackExternalFunds({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: CustomAppBar(
          title: 'trackExternalFunds'.tr,
        ),
        body: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: Dimens.edgeInsets16,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'startTrackingYourMutual'.tr,
                    style: Styles.regular14Grey,
                  ),
                  Dimens.boxHeight20,
                  Text(
                    '${'step'.tr} 1',
                    style: Styles.boldBlack18,
                  ),
                  Dimens.boxHeight10,
                  TrackExternalFundStepCard(
                    title: 'Generate consolidated statements',
                    subTitle:
                        'Go to KARVY website to generate your latest consolidated statement.',
                    imageIcon: AssetConstants.forwardCs,
                    onTap: () {},
                  ),
                  Dimens.boxHeight20,
                  Text(
                    '${'step'.tr} 2',
                    style: Styles.boldBlack18,
                  ),
                  Dimens.boxHeight10,
                  TrackExternalFundStepCard(
                    title: 'forwardConsolidatedStatements'.tr,
                    subTitle: 'Simply Forward the KARVY email to us.',
                    imageIcon: AssetConstants.generateCs,
                    onTap: () {
                      Get.to(ForwordConsolidatedStatement.new);
                    },
                  ),
                  Dimens.boxHeight20,
                  Container(
                    width: Get.width,
                    margin: Dimens.edgeInsets0_10_0_10,
                    child: Center(
                      child: Text(
                        'Or',
                        style: Styles.regular14Grey,
                      ),
                    ),
                  ),
                  Dimens.boxHeight20,
                  TrackExternalFundStepCard(
                    title: 'uploadConsolidatedStatements'.tr,
                    subTitle:
                        'Already have the consolidated statement handy? Simply upload here.',
                    imageIcon: AssetConstants.uploadCs,
                    onTap: () {
                      Get.to(UploadConsolidatedStatement.new);
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}

class TrackExternalFundStepCard extends StatelessWidget {
  TrackExternalFundStepCard({
    Key? key,
    required this.imageIcon,
    this.onTap,
    required this.subTitle,
    required this.title,
  }) : super(key: key);
  final String? title;
  final String? subTitle;
  final void Function()? onTap;
  final String? imageIcon;

  @override
  Widget build(BuildContext context) => Container(
        width: Dimens.percentWidth(1),
        height: Dimens.percentHeight(.15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(Dimens.twelve),
          boxShadow: Styles.cardShadow,
          color: Colors.white,
        ),
        child: GestureDetector(
          onTap: () {
            onTap!();
          },
          child: Padding(
            padding: Dimens.edgeInsets15_0_15_0,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CircleIcon(
                  color: ColorsValue.pinkColor,
                  svgColor: ColorsValue.blueColor,
                  isSvg: true,
                  image: imageIcon!,
                ),
                Dimens.boxWidth15,
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        title!,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: Styles.medium14Black,
                      ),
                      Dimens.boxHeight15,
                      Text(
                        subTitle!,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                        style: Styles.regular12Grey,
                      )
                    ],
                  ),
                ),
                Dimens.boxWidth10,
                SvgPicture.asset(
                  AssetConstants.rightArrow,
                  height: Dimens.eighteen,
                  width: Dimens.eighteen,
                ),
              ],
            ),
          ),
        ),
      );
}
