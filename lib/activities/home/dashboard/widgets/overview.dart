import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class Overview extends StatelessWidget {
  const Overview({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                /// For new user
                /// ==================================
                _controller.isOverViewNewUser
                    ? Column(
                        children: [
                          Dimens.boxHeight80,
                          SvgPicture.asset(AssetConstants.overviewBg),
                          Dimens.boxHeight30,
                          Text(
                            'Already invested Somewhere?',
                            style: Styles.bold18Black,
                          ),
                          Dimens.boxHeight30,
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Padding(
                              padding: Dimens.edgeInsets5,
                              child: SizedBox(
                                width: Dimens.percentWidth(0.6),
                                height: Dimens.percentHeight(.08),
                                child: ElevatedButton(
                                  style: Styles.elevatedButtonTheme.style!
                                      .copyWith(
                                    backgroundColor: MaterialStateProperty
                                        .resolveWith<Color>(
                                      (Set<MaterialState> states) => (0 == 0)
                                          ? ColorsValue.primaryColor
                                          : ColorsValue.primaryColor
                                              .withOpacity(.5),
                                    ),
                                    textStyle: MaterialStateProperty.all(
                                      Styles.bold16White,
                                    ),
                                  ),
                                  onPressed: () {
                                    _controller.isOverViewNewUser = false;
                                    _controller.update();
                                  },
                                  child: Text(
                                    'Explore Now'.tr,
                                    style: Styles.bold18Black
                                        .copyWith(color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Dimens.boxHeight15,
                          Text(
                            'I don’t have investments elsewhere',
                            style: Styles.regular12Grey,
                          ),
                        ],
                      )
                    :

                    /// ==================================
                    /// For Existing user
                    /// ==================================

                    Column(
                        children: [
                          Padding(
                            padding: Dimens.edgeInsets16_0_16_0,
                            child: Container(
                              width: Dimens.percentWidth(1),
                              constraints: const BoxConstraints(
                                  maxHeight: double.infinity),
                              decoration: BoxDecoration(
                                  boxShadow: Styles.cardShadow,
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.circular(Dimens.twelve)),
                              child: Padding(
                                padding: Dimens.edgeInsets15,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'totalInsvestmentValue'.tr,
                                      style: Styles.medium16Primary,
                                    ),
                                    Dimens.boxHeight5,
                                    Text.rich(
                                      TextSpan(
                                        text: '\u{20b9} ',
                                        style: Styles.bold14Black,
                                        children: [
                                          TextSpan(
                                            text: '72,000',
                                            style: Styles.bold14Black,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Dimens.boxHeight20,
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          children: [
                                            Text(
                                              'currentValue'.tr,
                                              style: Styles.medium12Grey,
                                            ),
                                            Dimens.boxHeight5,
                                            Text.rich(
                                              TextSpan(
                                                text: '\u{20b9} ',
                                                style: Styles.bold14Black,
                                                children: [
                                                  TextSpan(
                                                    text: '69.901',
                                                    style: Styles.bold14Black,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: [
                                            Text(
                                              'returnsAnnualised'.tr,
                                              style: Styles.medium12Grey,
                                            ),
                                            Dimens.boxHeight5,
                                            Text.rich(
                                              TextSpan(
                                                text: '\u{20b9} ',
                                                style: Styles.bold14Black
                                                    .copyWith(
                                                        color: ColorsValue
                                                            .shineGreenColor),
                                                children: [
                                                  TextSpan(
                                                    text: '69.901',
                                                    style: Styles.bold14Grey
                                                        .copyWith(
                                                            color: ColorsValue
                                                                .shineGreenColor),
                                                  ),
                                                  TextSpan(
                                                    text: ' (24%)',
                                                    style: Styles.bold10Black
                                                        .copyWith(
                                                            color: ColorsValue
                                                                .shineGreenColor),
                                                  ),
                                                  WidgetSpan(
                                                    child: Icon(
                                                      Icons
                                                          .arrow_forward_ios_rounded,
                                                      size: Dimens.fifteen,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Dimens.boxHeight20,
                          Padding(
                            padding: Dimens.edgeInsets16_0_16_0,
                            child: Container(
                              width: Dimens.percentWidth(1),
                              height: Dimens.percentHeight(0.3),
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.circular(Dimens.twelve),
                                color: ColorsValue.pinkColor,
                              ),
                            ),
                          ),
                          Dimens.boxHeight10,
                          const Divider(),
                          Dimens.boxHeight10,
                          Padding(
                            padding: Dimens.edgeInsets16_0_16_0,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Column(
                                  children: [
                                    Text(
                                      '20',
                                      style: Styles.bold14Primary,
                                    ),
                                    Dimens.boxHeight5,
                                    Text(
                                      'mutualFunds'.tr,
                                      style: Styles.medium12Black,
                                    ),
                                  ],
                                ),
                                Column(
                                  children: [
                                    Text(
                                      '8',
                                      style: Styles.bold14Primary,
                                    ),
                                    Dimens.boxHeight5,
                                    Text(
                                      'activeSip'.tr,
                                      style: Styles.medium12Black,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Dimens.boxHeight10,
                          const Divider(),
                          Dimens.boxHeight10,
                          Theme(
                            data: Theme.of(context)
                                .copyWith(dividerColor: Colors.transparent),
                            child: Padding(
                              padding: Dimens.edgeInsets5_0_5_0,
                              child: ExpansionTile(
                                title: Text(
                                  'myMcPortfolioSplit'.tr,
                                  style: Styles.bold16Black,
                                ),
                                initiallyExpanded: true,
                                childrenPadding: Dimens.edgeInsets0_10_0_0,
                                children: <Widget>[
                                  Padding(
                                    padding: Dimens.edgeInsets10_0_10_0,
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            height: Dimens.oneHundredFifty,
                                            decoration: BoxDecoration(
                                              color:
                                                  Colors.amber.withOpacity(.3),
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(12),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Dimens.boxWidth20,
                                        Expanded(
                                          child: Column(
                                            children: [
                                              const ContainerType1(
                                                color: Colors.purple,
                                                title: 'Goal Based MF',
                                                value: '90.0',
                                              ),
                                              Dimens.boxHeight10,
                                              const ContainerType1(
                                                color: Colors.red,
                                                title: 'Our Solutions',
                                                value: '4',
                                              ),
                                              Dimens.boxHeight10,
                                              const ContainerType1(
                                                color: Colors.yellow,
                                                title: 'Direct MF',
                                                value: '4',
                                              ),
                                              Dimens.boxHeight10,
                                              const ContainerType1(
                                                color: Colors.yellow,
                                                title: 'Wealth Creation',
                                                value: '4',
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Dimens.boxHeight20,
                                  Container(
                                    margin: Dimens.edgeInsets15_0_15_0,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: const BorderRadius.all(
                                          Radius.circular(10)),
                                      boxShadow: Styles.cardShadow,
                                    ),
                                    child: Column(
                                      children: [
                                        Table(
                                          defaultVerticalAlignment:
                                              TableCellVerticalAlignment.middle,
                                          columnWidths: {
                                            0: const FlexColumnWidth(1),
                                            1: const FlexColumnWidth(1),
                                          },
                                          children: [
                                            const TableRow(
                                              decoration: BoxDecoration(
                                                color: ColorsValue
                                                    .lightBgContainerColor,
                                                borderRadius: BorderRadius.only(
                                                  topLeft: Radius.circular(10),
                                                  topRight: Radius.circular(10),
                                                ),
                                              ),
                                              children: [
                                                TableWidget(
                                                  title: 'MC Portfolio',
                                                  isHeading: true,
                                                ),
                                                TableWidget(
                                                  title: 'Current Value',
                                                  isHeading: true,
                                                ),
                                                TableWidget(
                                                  title: 'Total Returns',
                                                  isHeading: true,
                                                ),
                                              ],
                                            ),
                                            TableRow(
                                              decoration: BoxDecoration(
                                                border: Border(
                                                  bottom: BorderSide(
                                                      width: 1,
                                                      color: Colors.grey
                                                          .withOpacity(.2)),
                                                ),
                                              ),
                                              children: [
                                                const TableWidget(
                                                  title: 'Goal Based MF',
                                                  currencyPrefix: false,
                                                  alignment:
                                                      Alignment.centerLeft,
                                                ),
                                                const TableWidget(
                                                  title: '10,000',
                                                ),
                                                const TableWidget(
                                                  title: '10,000',
                                                ),
                                              ],
                                            ),
                                            TableRow(
                                              decoration: BoxDecoration(
                                                border: Border(
                                                  bottom: BorderSide(
                                                      width: 1,
                                                      color: Colors.grey
                                                          .withOpacity(.2)),
                                                ),
                                              ),
                                              children: [
                                                const TableWidget(
                                                  title: 'Our Solutions',
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  currencyPrefix: false,
                                                ),
                                                const TableWidget(
                                                  title: '10,000',
                                                ),
                                                const TableWidget(
                                                  title: '10,000',
                                                ),
                                              ],
                                            ),
                                            TableRow(
                                              decoration: BoxDecoration(
                                                border: Border(
                                                  bottom: BorderSide(
                                                      width: 1,
                                                      color: Colors.grey
                                                          .withOpacity(.2)),
                                                ),
                                              ),
                                              children: [
                                                const TableWidget(
                                                  title: 'Direct Mf',
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  currencyPrefix: false,
                                                ),
                                                const TableWidget(
                                                  title: '10,000',
                                                ),
                                                const TableWidget(
                                                  title: '10,000',
                                                ),
                                              ],
                                            ),
                                            const TableRow(
                                              children: [
                                                TableWidget(
                                                  title: 'Wealth Creation',
                                                  alignment:
                                                      Alignment.centerLeft,
                                                  currencyPrefix: false,
                                                ),
                                                TableWidget(
                                                  title: '10,000',
                                                ),
                                                TableWidget(
                                                  title: '10,000',
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Dimens.boxHeight5,
                          const Divider(),
                          Theme(
                            data: Theme.of(context)
                                .copyWith(dividerColor: Colors.transparent),
                            child: Padding(
                              padding: Dimens.edgeInsets5_0_5_0,
                              child: ExpansionTile(
                                title: Text(
                                  'Fund Split by Capital Gain'.tr,
                                  style: Styles.bold16Black,
                                ),
                                initiallyExpanded: true,
                                childrenPadding: Dimens.edgeInsets0_10_0_0,
                                children: <Widget>[
                                  Padding(
                                    padding: Dimens.edgeInsets10_0_10_0,
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            height: Dimens.oneHundredFifty,
                                            decoration: BoxDecoration(
                                              color:
                                                  Colors.amber.withOpacity(.3),
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(12),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Dimens.boxWidth20,
                                        Expanded(
                                          child: Column(
                                            children: [
                                              const ContainerType1(
                                                color: ColorsValue.primaryColor,
                                                title: 'Long Term',
                                                value: '\u{20b9} 10,000',
                                              ),
                                              Dimens.boxHeight10,
                                              const ContainerType1(
                                                color:
                                                    ColorsValue.secondaryColor,
                                                title: 'Short Term',
                                                value: '\u{20b9} 30,000',
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Dimens.boxHeight5,
                          const Divider(),
                          Theme(
                            data: Theme.of(context)
                                .copyWith(dividerColor: Colors.transparent),
                            child: Padding(
                              padding: Dimens.edgeInsets5_0_5_0,
                              child: ExpansionTile(
                                title: Text(
                                  'myTopPerformingFunds'.tr,
                                  style: Styles.bold16Black,
                                ),
                                initiallyExpanded: true,
                                childrenPadding: Dimens.edgeInsets0_10_0_0,
                                children: <Widget>[
                                  Padding(
                                    padding: Dimens.edgeInsets10_0_10_0,
                                    child: SmallFundCard(
                                        bankIcon: AssetConstants.hdfcLogo,
                                        tagsList: ['SIP'],
                                        title:
                                            'DSP Tax Saver Direct Plan Growth',
                                        currency: true,
                                        returns: '3000',
                                        percentage: '4000'),
                                  ),
                                  Dimens.boxHeight15,
                                  Padding(
                                    padding: Dimens.edgeInsets10_0_10_0,
                                    child: SmallFundCard(
                                        bankIcon: AssetConstants.hdfcLogo,
                                        tagsList: ['SIP'],
                                        title:
                                            'DSP Tax Saver Direct Plan Growth',
                                        returns: '3000',
                                        currency: true,
                                        percentage: '4000'),
                                  ),
                                  Dimens.boxHeight15,
                                  Padding(
                                    padding: Dimens.edgeInsets10_0_10_0,
                                    child: SmallFundCard(
                                        bankIcon: AssetConstants.hdfcLogo,
                                        tagsList: ['SIP'],
                                        title:
                                            'DSP Tax Saver Direct Plan Growth',
                                        returns: '3000',
                                        currency: true,
                                        percentage: '4000'),
                                  ),
                                  Dimens.boxHeight20,
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),

                /// ==================================
                /// ==================================
              ],
            ),
          ),
        ),
      );
}

class TableWidget extends StatelessWidget {
  const TableWidget({
    Key? key,
    this.title,
    this.alignment,
    this.padding,
    this.style,
    this.currencyPrefix = true,
    this.isHeading = false,
  }) : super(key: key);
  final String? title;
  final Alignment? alignment;
  final EdgeInsetsGeometry? padding;
  final TextStyle? style;
  final bool? currencyPrefix;
  final bool? isHeading;

  @override
  Widget build(BuildContext context) => Align(
        alignment: alignment ?? Alignment.center,
        child: Padding(
          padding: padding ?? Dimens.edgeInsets10,
          child: Container(
            child: isHeading!
                ? Text(title ?? '', style: style ?? Styles.medium12Black)
                : Text.rich(
                    TextSpan(
                      text: currencyPrefix! ? '\u{20b9} ' : '',
                      style: style ?? Styles.medium12Grey,
                      children: [
                        TextSpan(
                          text: title ?? '',
                          style: style ?? Styles.medium12Grey,
                        ),
                      ],
                    ),
                  ),
          ),
        ),
      );
}
