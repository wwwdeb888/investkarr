import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class DashboardInvestYourself extends StatelessWidget {
  const DashboardInvestYourself({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                /// For new user
                /// ==================================
                _controller.isInvestYourselfNewUser
                    ? Column(
                        children: [
                          Dimens.boxHeight80,
                          SvgPicture.asset(AssetConstants.investyourselfBg),
                          Dimens.boxHeight30,
                          Text(
                            'Already invested Somewhere?',
                            style: Styles.bold18Black,
                          ),
                          Dimens.boxHeight30,
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Padding(
                              padding: Dimens.edgeInsets5,
                              child: SizedBox(
                                width: Dimens.percentWidth(0.65),
                                height: Dimens.percentHeight(.08),
                                child: ElevatedButton(
                                  style: Styles.elevatedButtonTheme.style!
                                      .copyWith(
                                    backgroundColor: MaterialStateProperty
                                        .resolveWith<Color>(
                                      (Set<MaterialState> states) => (0 == 0)
                                          ? ColorsValue.primaryColor
                                          : ColorsValue.primaryColor
                                              .withOpacity(.5),
                                    ),
                                    textStyle: MaterialStateProperty.all(
                                      Styles.bold16White,
                                    ),
                                  ),
                                  onPressed: () {
                                    Get.to(const TrackExternalFunds());
                                  },
                                  child: Text(
                                    'Track External Funds'.tr,
                                    style: Styles.bold18Black
                                        .copyWith(color: Colors.white),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Dimens.boxHeight15,
                          Text(
                            'I don’t have investments elsewhere',
                            style: Styles.regular12Grey,
                          ),
                        ],
                      )
                    :

                    /// ==================================
                    /// For Existing user
                    /// ==================================

                    Column(
                        children: [
                          Padding(
                            padding: Dimens.edgeInsets16_0_16_0,
                            child: Container(
                              width: Dimens.percentWidth(1),
                              height: Dimens.percentHeight(0.09),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: ColorsValue.primaryColor,
                                      width: Dimens.one),
                                  borderRadius:
                                      BorderRadius.circular(Dimens.twelve),
                                  color: ColorsValue.primaryColor
                                      .withOpacity(0.04)),
                              child: GestureDetector(
                                onTap: () {
                                  Get.to(const MyFunds());
                                },
                                child: Padding(
                                  padding: Dimens.edgeInsets10,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          CircleIcon(
                                            image: AssetConstants.myFund,
                                            isSvg: true,
                                            svgColor: ColorsValue.primaryColor,
                                            color: Colors.white,
                                          ),
                                          Dimens.boxWidth10,
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text(
                                                'My Funds',
                                                style: Styles.medium12Black,
                                              ),
                                              Dimens.boxHeight5,
                                              Text(
                                                'Check order status & active funds',
                                                style: Styles.regular12Grey,
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                      Dimens.boxWidth10,
                                      const Icon(
                                        Icons.keyboard_arrow_right,
                                        color: ColorsValue.primaryColor,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Dimens.boxHeight15,
                          Padding(
                            padding: Dimens.edgeInsets16_0_16_0,
                            child: Container(
                              width: Dimens.percentWidth(1),
                              constraints: const BoxConstraints(
                                  maxHeight: double.infinity),
                              decoration: BoxDecoration(
                                  boxShadow: Styles.cardShadow,
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.circular(Dimens.twelve)),
                              child: Padding(
                                padding: Dimens.edgeInsets15,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'totalInsvestmentValue'.tr,
                                      style: Styles.medium16Primary,
                                    ),
                                    Dimens.boxHeight5,
                                    Text.rich(
                                      TextSpan(
                                        text: '\u{20b9} ',
                                        style: Styles.bold14Black,
                                        children: [
                                          TextSpan(
                                            text: '72,000',
                                            style: Styles.bold14Black,
                                          ),
                                        ],
                                      ),
                                    ),
                                    Dimens.boxHeight20,
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          children: [
                                            Text(
                                              'currentValue'.tr,
                                              style: Styles.medium12Grey,
                                            ),
                                            Dimens.boxHeight5,
                                            Text.rich(
                                              TextSpan(
                                                text: '\u{20b9} ',
                                                style: Styles.bold14Black,
                                                children: [
                                                  TextSpan(
                                                    text: '69.901',
                                                    style: Styles.bold14Black,
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                        Column(
                                          children: [
                                            Text(
                                              'returnsAnnualised'.tr,
                                              style: Styles.medium12Grey,
                                            ),
                                            Dimens.boxHeight5,
                                            Text.rich(
                                              TextSpan(
                                                text: '\u{20b9} ',
                                                style: Styles.bold14Black
                                                    .copyWith(
                                                        color: ColorsValue
                                                            .shineGreenColor),
                                                children: [
                                                  TextSpan(
                                                    text: '69.901',
                                                    style: Styles.bold14Grey
                                                        .copyWith(
                                                            color: ColorsValue
                                                                .shineGreenColor),
                                                  ),
                                                  TextSpan(
                                                    text: ' (24%)',
                                                    style: Styles.bold10Black
                                                        .copyWith(
                                                            color: ColorsValue
                                                                .shineGreenColor),
                                                  ),
                                                  WidgetSpan(
                                                    child: Icon(
                                                      Icons
                                                          .arrow_forward_ios_rounded,
                                                      size: Dimens.fifteen,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Dimens.boxHeight20,
                          Padding(
                            padding: Dimens.edgeInsets16_0_16_0,
                            child: Container(
                              width: Dimens.percentWidth(1),
                              height: Dimens.percentHeight(0.3),
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.circular(Dimens.twelve),
                                color: ColorsValue.pinkColor,
                              ),
                            ),
                          ),
                          Dimens.boxHeight10,
                          const Divider(),
                          Dimens.boxHeight10,
                          Padding(
                            padding: Dimens.edgeInsets16_0_16_0,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Column(
                                  children: [
                                    Text(
                                      '20',
                                      style: Styles.bold14Primary,
                                    ),
                                    Dimens.boxHeight5,
                                    Text(
                                      'mutualFunds'.tr,
                                      style: Styles.medium12Black,
                                    ),
                                  ],
                                ),
                                Column(
                                  children: [
                                    Text(
                                      '8',
                                      style: Styles.bold14Primary,
                                    ),
                                    Dimens.boxHeight5,
                                    Text(
                                      'activeSip'.tr,
                                      style: Styles.medium12Black,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Dimens.boxHeight10,
                          const Divider(),
                          Dimens.boxHeight10,
                          Theme(
                            data: Theme.of(context)
                                .copyWith(dividerColor: Colors.transparent),
                            child: Padding(
                              padding: Dimens.edgeInsets5_0_5_0,
                              child: ExpansionTile(
                                title: Text(
                                  'Investment Analysis'.tr,
                                  style: Styles.bold16Black,
                                ),
                                initiallyExpanded: true,
                                childrenPadding: Dimens.edgeInsets0_10_0_0,
                                children: <Widget>[
                                  Container(
                                    margin: const EdgeInsets.only(
                                      left: 16,
                                    ),
                                    child: DefaultTabController(
                                      length: 4,
                                      child: Column(
                                        children: [
                                          Container(
                                            padding: Dimens.edgeInsets10,
                                            decoration: BoxDecoration(
                                              color: ColorsValue.primaryColor
                                                  .withOpacity(.1),
                                              borderRadius:
                                                  const BorderRadius.only(
                                                topLeft: Radius.circular(50),
                                                bottomLeft: Radius.circular(50),
                                              ),
                                            ),
                                            child: TabBar(
                                              labelPadding: Dimens.edgeInsets16,
                                              onTap: (v) {
                                                _controller
                                                    .selectIndexForAnalysisInvestment(
                                                        v);
                                              },
                                              indicator: BoxDecoration(
                                                color: Colors.white,
                                                borderRadius:
                                                    const BorderRadius.all(
                                                  Radius.circular(50),
                                                ),
                                                boxShadow: [
                                                  BoxShadow(
                                                    color: Colors.grey
                                                        .withOpacity(0.1),
                                                    spreadRadius: 4,
                                                    blurRadius: 3,
                                                    offset: const Offset(0, 2),
                                                  ),
                                                ],
                                              ),
                                              tabs: [
                                                Text(
                                                  _controller.analysisTitle[0],
                                                  style: _controller
                                                              .selectedAnalysisIndex ==
                                                          0
                                                      ? Styles.bold14Primary
                                                      : Styles.medium12Black,
                                                ),
                                                Column(
                                                  children: [
                                                    Text(
                                                      _controller
                                                          .analysisTitle[1],
                                                      style: _controller
                                                                  .selectedAnalysisIndex ==
                                                              1
                                                          ? Styles.bold14Primary
                                                          : Styles
                                                              .medium12Black,
                                                    ),
                                                    Dimens.boxHeight5,
                                                    Text(
                                                      _controller
                                                          .analysisSubtitle[1],
                                                      style: _controller
                                                                  .selectedAnalysisIndex ==
                                                              1
                                                          ? Styles
                                                              .medium10Primary
                                                          : Styles.medium10Grey,
                                                    ),
                                                  ],
                                                ),
                                                Column(
                                                  children: [
                                                    Text(
                                                      _controller
                                                          .analysisTitle[2],
                                                      style: _controller
                                                                  .selectedAnalysisIndex ==
                                                              2
                                                          ? Styles.bold14Primary
                                                          : Styles
                                                              .medium12Black,
                                                    ),
                                                    Dimens.boxHeight5,
                                                    Text(
                                                      _controller
                                                          .analysisSubtitle[2],
                                                      style: _controller
                                                                  .selectedAnalysisIndex ==
                                                              2
                                                          ? Styles
                                                              .medium10Primary
                                                          : Styles.medium10Grey,
                                                    ),
                                                  ],
                                                ),
                                                Column(
                                                  children: [
                                                    Text(
                                                      _controller
                                                          .analysisTitle[3],
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      style: _controller
                                                                  .selectedAnalysisIndex ==
                                                              3
                                                          ? Styles.bold14Primary
                                                          : Styles
                                                              .medium12Black,
                                                    ),
                                                    Dimens.boxHeight5,
                                                    Text(
                                                      _controller
                                                          .analysisSubtitle[3],
                                                      style: _controller
                                                                  .selectedAnalysisIndex ==
                                                              3
                                                          ? Styles
                                                              .medium10Primary
                                                          : Styles.medium10Grey,
                                                    ),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Dimens.boxHeight30,
                                  Padding(
                                    padding: Dimens.edgeInsets10_0_10_0,
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Container(
                                            height: Dimens.oneHundredFifty,
                                            decoration: BoxDecoration(
                                              color:
                                                  Colors.amber.withOpacity(.3),
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(12),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Dimens.boxWidth20,
                                        _controller.selectedAnalysisIndex == 1
                                            ? Expanded(
                                                child: Column(
                                                  children: [
                                                    const ContainerType1(
                                                      color: ColorsValue
                                                          .primaryColor,
                                                      title: 'Large Cap',
                                                      value: '90.0',
                                                    ),
                                                    Dimens.boxHeight10,
                                                    const ContainerType1(
                                                      color: Colors.grey,
                                                      title: 'Mid Cap',
                                                      value: '4',
                                                    ),
                                                    Dimens.boxHeight10,
                                                    const ContainerType1(
                                                      color: Colors.yellow,
                                                      title: 'Small Cap',
                                                      value: '4',
                                                    ),
                                                  ],
                                                ),
                                              )
                                            : Expanded(
                                                child: Column(
                                                  children: [
                                                    const ContainerType1(
                                                      color: Colors.purple,
                                                      title: 'Goal Based MF',
                                                      value: '90.0',
                                                    ),
                                                    Dimens.boxHeight10,
                                                    const ContainerType1(
                                                      color: Colors.red,
                                                      title: 'Our Solutions',
                                                      value: '4',
                                                    ),
                                                    Dimens.boxHeight10,
                                                    const ContainerType1(
                                                      color: Colors.yellow,
                                                      title: 'Direct MF',
                                                      value: '4',
                                                    ),
                                                    Dimens.boxHeight10,
                                                    const ContainerType1(
                                                      color: Colors.yellow,
                                                      title: 'Wealth Creation',
                                                      value: '4',
                                                    ),
                                                  ],
                                                ),
                                              ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Dimens.boxHeight10,
                          const Divider(),
                          Theme(
                            data: Theme.of(context)
                                .copyWith(dividerColor: Colors.transparent),
                            child: Padding(
                              padding: Dimens.edgeInsets5_0_5_0,
                              child: ExpansionTile(
                                title: Text(
                                  'Asset Analysis'.tr,
                                  style: Styles.bold16Black,
                                ),
                                initiallyExpanded: true,
                                childrenPadding: Dimens.edgeInsets0_10_0_0,
                                children: <Widget>[
                                  DefaultTabController(
                                    length: 4,
                                    child: Column(
                                      children: [
                                        Container(
                                          margin: const EdgeInsets.only(
                                            left: 16,
                                          ),
                                          padding: Dimens.edgeInsets7,
                                          decoration: BoxDecoration(
                                            color: ColorsValue.primaryColor
                                                .withOpacity(.1),
                                            borderRadius:
                                                const BorderRadius.only(
                                              topLeft: Radius.circular(50),
                                              bottomLeft: Radius.circular(50),
                                            ),
                                          ),
                                          child: TabBar(
                                            labelPadding: Dimens.edgeInsets16,
                                            onTap: (v) {
                                              _controller
                                                  .selectIndexForAssetAnalysis(
                                                      v);
                                            },
                                            indicator: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  const BorderRadius.all(
                                                Radius.circular(50),
                                              ),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.grey
                                                      .withOpacity(0.1),
                                                  spreadRadius: 4,
                                                  blurRadius: 3,
                                                  offset: const Offset(0, 2),
                                                ),
                                              ],
                                            ),
                                            tabs: [
                                              Text(
                                                'all'.tr,
                                                style: _controller
                                                            .assetAnalysisIndex ==
                                                        0
                                                    ? Styles.bold14Primary
                                                    : Styles.medium12Black,
                                              ),
                                              Column(
                                                children: [
                                                  Text(
                                                    'equity'.tr,
                                                    style: _controller
                                                                .assetAnalysisIndex ==
                                                            1
                                                        ? Styles.bold14Primary
                                                        : Styles.medium12Black,
                                                  ),
                                                  Dimens.boxHeight5,
                                                  Text(
                                                    '95.9%'.tr,
                                                    style: _controller
                                                                .assetAnalysisIndex ==
                                                            1
                                                        ? Styles.medium10Primary
                                                        : Styles.medium10Grey,
                                                  ),
                                                ],
                                              ),
                                              Column(
                                                children: [
                                                  Text(
                                                    'Debt'.tr,
                                                    style: _controller
                                                                .assetAnalysisIndex ==
                                                            2
                                                        ? Styles.bold14Primary
                                                        : Styles.medium12Black,
                                                  ),
                                                  Dimens.boxHeight5,
                                                  Text(
                                                    '95.9%'.tr,
                                                    style: _controller
                                                                .assetAnalysisIndex ==
                                                            2
                                                        ? Styles.medium10Primary
                                                        : Styles.medium10Grey,
                                                  ),
                                                ],
                                              ),
                                              Column(
                                                children: [
                                                  Text(
                                                    'Others'.tr,
                                                    style: _controller
                                                                .assetAnalysisIndex ==
                                                            3
                                                        ? Styles.bold14Primary
                                                        : Styles.medium12Black,
                                                  ),
                                                  Dimens.boxHeight5,
                                                  Text(
                                                    '95.9%'.tr,
                                                    style: _controller
                                                                .assetAnalysisIndex ==
                                                            3
                                                        ? Styles.medium10Primary
                                                        : Styles.medium10Grey,
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                        Dimens.boxHeight20,
                                        Padding(
                                          padding: Dimens.edgeInsets16_0_16_0,
                                          child: Column(
                                            children: [
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    'Top Holdings',
                                                    style: Styles.medium14Black,
                                                  ),
                                                  Row(
                                                    children: [
                                                      Text(
                                                        'Assets',
                                                        style: Styles
                                                            .bold14Primary,
                                                      ),
                                                      Dimens.boxWidth2,
                                                      const Icon(
                                                          Icons.arrow_drop_down)
                                                    ],
                                                  ),
                                                ],
                                              ),
                                              Dimens.boxHeight20,
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    'Infosys ltd',
                                                    style: Styles.regular14Grey,
                                                  ),
                                                  Text(
                                                    '24.30%',
                                                    style: Styles.medium14Black,
                                                  ),
                                                ],
                                              ),
                                              Dimens.boxHeight20,
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    'Infosys ltd',
                                                    style: Styles.regular14Grey,
                                                  ),
                                                  Text(
                                                    '24.30%',
                                                    style: Styles.medium14Black,
                                                  ),
                                                ],
                                              ),
                                              Dimens.boxHeight20,
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    'Infosys ltd',
                                                    style: Styles.regular14Grey,
                                                  ),
                                                  Text(
                                                    '24.30%',
                                                    style: Styles.medium14Black,
                                                  ),
                                                ],
                                              ),
                                              Dimens.boxHeight20,
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceBetween,
                                                children: [
                                                  Text(
                                                    'Infosys ltd',
                                                    style: Styles.regular14Grey,
                                                  ),
                                                  Text(
                                                    '24.30%',
                                                    style: Styles.medium14Black,
                                                  ),
                                                ],
                                              ),
                                              Dimens.boxHeight20,
                                              Align(
                                                alignment: Alignment.centerLeft,
                                                child: Text(
                                                  'See All holdings',
                                                  style: Styles.medium14Primary,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Dimens.boxHeight30,
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),

                /// ==================================
                /// ==================================
              ],
            ),
          ),
        ),
      );
}

class AnalysisTitleContainer extends StatelessWidget {
  AnalysisTitleContainer({
    Key? key,
    this.isSelected = false,
    required this.title,
    this.subTitle,
    this.isNotAll = false,
    this.onTap,
  }) : super(key: key);

  bool isSelected;
  String? title;
  String? subTitle;
  bool isNotAll;
  void Function()? onTap;

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () {
          onTap!();
        },
        child: Container(
          margin: Dimens.edgeInsets0_5_0_5,
          padding: Dimens.edgeInsets15,
          height: Dimens.fifty,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Dimens.fifty),
            color: isSelected ? Colors.white : Colors.transparent,
          ),
          constraints: const BoxConstraints(maxWidth: double.infinity),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  title ?? '',
                  style:
                      isSelected ? Styles.bold14Primary : Styles.medium12Black,
                ),
                if (isNotAll) Dimens.boxHeight5,
                if (isNotAll)
                  Text(
                    subTitle ?? '',
                    style: isSelected
                        ? Styles.medium10Primary
                        : Styles.medium10Grey,
                  ),
              ],
            ),
          ),
        ),
      );
}

class CustomTabBarViewData extends StatelessWidget {
  const CustomTabBarViewData({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Row(
              children: [
                Text(
                  'topHoldings'.tr,
                  style: Styles.bold16Black,
                ),
                const Spacer(),
                Text(
                  'assets'.tr,
                  style: Styles.primary16Bold,
                ),
                const Icon(
                  Icons.arrow_upward_rounded,
                  size: 10,
                ),
              ],
            ),
            Dimens.boxHeight10,
            Container(
              margin: const EdgeInsets.only(top: 8),
              child: Row(
                children: [
                  Text(
                    'Infosys ltd',
                    style: Styles.grey15,
                  ),
                  const Spacer(),
                  Text(
                    '24.30%'.tr,
                    style: Styles.grey15,
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 8),
              child: Row(
                children: [
                  Text(
                    'Infosys ltd',
                    style: Styles.grey15,
                  ),
                  const Spacer(),
                  Text(
                    '24.30%'.tr,
                    style: Styles.grey15,
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(top: 8),
              child: Row(
                children: [
                  Text(
                    'Infosys ltd',
                    style: Styles.grey15,
                  ),
                  const Spacer(),
                  Text(
                    '24.30%'.tr,
                    style: Styles.grey15,
                  ),
                ],
              ),
            ),
            Dimens.boxHeight5,
            Text(
              'seeAllHoldings'.tr,
              style: Styles.bold14Primary,
            ),
          ],
        ),
      );
}
