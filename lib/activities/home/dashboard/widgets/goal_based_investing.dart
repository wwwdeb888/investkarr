import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class GoalBasedInvesting extends StatelessWidget {
  const GoalBasedInvesting({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
      builder: (_controller) => SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: Dimens.edgeInsets16,
                  child: DefaultContainer(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ColumnTile(
                          title: 'currentValue'.tr,
                          titleStyle: Styles.grey12,
                          valueStyle: Styles.bold14Black,
                          value: '\u{20b9} 2,00,000',
                        ),
                        ColumnTile(
                          title: 'totalReturns'.tr,
                          value: '\u{20b9} 69,901',
                          titleStyle: Styles.grey12,
                          valueStyle:
                              Styles.bold14Black.copyWith(color: ColorsValue.shineGreenColor),
                          valueAlignment: CrossAxisAlignment.end,
                        ),
                      ],
                    ),
                  ),
                ),
                Dimens.boxHeight10,
                Padding(
                  padding: Dimens.edgeInsets16_0_16_0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        'My Goals (3)',
                        style: Styles.bold14Black,
                      ),
                      GestureDetector(
                        onTap: () {
                          showModalBottomSheet(
                            backgroundColor: Colors.transparent,
                            context: context,
                            builder: (ctx) => StatefulBuilder(
                                builder: (context, setState) => Container(
                                      decoration: const BoxDecoration(
                                        color: Colors.white,
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(25.0),
                                          topRight: Radius.circular(25.0),
                                        ),
                                      ),
                                      child: SingleChildScrollView(
                                        child: Column(
                                          children: [
                                            Dimens.boxHeight20,
                                            Container(
                                              width: 50,
                                              height: 6,
                                              decoration: BoxDecoration(
                                                color: ColorsValue.greyColor
                                                    .withOpacity(.2),
                                                borderRadius:
                                                    const BorderRadius.all(
                                                  Radius.circular(50),
                                                ),
                                              ),
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Padding(
                                                  padding: Dimens.edgeInsets16,
                                                  child: Text(
                                                    'filterBy'.tr,
                                                    style: Styles.medium18Black,
                                                  ),
                                                ),
                                                CustomTile(
                                                  title: 'All',
                                                  isCheck: _controller
                                                          .myGoalsSortedCheck ==
                                                      0,
                                                  onTap: () {
                                                    setState(() {
                                                      _controller
                                                          .myGoalsSortWith(0);
                                                    });
                                                  },
                                                ),
                                                CustomTile(
                                                  title: 'Cancelled',
                                                  isCheck: _controller
                                                          .myGoalsSortedCheck ==
                                                      1,
                                                  onTap: () {
                                                    setState(() {
                                                      _controller
                                                          .myGoalsSortWith(1);
                                                    });
                                                  },
                                                ),
                                                CustomTile(
                                                  title: 'Completed',
                                                  isCheck: _controller
                                                          .myGoalsSortedCheck ==
                                                      2,
                                                  onTap: () {
                                                    setState(() {
                                                      _controller
                                                          .myGoalsSortWith(2);
                                                    });
                                                  },
                                                ),
                                                Dimens.boxHeight15,
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                    )),
                          );
                        },
                        child: Row(
                          children: [
                            Text(
                              'all'.tr.toUpperCase(),
                              style: Styles.medium12Primary,
                            ),
                            Icon(
                              Icons.arrow_drop_down,
                              size: Dimens.eighteen,
                              color: ColorsValue.primaryColor,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: Dimens.edgeInsets16,
                  child: Stack(
                    children: [
                      Container(
                        height: Dimens.percentHeight(0.1),
                        width: Dimens.percentWidth(0.35),
                        decoration: BoxDecoration(
                          color: Colors.yellow,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(16),
                          ),
                          boxShadow: Styles.cardShadow,
                        ),
                      ),
                      DefaultContainer(
                        color: Colors.transparent,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                CircleIcon(
                                  image: 'assets/home/home_vector.svg',
                                  color: Colors.white,
                                  isSvg: true,
                                ),
                                Dimens.boxWidth15,
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Buying a House',
                                      style: Styles.medium16Black,
                                    ),
                                    Dimens.boxHeight5,
                                    Text(
                                      '₹35k of ₹3L',
                                      style: Styles.regular12Black,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '35%',
                                  style: Styles.bold16Black
                                      .copyWith(color: ColorsValue.shineGreenColor),
                                ),
                                Dimens.boxWidth2,
                                Icon(
                                  Icons.arrow_forward_ios_rounded,
                                  size: Dimens.twelve,
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: Dimens.edgeInsets16,
                  child: Stack(
                    children: [
                      Container(
                        height: Dimens.percentHeight(0.1),
                        width: Dimens.percentWidth(0.5),
                        decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(16),
                          ),
                          boxShadow: Styles.cardShadow,
                        ),
                      ),
                      DefaultContainer(
                        color: Colors.transparent,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                CircleIcon(
                                  image: AssetConstants.weddingRings,
                                  isSvg: false,
                                  color: Colors.white,
                                ),
                                Dimens.boxWidth15,
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Marriage',
                                      style: Styles.medium16Black,
                                    ),
                                    Dimens.boxHeight5,
                                    Text(
                                      '₹35k of ₹3L',
                                      style: Styles.regular12Black,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  '50%',
                                  style: Styles.bold16Black
                                      .copyWith(color: ColorsValue.shineGreenColor),
                                ),
                                Dimens.boxWidth2,
                                Icon(
                                  Icons.arrow_forward_ios_rounded,
                                  size: Dimens.twelve,
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: Dimens.edgeInsets16,
                  child: Stack(
                    children: [
                      Container(
                        height: Dimens.percentHeight(0.1),
                        width: Dimens.percentWidth(1),
                        decoration: BoxDecoration(
                          color: Colors.orange,
                          borderRadius: const BorderRadius.all(
                            Radius.circular(16),
                          ),
                          boxShadow: Styles.cardShadow,
                        ),
                      ),
                      DefaultContainer(
                        color: Colors.transparent,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                CircleIcon(
                                  image: AssetConstants.carCompact,
                                  color: Colors.white,
                                  isSvg: false,
                                ),
                                Dimens.boxWidth15,
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Marriage',
                                      style: Styles.medium16Black,
                                    ),
                                    Dimens.boxHeight5,
                                    Text(
                                      'Completed',
                                      style: Styles.regular12Black,
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                if (0 != 0)
                                  Text(
                                    '50%',
                                    style: Styles.bold16Black
                                        .copyWith(color: ColorsValue.shineGreenColor),
                                  ),
                                Dimens.boxWidth2,
                                Icon(
                                  Icons.arrow_forward_ios_rounded,
                                  size: Dimens.twelve,
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ));
}
