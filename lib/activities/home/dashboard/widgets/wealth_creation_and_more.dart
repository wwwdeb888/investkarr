import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class WealthCreationAndMore extends StatelessWidget {
  const WealthCreationAndMore({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
        child: Padding(
          padding: Dimens.edgeInsets16,
          child: Column(
            children: [
              DefaultContainer(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'wealthCreation'.tr,
                          style: Styles.medium16Primary,
                        ),
                        RightArrow(
                          onTap: () {},
                        )
                      ],
                    ),
                    Dimens.boxHeight20,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ColumnTile(
                          title: 'indexValue'.tr,
                          titleStyle: Styles.grey12,
                          valueStyle: Styles.bold14Black,
                          value: '109.87',
                        ),
                        ColumnTile(
                          title: 'Current Value'.tr,
                          titleStyle: Styles.grey12,
                          valueStyle: Styles.bold14Black,
                          value: '\u{20b9} 13,555',
                        ),
                        ColumnTile(
                          title: 'Total Returns'.tr,
                          titleStyle: Styles.grey12,
                          valueAlignment: CrossAxisAlignment.end,
                          valueStyle:
                              Styles.bold14Black.copyWith(color: ColorsValue.shineGreenColor),
                          value: '\u{20b9} 69,901',
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Dimens.boxHeight20,
              const Divider(),
              Dimens.boxHeight20,
              DefaultContainer(
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Curated Packs'.tr,
                          style: Styles.medium16Primary,
                        ),
                        RightArrow(
                          onTap: () {},
                        )
                      ],
                    ),
                    Dimens.boxHeight20,
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ColumnTile(
                          title: 'Current Value'.tr,
                          titleStyle: Styles.grey12,
                          valueStyle: Styles.bold14Black,
                          value: '\u{20b9} 1,30,299',
                        ),
                        ColumnTile(
                          title: 'Total Returns'.tr,
                          titleStyle: Styles.grey12,
                          valueAlignment: CrossAxisAlignment.end,
                          valueStyle:
                              Styles.bold14Black.copyWith(color: ColorsValue.shineGreenColor),
                          value: '\u{20b9} 69,901',
                        ),
                      ],
                    )
                  ],
                ),
              ),
              Dimens.boxHeight20,
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  '${'activated'.tr} (03)',
                  style: Styles.medium14Black,
                ),
              ),
              Dimens.boxHeight20,
              ActivatedTile(
                title: 'emergencyPack'.tr,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ColumnTile(
                      title: 'indexValue'.tr,
                      titleStyle: Styles.grey12,
                      valueStyle: Styles.bold14Black,
                      value: '109.87',
                    ),
                    ColumnTile(
                      title: 'Current Value'.tr,
                      titleStyle: Styles.grey12,
                      valueStyle: Styles.bold14Black,
                      value: '\u{20b9} 13,555',
                    ),
                    ColumnTile(
                      title: 'Total Returns'.tr,
                      titleStyle: Styles.grey12,
                      valueAlignment: CrossAxisAlignment.end,
                      valueStyle:
                          Styles.bold14Black.copyWith(color: ColorsValue.shineGreenColor),
                      value: '\u{20b9} 69,901',
                    ),
                  ],
                ),
              ),
              Dimens.boxHeight15,
              ActivatedTile(
                title: 'Tax Saver Pack'.tr,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ColumnTile(
                      title: 'indexValue'.tr,
                      titleStyle: Styles.grey12,
                      valueStyle: Styles.bold14Black,
                      value: '109.87',
                    ),
                    ColumnTile(
                      title: 'Current Value'.tr,
                      titleStyle: Styles.grey12,
                      valueStyle: Styles.bold14Black,
                      value: '\u{20b9} 13,555',
                    ),
                    ColumnTile(
                      title: 'Total Returns'.tr,
                      titleStyle: Styles.grey12,
                      valueAlignment: CrossAxisAlignment.end,
                      valueStyle:
                          Styles.bold14Black.copyWith(color: ColorsValue.shineGreenColor),
                      value: '\u{20b9} 69,901',
                    ),
                  ],
                ),
              ),
              Dimens.boxHeight15,
              ActivatedTile(
                title: 'ESG Pack'.tr,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ColumnTile(
                      title: 'indexValue'.tr,
                      titleStyle: Styles.grey12,
                      valueStyle: Styles.bold14Black,
                      value: '109.87',
                    ),
                    ColumnTile(
                      title: 'Current Value'.tr,
                      titleStyle: Styles.grey12,
                      valueStyle: Styles.bold14Black,
                      value: '\u{20b9} 13,555',
                    ),
                    ColumnTile(
                      title: 'Total Returns'.tr,
                      titleStyle: Styles.grey12,
                      valueAlignment: CrossAxisAlignment.end,
                      valueStyle:
                          Styles.bold14Black.copyWith(color: ColorsValue.shineGreenColor),
                      value: '\u{20b9} 69,901',
                    ),
                  ],
                ),
              ),
              Dimens.boxHeight20,
            ],
          ),
        ),
      );
}

class ActivatedTile extends StatelessWidget {
  const ActivatedTile({
    Key? key,
    this.title,
    this.child,
  }) : super(key: key);

  final String? title;
  final Widget? child;

  @override
  Widget build(BuildContext context) => Container(
        margin: const EdgeInsets.only(
          top: 8,
        ),
        child: DefaultContainer(
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    title!,
                    style: Styles.medium16Black,
                  ),
                  RightArrow(
                    onTap: () {},
                  )
                ],
              ),
              Dimens.boxHeight10,
              const Divider(),
              Dimens.boxHeight10,
              child!,
            ],
          ),
        ),
      );
}
