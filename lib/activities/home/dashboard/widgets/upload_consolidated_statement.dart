import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class UploadConsolidatedStatement extends StatelessWidget {
  const UploadConsolidatedStatement({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          appBar: CustomAppBar(
            title: 'uploadConsolidatedStatements'.tr,
          ),
          body: SafeArea(
            child: Padding(
              padding: Dimens.edgeInsets16,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        Text(
                          'selectFileToUpload'.tr,
                          style: Styles.bold16Black,
                        ),
                        Dimens.boxHeight15,
                        RichText(
                          text: TextSpan(
                              text: 'Upload your latest consolidated statement',
                              style: Styles.bold14Grey,
                              children: [
                                TextSpan(
                                    text: ' Excel file ',
                                    style: Styles.bold14Black),
                                const TextSpan(text: 'sent by CAMS.'),
                              ]),
                        ),
                        Dimens.boxHeight40,
                        _controller.consolidatedFileSelectToUpload
                            ? Container(
                                height: Dimens.percentHeight(0.07),
                                width: Dimens.percentWidth(1),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius:
                                      BorderRadius.circular(Dimens.twelve),
                                  border: Border.all(
                                      color: ColorsValue.primaryColor,
                                      width: Dimens.one),
                                ),
                                child: Padding(
                                  padding: Dimens.edgeInsets15_0_15_0,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        children: [
                                          SvgPicture.asset(
                                              AssetConstants.fileIc),
                                          Dimens.boxWidth15,
                                          Text(
                                            'Documeny.xls',
                                            style: Styles.regular14Grey,
                                          ),
                                        ],
                                      ),
                                      GestureDetector(
                                        onTap: () {
                                          _controller
                                                  .consolidatedFileSelectToUpload =
                                              false;
                                          _controller.update();
                                        },
                                        child: Icon(
                                          Icons.cancel_outlined,
                                          size: Dimens.twenty,
                                          color: ColorsValue.primaryColor,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            : GestureDetector(
                                onTap: () {
                                  _controller.consolidatedFileSelectToUpload =
                                      true;
                                  _controller.update();
                                },
                                child: DottedBorder(
                                  radius: Radius.circular(Dimens.twelve),
                                  borderType: BorderType.RRect,
                                  padding: Dimens.edgeInsets40,
                                  color: ColorsValue.primaryColor,
                                  strokeWidth: 1,
                                  dashPattern: [5],
                                  child: Center(
                                    child: Column(
                                      children: [
                                        Text(
                                          'Add Excel File to Upload',
                                          style: Styles.regular14Grey,
                                        ),
                                        Dimens.boxHeight15,
                                        Container(
                                          width: Dimens.percentWidth(0.3),
                                          height: Dimens.percentHeight(.05),
                                          decoration: BoxDecoration(
                                            color: ColorsValue.primaryColor,
                                            borderRadius: BorderRadius.circular(
                                                Dimens.fifty),
                                          ),
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              SvgPicture.asset(
                                                  AssetConstants.uploadIc),
                                              Dimens.boxWidth10,
                                              Text(
                                                'Upload',
                                                style: Styles.bold14White,
                                              ),
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                        Dimens.boxHeight40,
                        Text(
                          'enterThePasswordForYourConsolidatedAccountStatement'
                              .tr,
                          style: Styles.regular14Grey,
                        ),
                        Dimens.boxHeight15,
                        SizedBox(
                          height: Dimens.fourtyThree,
                          child: TextFormField(
                            controller: _controller.uploadConsolidatePassword,
                            onChanged: (v) {
                              _controller.update();
                            },
                            obscureText:
                                !_controller.isConsolidatePasswordShown,
                            obscuringCharacter: '*',
                            decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(
                                  Dimens.twelve,
                                ),
                                borderSide: BorderSide(
                                  color:
                                      ColorsValue.primaryColor.withOpacity(0.4),
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(
                                  Dimens.twelve,
                                ),
                                borderSide: BorderSide(
                                  color:
                                      ColorsValue.primaryColor.withOpacity(0.4),
                                ),
                              ),
                              filled: true,
                              fillColor: Colors.white,
                              hintStyle: Styles.textFieldHintColor16.copyWith(
                                color: ColorsValue.greyColor.withOpacity(0.5),
                              ),
                              contentPadding: Dimens.edgeInsets10,
                            ),
                          ),
                        ),
                        Dimens.boxHeight10,
                        Row(
                          children: [
                            SizedBox(
                              width: Dimens.twentyFive,
                              height: Dimens.twentyFive,
                              child: Checkbox(
                                value: _controller.isConsolidatePasswordShown,
                                shape: const CircleBorder(),
                                side: const BorderSide(
                                    color: ColorsValue.primaryColor),
                                checkColor: Colors.white,
                                activeColor: Colors.greenAccent,
                                onChanged: (value) {
                                  _controller.isConsolidatePasswordShown =
                                      value!;
                                  _controller.update();
                                },
                              ),
                            ),
                            Dimens.boxWidth10,
                            Text(
                              'Show Password',
                              style: Styles.regular12Grey,
                            ),
                            Dimens.boxWidth10,
                          ],
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) => (_controller
                                        .uploadConsolidatePassword
                                        .text
                                        .isNotEmpty &&
                                    _controller.consolidatedFileSelectToUpload)
                                ? ColorsValue.primaryColor
                                : ColorsValue.primaryColor.withOpacity(.5),
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16White,
                          ),
                        ),
                        onPressed: () {
                          if (_controller
                                  .uploadConsolidatePassword.text.isNotEmpty &&
                              _controller.consolidatedFileSelectToUpload) {
                            _controller.isInvestYourselfNewUser = false;
                            _controller.update();
                            Get.back<void>();
                            Get.back<void>();
                          } else {}
                        },
                        child: Text(
                          'Submit'.tr,
                          style: Styles.bold16White,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
