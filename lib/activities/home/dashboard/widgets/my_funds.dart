import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/activities/home/dashboard/widgets/fund_transaction.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class MyFunds extends StatelessWidget {
  const MyFunds({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
      builder: (_controller) => Scaffold(
            appBar: CustomAppBar(
              title: 'myFunds'.tr,
            ),
            body: SafeArea(
              child: Column(
                children: [
                  Theme(
                    data: Theme.of(context)
                        .copyWith(dividerColor: Colors.transparent),
                    child: ExpansionTile(
                      title: Text(
                        'ordersInProgress'.tr,
                        style: Styles.bold16Black,
                      ),
                      initiallyExpanded: false,
                      children: [
                        Padding(
                          padding: Dimens.edgeInsets16,
                          child: Column(
                            children: [
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Container(
                                    height: Dimens.fourtySix,
                                    width: Dimens.fourtySix,
                                    decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(Dimens.twelve),
                                      ),
                                    ),
                                    child: Image.asset(
                                      AssetConstants.hdfcLogo,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  Dimens.boxWidth10,
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        Text(
                                          'DSP Tax Saver Direct Plan Growth',
                                          style: Styles.medium16Black,
                                        ),
                                      ],
                                    ),
                                  ),
                                  const Spacer(),
                                ],
                              ),
                              Dimens.boxHeight15,
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: Text.rich(
                                      TextSpan(
                                        children: [
                                          TextSpan(
                                            text: 'Order Id : ',
                                            style: Styles.regular12Grey,
                                          ),
                                          TextSpan(
                                            text: '36278452894',
                                            style: Styles.medium12Black,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  Icon(
                                    Icons.watch_later_outlined,
                                    color: Colors.orange,
                                    size: Dimens.fifteen,
                                  ),
                                  Dimens.boxWidth5,
                                  Text(
                                    '23 Nov 2021',
                                    style: Styles.medium12Grey,
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                  const Divider(),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 16, top: 16, right: 16),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'activeFunds'.tr,
                          style: Styles.bold16Black,
                        ),
                        GestureDetector(
                          onTap: () {
                            showModalBottomSheet(
                              backgroundColor: Colors.transparent,
                              context: context,
                              builder: (ctx) => StatefulBuilder(
                                  builder: (context, setState) => Container(
                                        decoration: const BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(25.0),
                                            topRight: Radius.circular(25.0),
                                          ),
                                        ),
                                        child: SingleChildScrollView(
                                          child: Column(
                                            children: [
                                              Dimens.boxHeight20,
                                              Container(
                                                width: Dimens.fifty,
                                                height: Dimens.six,
                                                decoration: BoxDecoration(
                                                  color: ColorsValue.greyColor
                                                      .withOpacity(.2),
                                                  borderRadius:
                                                      const BorderRadius.all(
                                                    Radius.circular(50),
                                                  ),
                                                ),
                                              ),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                    padding:
                                                        Dimens.edgeInsets16,
                                                    child: Text(
                                                      'filterBy'.tr,
                                                      style:
                                                          Styles.medium18Black,
                                                    ),
                                                  ),
                                                  CustomTile(
                                                    title: 'All',
                                                    isCheck: _controller
                                                            .activeFundSortedCheck ==
                                                        0,
                                                    onTap: () {
                                                      setState(() {
                                                        _controller
                                                            .activeFundSortWith(
                                                                0);
                                                      });
                                                    },
                                                  ),
                                                  Dimens.boxHeight10,
                                                  CustomTile(
                                                    title:
                                                        'Suggested by expert',
                                                    isCheck: _controller
                                                            .activeFundSortedCheck ==
                                                        1,
                                                    onTap: () {
                                                      setState(() {
                                                        _controller
                                                            .activeFundSortWith(
                                                                1);
                                                      });
                                                    },
                                                  ),
                                                  Dimens.boxHeight10,
                                                  CustomTile(
                                                    title: 'External Funds',
                                                    isCheck: _controller
                                                            .activeFundSortedCheck ==
                                                        2,
                                                    onTap: () {
                                                      setState(() {
                                                        _controller
                                                            .activeFundSortWith(
                                                                2);
                                                      });
                                                    },
                                                  ),
                                                  Dimens.boxHeight10,
                                                  CustomTile(
                                                    title: 'SIPs',
                                                    isCheck: _controller
                                                            .activeFundSortedCheck ==
                                                        3,
                                                    onTap: () {
                                                      setState(() {
                                                        _controller
                                                            .activeFundSortWith(
                                                                3);
                                                      });
                                                    },
                                                  ),
                                                  Dimens.boxHeight10,
                                                  CustomTile(
                                                    title: 'Lumpsum',
                                                    isCheck: _controller
                                                            .activeFundSortedCheck ==
                                                        4,
                                                    onTap: () {
                                                      setState(() {
                                                        _controller
                                                            .activeFundSortWith(
                                                                4);
                                                      });
                                                    },
                                                  ),
                                                  Dimens.boxHeight10,
                                                ],
                                              )
                                            ],
                                          ),
                                        ),
                                      )),
                            );
                          },
                          child: Row(
                            children: [
                              Text(
                                'all'.tr,
                                style: Styles.medium12Primary,
                              ),
                              Icon(
                                Icons.arrow_drop_down,
                                size: Dimens.eighteen,
                                color: ColorsValue.primaryColor,
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Dimens.boxHeight15,
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Padding(
                            padding: Dimens.edgeInsets10_0_10_0,
                            child: SmallFundCard(
                              title: 'DSP Tax Saver Direct Plan Growth',
                              bankIcon: AssetConstants.hdfcLogo,
                              percentage: '4000',
                              tagsList: ['SIP', 'External'],
                              currency: true,
                              returns: '3000',
                              onTap: () {
                                Get.to(const FundTransaction());
                              },
                            ),
                          ),
                          Dimens.boxHeight20,
                          Padding(
                            padding: Dimens.edgeInsets10_0_10_0,
                            child: SmallFundCard(
                              title: 'DSP Tax Saver Direct Plan Growth',
                              bankIcon: AssetConstants.hdfcLogo,
                              percentage: '4000',
                              tagsList: ['SIP', 'External'],
                              showSuggested: true,
                              currency: true,
                              returns: '3000',
                              onTap: () {
                                Get.to(const FundTransaction());
                              },
                            ),
                          ),
                          Dimens.boxHeight20,
                          Padding(
                            padding: Dimens.edgeInsets10_0_10_0,
                            child: SmallFundCard(
                              title: 'DSP Tax Saver Direct Plan Growth',
                              bankIcon: AssetConstants.hdfcLogo,
                              percentage: '4000',
                              tagsList: ['SIP'],
                              showSuggested: true,
                              currency: true,
                              returns: '3000',
                              onTap: () {
                                Get.to(const FundTransaction());
                              },
                            ),
                          ),
                          Dimens.boxHeight20,
                          Padding(
                            padding: Dimens.edgeInsets10_0_10_0,
                            child: SmallFundCard(
                              title: 'DSP Tax Saver Direct Plan Growth',
                              bankIcon: AssetConstants.hdfcLogo,
                              percentage: '4000',
                              tagsList: ['SIP', 'External'],
                              showSuggested: true,
                              currency: true,
                              returns: '3000',
                              onTap: () {
                                Get.to(const FundTransaction());
                              },
                            ),
                          ),
                          Dimens.boxHeight20,
                          Padding(
                            padding: Dimens.edgeInsets10_0_10_0,
                            child: SmallFundCard(
                              title: 'DSP Tax Saver Direct Plan Growth',
                              bankIcon: AssetConstants.hdfcLogo,
                              percentage: '4000',
                              tagsList: ['SIP', 'External'],
                              showSuggested: true,
                              currency: true,
                              returns: '3000',
                              onTap: () {
                                Get.to(const FundTransaction());
                              },
                            ),
                          ),
                          Dimens.boxHeight20,
                          Padding(
                            padding: Dimens.edgeInsets10_0_10_0,
                            child: SmallFundCard(
                              title: 'DSP Tax Saver Direct Plan Growth',
                              bankIcon: AssetConstants.hdfcLogo,
                              percentage: '4000',
                              tagsList: ['SIP', 'External'],
                              showSuggested: true,
                              currency: true,
                              returns: '3000',
                              onTap: () {
                                Get.to(const FundTransaction());
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ));
}

class CustomTile extends StatelessWidget {
  const CustomTile({
    Key? key,
    this.title,
    this.isCheck = false,
    this.style,
    this.onTap,
  }) : super(key: key);

  final String? title;
  final TextStyle? style;
  final bool? isCheck;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) => GestureDetector(
        onTap: () {
          onTap!();
        },
        child: Container(
          color: isCheck! ? ColorsValue.lightBgContainerColor : Colors.white,
          padding: Dimens.edgeInsets0_10_0_10,
          child: Padding(
            padding: Dimens.edgeInsets16_0_16_0,
            child: GestureDetector(
              onTap: () {
                onTap!();
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    '$title',
                    style:
                        isCheck! ? Styles.medium14Black : Styles.regular14Grey,
                  ),
                  if (isCheck!)
                    Container(
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: ColorsValue.primaryColor,
                      ),
                      padding: Dimens.edgeInsets2,
                      child: Icon(
                        Icons.check,
                        size: Dimens.sixTeen,
                        color: Colors.white,
                      ),
                    ),
                ],
              ),
            ),
          ),
        ),
      );
}
