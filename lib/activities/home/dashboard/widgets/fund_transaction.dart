import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class FundTransaction extends StatelessWidget {
  const FundTransaction({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        appBar: CustomAppBar(
          title: 'fundTransaction'.tr,
        ),
        body: SafeArea(
          child: Padding(
            padding: Dimens.edgeInsets16,
            child: Column(
              children: [
                Expanded(
                  child: ListView(
                    children: [
                      DefaultContainer(
                        child: Column(
                          children: [
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  height: Dimens.fourty,
                                  width: Dimens.fourty,
                                  decoration: const BoxDecoration(
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(12),
                                    ),
                                  ),
                                  child: Image.asset(
                                    AssetConstants.hdfcLogo,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                                Dimens.boxWidth15,
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'DSP Tax Saver Direct Plan Growth',
                                        style: Styles.medium16Black,
                                      ),
                                      Dimens.boxHeight10,
                                    ],
                                  ),
                                ),
                                Dimens.boxWidth20,
                                Column(
                                  children: [
                                    Dimens.boxHeight10,
                                    GestureDetector(
                                      onTap: () {},
                                      child: SvgPicture.asset(
                                        AssetConstants.rightArrow,
                                        height: Dimens.eighteen,
                                        width: Dimens.eighteen,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            const Divider(),
                            Dimens.boxHeight10,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    SizedBox(
                                      width: Dimens.percentWidth(0.22),
                                      child: Text(
                                        'currentValue'.tr,
                                        style: Styles.grey12,
                                      ),
                                    ),
                                    Dimens.boxWidth10,
                                    Text(
                                      '2,00,000',
                                      style: Styles.medium12Grey,
                                    ),
                                  ],
                                ),
                                KeyValue(
                                  title: 'Folio No.'.tr,
                                  value: '36278452894',
                                ),
                              ],
                            ),
                            Dimens.boxHeight10,
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    SizedBox(
                                      width: Dimens.percentWidth(0.22),
                                      child: Text(
                                        'Avg NAV'.tr,
                                        style: Styles.grey12,
                                      ),
                                    ),
                                    Dimens.boxWidth10,
                                    Text(
                                      '273.09',
                                      style: Styles.medium12Grey,
                                    ),
                                  ],
                                ),
                                KeyValue(
                                  title: 'Balance Unit'.tr,
                                  value: '273.09',
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      Dimens.boxHeight20,
                      const Divider(),
                      Dimens.boxHeight10,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ColumnTile(
                            title: 'invested'.tr,
                            titleStyle: Styles.grey12,
                            value: '\u{20b9} 40,000',
                            valueStyle: Styles.bold14Grey,
                          ),
                          ColumnTile(
                            title: 'currentValue'.tr,
                            titleStyle: Styles.grey12,
                            value: '\u{20b9} 40,000',
                            valueStyle: Styles.bold14Grey,
                          ),
                          ColumnTile(
                            title: 'totalreturns'.tr,
                            titleStyle: Styles.grey12,
                            value: '\u{20b9} 40,000',
                            color: ColorsValue.shineGreenColor,
                            valueStyle:
                                Styles.bold14Grey.copyWith(color: ColorsValue.shineGreenColor),
                          ),
                        ],
                      ),
                      Dimens.boxHeight10,
                      const Divider(
                        color: ColorsValue.blackColor,
                      ),
                      Theme(
                        data: Theme.of(context)
                            .copyWith(dividerColor: Colors.transparent),
                        child: ExpansionTile(
                          tilePadding: Dimens.edgeInsets0,
                          title: Text(
                            'transactionHistory'.tr,
                            style: Styles.bold16Black,
                          ),
                          initiallyExpanded: true,
                          childrenPadding: Dimens.edgeInsets0_10_0_0,
                          children: [
                            Container(
                              padding: Dimens.edgeInsets0_0_0_15,
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: ColorsValue.greyColor
                                              .withOpacity(0.2)))),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'invest'.tr,
                                        style: Styles.medium12Black,
                                      ),
                                      Dimens.boxHeight10,
                                      Text(
                                        '01 Jul 2021',
                                        style: Styles.medium12Black,
                                      ),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      RichText(
                                        text: TextSpan(
                                            text: 'Units  ',
                                            style: Styles.medium12Black,
                                            children: [
                                              TextSpan(
                                                  text: '15.183',
                                                  style: Styles.bold14Grey),
                                            ]),
                                      ),
                                      Dimens.boxHeight10,
                                      RichText(
                                        text: TextSpan(
                                            text: 'NAV   ',
                                            style: Styles.medium12Black,
                                            children: [
                                              TextSpan(
                                                  text: '329.3021',
                                                  style: Styles.bold14Grey),
                                            ]),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '\u{20b9} 47,000',
                                        style: Styles.bold14Grey,
                                      ),
                                      Dimens.boxHeight10,
                                      Text(
                                        'Completed',
                                        style: Styles.medium12Black,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Dimens.boxHeight20,
                            Container(
                              padding: Dimens.edgeInsets0_0_0_15,
                              decoration: BoxDecoration(
                                  border: Border(
                                      bottom: BorderSide(
                                          color: ColorsValue.greyColor
                                              .withOpacity(0.2)))),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'invest'.tr,
                                        style: Styles.medium12Black,
                                      ),
                                      Dimens.boxHeight10,
                                      Text(
                                        '01 Jul 2021',
                                        style: Styles.medium12Black,
                                      ),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      RichText(
                                        text: TextSpan(
                                            text: 'Units  ',
                                            style: Styles.medium12Black,
                                            children: [
                                              TextSpan(
                                                  text: '15.183',
                                                  style: Styles.bold14Grey),
                                            ]),
                                      ),
                                      Dimens.boxHeight10,
                                      RichText(
                                        text: TextSpan(
                                            text: 'NAV   ',
                                            style: Styles.medium12Black,
                                            children: [
                                              TextSpan(
                                                  text: '329.3021',
                                                  style: Styles.bold14Grey),
                                            ]),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        '\u{20b9} 47,000',
                                        style: Styles.bold14Grey,
                                      ),
                                      Dimens.boxHeight10,
                                      Text(
                                        'Completed',
                                        style: Styles.medium12Black,
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Dimens.boxHeight20,
                    ],
                  ),
                ),
                Row(
                  children: [
                    Expanded(
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: Dimens.edgeInsets5,
                          child: SizedBox(
                            width: Dimens.percentWidth(1),
                            height: Dimens.percentHeight(.07),
                            child: ElevatedButton(
                              style: Styles.elevatedButtonTheme.style!.copyWith(
                                elevation: MaterialStateProperty.all(0),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(Dimens.fifty),
                                        side: BorderSide(
                                            width: Dimens.two,
                                            color: ColorsValue.primaryColor))),
                                backgroundColor:
                                    MaterialStateProperty.resolveWith<Color>(
                                  (Set<MaterialState> states) => (0 == 0)
                                      ? ColorsValue.backgroundColor
                                      : ColorsValue.primaryColor
                                          .withOpacity(.5),
                                ),
                                textStyle: MaterialStateProperty.all(
                                  Styles.bold16White,
                                ),
                              ),
                              onPressed: () {},
                              child: Text(
                                'Redeem'.tr,
                                style: Styles.bold16Primary,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Padding(
                          padding: Dimens.edgeInsets5,
                          child: SizedBox(
                            width: Dimens.percentWidth(1),
                            height: Dimens.percentHeight(.07),
                            child: ElevatedButton(
                              style: Styles.elevatedButtonTheme.style!.copyWith(
                                elevation: MaterialStateProperty.all(0),
                                shape: MaterialStateProperty.all<
                                        RoundedRectangleBorder>(
                                    RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(Dimens.fifty),
                                        side: BorderSide(
                                            width: Dimens.two,
                                            color: ColorsValue.primaryColor))),
                                backgroundColor:
                                    MaterialStateProperty.resolveWith<Color>(
                                  (Set<MaterialState> states) => (0 == 0)
                                      ? ColorsValue.primaryColor
                                      : ColorsValue.primaryColor
                                          .withOpacity(.5),
                                ),
                                textStyle: MaterialStateProperty.all(
                                  Styles.bold16White,
                                ),
                              ),
                              onPressed: () {},
                              child: Text(
                                'Invest More'.tr,
                                style: Styles.bold16White,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
}

class KeyValue extends StatelessWidget {
  const KeyValue({
    Key? key,
    this.title,
    this.value,
  }) : super(key: key);

  final String? title;
  final String? value;

  @override
  Widget build(BuildContext context) => Row(
        children: [
          Text(
            '$title ',
            style: Styles.grey12,
          ),
          Dimens.boxWidth10,
          Text(
            '$value',
            style: Styles.medium12Grey,
          ),
        ],
      );
}
