import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:investkarr/widgets/app_bar.dart';

class ForwordConsolidatedStatement extends StatelessWidget {
  const ForwordConsolidatedStatement({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => GetBuilder<HomeController>(
        builder: (_controller) => Scaffold(
          appBar: CustomAppBar(
            title: 'forwordConsolidatedStaterment'.tr,
          ),
          body: SafeArea(
            child: Padding(
              padding: Dimens.edgeInsets16,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: ListView(
                      children: [
                        Text(
                          'forwardTheConsolidatedStatement'.tr,
                          style: Styles.regular14Grey,
                        ),
                        Dimens.boxHeight20,
                        DottedBorder(
                          radius: Radius.circular(Dimens.twelve),
                          borderType: BorderType.RRect,
                          padding: Dimens.edgeInsets16,
                          color: ColorsValue.primaryColor,
                          strokeWidth: 1,
                          dashPattern: [5],
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'portfolio@mc.com',
                                style: Styles.regular14Grey,
                              ),
                              Row(
                                children: [
                                  SvgPicture.asset(AssetConstants.copyIc),
                                  Dimens.boxWidth8,
                                  Text(
                                    'Copy',
                                    style: Styles.medium14Grey.copyWith(
                                        color: ColorsValue.primaryColor),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Dimens.boxHeight15,
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              'didntGetEmail'.tr,
                              style: Styles.regular12Grey,
                            ),
                            Text(
                              'viewSampleEmail'.tr,
                              style: Styles.regular12Grey
                                  .copyWith(color: ColorsValue.primaryColor),
                            ),
                          ],
                        ),
                        Dimens.boxHeight25,
                        const Divider(),
                        Dimens.boxHeight25,
                        Text(
                          'confirmYourEmailLinkedToYourMutualFundInvestments'
                              .tr,
                          style: Styles.regular14Grey,
                        ),
                        Dimens.boxHeight20,
                        Text(
                          'yourusername@domain.com',
                          style: Styles.regular16Grey
                              .copyWith(color: ColorsValue.blackColor),
                        ),
                        Dimens.boxHeight5,
                        const Divider(
                          color: ColorsValue.greyColor,
                        ),
                        Dimens.boxHeight40,
                        Text(
                          'enterThePasswordForYourConsolidatedAccountStatement'
                              .tr,
                          style: Styles.regular14Grey,
                        ),
                        Dimens.boxHeight15,
                        SizedBox(
                          height: Dimens.fourtyThree,
                          child: TextFormField(
                            controller: _controller.consolidatePassword,
                            onChanged: (v) {
                              _controller.update();
                            },
                            obscureText:
                                !_controller.isConsolidatePasswordShown,
                            obscuringCharacter: '*',
                            decoration: InputDecoration(
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(
                                  Dimens.twelve,
                                ),
                                borderSide: BorderSide(
                                  color:
                                      ColorsValue.primaryColor.withOpacity(0.4),
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(
                                  Dimens.twelve,
                                ),
                                borderSide: BorderSide(
                                  color:
                                      ColorsValue.primaryColor.withOpacity(0.4),
                                ),
                              ),
                              filled: true,
                              fillColor: Colors.white,
                              hintStyle: Styles.textFieldHintColor16.copyWith(
                                color: ColorsValue.greyColor.withOpacity(0.5),
                              ),
                              contentPadding: Dimens.edgeInsets10,
                            ),
                          ),
                        ),
                        Dimens.boxHeight10,
                        Row(
                          children: [
                            SizedBox(
                              width: Dimens.twentyFive,
                              height: Dimens.twentyFive,
                              child: Checkbox(
                                value: _controller.isConsolidatePasswordShown,
                                shape: const CircleBorder(),
                                side: const BorderSide(
                                    color: ColorsValue.primaryColor),
                                checkColor: Colors.white,
                                activeColor: Colors.greenAccent,
                                onChanged: (value) {
                                  _controller.isConsolidatePasswordShown =
                                      value!;
                                  _controller.update();
                                },
                              ),
                            ),
                            Dimens.boxWidth10,
                            Text(
                              'Show Password',
                              style: Styles.regular12Grey,
                            ),
                            Dimens.boxWidth10,
                          ],
                        ),
                      ],
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) => (_controller
                                    .consolidatePassword.text.isNotEmpty)
                                ? ColorsValue.primaryColor
                                : ColorsValue.primaryColor.withOpacity(.5),
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16White,
                          ),
                        ),
                        onPressed: () {
                          if (_controller.consolidatePassword.text.isNotEmpty) {
                            _controller.isInvestYourselfNewUser = false;
                            _controller.update();
                            Get.back<void>();
                            Get.back<void>();
                          } else {}
                        },
                        child: Text(
                          'Submit'.tr,
                          style: Styles.bold16White,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
}
