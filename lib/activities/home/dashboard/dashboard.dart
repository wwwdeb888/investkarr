import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard>
    with SingleTickerProviderStateMixin {
  TabController? tabController;

  @override
  void initState() {
    tabController = TabController(vsync: this, length: 4);
    super.initState();
  }

  @override
  Widget build(BuildContext context) => DefaultTabController(
        length: 4,
        child: GetBuilder<HomeController>(
            builder: (_controller) => SizedBox(
                  width: Get.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: Dimens.edgeInsets16_20_0_20,
                        child: Text(
                          'dashboard'.tr,
                          style: Styles.boldBlack22,
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                  color: Colors.grey.withOpacity(0.2),
                                  width: 1)),
                        ),
                        child: TabBar(
                          padding: Dimens.edgeInsets0_10_0_0,
                          isScrollable: true,
                          indicatorSize: TabBarIndicatorSize.label,
                          unselectedLabelColor: ColorsValue.greyColor,
                          unselectedLabelStyle: Styles.medium16Black,
                          indicatorColor: ColorsValue.primaryColor,
                          labelColor: ColorsValue.primaryColor,
                          labelStyle: Styles.medium16Black,
                          controller: tabController,
                          tabs: [
                            Column(
                              children: [
                                const Text('Overview'),
                                Dimens.boxHeight10,
                              ],
                            ),
                            Column(
                              children: [
                                const Text('Invest Yourself'),
                                Dimens.boxHeight10,
                              ],
                            ),
                            Column(
                              children: [
                                Text('wealthCreation&more'.tr),
                                Dimens.boxHeight10,
                              ],
                            ),
                            Column(
                              children: [
                                Text('goalBasedInvesting'.tr),
                                Dimens.boxHeight10,
                              ],
                            ),
                          ],
                        ),
                      ),
                      Dimens.boxHeight20,
                      Expanded(
                        child: TabBarView(
                          controller: tabController,
                          children: [
                            const Overview(),
                            const DashboardInvestYourself(),
                            const WealthCreationAndMore(),
                            const GoalBasedInvesting(),
                          ],
                        ),
                      )
                    ],
                  ),
                )),
      );
}
