import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class SignInBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(SignInController.new);
  }
}