import 'package:flutter/material.dart';
import 'package:investkarr/lib.dart';

class SignInView extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: Stack(
          children: [
            Padding(
              padding: Dimens.edgeInsets20,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Column(
                      children: [
                        Container(
                          width: Dimens.thirtyTwo,
                          height: Dimens.thirtyTwo,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(Dimens.fifty),
                            color: ColorsValue.blackColor,
                          ),
                          child: Center(
                            child: Text(
                              'M',
                              style: Styles.boldWhite16,
                            ),
                          ),
                        ),
                        Dimens.boxHeight10,
                        Text(
                          StringConstants.marwarCapital,
                          style: Styles.boldBlack20,
                        ),
                      ],
                    ),
                  ),
                  Dimens.boxHeight60,
                  SizedBox(
                    width: Dimens.percentWidth(1),
                    height: Dimens.percentHeight(.07),
                    child: ElevatedButton(
                      style: Styles.elevatedButtonTheme.style!.copyWith(
                        backgroundColor:
                            MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) =>
                              ColorsValue.primaryColor,
                        ),
                        textStyle: MaterialStateProperty.all(
                          Styles.bold16White,
                        ),
                      ),
                      onPressed: RouteManagement.goToSignInWithEmail,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Image.asset(
                            AssetConstants.mail,
                            width: Dimens.twenty,
                            height: Dimens.twenty,
                          ),
                          Text(
                            StringConstants.loginWithEmail,
                            style: Styles.bold16White,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Dimens.boxHeight16,
                  SizedBox(
                    width: Dimens.percentWidth(1),
                    height: Dimens.percentHeight(.07),
                    child: ElevatedButton(
                      style: Styles.elevatedButtonTheme.style!.copyWith(
                        backgroundColor:
                            MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) => Colors.white,
                        ),
                        textStyle: MaterialStateProperty.all(
                          Styles.bold16Black,
                        ),
                      ),
                      onPressed: RouteManagement.goToSignInEnterAppPin,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Image.asset(
                            AssetConstants.google,
                            width: Dimens.twenty,
                            height: Dimens.twenty,
                          ),
                          Text(
                            StringConstants.loginWithGoogle,
                            style: Styles.bold16Black,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Dimens.boxHeight16,
                  SizedBox(
                    width: Dimens.percentWidth(1),
                    height: Dimens.percentHeight(.07),
                    child: ElevatedButton(
                      style: Styles.elevatedButtonTheme.style!.copyWith(
                        backgroundColor:
                            MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) => Colors.black,
                        ),
                        textStyle: MaterialStateProperty.all(
                          Styles.bold16White,
                        ),
                      ),
                      onPressed: RouteManagement.goToSignInEnterAppPin,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Image.asset(
                            AssetConstants.apple,
                            width: Dimens.twenty,
                            height: Dimens.twenty,
                          ),
                          Text(
                            StringConstants.loginWithApple,
                            style: Styles.bold16White,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Dimens.boxHeight30,
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        width: Dimens.percentWidth(.4),
                        child: const Divider(
                          thickness: 1,
                          color: Color(0xffB8B8B8),
                        ),
                      ),
                      Text(
                        StringConstants.or,
                        style: Styles.grey12,
                      ),
                      SizedBox(
                        width: Dimens.percentWidth(.4),
                        child: const Divider(
                          thickness: 1,
                          color: Color(0xffB8B8B8),
                        ),
                      ),
                    ],
                  ),
                  Dimens.boxHeight30,
                  SizedBox(
                    width: Dimens.percentWidth(1),
                    height: Dimens.percentHeight(.07),
                    child: ElevatedButton(
                      style: Styles.elevatedButtonTheme.style!.copyWith(
                        backgroundColor:
                            MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) =>
                              ColorsValue.primaryColor,
                        ),
                        textStyle: MaterialStateProperty.all(
                          Styles.bold16White,
                        ),
                      ),
                      onPressed: RouteManagement.goToSignInWithPhone,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Image.asset(
                            AssetConstants.phone,
                            width: Dimens.twenty,
                            height: Dimens.twenty,
                          ),
                          Text(
                            StringConstants.loginWithPhone,
                            style: Styles.bold16White,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: Dimens.edgeInsets30,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      StringConstants.noAccount,
                      style: Styles.bold12Black,
                    ),
                    GestureDetector(
                      onTap: RouteManagement.goToOffSignUp,
                      child: Text(
                        StringConstants.signUp,
                        style: Styles.bold12Primary,
                      ),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      );
}
