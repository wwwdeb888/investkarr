import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:investkarr/lib.dart';
import 'package:pinput/pin_put/pin_put.dart';

class SignInEnterAppPin extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<SignInController>(
          builder: (_controller) => ColorfulSafeArea(
            color: ColorsValue.backgroundColor,
            child: Stack(
              children: [
                Column(
                  children: [
                    Dimens.boxHeight5,
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            SizedBox(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              child: IconButton(
                                constraints: const BoxConstraints(),
                                padding: EdgeInsets.zero,
                                onPressed: () {
                                  Get.back<dynamic>();
                                },
                                icon: const Icon(
                                  Icons.arrow_back_ios,
                                  color: ColorsValue.primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                        Image.asset(
                          AssetConstants.splashLogo1,
                          width: Dimens.thirtyTwo,
                          height: Dimens.thirtyTwo,
                        ),
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            Container(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              color: Colors.transparent,
                            ),
                          ],
                        ),
                      ],
                    ),
                    Dimens.boxHeight10,
                  ],
                ),
                Padding(
                  padding: Dimens.edgeInsets20,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Dimens.boxHeight80,
                      Container(
                        width: Dimens.fourtyEight,
                        height: Dimens.fourtyEight,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(Dimens.fifty),
                          color: Colors.white,
                        ),
                        child: Image.asset(
                          AssetConstants.lock,
                          scale: 3,
                        ),
                      ),
                      Dimens.boxHeight25,
                      Text(
                        StringConstants.enterAppPin,
                        style: Styles.boldBlack15,
                      ),
                      Dimens.boxHeight25,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: Dimens.percentWidth(.5),
                            child: PinPut(
                              autofocus: true,
                              // eachFieldMargin: Dimens.edgeInsets0_0_20_0,
                              mainAxisSize: MainAxisSize.max,
                              fieldsCount: 4,
                              controller: _controller.pinPutController3,
                              enabled: true,
                              onSubmit: (_) {
                                // controller.onVerifiy();
                              },
                              onChanged: _controller.onOTPChanged3,
                              eachFieldHeight: Dimens.fourty + Dimens.fifteen,
                              textStyle: Styles.boldBlack22,
                              submittedFieldDecoration: Styles.pinPutDecoration,
                              selectedFieldDecoration:
                                  Styles.enabledPinPutDecoration,
                              followingFieldDecoration: Styles.pinPutDecoration,
                            ),
                          ),
                        ],
                      ),
                      Dimens.boxHeight20,
                      GestureDetector(
                        onTap: () async {
                          await GetStorage('appData')
                              .write('showResetPassword', false);
                          RouteManagement.goToForgotPinOrPasswordPAN();
                        },
                        child: Text(
                          StringConstants.forgotAppPin,
                          style: Styles.black14Underline,
                        ),
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: Dimens.edgeInsets20,
                    child: SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) =>
                                _controller.isAppPinValid
                                    ? ColorsValue.primaryColor
                                    : ColorsValue.primaryColor.withOpacity(.5),
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16White,
                          ),
                        ),
                        onPressed: _controller.isAppPinValid
                            ? _controller.verifyPin
                            : null,
                        child: Text(
                          StringConstants.confirm,
                          style: Styles.bold16White,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
