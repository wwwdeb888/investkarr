import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class LoginOrRegisterView extends StatefulWidget {
  @override
  State<LoginOrRegisterView> createState() => _LoginOrRegisterViewState();
}

class _LoginOrRegisterViewState extends State<LoginOrRegisterView> {
  int currentPage = 0;

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<LoginOrRegisterController>(
          builder: (_controller) => Padding(
            padding: Dimens.edgeInsets20,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset(
                  AssetConstants.splashLogo2,
                  width: Dimens.percentWidth(.25),
                ),
                SizedBox(
                  width: Dimens.percentWidth(1),
                  height: Dimens.percentHeight(.5),
                  child: PageView.builder(
                      itemCount: _controller.pages.length,
                      onPageChanged: (value) {
                        setState(() {
                          currentPage = value;
                        });
                      },
                      controller: _controller.controller,
                      itemBuilder: (context, index) =>
                          _controller.pages[index % _controller.pages.length]),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: List.generate(
                    _controller.pages.length,
                    (index) => buildDot(index: index),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: Dimens.percentWidth(.4),
                      child: FormSubmitWidget(
                        opacity: 1,
                        backgroundColor: Colors.white,
                        padding: Dimens.edgeInsets15,
                        text: StringConstants.login,
                        textStyle: Styles.bold16Primary,
                        onTap: RouteManagement.goToSignIn,
                      ),
                    ),
                    SizedBox(
                      width: Dimens.percentWidth(.4),
                      child: FormSubmitWidget(
                        opacity: 1,
                        backgroundColor: ColorsValue.primaryColor,
                        padding: Dimens.edgeInsets15,
                        text: StringConstants.signUp,
                        textStyle: Styles.bold16White,
                        onTap: RouteManagement.goToSignUp,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
  AnimatedContainer buildDot({int? index}) => AnimatedContainer(
        duration: const Duration(milliseconds: 450),
        margin: const EdgeInsets.only(right: 5),
        height: 6,
        width: currentPage == index ? 20 : 6,
        decoration: BoxDecoration(
          color: currentPage == index
              ? ColorsValue.primaryColor
              : const Color(0xFFD8D8D8),
          borderRadius: BorderRadius.circular(3),
        ),
      );
}
