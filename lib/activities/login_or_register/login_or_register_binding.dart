import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class LoginOrRegisterBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(LoginOrRegisterController.new);
  }
}
