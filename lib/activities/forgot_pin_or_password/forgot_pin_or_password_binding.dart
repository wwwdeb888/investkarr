import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class ForgotPinOrPasswordBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(ForgotPinOrPasswordController.new);
  }
}
