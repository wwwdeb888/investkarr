import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:investkarr/lib.dart';

class ForgotPinOrPasswordDateOfBirth extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<ForgotPinOrPasswordController>(
          builder: (_controller) => ColorfulSafeArea(
            color: ColorsValue.backgroundColor,
            child: Stack(
              children: [
                Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            SizedBox(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              child: IconButton(
                                constraints: const BoxConstraints(),
                                padding: EdgeInsets.zero,
                                onPressed: () {
                                  Get.back<dynamic>();
                                },
                                icon: const Icon(
                                  Icons.arrow_back_ios,
                                  color: ColorsValue.primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                        CircleAvatar(
                          radius: Dimens.seventeen,
                          backgroundColor: Colors.white,
                          child: Image.asset(
                            AssetConstants.splashLogo1,
                            width: Dimens.seventeen,
                            height: Dimens.seventeen,
                          ),
                        ),
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            Container(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              color: Colors.transparent,
                            ),
                          ],
                        ),
                      ],
                    ),
                    Dimens.boxHeight10,
                  ],
                ),
                Padding(
                  padding: Dimens.edgeInsets20,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Dimens.boxHeight80,
                      Text(
                        StringConstants.accountRecovery,
                        style: Styles.bold24Black,
                      ),
                      Dimens.boxHeight15,
                      Text(
                        StringConstants.helpsToShow,
                        style: Styles.textFieldHintColor14,
                        textAlign: TextAlign.center,
                      ),
                      Dimens.boxHeight50,
                      _controller.isPanShowing
                          ? FormFieldWidget(
                              elevation: 0,
                              autoFocus: true,
                              isFilled: true,
                              maxLength: 10,
                              textCapitalization: TextCapitalization.characters,
                              textInputType: TextInputType.emailAddress,
                              borderColor: ColorsValue.formFieldBorderColor,
                              fillColor: ColorsValue.backgroundColor,
                              hintText: StringConstants.panNumber,
                              hintStyle: Styles.textFieldHintColor16,
                              contentPadding: Dimens.edgeInsets10,
                              errorStyle: Styles.red13,
                              onChange: (String v) {
                                _controller.checkIfPanIsValid(v);
                              },
                            )
                          : FormFieldWidget(
                              elevation: 0,
                              autoFocus: true,
                              isFilled: true,
                              maxLength: 10,
                              textCapitalization: TextCapitalization.characters,
                              textInputType: TextInputType.emailAddress,
                              borderColor: ColorsValue.formFieldBorderColor,
                              fillColor: ColorsValue.backgroundColor,
                              hintText: StringConstants.dateOfBirth,
                              hintStyle: Styles.textFieldHintColor16,
                              contentPadding: Dimens.edgeInsets10,
                              errorStyle: Styles.red13,
                              isReadOnly: true,
                              onTap: () {
                                Get.bottomSheet<dynamic>(
                                  Container(
                                    color: Colors.white,
                                    height: Dimens.percentHeight(.35),
                                    child: Padding(
                                      padding: Dimens.edgeInsets10,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          GestureDetector(
                                            onTap: () {
                                              Get.back<dynamic>();
                                            },
                                            child: Text(
                                              StringConstants.done,
                                              style: Styles.primaryBold13,
                                            ),
                                          ),
                                          Container(
                                            height: Dimens.percentHeight(.3),
                                            color: Colors.white,
                                            child: CupertinoDatePicker(
                                              mode:
                                                  CupertinoDatePickerMode.date,
                                              onDateTimeChanged: (value) {
                                                if (value
                                                        .difference(
                                                            DateTime.now())
                                                        .inDays <
                                                    -9131) {
                                                  final formatter =
                                                      DateFormat('dd-MM-yyyy');
                                                  final formatted =
                                                      formatter.format(value);
                                                  _controller.dob.text =
                                                      formatted;
                                                  _controller
                                                      .checkIfDateIsValid(
                                                          formatted);
                                                  _controller.update();
                                                }
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                  isDismissible: false,
                                );
                              },
                              suffixIcon: const Icon(
                                Icons.calendar_today_outlined,
                                color: ColorsValue.formFieldBorderColor,
                              ),
                              textEditingController: _controller.dob,
                              onChange: (String value) {},
                            ),
                      Dimens.boxHeight10,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          GestureDetector(
                            onTap: () {
                              _controller.showPanAndDob();
                            },
                            child: Text(
                              !_controller.isPanShowing
                                  ? StringConstants.usePAN
                                  : StringConstants.useDOB,
                              style: Styles.boldBlack15,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: Dimens.edgeInsets20,
                    child: SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) =>
                                _controller.isDOBValid
                                    ? ColorsValue.primaryColor
                                    : ColorsValue.primaryColor.withOpacity(.5),
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16White,
                          ),
                        ),
                        onPressed: () {
                          if (_controller.isDOBValid &&
                              _controller.showResetPassword == true) {
                            RouteManagement.goToForgotPinOrPasswordEmailSent();
                          } else {}
                        },
                        child: Text(
                          StringConstants.sendResetRequest,
                          style: Styles.bold16White,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
