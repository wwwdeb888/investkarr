import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class ForgotPinOrPasswordPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<ForgotPinOrPasswordController>(
          builder: (_controller) => ColorfulSafeArea(
            color: ColorsValue.backgroundColor,
            child: Stack(
              children: [
                Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            SizedBox(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              child: IconButton(
                                constraints: const BoxConstraints(),
                                padding: EdgeInsets.zero,
                                onPressed: () {
                                  Get.back<dynamic>();
                                },
                                icon: const Icon(
                                  Icons.arrow_back_ios,
                                  color: ColorsValue.primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                        CircleAvatar(
                          radius: Dimens.seventeen,
                          backgroundColor: Colors.white,
                          child: Image.asset(
                            AssetConstants.splashLogo1,
                            width: Dimens.seventeen,
                            height: Dimens.seventeen,
                          ),
                        ),
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            Container(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              color: Colors.transparent,
                            ),
                          ],
                        ),
                      ],
                    ),
                    Dimens.boxHeight10,
                  ],
                ),
                Padding(
                  padding: Dimens.edgeInsets20,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Dimens.boxHeight80,
                      Text(
                        StringConstants.accountRecovery,
                        style: Styles.bold24Black,
                      ),
                      Dimens.boxHeight15,
                      SizedBox(
                        width: Dimens.percentWidth(.8),
                        child: Text(
                          StringConstants.helpsToShow,
                          style: Styles.textFieldHintColor14,
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Dimens.boxHeight50,
                      FormFieldWidget(
                        elevation: 0,
                        autoFocus: true,
                        isFilled: true,
                        textInputType: TextInputType.emailAddress,
                        borderColor: ColorsValue.formFieldBorderColor,
                        fillColor: ColorsValue.backgroundColor,
                        hintText: StringConstants.enterEmail,
                        hintStyle: Styles.textFieldHintColor16,
                        contentPadding: Dimens.edgeInsets10,
                        errorText: _controller.emailErrorText,
                        errorStyle: Styles.red13,
                        onChange: (String v) {
                          _controller.checkIfEmailIsValid(v);
                        },
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: Dimens.edgeInsets20,
                    child: SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) =>
                                _controller.isEmailValid
                                    ? ColorsValue.primaryColor
                                    : ColorsValue.primaryColor.withOpacity(.5),
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16White,
                          ),
                        ),
                        onPressed: () {
                          // _controller.isEmailValid
                          //   ? _controller.forgotPasswordOfMail
                          //   : null
                          RouteManagement.goToForgotPinOrPasswordDateOfBirth();
                        },
                        child: Text(
                          StringConstants.next,
                          style: Styles.bold16White,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
