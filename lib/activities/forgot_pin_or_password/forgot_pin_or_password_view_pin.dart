import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class ForgotPinOrPasswordViewPAN extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<ForgotPinOrPasswordController>(
          builder: (_controller) => ColorfulSafeArea(
            color: ColorsValue.backgroundColor,
            child: Stack(
              children: [
                Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            SizedBox(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              child: IconButton(
                                constraints: const BoxConstraints(),
                                padding: EdgeInsets.zero,
                                onPressed: () {
                                  Get.back<dynamic>();
                                },
                                icon: const Icon(
                                  Icons.arrow_back_ios,
                                  color: ColorsValue.primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                        CircleAvatar(
                          radius: Dimens.seventeen,
                          backgroundColor: Colors.white,
                          child: Image.asset(
                            AssetConstants.splashLogo1,
                            width: Dimens.seventeen,
                            height: Dimens.seventeen,
                          ),
                        ),
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            Container(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              color: Colors.transparent,
                            ),
                          ],
                        ),
                      ],
                    ),
                    Dimens.boxHeight10,
                  ],
                ),
                Padding(
                  padding: Dimens.edgeInsets20,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Dimens.boxHeight80,
                      Text(
                        StringConstants.accountRecovery,
                        style: Styles.bold24Black,
                      ),
                      Dimens.boxHeight15,
                      Text(
                        StringConstants.helpsToShow,
                        style: Styles.textFieldHintColor14,
                        textAlign: TextAlign.center,
                      ),
                      Dimens.boxHeight50,
                      FormFieldWidget(
                        elevation: 0,
                        autoFocus: true,
                        isFilled: true,
                        maxLength: 10,
                        textCapitalization: TextCapitalization.characters,
                        textInputType: TextInputType.emailAddress,
                        borderColor: ColorsValue.formFieldBorderColor,
                        fillColor: ColorsValue.backgroundColor,
                        hintText: StringConstants.panNumber,
                        hintStyle: Styles.textFieldHintColor16,
                        contentPadding: Dimens.edgeInsets10,
                        errorStyle: Styles.red13,
                        onChange: (String v) {
                          _controller.checkIfPanIsValid(v);
                        },
                      ),
                      Dimens.boxHeight10,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          GestureDetector(
                            onTap: RouteManagement
                                .goToForgotPinOrPasswordDateOfBirth,
                            child: Text(
                              StringConstants.useDOB,
                              style: Styles.boldBlack15,
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: Dimens.edgeInsets20,
                    child: SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) =>
                                _controller.isPanValid
                                    ? ColorsValue.primaryColor
                                    : ColorsValue.primaryColor.withOpacity(.5),
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16White,
                          ),
                        ),
                        onPressed: () {
                          if (_controller.isPanValid &&
                              _controller.showResetPassword == true) {
                            RouteManagement.goToForgotPinOrPasswordResetPassword();
                          } else {
                            RouteManagement.goToForgotPinOrPasswordResetAppPin();
                          }
                        },
                        child: Text(
                          StringConstants.sendResetRequest,
                          style: Styles.bold16White,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
