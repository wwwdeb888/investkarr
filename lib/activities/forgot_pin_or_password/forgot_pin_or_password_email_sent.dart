import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class ForgotPinOrPasswordEmailSent extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<ForgotPinOrPasswordController>(
          builder: (_controller) => ColorfulSafeArea(
            color: ColorsValue.backgroundColor,
            child: Stack(
              alignment: Alignment.center,
              children: [
                Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            SizedBox(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              child: IconButton(
                                constraints: const BoxConstraints(),
                                padding: EdgeInsets.zero,
                                onPressed: () {
                                  Get.back<dynamic>();
                                },
                                icon: const Icon(
                                  Icons.arrow_back_ios,
                                  color: ColorsValue.primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                        CircleAvatar(
                          radius: Dimens.seventeen,
                          backgroundColor: Colors.white,
                          child: Image.asset(
                            AssetConstants.splashLogo1,
                            width: Dimens.seventeen,
                            height: Dimens.seventeen,
                          ),
                        ),
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            Container(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              color: Colors.transparent,
                            ),
                          ],
                        ),
                      ],
                    ),
                    Dimens.boxHeight10,
                  ],
                ),
                Padding(
                  padding: Dimens.edgeInsets20,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Dimens.boxHeight80,
                      Text(
                        StringConstants.emailHasBeenSent,
                        style: Styles.bold24Black,
                      ),
                      Dimens.boxHeight15,
                      SizedBox(
                        width: Dimens.percentWidth(.82),
                        child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(
                                text: StringConstants.youWillReceiveForgotLink,
                                style: Styles.textFieldHintColor14,
                                children: [
                                  TextSpan(
                                    text: ' nathan.roberts@example.com. ',
                                    style: Styles.bold14Black,
                                  ),
                                  TextSpan(
                                      text:
                                          StringConstants.pleaseClickOnTheLink)
                                ])),
                      ),
                      Dimens.boxHeight50,
                      Image.asset('assets/sign_up/emailsent.png'),
                    ],
                  ),
                ),
                Positioned(
                  bottom: Dimens.sixty,
                  child: SizedBox(
                    width: Dimens.percentWidth(.9),
                    height: Dimens.percentHeight(.07),
                    child: ElevatedButton(
                      style: Styles.elevatedButtonTheme.style!.copyWith(
                        backgroundColor:
                            MaterialStateProperty.resolveWith<Color>(
                          (Set<MaterialState> states) =>
                              ColorsValue.primaryColor,
                        ),
                        textStyle: MaterialStateProperty.all(
                          Styles.bold16White,
                        ),
                      ),
                      onPressed:
                          RouteManagement.goToForgotPinOrPasswordResetPassword,
                      child: Text(
                        StringConstants.goBackToLogin,
                        style: Styles.bold16White,
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: Dimens.edgeInsets30,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          StringConstants.didntReceiveTheLink,
                          style: Styles.bold12Black,
                        ),
                        GestureDetector(
                          onTap: () {},
                          child: Text(
                            StringConstants.resend,
                            style: Styles.bold12Primary,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      );
}
