import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:pinput/pin_put/pin_put.dart';

class ForgotPinOrPasswordVerifyOTP extends StatelessWidget {
  const ForgotPinOrPasswordVerifyOTP({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<ForgotPinOrPasswordController>(
          builder: (_controller) => ColorfulSafeArea(
            color: ColorsValue.backgroundColor,
            child: Stack(
              children: [
                Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            SizedBox(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              child: IconButton(
                                constraints: const BoxConstraints(),
                                padding: EdgeInsets.zero,
                                onPressed: () {
                                  Get.back<dynamic>();
                                },
                                icon: const Icon(
                                  Icons.arrow_back_ios,
                                  color: ColorsValue.primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                        CircleAvatar(
                          radius: Dimens.seventeen,
                          backgroundColor: Colors.white,
                          child: Image.asset(
                            AssetConstants.splashLogo1,
                            width: Dimens.seventeen,
                            height: Dimens.seventeen,
                          ),
                        ),
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            Container(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              color: Colors.transparent,
                            ),
                          ],
                        ),
                      ],
                    ),
                    Dimens.boxHeight10,
                  ],
                ),
                Padding(
                  padding: Dimens.edgeInsets20,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Dimens.boxHeight80,
                      Text(
                        StringConstants.verifyWithOTP,
                        style: Styles.bold24Black,
                      ),
                      Dimens.boxHeight5,
                      Text(
                        '${_controller.emailId}',
                        style: Styles.black15,
                        textAlign: TextAlign.center,
                      ),
                      Dimens.boxHeight50,
                      PinPut(
                        autofocus: true,
                        mainAxisSize: MainAxisSize.max,
                        fieldsCount: 6,
                        controller: _controller.pinPutController2,
                        enabled: true,
                        onSubmit: (_) {
                          // controller.onVerifiy();
                        },
                        onChanged: _controller.onOTPChanged2,
                        eachFieldHeight: Dimens.fourty + Dimens.five,
                        eachFieldWidth: Dimens.thirty + Dimens.five,
                        textStyle: Styles.boldBlack22,
                        submittedFieldDecoration: Styles.pinPutDecoration,
                        selectedFieldDecoration: Styles.enabledPinPutDecoration,
                        followingFieldDecoration: Styles.pinPutDecoration,
                      ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: Dimens.edgeInsets20,
                    child: SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) =>
                                _controller.isOTPValid
                                    ? ColorsValue.primaryColor
                                    : ColorsValue.primaryColor.withOpacity(.5),
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16White,
                          ),
                        ),
                        onPressed: _controller.isOTPValid
                            ? _controller.verifyEmailOTP
                            : null,
                        child: Text(
                          StringConstants.verify,
                          style: Styles.bold16White,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
