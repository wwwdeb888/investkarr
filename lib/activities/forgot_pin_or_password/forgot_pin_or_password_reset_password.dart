import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class ForgotPinOrPasswordResetPassword extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: ColorfulSafeArea(
          color: ColorsValue.backgroundColor,
          child: GetBuilder<ForgotPinOrPasswordController>(
            builder: (_controller) => Stack(
              children: [
                Column(
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            SizedBox(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              child: IconButton(
                                constraints: const BoxConstraints(),
                                padding: EdgeInsets.zero,
                                onPressed: () {
                                  Get.back<dynamic>();
                                },
                                icon: const Icon(
                                  Icons.arrow_back_ios,
                                  color: ColorsValue.primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                        CircleAvatar(
                          radius: Dimens.seventeen,
                          backgroundColor: Colors.white,
                          child: Image.asset(
                            AssetConstants.splashLogo1,
                            width: Dimens.seventeen,
                            height: Dimens.seventeen,
                          ),
                        ),
                        Row(
                          children: [
                            Dimens.boxWidth20,
                            Container(
                              width: Dimens.twenty,
                              height: Dimens.twenty,
                              color: Colors.transparent,
                            ),
                          ],
                        ),
                      ],
                    ),
                    Dimens.boxHeight10,
                  ],
                ),
                Padding(
                  padding: Dimens.edgeInsets20,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Dimens.boxHeight80,
                      Text(
                        StringConstants.resetAPassword,
                        style: Styles.bold24Black,
                      ),
                      Dimens.boxHeight50,
                      // FormFieldWidget(
                      //   elevation: 0,
                      //   isObscureText: !_controller.isOldPasswordVisible,
                      //   obscureCharacter: '*',
                      //   autoFocus: true,
                      //   isFilled: true,
                      //   textInputAction: TextInputAction.next,
                      //   textInputType: TextInputType.text,
                      //   borderColor: ColorsValue.formFieldBorderColor,
                      //   fillColor: ColorsValue.backgroundColor,
                      //   hintText: StringConstants.enterOldPassword,
                      //   hintStyle: Styles.textFieldHintColor16,
                      //   contentPadding: Dimens.edgeInsets10,
                      //   onChange: (String v) {
                      //     _controller.oldPassword = v;
                      //   },
                      //   suffixIcon: IconButton(
                      //     onPressed: _controller.updateOldPasswordVisibility,
                      //     icon: _controller.isOldPasswordVisible
                      //         ? Image.asset(
                      //             AssetConstants.passwordVisible,
                      //             width: Dimens.twenty,
                      //             height: Dimens.twenty,
                      //           )
                      //         : Image.asset(
                      //             AssetConstants.passwordHidden,
                      //             width: Dimens.twenty,
                      //             height: Dimens.twenty,
                      //           ),
                      //   ),
                      // ),
                      Dimens.boxHeight20,
                      FormFieldWidget(
                        elevation: 0,
                        isObscureText: !_controller.isPasswordVisible,
                        obscureCharacter: '*',
                        autoFocus: true,
                        // errorText: _controller.passwordErrors,
                        isFilled: true,
                        textInputAction: TextInputAction.done,
                        textInputType: TextInputType.text,
                        borderColor: ColorsValue.formFieldBorderColor,
                        fillColor: ColorsValue.backgroundColor,
                        hintText: StringConstants.enterPassword,
                        hintStyle: Styles.textFieldHintColor16,
                        contentPadding: Dimens.edgeInsets10,
                        onChange: (String v) {
                          _controller.checkIfPasswordIsValid(v);
                        },
                        suffixIcon: IconButton(
                          onPressed: _controller.updatePasswordVisibility,
                          icon: _controller.isPasswordVisible
                              ? Image.asset(
                                  AssetConstants.passwordVisible,
                                  width: Dimens.twenty,
                                  height: Dimens.twenty,
                                )
                              : Image.asset(
                                  AssetConstants.passwordHidden,
                                  width: Dimens.twenty,
                                  height: Dimens.twenty,
                                ),
                        ),
                      ),
                      Dimens.boxHeight10,
                      _controller.isPasswordValid
                          ? Dimens.box0
                          : PassWordErrorWidget(
                              error: _controller.passwordErrors,
                              iconTextStyle: Styles.red13,
                            ),
                    ],
                  ),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: Dimens.edgeInsets20,
                    child: SizedBox(
                      width: Dimens.percentWidth(1),
                      height: Dimens.percentHeight(.07),
                      child: ElevatedButton(
                        style: Styles.elevatedButtonTheme.style!.copyWith(
                          backgroundColor:
                              MaterialStateProperty.resolveWith<Color>(
                            (Set<MaterialState> states) =>
                                _controller.isPasswordValid
                                    ? ColorsValue.primaryColor
                                    : ColorsValue.primaryColor.withOpacity(.5),
                          ),
                          textStyle: MaterialStateProperty.all(
                            Styles.bold16White,
                          ),
                        ),
                        onPressed: () {
                          if (_controller.isPasswordValid) {
                            _controller.updatePassword();
                          }
                        },
                        child: Text(
                          StringConstants.confirm,
                          style: Styles.bold16White,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      );
}
