import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class FundOrderingInvestmentCart extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: GetBuilder<FundOrderingController>(
          builder: (_controller) => ColorfulSafeArea(
            bottom: false,
            color: ColorsValue.backgroundColor,
            child: Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        IconButton(
                            onPressed: () {
                              Get.back<dynamic>();
                            },
                            icon: const Icon(Icons.arrow_back_ios)),
                        Dimens.boxWidth20,
                        Text(
                          StringConstants.investmentCart,
                          style: Styles.black16,
                        ),
                      ],
                    ),
                    Padding(
                      padding: Dimens.edgeInsets20,
                      child: Text(
                        'SIP Funds (3)',
                        style: Styles.black16,
                      ),
                    ),
                    Padding(
                      padding: Dimens.edgeInsets10,
                      child: SizedBox(
                        width: Dimens.percentWidth(1),
                        height: Dimens.percentHeight(.7),
                        child: ListView.builder(
                          padding: Dimens.edgeInsets0,
                          itemCount: 10,
                          itemBuilder: (context, index) => Padding(
                            padding: Dimens.edgeInsets0_0_0_10,
                            child: Card(
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(Dimens.ten),
                              ),
                              child: Padding(
                                padding: Dimens.edgeInsets10,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Image.asset(
                                      AssetConstants.hdfcLogo,
                                      height: Dimens.fourty,
                                      width: Dimens.fourty,
                                    ),
                                    Dimens.boxWidth10,
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          width: Dimens.percentWidth(.6),
                                          child: Text(
                                            'ICICI Prudential Technology Direct plan growth',
                                            style: Styles.black16,
                                          ),
                                        ),
                                        Dimens.boxHeight10,
                                        Row(
                                          children: [
                                            Text(
                                              StringConstants.sipDate,
                                              style: Styles.black12,
                                            ),
                                            Text(
                                              ' 1st ',
                                              style: Styles.black12,
                                            ),
                                            Text(
                                              StringConstants.edit,
                                              style: Styles.primary12Underline,
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                    const Spacer(),
                                    Column(
                                      children: [
                                        GestureDetector(
                                          onTap: () {
                                            Get.bottomSheet<dynamic>(
                                              Container(
                                                width: Dimens.percentWidth(1),
                                                height:
                                                    Dimens.percentHeight(.3),
                                                decoration: BoxDecoration(
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.only(
                                                    topRight: Radius.circular(
                                                      Dimens.twenty,
                                                    ),
                                                    topLeft: Radius.circular(
                                                      Dimens.twenty,
                                                    ),
                                                  ),
                                                ),
                                                child: Padding(
                                                  padding: Dimens.edgeInsets20,
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .spaceEvenly,
                                                    children: [
                                                      Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: [
                                                          Text(
                                                            StringConstants
                                                                .removeFromCart,
                                                            style: Styles
                                                                .boldBlack15,
                                                          ),
                                                          Dimens.boxHeight15,
                                                          Text(
                                                            StringConstants
                                                                .saveFunds,
                                                            style:
                                                                Styles.black15,
                                                          ),
                                                        ],
                                                      ),
                                                      Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          SizedBox(
                                                            width: Dimens
                                                                .percentWidth(
                                                                    .43),
                                                            child:
                                                                FormSubmitWidget(
                                                              opacity: 1,
                                                              backgroundColor:
                                                                  Colors.white,
                                                              padding: Dimens
                                                                  .edgeInsets15,
                                                              text:
                                                                  StringConstants
                                                                      .remove,
                                                              textStyle: Styles
                                                                  .bold16Primary,
                                                              onTap: () {},
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            width: Dimens
                                                                .percentWidth(
                                                                    .43),
                                                            child:
                                                                FormSubmitWidget(
                                                              opacity: 1,
                                                              backgroundColor:
                                                                  ColorsValue
                                                                      .primaryColor,
                                                              padding: Dimens
                                                                  .edgeInsets15,
                                                              text: StringConstants
                                                                  .addToWatchList,
                                                              textStyle: Styles
                                                                  .bold16White,
                                                              onTap: () {},
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            );
                                          },
                                          child: Image.asset(
                                            AssetConstants.deleteInvestment,
                                            width: Dimens.twentyFive,
                                            height: Dimens.twentyFive,
                                          ),
                                        ),
                                        Dimens.boxHeight20,
                                        Text(
                                          '₹2000',
                                          style: Styles.boldBlack15,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  
                  ],
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    width: Dimens.percentWidth(1),
                    height: Dimens.hundred,
                    color: Colors.white,
                    child: Padding(
                      padding: Dimens.edgeInsets20,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                StringConstants.totalAmount,
                                style: Styles.black12,
                              ),
                              Text(
                                '₹5000',
                                style: Styles.boldBlack22,
                              ),
                            ],
                          ),
                          const Spacer(),
                          SizedBox(
                            width: Dimens.percentWidth(.4),
                            child: FormSubmitWidget(
                              opacity: 1,
                              backgroundColor: ColorsValue.primaryColor,
                              padding: Dimens.edgeInsets15,
                              text: StringConstants.payNow,
                              textStyle: Styles.bold16White,
                              onTap: RouteManagement
                                  .goToFundOrderingPaymentOptions,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      );
}
