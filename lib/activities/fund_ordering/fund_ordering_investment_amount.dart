import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class FundOrderingInvestmentAmount extends StatelessWidget {
  const FundOrderingInvestmentAmount({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: ColorfulSafeArea(
          color: ColorsValue.backgroundColor,
          bottom: false,
          child: GetBuilder<FundOrderingController>(
            builder: (_controller) => GestureDetector(
              behavior: HitTestBehavior.opaque,
              onPanDown: (_) {
                var currentFocus = FocusScope.of(context);
                if (!currentFocus.hasPrimaryFocus) {
                  currentFocus.unfocus();
                }
              },
              child: Stack(
                children: [
                  Column(
                    children: [
                      Row(
                        children: [
                          IconButton(
                              onPressed: () {
                                Get.back<dynamic>();
                              },
                              icon: const Icon(Icons.arrow_back_ios)),
                          Dimens.boxWidth20,
                          SizedBox(
                            width: Dimens.percentWidth(.3),
                            height: Dimens.fifty,
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(
                                isExpanded: false,
                                value: _controller.selectedInvestment.value,
                                icon: const Icon(
                                  Icons.arrow_drop_down,
                                  color: ColorsValue.textColor,
                                ),
                                items: _controller.investmentList,
                                hint: Padding(
                                  padding: Dimens.edgeInsets10,
                                  child: Text(
                                    'SIP',
                                    style: Styles.textFieldHintColor16,
                                  ),
                                ),
                                onChanged: (value) {
                                  _controller.selectedInvestment.value =
                                      value as InvestmentList?;
                                  if (_controller
                                          .selectedInvestment.value?.value ==
                                      2) {
                                    _controller.isSipDateVisible = false;
                                  } else {
                                    _controller.isSipDateVisible = true;
                                  }
                                  _controller.update();
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                      Dimens.boxHeight80,
                      Text(
                        StringConstants.enterInvestmentAmount,
                        style: Styles.black16,
                      ),
                      Dimens.boxHeight30,
                      SizedBox(
                        width: Dimens.percentWidth(.35),
                        height: Dimens.eighty,
                        child: TextField(
                          style: Styles.primaryBold22,
                          keyboardType: TextInputType.number,
                          decoration: InputDecoration(
                            enabledBorder: InputBorder.none,
                            disabledBorder: InputBorder.none,
                            border: InputBorder.none,
                            hintText: '0',
                            hintStyle: Styles.primaryBold22,
                            prefixIcon: Padding(
                              padding:
                                  EdgeInsets.fromLTRB(0, Dimens.fourteen, 0, 0),
                              child: Text(
                                '₹',
                                style: Styles.primaryBold22,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ),
                      ),
                      Dimens.boxHeight10,
                      Padding(
                        padding: Dimens.edgeInsets10,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: Dimens.percentWidth(.28),
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.circular(Dimens.fifty),
                                border: Border.all(color: Colors.grey),
                              ),
                              child: Padding(
                                padding: Dimens.edgeInsets10,
                                child: Text(
                                  '+ ₹ 5000',
                                  textAlign: TextAlign.center,
                                  style: Styles.black16,
                                ),
                              ),
                            ),
                            Container(
                              width: Dimens.percentWidth(.28),
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.circular(Dimens.fifty),
                                border: Border.all(color: Colors.grey),
                              ),
                              child: Padding(
                                padding: Dimens.edgeInsets10,
                                child: Text(
                                  '+ ₹ 10000',
                                  textAlign: TextAlign.center,
                                  style: Styles.black16,
                                ),
                              ),
                            ),
                            Container(
                              width: Dimens.percentWidth(.28),
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.circular(Dimens.fifty),
                                border: Border.all(color: Colors.grey),
                              ),
                              child: Padding(
                                padding: Dimens.edgeInsets10,
                                child: Text(
                                  '+ ₹ 25000',
                                  textAlign: TextAlign.center,
                                  style: Styles.black16,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: Dimens.edgeInsets10,
                        child: Visibility(
                          visible: _controller.isSipDateVisible,
                          child: GestureDetector(
                            onTap: () {
                              Get.bottomSheet<dynamic>(
                                Container(
                                  width: Dimens.percentWidth(1),
                                  height: Dimens.percentHeight(.5),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(Dimens.twenty),
                                      topLeft: Radius.circular(Dimens.twenty),
                                    ),
                                    color: Colors.white,
                                  ),
                                  child: Padding(
                                    padding: Dimens.edgeInsets20,
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Row(
                                          children: [
                                            Text(
                                              StringConstants
                                                  .sipInstallmentDate,
                                              style: Styles.black18,
                                              textAlign: TextAlign.start,
                                            ),
                                          ],
                                        ),
                                        StatefulBuilder(
                                          builder: (BuildContext context,
                                                  StateSetter setState) =>
                                              Wrap(
                                            direction: Axis.horizontal,
                                            children: _controller.datesList
                                                .map(
                                                  (item) => Padding(
                                                    padding: Dimens.edgeInsets7,
                                                    child: GestureDetector(
                                                      onTap: () {
                                                        _controller
                                                                .selectedDate =
                                                            int.parse(item);
                                                        setState(() {});
                                                      },
                                                      child: CircleAvatar(
                                                        backgroundColor: _controller
                                                                    .selectedDate ==
                                                                int.parse(item)
                                                            ? ColorsValue
                                                                .primaryColor
                                                            : Colors.white,
                                                        radius: Dimens.fifteen,
                                                        child: Text(
                                                          item,
                                                          style: _controller
                                                                      .selectedDate ==
                                                                  int.parse(
                                                                      item)
                                                              ? Styles.white15
                                                              : Styles.black15,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                )
                                                .toList(),
                                          ),
                                        ),
                                        Dimens.boxHeight20,
                                        Text(
                                          StringConstants.nextInstallmentDate,
                                          style: Styles.primaryBold13,
                                        ),
                                        Dimens.boxHeight15,
                                        SizedBox(
                                          width: Dimens.percentWidth(1),
                                          child: FormSubmitWidget(
                                            opacity: 1,
                                            backgroundColor:
                                                ColorsValue.primaryColor,
                                            padding: Dimens.edgeInsets15,
                                            text: StringConstants.confirm,
                                            textStyle: Styles.bold16White,
                                            onTap: Get.back,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(Dimens.ten),
                                color: Colors.white,
                              ),
                              child: Padding(
                                padding: Dimens.edgeInsets10,
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        const Icon(
                                          Icons.calendar_today_outlined,
                                          color: Colors.black87,
                                        ),
                                        Dimens.boxWidth10,
                                        Text(
                                          StringConstants.sipDate,
                                          style: Styles.black12,
                                        ),
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          '1 of every month',
                                          style: Styles.bold12Primary,
                                        ),
                                        Dimens.boxWidth10,
                                        const Icon(
                                          Icons.arrow_drop_down,
                                          color: ColorsValue.primaryColor,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      width: Dimens.percentWidth(1),
                      height: Dimens.hundred,
                      color: Colors.white,
                      child: Padding(
                        padding: Dimens.edgeInsets20,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            SizedBox(
                              width: Dimens.percentWidth(.4),
                              child: FormSubmitWidget(
                                opacity: 1,
                                backgroundColor: Colors.white,
                                padding: Dimens.edgeInsets15,
                                text: StringConstants.addToCart,
                                textStyle: Styles.bold16Primary,
                                onTap: RouteManagement.goToFundOrderingSuccess,
                              ),
                            ),
                            SizedBox(
                              width: Dimens.percentWidth(.4),
                              child: FormSubmitWidget(
                                opacity: 1,
                                backgroundColor: ColorsValue.primaryColor,
                                padding: Dimens.edgeInsets15,
                                text: StringConstants.investNow,
                                textStyle: Styles.bold16White,
                                onTap: RouteManagement.goToFundOrderingSuccess,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      );
}
