import 'package:colorful_safe_area/colorful_safe_area.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:investkarr/lib.dart';
import 'package:swipebuttonflutter/swipebuttonflutter.dart';

class FundOrderingPaymentOptions extends StatelessWidget {
  const FundOrderingPaymentOptions({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: ColorsValue.backgroundColor,
        body: ColorfulSafeArea(
          bottom: false,
          color: ColorsValue.backgroundColor,
          child: GetBuilder<FundOrderingController>(
            builder: (_controller) => Stack(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        IconButton(
                            onPressed: () {
                              Get.back<dynamic>();
                            },
                            icon: const Icon(Icons.arrow_back_ios)),
                        Dimens.boxWidth20,
                        Text(
                          StringConstants.paymentOptions,
                          style: Styles.black16,
                        ),
                      ],
                    ),
                    Padding(
                      padding: Dimens.edgeInsets10,
                      child: SizedBox(
                        width: Dimens.percentWidth(1),
                        height: Dimens.percentHeight(.7),
                        child:
                         ListView.builder(
                          padding: Dimens.edgeInsets0,
                          itemCount: 5,
                          itemBuilder: (context, index) => Padding(
                            padding: Dimens.edgeInsets0_0_0_10,
                            child: Card(
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(Dimens.ten),
                              ),
                              child: Padding(
                                padding: Dimens.edgeInsets10,
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Image.asset(
                                      AssetConstants.netBanking,
                                      height: Dimens.twenty,
                                      width: Dimens.twenty,
                                    ),
                                    Dimens.boxWidth10,
                                    Text(
                                      'Net Banking',
                                      style: Styles.black16,
                                    ),
                                    const Spacer(),
                                    SizedBox(
                                      width: Dimens.twentyFour,
                                      height: Dimens.twentyFour,
                                      child: Checkbox(
                                        value:
                                            _controller.checkBoxValue == index
                                                ? true
                                                : false,
                                        checkColor: Colors.white,
                                        activeColor: Colors.greenAccent,
                                        onChanged: (value) {
                                          _controller.checkBoxValue = index;
                                          _controller.update();
                                        },
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      
                      ),
                    ),
                  ],
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    width: Dimens.percentWidth(1),
                    height: Dimens.percentHeight(.23),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(Dimens.fourty),
                        topRight: Radius.circular(Dimens.fourty),
                      ),
                    ),
                    child: Padding(
                      padding: Dimens.edgeInsets20,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                StringConstants.amountPayable,
                                style: Styles.black15,
                              ),
                              Text(
                                '₹5000',
                                style: Styles.boldBlack22,
                              ),
                            ],
                          ),
                          SwipingButton(
                            height: Dimens.fifty,
                            iconColor: ColorsValue.primaryColor,
                            swipeButtonColor: Colors.white,
                            backgroundColor: ColorsValue.primaryColor,
                            text: StringConstants.swipeToPay,
                            buttonTextStyle: Styles.black16,
                            onSwipeCallback: RouteManagement
                                .goToFundOrderingInvestmentAmount,
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      );
}
