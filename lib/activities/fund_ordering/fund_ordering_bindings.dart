import 'package:get/get.dart';
import 'package:investkarr/lib.dart';

class FundOrderingBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(FundOrderingController.new);
  }
}
