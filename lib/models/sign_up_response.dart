// To parse this JSON data, do
//
//     final signUpResponse = signUpResponseFromJson(jsonString);

import 'dart:convert';

SignUpResponse signUpResponseFromJson(String str) =>
    SignUpResponse.fromJson(json.decode(str) as Map<String, dynamic>);

String signUpResponseToJson(SignUpResponse data) => json.encode(data.toJson());

class SignUpResponse {
  SignUpResponse({
    this.success,
    this.statusCode,
    this.message,
    this.result,
  });

  factory SignUpResponse.fromJson(Map<String, dynamic> json) => SignUpResponse(
        success: json['success'] as bool? ?? false,
        statusCode: json['status_code'] as int? ?? 0,
        message: json['message'] as String? ?? '',
        result: json['result'] == null
            ? null
            : SignUpData.fromJson(json['result'] as Map<String, dynamic>),
      );

  bool? success;
  int? statusCode;
  String? message;
  SignUpData? result;

  Map<String, dynamic> toJson() => <String, dynamic>{
        'success': success == null ? null : success,
        'status_code': statusCode == null ? null : statusCode,
        'message': message == null ? null : message,
        'result': result,
      };
}

class SignUpData {
  SignUpData({
    this.accessToken,
    this.user,
  });

  factory SignUpData.fromJson(Map<String, dynamic> json) => SignUpData(
        accessToken: json['access_token'] as String? ?? '',
        user: json['user'] == null
            ? null
            : User.fromJson(json['user'] as Map<String, dynamic>),
      );

  String? accessToken;
  User? user;

  Map<String, dynamic> toJson() => <String, dynamic>{
        'access_token': accessToken == null ? null : accessToken,
        'user': user,
      };
}

class User {
  User({
    this.id,
    this.email,
    this.firstName,
    this.lastName,
    this.pan,
    this.isPanVerified,
    this.deviceId,
    this.mobileNumber,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        id: json['id'] as int? ?? 0,
        email: json['email'] as String? ?? '',
        firstName: json['first_name'] as String? ?? '',
        lastName: json['last_name'] as String? ?? '',
        pan: json['pan'] as String? ?? '',
        isPanVerified: json['is_pan_verified'] as int? ?? 0,
        deviceId: json['device_id'] as String? ?? '',
        mobileNumber: json['mobile_number'] as String? ?? '',
      );

  int? id;
  String? email;
  String? firstName;
  String? lastName;
  String? pan;
  int? isPanVerified;
  String? deviceId;
  String? mobileNumber;

  Map<String, dynamic> toJson() => <String, dynamic>{
        'id': id == null ? null : id,
        'email': email == null ? null : email,
        'first_name': firstName == null ? null : firstName,
        'last_name': lastName == null ? null : lastName,
        'pan': pan == null ? null : pan,
        'is_pan_verified': isPanVerified == null ? null : isPanVerified,
        'device_id': deviceId == null ? null : deviceId,
        'mobile_number': mobileNumber == null ? null : mobileNumber,
      };
}
