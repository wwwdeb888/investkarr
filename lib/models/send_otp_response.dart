// To parse this JSON data, do
//
//     final sendOtpResponse = sendOtpResponseFromJson(jsonString);

import 'dart:convert';

SendOtpResponse sendOtpResponseFromJson(String str) =>
    SendOtpResponse.fromJson(json.decode(str) as Map<String, dynamic>);

String sendOtpResponseToJson(SendOtpResponse data) =>
    json.encode(data.toJson());

class SendOtpResponse {
  SendOtpResponse({
    this.success,
    this.statusCode,
    this.message,
    this.result,
  });

  factory SendOtpResponse.fromJson(Map<String, dynamic> json) =>
      SendOtpResponse(
        success: json['success'] as bool? ?? false,
        statusCode: json['status_code'] as int? ?? 0,
        message: json['message'] as String? ?? '',
        result: json['result'] == null
            ? null
            : SendOTPResponse.fromJson(json['result'] as Map<String, dynamic>),
      );

  bool? success;
  int? statusCode;
  String? message;
  SendOTPResponse? result;

  Map<String, dynamic> toJson() => <String, dynamic>{
        'success': success == null ? null : success,
        'status_code': statusCode == null ? null : statusCode,
        'message': message == null ? null : message,
        'result': result,
      };
}

class SendOTPResponse {
  SendOTPResponse({
    this.otp,
  });

  String? otp;
  factory SendOTPResponse.fromJson(Map<String, dynamic> json) =>
      SendOTPResponse(
        otp: json['OTP'] as String? ?? '',
      );
  Map<String, dynamic> toJson() => <String, dynamic>{
        'OTP': otp == null ? null : otp,
      };
}

SmsApiResponse sendSmsOtpResponseFromJson(String str) =>
    SmsApiResponse.fromJson(json.decode(str) as Map<String, dynamic>);

String sendSmsOtpResponseToJson(SmsApiResponse data) =>
    json.encode(data.toJson());

class SmsApiResponse {
  SmsApiResponse({required this.data});
  SmsOtpResponse data;

  factory SmsApiResponse.fromJson(Map<String, dynamic> json) => SmsApiResponse(
        data: SmsOtpResponse.fromJson(
            json['data']['response'] as Map<String, dynamic>),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{'data': data};
}

class SmsOtpResponse {
  SmsOtpResponse({this.id, this.phone, this.status});
  factory SmsOtpResponse.fromJson(Map<String, dynamic> json) => SmsOtpResponse(
        id: json['id'] as String,
        phone: json['phone'] as String,
        status: json['status'] as String,
      );

  String? id;
  String? phone;
  String? status;
  Map<String, dynamic> toJson() => <String, dynamic>{
        // ignore: unnecessary_null_comparison
        'status': status == null ? null : status,
        'phone': phone,
        'id': id,
      };
}
