import 'package:flutter/material.dart';
import 'package:get/get.dart';

// coverage:ignore-file
class TranslationsFile extends Translations {
  /// List of locales used in the application
  static const listOfLocales = <Locale>[
    Locale('en'),
  ];

  @override
  Map<String, Map<String, String>> get keys => {
        'en': {
          'appName': 'Investkarr',
          'appPinSuccess': 'App Pin set successfully!',
          'shouldBe6Characters': 'Password length should be greater than 6',
          'premiumMembership': 'Premium Membership',
          'shouldHaveOneDigit': 'Password should contain 1 digit',
          'shouldHaveOneSpecialCharacter':
              'Password should contain 1 special character',
          'shouldHaveOneUppercaseLetter':
              'Password should contain 1 uppercase letter',
          'goalBasedInvesting': 'Goal based investing',
          'assets': 'Assets',
          'suggestedGoalsForYou': 'Suggested Goals for you',
          'seeAllHoldings': 'See All holdings',
          'topHoldings': 'Top Holdings',
          'ordersInProgress': 'Orders In Progress',
          'investIntoTimeTestedPortfolios':
              'Invest into time tested portfolios',
          'zeroCommission': 'Zero commission',
          'investIntoDirectPlans': 'Invest into direct plans',
          'unbiasedAdvice': 'Unbiased advice',
          'assetAnalysis': 'Asset Analysis',
          'suggestedForYou': 'Suggested For You',
          'independentCompany':
              'Independent company, not part of any large company',
          'dataSecurity': 'Data security',
          'weDoNotSell': 'We do not sell any of your data',
          'login': 'Login',
          'signUp': 'Signup',
          'smarterWay': 'Smarter way to grow your wealth.',
          'signUpWithGoogle': 'Signup with Google',
          'signUpWithApple': 'Signup with Apple',
          'signUpWithEmail': 'Signup with Email',
          'alreadyHaveAnAccount': 'Already have an account? ',
          'niceToMeetYou': 'Nice to meet you!',
          'sendOTP': 'Send OTP',
          'enterEmail': 'Enter email',
          'verifyWithOTP': 'Verify with OTP!',
          'verify': 'Verify',
          'resendOTP': 'Resend OTP',
          'welcomeBack': 'Welcome back',
          'niceToMeetYouAgain': 'Nice to meet you again!',
          'next': 'Continue',
          'done': 'Done',
          'firstName': 'First name',
          'noAccount': 'Don\'t have an account? ',
          'or': 'or',
          'dateOfBirth': 'Date of birth',
          'asMentioned': 'As mentioned on your PAN card',
          'createAccount': 'Create Account',
          'lastName': 'Last name',
          'setAppPin': 'Set App Pin',
          'enterAppPin': 'Enter App Pin',
          'forgotAppPin': 'Forgot App pin?',
          'resetAPassword': 'Reset password',
          'forgotPassword': 'Forgot Password',
          'kycReady': 'Get KYC ready in few easy steps!',
          'sipInstallmentDate': 'Sip Installment Date',
          'enterEmailOrPhone': 'Enter email/phone',
          'branchName': 'Branch name :',
          'investYourself': 'Invest Yourself',
          'explore': 'Explore',
          'forwordConsolidatedStaterment': 'Forword Cosolidated Statement',
          'step': 'Step',
          'exploreNow': 'Explore Now',
          'shortTermGoals': 'Short Term Goals',
          'forwardTheConsolidatedStatement':
              'Forward the consolidated statement sent to the following email',
          'iDontHaveInvestmentElsewhere': 'I Dont Have Investment elsehere',
          'dashboard': 'Dashboard',
          'alreadyInvestedSomewhere': 'Already Invested Somewhere',
          'profile': 'Profile',
          'viewSampleEmail': 'View Sample Email',
          'nextInstallmentDate': 'Next SIP Installment on',
          'mutualFunds': 'Mutual Funds',
          'wealthCreation': 'Wealth creation',
          'wealthCreation&more': 'Wealth creation & More',
          'myFunds': 'My Funds',
          'emergencyPack': 'Emergency Pack',
          'activeFunds': 'Active Funds',
          'activated': 'Activated',
          'checkOrderStatusActiveFunds': 'Check order status & active funds',
          'enterThePasswordForYourConsolidatedAccountStatement':
              'Enter the password for your consolidated account statement',
          'didntGetEmail': 'Didn’t get email?',
          'confirmYourEmailLinkedToYourMutualFundInvestments':
              'Confirm your email linked to your mutual fund investments',
          'jumpStart':
              'Let us jump-start your wealth creation journey with our research-backed solution.',
          'completeRiskProfile': 'Complete Risk profile',
          'investLifeGoal':
              'Invest for your various short term or long term life goals.',
          'exploreAll': 'Explore all mutual funds available in the industry ',
          'toGetMore': 'To get more personalized mutual fund recommendations',
          'curatedPacks': 'Curated Packs',
          'currentValue': 'Current Value',
          'myTopPerformingFunds': 'My Top Performing Funds',
          'fundSplitByCapitalGain': 'Fund Split by Capital Gain',
          'returnsAnnualised': 'Returns (Annualised)',
          'totalInsvestmentValue': 'Total Insvestment Value',
          'typeOfInvestment': 'Type of investment ',
          'emergency': 'Emergency',
          'settingAGoal': 'Setting a goal',
          'giveYourGoalAName': 'Give your goal a name',
          'inHowManyYears': 'In How Many Years',
          'goalName': 'Goal Name',
          'selectPaymentMethod': 'Select Payment method ',
          'goalAmount': 'Goal Amount',
          'fullPayment': 'Full Payment',
          'lumpsum': 'Lumpsum',
          'loan': 'Loan',
          'goalSummary': 'Goal Summary',
          'sip': 'SIP',
          'inflationRate': 'Inflation Rate',
          'totalAmount': 'Total Amount',
          'whatIsTheCostOfYourRide': 'What is the Cost of your Ride?',
          'willThereBeAnyFutureInflows': 'Will there be any future inflows?',
          'create': 'Create',
          'goalMAountGoals': 'Short term Goals',
          'longTermGoals': 'Long Term Goals',
          'whenDoYouPlanToBuyYourRide': 'When do you plan to buy your ride ?',
          'retirement': 'Retirement',
          'activeSip': 'Active SIP',
          'totalReturns': 'Total Returns',
          'myMcPortfolioSplit': 'My MC Portfolio Split',
          'selectFileToUpload': 'Select file to upload',
          'uploadConsolidatedStatements': 'Upload consolidated statements',
          'generateConsolidatedStatements': 'Generate consolidated statements',
          'forwardConsolidatedStatements': 'Forward consolidated statements',
          'investmentCart': 'Investment Cart',
          'riskFunds': 'This pack offers a combination of low risk funds',
          'calculate': 'Calculate',
          'howMuchWouldYouLikeToIncreaseOnAnAnnualBasis':
              'How much would you like to increase on an annual basis?',
          'investMore': 'Invest More',
          'doYouWishToAddInitialAmount': 'Do you wish to add initial amount?',
          'addAnInitialAmountToYourGoalSIP':
              'Add an Initial amount to your goal’s SIP',
          'transactionHistory': 'Transaction History',
          'invested': 'Invested',
          'tryAddingSmallAmountInitiallyToLowerTheMonthlySIP':
              'Try adding a small amount initially to lower the monthly SIP',
          'recommendedVideos': 'Recommended Videos',
          'viewAll': 'View all',
          'filterBy': 'Filter by',
          'taxSaver': 'Tax Saver',
          'offersELSS': 'This pack offers a combination of ELSS funds. ',
          'recommendedReads': 'Recommended reads',
          'tools': 'Tools',
          'sipCalculator': 'SIP Calculator',
          'investmentAmount': 'Investment Amount',
          'categoriesOfMF': 'Categories of Mutual Funds',
          'rebalanceAvailable': 'Rebalance update available',
          'equity': 'Equity',
          'expReturns': 'Exp. Returns',
          'monthlySip': 'Monthly SIP',
          'addToCart': 'Add to Cart',
          'debts': 'Debts',
          'investNow': 'Invest Now',
          'hybrid': 'Hybrid',
          'international': 'International',
          'setupMandate': 'Setup a mandate',
          'setGoal': 'Set Goal',
          'goldFunds': 'Gold Funds',
          'fundTransaction': 'Fund Transaction',
          'others': 'Others',
          'myWatchlist': 'My Watchlist',
          '': 'My Goals',
          'trackExternalFunds': 'Track External Funds',
          'keepTrack': 'Keep track of your favorite mutual funds',
          'startTracking': 'Start Tracking',
          'automateSIP': 'Automate Your SIPs Each Month To Make It Hassle-Free',
          'passive': 'Passive',
          'indexFunds': 'Index Funds',
          'startTrackingYourMutual':
              'Start tracking your mutual fund investments made via an external platform',
          'enterOldPassword': 'Enter old password',
          'enterNewPassword': 'Enter new password',
          'paymentOptions': 'Payment Options',
          'fofs': 'FOFs',
          'marketCap': 'Market Cap',
          'largeCap': 'Large Cap',
          'multiCap': 'Multi Cap',
          'sectorAndThemes': 'Sector & Themes',
          'esg': 'ESG',
          'elss': 'ELSS',
          'banking': 'Banking',
          'taxSaving': 'Tax Saving',
          'orderPlacedSuccessfully': 'Order Placed Successfully!',
          'rateYourExperience': 'Rate your experience',
          'sipDate': 'SIP Date',
          'amountPayable': 'Amount Payable',
          'swipeToPay': 'Swipe to Pay',
          'enterInvestmentAmount': 'Enter Investment Amount',
          'edit': 'Edit',
          'totalAmount': 'Total Amount',
          'payNow': 'Pay Now',
          'removeFromCart': 'Are you sure you want to remove it from the cart?',
          'saveFunds': 'You can save funds to your watchlist to use later',
          'remove': 'Remove',
          'addToWatchList': 'Add to Watchlist',
          'accountNumber': 'Account number',
          'addAnotherAccount': 'Add Another Account',
          'youCanAdd': 'You can add up to 5 bank accounts',
          'verifyBank': 'Verify Bank',
          'bankIsVerified': 'Bank is Verified',
          'allSet': 'You’re set to make payments',
          'iFSCCode': 'IFSC Code :',
          'address': 'Address :',
          'investmentAnalysis': 'Investment Analysis',
          'enterBankAccountNumber': 'Enter bank Account Number',
          'name': 'Enter nominee name',
          'showBankAccountNumber': 'Show Account Number',
          'chooseYourBranch': 'Choose your branch',
          'searchBank': 'Search Bank',
          'relationship': 'Relationship',
          'addBankAccount': 'Add Bank account',
          'confirmBankDetails': 'Confirm Bank Details',
          'accountShouldBelongTo': 'Account should belong to',
          'popularBanks': 'Popular Banks',
          'showPassword': 'Show Password',
          'sendNotificationToNominee': 'Send a notification to the nominee',
          'smoothReg':
              'For a smooth registration process, please keep the following items ready before proceeding',
          'accountRecovery': 'Account recovery',
          'proofOfIdentity': 'Proof of Identity',
          'income': 'Income',
          'selectOne': '*Select one of the option',
          'nomineeDetailsRequired': 'Nominee Details Required',
          'mandatoryPAN': 'Pan is mandatory to invest in India',
          'proofOfAddress': 'Proof of Address',
          'aadhaar': 'Aadhaar card or Driving License or passport',
          'chequeBook': 'Cheque Book',
          'iFSC': 'For IFSC code and Bank Account number',
          'penPaper': 'Pen & Paper',
          'signature': 'For your signature on plain paper',
          'startKYC': 'Start KYC',
          'helpsToShow':
              'This helps to show that this account really belongs to you',
          'sendResetRequest': 'Send reset request',
          'useDOB': 'Use Date of birth',
          'usePAN': 'Use PAN number',
          'resetAppPin': 'Reset App Pin',
          'confirmAppPin': 'Confirm App Pin',
          'doubleProtection': 'Double protection',
          'phoneLockEnabled':
              'We’ve enabled phone lock for you as an added security measure',
          'canBeChanged': 'This can be changed later in the app',
          'setAPassword': 'Set a password',
          'toAccessYourAccount': 'To access your account linked to',
          'enterPassword': 'Enter password',
          'pleaseEnterValidEmail': 'Please ensure the email entered is valid',
          'confirm': 'Confirm',
          'shouldBe8Characters': 'Min. 8 characters',
          'oneUppercase': '1 upper case character (A-Z)',
          'oneDigit': '1 digit (0-9)',
          'oneSpecial': '1 special (,.@/)',
          'verifyYourMobile': 'Verify your mobile!',
          'verifyPanCard': 'Let’s verify your PAN Card',
          'panMandatory': 'PAN is mandatory to invest in india',
          'panNumber': 'PAN number',
          'mobileNumber': 'Mobile number',
          'loginWithGoogle': 'Login with Google',
          'loginWithEmail': 'Login with Email',
          'loginWithApple': 'Login with Apple',
          'loginWithPhone': 'Login with Phone',
          'all': 'All',
          'makeSurePhoneNumberLinkedToAadhaar':
              'Make sure to use the number linked to your aadhar ',
          'whatDoWeCallYou': 'What do we call you?',
          'nameMatches':
              'Make sure this name matches the one appearing on your PAN card',
          'waiting':
              'Waiting to automatically detect the 6 digit secure code sent to ',
          'chooseTheEmail':
              'Choose the email you would like to use to login in to your account',
          'marwarCapital': 'Marwar Capital',
          'emailHasBeenSent': 'Email has been sent',
          'goBackToLogin': 'Go back to login',
          'didntReceiveTheLink': 'Didn\'t receive the link?',
          'resend': 'Resend',
          'youWillReceiveForgotLink':
              'You will receive a link to reset your password in your inbox',
          'pleaseClickOnTheLink': 'Please click on the link',
          'pleaseEnterAValidOtp': 'Please enter a valid OTP',
          'otpVerifiedSuccessfully': 'OTP Verified successfully',
          'skip': 'Skip',
          'fatcaDeclaration': 'FATCA Declaration',
          'areYouAPoliticallyExposedPerson':
              'Are you a politically exposed person?',
          'areYouRelatedToAPoliticallyExposedPerson':
              'Are you related to a politically exposed person?',
          'areYouATaxResidentOfAnyOtherCountry':
              'Are you a tax resident of any other country?',
          'areYouACitizenOfIndia': 'Are you a citizen of India?',
          'selectYourPlaceOfBirth': 'Select your place of birth',
          'selectYourCountryOfNationality':
              'Select your Country of nationality',
          'iAgreeToAllTAndCAndFatcaDeclarations':
              'I agree to all T & C and FATCA declarations',
          'yes': 'Yes',
          'no': 'No',
          'loremIpsum':
              'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ultricies sit amet',
          'scanChequeLeaf': 'Scan Cheque leaf',
          'scanCheque': 'Scan Cheque',
          'yourbankCancelledChequeYouUpload':
              'Your bank account details will be fetched from the cancelled cheque you upload',
          'pleaseMakeSureIsClearlyVisible':
              'Please make sure the information outlined in red in the sample cheque above is clearly visible',
          'gallery': 'Gallery',
          'camera': 'Camera',
          'pictureIsNotClear': 'Picture is not clear',
          'makeSureThereIsNoBlurOrGlare': 'Make sure there is no blur or glare',
          'takeNewPicture': 'Take New Picture',
          'occupation': 'Occupation',
        },
      };
}
