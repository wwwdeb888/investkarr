import 'dart:core';

import 'package:investkarr/lib.dart';

class ApiClient {
  ApiWrapper apiWrapper = ApiWrapper();

  Future<ResponseModel> sendOTP(
      {required String keyType1,
      required String value1,
      required String typeValue,
      required bool isLoading}) async {
    var data = {'$keyType1': '$value1', 'type': '$typeValue'};
    var response = await apiWrapper.makeRequest('send-otp', Request.post, data,
        isLoading, {'Content-Type': 'application/json'});
    return response;
  }

  Future<ResponseModel> verifyOTP(
      {required String keyType1,
      required String value1,
      required String typeValue,
      required bool isLogin,
      required String otp,
      required bool isLoading}) async {
    Map<String, dynamic> data;
    if (isLogin == true) {
      data = <String, dynamic>{
        '$keyType1': '$value1',
        'type': '$typeValue',
        'otp': '$otp',
        'is_login': 1
      };
    } else {
      data = <String, dynamic>{
        '$keyType1': '$value1',
        'type': '$typeValue',
        'otp': '$otp',
      };
    }
    print(data);
    var response = await apiWrapper.makeRequest('verify/otp', Request.post,
        data, isLoading, {'Content-Type': 'application/json'});
    return response;
  }

  Future<ResponseModel> signUp(
      {required String email,
      required String firstName,
      required String lastName,
      required String panNumber,
      required String dob,
      required String password,
      required String deviceId,
      required String mobileNumber,
      required String appPin,
      required bool isLoading}) async {
    var data = {
      'email': email,
      'first_name': firstName,
      'last_name': lastName,
      'pan': panNumber,
      'dob': dob,
      'password': password,
      'device_id': deviceId,
      'mobile_number': mobileNumber,
      'pin': appPin
    };
    print(data);
    var response = await apiWrapper.makeRequest('register', Request.post, data,
        isLoading, {'Content-Type': 'application/json'});
    return response;
  }

  Future<ResponseModel> loginWithEmail(
      {required String email,
      required String password,
      required String type,
      required bool isLoading}) async {
    var data = {'email': email, 'password': password, 'type': type};
    var response = await apiWrapper.makeRequest('login', Request.post, data,
        isLoading, {'Content-Type': 'application/json'});
    return response;
  }

  Future<ResponseModel> loginWithPin(
      {required String pin,
      required bool isLoading,
      required String token}) async {
    var data = {'pin': pin};
    var response = await apiWrapper.makeRequest(
        'login-pin',
        Request.post,
        data,
        isLoading,
        {'Content-Type': 'application/json', 'Authorization': 'Bearer $token'});
    return response;
  }

  Future<ResponseModel> updatePin(
      {required String pin,
      required bool isLoading,
      required String token}) async {
    var data = {'pin': pin};
    var response = await apiWrapper.makeRequest(
        'update/pin',
        Request.post,
        data,
        isLoading,
        {'Content-Type': 'application/json', 'Authorization': 'Bearer $token'});
    return response;
  }

  Future<ResponseModel> forgotPassword(
      {required String email,
      required String type,
      required String dob,
      required bool isLoading,
      required String pan}) async {
    var data = {'email': email, 'type': type, 'dob': dob, 'pan': pan};
    var response = await apiWrapper.makeRequest('forgot/password', Request.post,
        data, isLoading, {'Content-Type': 'application/json'});
    return response;
  }

  Future<ResponseModel> updatePassword(
      {required String newPassword,
      required String token,
      required bool isLoading}) async {
    var data = {'password': newPassword};
    var response = await apiWrapper.makeRequest(
        'update/password',
        Request.post,
        data,
        isLoading,
        {'Content-Type': 'application/json', 'Authorization': 'Bearer $token'});
    return response;
  }

  Future<ResponseModel> sendSms(
      {required String phone,
      required bool isLoading,
      required String otp}) async {
    var data = {'otp': otp, 'phoneNo': phone};
    var response = await apiWrapper.makeRequest(
        'sms',
        Request.post,
        data,
        isLoading,
        {
          'Content-Type': 'application/json',
        },
        true);
    return response;
  }
}
